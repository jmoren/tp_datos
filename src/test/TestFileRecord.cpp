/*
 * TestFileRecord.cpp
 *
 *  Created on: 18/10/2013
 *      Author: pablo
 */

#include <iostream>
#include <exception>
#include <cstring>
#include <string>
#include "../lib/Constantes.h"
#include "../lib/RegistroVariable.h"
#include "../lib/FileRecords.h"

using namespace std;

class TestFileRecord{

public:
	TestFileRecord(){}
	virtual ~TestFileRecord(){}

	void crearArchivo(string path){
		cout << "-----------------------------------------------------" << endl;
		cout << "Se crea el archivo." << endl;
		cout << "-----------------------------------------------------" << endl;

		try{
			this->file = new FileRecords(path.c_str());
		}catch (exception& e){
			cout << "Excepcion lanzada: " << e.what() << endl;
		}
		cout << "Creacion de Archivo ----> OK" << endl;

		delete this->file;

	}

	void insertarRegistros(string path){

		cout << "-----------------------------------------------------" << endl;
		cout << "Abro el archivo y escribo los registros." << endl;
		cout << "-----------------------------------------------------" << endl;

		try{
			this->file = new FileRecords(path.c_str(),rw);
		}catch (exception& e){
			cout << "Excepcion lanzada: " << e.what() << endl;
		}
		cout << "Archivo abierto para insercion ----> OK" << endl;

		string s1 ("Este es el primer registro variable. Comienzo de las pruebas de insercion en el archivo");
		string s2 ("Este es el segundo registro variable. Bastante mas largo que el primero");
		string s3 ("Este es el ultimo");

		char* dato1 = new char[s1.length() + 1];
		char* dato2 = new char[s2.length() + 1];
		char* dato3 = new char[s3.length() + 1];

		strcpy(dato1,s1.c_str());
		strcpy(dato2,s2.c_str());
		strcpy(dato3,s3.c_str());

		int size1 =  strlen(dato1) + 1 + sizeof(unsigned int)*2;
		int size2 =  strlen(dato2) + 1 + sizeof(unsigned int)*2;
		int size3 =  strlen(dato3) + 1 + sizeof(unsigned int)*2;

		this->rv1 = new RegistroVariable(1,size1,dato1);
		this->rv2 = new RegistroVariable(2,size2,dato2);
		this->rv3 = new RegistroVariable(3,size3,dato3);

		this->file->write(this->rv1);
		cout << "Se inserto el registro 1" << endl;
		this->file->write(this->rv2);
		cout << "Se inserto el registro 2" << endl;
		this->file->write(this->rv3);
		cout << "Se inserto el registro 3" << endl;

		delete this->file;

		delete this->rv1;
		delete this->rv2;
		delete this->rv3;

	}

	void leerRegistros(string path){

		cout << "-----------------------------------------------------" << endl;
		cout << "Abro el archivo y leo los registros secuencialmente." << endl;
		cout << "-----------------------------------------------------" << endl;

		try{
			this->file = new FileRecords(path.c_str(),rw);
		}catch (exception& e){
			cout << "Excepcion lanzada: " << e.what() << endl;
		}
		cout << "Archivo abierto ----> OK" << endl;

		this->rv1 = this->file->readNext();

		cout << "R1 leido: num: " << this->rv1->getId() << " tam: " << this->rv1->getTamanio() << " dato: " <<this->rv1->getDato() << endl;

		this->rv2 = this->file->readNext();

		cout << "R2 leido: num:" << this->rv2->getId() << "tam: " << this->rv2->getTamanio() << "dato: " <<this->rv2->getDato() << endl;

		this->rv3 = this->file->readNext();

		cout << "R3 leido: num:" << this->rv3->getId() << "tam: " << this->rv3->getTamanio() << "dato: " <<this->rv3->getDato() << endl;

		delete this->file;
		delete this->rv1;
		delete this->rv2;
		delete this->rv3;
	}

	void borrarUnRegistro(string path){
		try{
			this->file = new FileRecords(path.c_str(),rw);
		}catch (exception& e){
			cout << "Excepcion lanzada: " << e.what() << endl;
		}
		cout << "Archivo abierto ----> OK" << endl;

		this->file->seekRec(2);
		this->file->deleteCurrent();

		cout << "Registro Borrado" << endl;

		delete this->file;

	}

	void insertarOtroReg(string path){
		try{
			this->file = new FileRecords(path.c_str(),rw);
		}catch (exception& e){
			cout << "Excepcion lanzada: " << e.what() << endl;
		}
		cout << "Archivo abierto para insercion ----> OK" << endl;

		string s1 ("Este es un nuevo registro que debe insertarse en la posicion final de la tabla porque no entra en el libre.");
		string s2 ("Este si entra en el libre.");
		char* dato1 = new char[s1.length() + 1];
		char* dato2 = new char[s2.length() + 1];
		strcpy(dato1,s1.c_str());
		strcpy(dato2,s2.c_str());
		int size1 =  strlen(dato1) + 1 + sizeof(unsigned int)*2;
		int size2 =  strlen(dato2) + 1 + sizeof(unsigned int)*2;

		this->rv1 = new RegistroVariable(4,size1,dato1);
		this->rv2 = new RegistroVariable(5,size2,dato2);
		this->file->write(this->rv1);
		cout << "Se inserto el registro 4" << endl;
		this->file->write(this->rv2);
		cout << "Se inserto el registro 5" << endl;

		delete this->file;
		delete this->rv1;
		delete this->rv2;
	}

	void actualizarRegistro(string path){
		try{
			this->file = new FileRecords(path.c_str(),rw);
		}catch (exception& e){
			cout << "Excepcion lanzada: " << e.what() << endl;
		}
		cout << "Archivo abierto ----> OK" << endl;

		string s1 ("Actualizacion del registro 1. Entra porque es mas corto");
		string s2 ("Actualizacion del registro 3. Este no deberia entrar, debe reescribirse al final del archivo y liberar el 3");
		char* dato1 = new char[s1.length() + 1];
		char* dato2 = new char[s2.length() + 1];
		strcpy(dato1,s1.c_str());
		strcpy(dato2,s2.c_str());
		int size1 =  strlen(dato1) + 1 + sizeof(unsigned int)*3;
		int size2 =  strlen(dato2) + 1 + sizeof(unsigned int)*3;

		this->rv1 = new RegistroVariable(1,size1,dato1);
		this->rv2 = new RegistroVariable(3,size2,dato2);

		this->file->update(this->rv1);
		cout << "Se actualizo el registro 1" << endl;
		cout << "R1 leido: num: " << this->rv1->getId() << " tam: " << this->rv1->getTamanio() << " dato: " <<this->rv1->getDato() << endl;

		this->file->update(this->rv2);
		cout << "Se actualizo el registro 3" << endl;
		cout << "R1 leido: num: " << this->rv2->getId() << " tam: " << this->rv2->getTamanio() << " dato: " <<this->rv2->getDato() << endl;

		delete this->file;
		delete this->rv1;
		delete this->rv2;

	}

	void leerSecuencial(string path){
		try{
			this->file = new FileRecords(path.c_str(),rw);
		}catch (exception& e){
			cout << "Excepcion lanzada: " << e.what() << endl;
		}
		cout << "Archivo abierto ----> OK" << endl;
		cout << "--------------------------------------------" << endl;
		cout << "--------------------------------------------" << endl;
		cout << "Comienzo de lectura secuencial de todos los registros como quedaron" << endl;
		RegistroVariable* reg;

		for (unsigned int i=0; i < this->file->getTotalRecords();i++){

			reg = this->file->readNext();
			cout << "Reg leido -  num: " << reg->getId() << " tam: " << reg->getTamanio() << " dato: " <<reg->getDato() << endl;
			delete reg;
		}

		delete this->file;
	}

	void run(){
		string f ("file.dat");
		this->crearArchivo(f);
		this->insertarRegistros(f);
		this->leerRegistros(f);
		this->borrarUnRegistro(f);
		this->insertarOtroReg(f);
		this->actualizarRegistro(f);
		this->leerSecuencial(f);
	}

private:
	FileRecords* file;
	RegistroVariable* rv1;
	RegistroVariable* rv2;
	RegistroVariable* rv3;

};

int main(){

	TestFileRecord test1;

	test1.run();

	return 0;
}
