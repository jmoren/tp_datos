#include "../lib/encryption/cipher.h"

int main(int argc, char const *argv[])
{
	Cipher *cipher;
	string masterKey;
	string temp;
	string result_string  = "";
	string new_key = "probando2";

	masterKey = Funciones::read_master_key();
	cipher    = new Cipher(masterKey);

	cout << "Key: " << masterKey << endl;

	const char* masterKeyC = masterKey.c_str();
	if(cipher->validateKey())
	  	cout << "La key " << masterKeyC << "es valida" << endl;
	else
	  	cout << "La key " << masterKeyC << " no es valida para usar" << endl;


	cout << "\033[1;34mIngrese texto para encriptar:\033[0m ";
  getline(cin, temp);

  result_string = cipher->encrypt_block(temp);
  cout << "Encriptado: " << result_string << endl;
  result_string = cipher->decrypt_block(result_string);
  cout << "Desencriptado: " << result_string << endl;

  delete(cipher);

  cipher = new Cipher(masterKey);

  result_string = cipher->encrypt_block(new_key);
  cout << "Encriptado: " << result_string << endl;

  delete(cipher);
  cipher = new Cipher(result_string);
  cout << "Validamos una clave al azar (como si fuese un password): " <<  result_string << endl;

  if(cipher->validateKey())
  	cout << "La key es valida" << endl;
  else
  	cout << "La key no es valida para usar" << endl;

	delete(cipher);
	return 0;
}
