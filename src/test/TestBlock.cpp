#include "../lib/RegistroGenerico.h"
#include "../lib/Block.h"
#include "../lib/BlockHeader.h"
#include "../lib/FileBlock.h"

#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

void print_block(Block *memBlock){
  cout << "\n";
  cout << "Numero: " << memBlock->getNumber() << endl;
  cout << "Tamanio del Bloque: " << memBlock->getBlockSize() << endl;
  cout << "Espacio libre: " << memBlock->getFreeSpace() << endl;
  cout << "Cant. de Registros: " << memBlock->getTotalRecords() << endl;
  cout << "Estado: " << memBlock->getStatus() << endl;
  cout << "Registro Libre: " << memBlock->getNextFreeBlock() << endl;
  cout << "\n";
}

void add_registros(Block *memBlock, int size){
  RegistroGenerico *reg;
  int i, tam, res;
  string temp;
  char *dato;

  for(i=0;i<size;i++){
    cout << "\033[1;34mIngrese registro " << i+1 << " :\033[0m ";
    getline(cin, temp);
    tam = temp.length() + 1 + sizeof(int);
    dato = new char[temp.length()+1]();
    strcpy(dato, temp.c_str());

    reg = new RegistroGenerico(tam, dato);
    res = memBlock->setRegistroGenerico(reg);
    if(res == SUCCESS_SAVE_RECORD)
      cout << "Se guardo con exito el registro" << endl;
    else
      cout << "No se pudo guardar el registro" << endl;
  }
}

void remove_registro(Block *memBlock){
  int i;
  string temp;
  cout << "\033[1;34mBorrar registro numero ( 1 - "<< memBlock->getTotalRecords() << "):\033[0m ";
  getline(cin, temp);
  i = atoi(temp.c_str());
  memBlock->eliminarRegistroGenerico(i);
}

void print_records(Block *memBlock){
  RegistroGenerico * rec;
  cout << "\n";
  for(unsigned int i=0; i < memBlock->getTotalRecords(); i++){
    rec = memBlock->getRegistroGenerico(i);
    cout << "\033[1;32mRegistro "<< i+1 << ": \033[0m" << rec->getDato() << " (" << rec->getTamanio() << ")" << endl;
  }
}

void print_header(FileBlock *file){
  cout << "Header:" << endl;
  cout << "\tTotal Blocks: " << file->header->getTotalBlocks() << endl;
  cout << "\tBlock size: "   << file->header->getBlockSize() << endl;
  cout << "\tFree block: "   << file->header->getFreeBlock() <<  endl;
}

void list_free_blocks(FileBlock *file_block){
  int free_block;
  Block *block;

  free_block = file_block->header->getFreeBlock();
  while(free_block != -1){
    block = file_block->readBlock((unsigned int) free_block); // new block
    cout <<  block->getNumber() << " ";
    free_block = block->getNextFreeBlock();
    delete block; // delete block
  }
}

int main(){
  char path[23] = "test_file.bin";
  int block_size = 512;
  int i, k = 0;
  string temp;
  Block *current_block;
  FileBlock *file_block;

  file_block = new FileBlock(path, block_size);
  print_header(file_block);

  // create blocks with records. Write everything
  for(int i=0; i<3;i++){
    current_block = file_block->getNewBlock();
    print_block(current_block);
    add_registros(current_block, 3);
    file_block->writeBlock(current_block);
    delete(current_block);
  }

  // close and open again
  cout << "-----------------------------------------" << endl;
  cout << "       Terminada la carga de datos       " << endl;
  cout << "-----------------------------------------" << endl;

  // Cerramos y reabrimos file_block
  delete(file_block);
  file_block = new FileBlock(path);

  // read one block
  cout << "\033[1;34mLeer bloque ( 1 - "<< file_block->header->getTotalBlocks() << "):\033[0m ";
  getline(cin, temp);
  i = atoi(temp.c_str());
  current_block = file_block->readBlock(i);

  // print header and records
  print_records(current_block);
  print_block(current_block);

  // delete 2 records
  for(int i=0; i<2; i++){
    remove_registro(current_block);
    file_block->writeBlock(current_block);
    print_records(current_block);
    print_block(current_block);
  }

  // free two blocks
  for(int i=0; i<2; i++){
    cout << "\033[1;34mLiberamos bloque ( 1 - "<< file_block->header->getTotalBlocks() << "):\033[0m ";
    getline(cin, temp);
    k = atoi(temp.c_str());

    // liberamos antes de obtener otro bloque
    delete(current_block);
    current_block = file_block->readBlock(k);

    file_block->deleteBlock(current_block->getNumber());
  }

  // print free blocks
  cout << "\n\033[1;33mBloques libres: ";
  list_free_blocks(file_block);
  cout << "\n\033[0m";

  // write new two blocks
  for(int i=0; i<2; i++){

    // liberamos antes de obtener otro bloque
    delete current_block;
    current_block = file_block->getNewBlock();

    print_block(current_block);
    add_registros(current_block, 3);
    file_block->writeBlock(current_block);
  }

  print_header(file_block);

  // close and clean
  delete current_block;
  delete file_block;

  return 0;
}
