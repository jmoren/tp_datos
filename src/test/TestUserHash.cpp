#include "../lib/hashing/Hashing.h"
#include "../models/users/user_manager.h"

void print_header(FileBlock *file){
  cout << "Header:" << endl;
  cout << "\tTotal Blocks: " << file->header->getTotalBlocks() << endl;
  cout << "\tBlock size: "   << file->header->getBlockSize() << endl;
  cout << "\tFree block: "   << file->header->getFreeBlock() <<  endl;
}

void print_table(Hashing *hashing){
  unsigned int i;
  cout << "Tam table: " << hashing->table->tam_table << endl;
  for(i = 0; i < hashing->table->rows.size(); i++){
    cout << "\tRow: " << i << ", Bucket: " << hashing->table->rows.at(i)->bucket_id << endl;
  }
}

void initialize(){
  UserManager *manager;
  Bucket *bucket;

  manager = new UserManager(TAMANIO_BLOQUE);
  manager->hashing->writeTable();
  delete(manager);

  manager = new UserManager();
  bucket = manager->hashing->getNewBucket();
  bucket->dispersion = 1;
  manager->hashing->table->addRow(bucket->id);

  manager->hashing->saveBucket(bucket);
  manager->hashing->writeTable();

  delete(manager);
}

int main (int argc, char const* argv[]){
  UserManager *manager;
  User *user;
  string temp;
  int key, i;

  // initialize user_manager data
  initialize();

  manager = new UserManager();

  // Start work
  for(i=0; i < 3; i++){
    cout << "\nAdd user test" << endl;
    cout <<   "-------------" << endl;
    user = manager->addUser();
    delete(user);
  }

  // Delete one user
  cout << "\nDelete user test" << endl;
  cout <<   "----------------" << endl;

  cout << "\033[1;34mIngrese DNI:\033[0m ";
  getline(cin, temp);
  key = atoi(temp.c_str());

  user = manager->findUser(key);

  if(user){
    manager->printUser(user);

    cout << "\033[1;34mEsta seguro? (y/N):\033[0m ";
    getline(cin, temp);

    if((temp.compare("Y") == 0) || (temp.compare("y") == 0))
      manager->deleteUser(user->dni);
    else
      cout << "Cancelado..." << endl;
    delete(user);
  }else
    cout << "Ususario no encontrado!" <<  endl;

  // Start update
  cout << "\nUpdate user test" << endl;
  cout <<   "----------------" << endl;

  cout << "\033[1;34mIngrese DNI:\033[0m ";
  getline(cin, temp);
  key = atoi(temp.c_str());

  user = manager->findUser(key);
  if(user){
    user = manager->editUser(user);
    manager->printUser(user);
  }else
    cout << "Ususario no encontrado!" <<  endl;
  delete(user);

  // Start find
  cout << "\nFind user test (after delete)" << endl;
  cout <<   "-----------------------------" << endl;

  cout << "\033[1;34mIngrese DNI:\033[0m ";
  getline(cin, temp);
  key = atoi(temp.c_str());

  user = manager->findUser(key);
  if(user)
    manager->printUser(user);
  else
    cout << "Ususario no encontrado!" <<  endl;
  delete(user);


  manager->listUsers();
  // clean
  delete(manager);
  return 0;
}
