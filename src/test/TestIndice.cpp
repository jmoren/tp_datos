#include "../lib/Constantes.h"
#include "../lib/RegistroVariable.h"
#include "../lib/FileRecords.h"
#include "../lib/indiceInvertido/Vocabulario.h"
#include "../lib/indiceInvertido/Termino.h"
#include "../lib/indiceInvertido/TerminosXOrden.h"

class TestIndice{
public:
  TestIndice(){}
  ~TestIndice(){}

  void testTermino(){
    FileRecords f("pruebaTerminos.dat");
    Termino* unTermino = new Termino(1,"Hola");

    f.write(unTermino);
    f.close();
  }

  void creoVocabulario(){
    Vocabulario* v = new Vocabulario("pruebaVocabulario.dat");
    delete v;
  }

  void testEscrituraVocabulario(){
    Vocabulario* v = new Vocabulario("pruebaVocabulario.dat",rw);

    Termino* t1 = new Termino(1,"Pablo");
    Termino* t2 = new Termino(2,"Comida");
    Termino* t3 = new Termino(3,"Vaso");
    Termino* t4 = new Termino(77,"tpdemierda");

    v->agregarTermino(t1);
    v->agregarTermino(t2);
    v->agregarTermino(t3);
    v->agregarTermino(t4);

    v->persistir();

    delete v;

  }

  void testLecturaVocabulario(){
    Vocabulario* v = new Vocabulario("pruebaVocabulario.dat",rw);
    list<RegVocabulario*> terminos;
    terminos = v->getTerminos();

    list<RegVocabulario*>::iterator it = terminos.begin();
    for (unsigned int i = 0; i<terminos.size();i++){
      cout << "Termino: " << (*it)->getTermino()->getId() << " - Contenido: " << (*it)->getTermino()->getWord() << endl;
      it++;
    }
  }

  void crearTerminosXOrden(){
    string path("terminos_x_orden.ind");
    TerminosXOrden* tXOrden = new TerminosXOrden(path.c_str());
    delete tXOrden;
  }

  void testTerminosXOrden(){
    string path("terminos_x_orden.ind");
    TerminosXOrden* tXOrden = new TerminosXOrden(path.c_str(),rw);

    tXOrden->newTermino("termino");
    tXOrden->newTermino("termino compuesto");
    tXOrden->newTermino("loco");
    tXOrden->newTermino("FIUBA");
    tXOrden->newTermino("oenafioni");

    tXOrden->persistir();

    delete tXOrden;

  }

  void testAgregadoDeTerminos(){
    Termino* miTermino;
    string path("terminos_x_orden.ind");
    TerminosXOrden* tXOrden = new TerminosXOrden(path.c_str(),rw);

    miTermino = tXOrden->getTermino("loco");
    cout << miTermino->getId() << miTermino->getDato() << endl;
    miTermino = tXOrden->getTermino("termino compuesto");
    cout << miTermino->getId() << miTermino->getDato() << endl;
    miTermino = tXOrden->getTermino("FIUBA");
    cout << miTermino->getId() << miTermino->getDato() << endl;
    miTermino = tXOrden->getTermino("termino");
    cout << miTermino->getId() << miTermino->getDato() << endl;

    tXOrden->newTermino("Otro termino");
    miTermino = tXOrden->getTermino("Otro termino");
    cout << miTermino->getId() << miTermino->getDato() << endl;

    tXOrden->persistir();
    delete tXOrden;

  }

};

int main(){
  TestIndice test;

  test.testTermino();
  test.creoVocabulario();
  test.testEscrituraVocabulario();
  test.testLecturaVocabulario();
  test.crearTerminosXOrden();
  test.testTerminosXOrden();
  test.testAgregadoDeTerminos();
  return 0;
}




