
#include "../lib/hashing/Hashing.h"

void print_header(FileBlock *file){
  cout << "Header:" << endl;
  cout << "\tTotal Blocks: " << file->header->getTotalBlocks() << endl;
  cout << "\tBlock size: "   << file->header->getBlockSize() << endl;
  cout << "\tFree block: "   << file->header->getFreeBlock() <<  endl;
}

void print_table(Hashing *hashing){
  unsigned int i;

  cout << "Tam table: " << hashing->table->tam_table << endl;
  for(i = 0; i < hashing->table->rows.size(); i++){
    cout << "\tBucket: " << hashing->table->rows.at(i)->bucket_id << endl;
  }
}

int main (int argc, char const* argv[]){
  const char path[12] = "user_db.bin";
  Hashing *hashing;
  int i;

  // initialize
  cout << "Create \n\n";
  hashing = new Hashing(path, TAMANIO_BLOQUE);
  hashing->writeTable();
  print_header(hashing->block_table);
  delete(hashing);

  // restart: add buckets
  hashing = new Hashing(path);
  for(i = 0;i < 4; i++){
    Bucket *bucket = hashing->getNewBucket();
    hashing->table->addRow(bucket->id);
    hashing->saveBucket(bucket);
  }
  hashing->writeTable();
  print_table(hashing);
  cout << "==========================\n\n";
  delete(hashing);

  // restart: duplicate
  cout << "Duplicate \n\n";
  hashing = new Hashing(path);
  hashing->table->duplicate();
  hashing->writeTable();
  print_table(hashing);
  cout << "==========================\n\n";
  delete(hashing);

  // restart: truncate
  cout << "Truncate \n\n";
  hashing = new Hashing(path);
  hashing->table->truncate();
  hashing->writeTable();
  print_table(hashing);
  cout << "==========================\n\n";
  delete(hashing);

  // restart: update
  cout << "Update \n\n";
  hashing = new Hashing(path);
  hashing->table->update(2, 3);
  hashing->writeTable();
  print_table(hashing);
  cout << "==========================\n\n";
  delete(hashing);

  return 0;
}
