#include "Bucket.h"

template <typename T>
std::string to_string(T value)
{
  std::ostringstream os ;
  os << value ;
  return os.str() ;
}

Bucket::Bucket(){
  this->id            = 0;
  this->free_space    = TAMANIO_BLOQUE - sizeof(int) * 6;
  this->total_records = 0;
  this->status        = STATUS_OCCU;
  this->dispersion    = 0;
}

Bucket::~Bucket(){
  while (!this->records.empty()){
    RegistroId *registro = this->records.back();
    this->records.pop_back();
    if(registro)
      delete(registro);
  }
}

int Bucket::findRegistro(int key){
  bool found = false;
  int position = -1;
  unsigned int i = 0;

  while(!found && (i < this->records.size())){
    if(this->records.at(i)->getKey() == key){
      position = i;
    }
    i++;
  }

  return position;
}

RegistroId *Bucket::getRegistro(int key){
  RegistroId *reg;
  int position = -1;

  position = this->findRegistro(key);
  if(position != -1){
    reg = this->records.at(position);
  }else{
    reg = NULL;
  }

  return reg;
}

int Bucket::setRegistro(RegistroId *registro){
  int bucket_size = TAMANIO_BLOQUE - sizeof(int)*6;
  int res;

  if((int) registro->getSize() >= bucket_size){
    res = INVALID_RECORD;
  }else{
    if(registro->getSize() < this->free_space){
      this->records.push_back(registro);
      this->free_space    -= registro->getSize();
      this->total_records += 1;
      res = SUCCESS_SAVE_RECORD;
    }else{
      res = ERROR_SAVE_RECORD;
    }
  }

  return res;
}

RegistroId *Bucket::deleteRegistro(int key){
  RegistroId *registro;
  int position = -1;
  int size;

  position = this->findRegistro(key);
  if(position != -1){

    registro = this->records.at(position);
    size = registro->getSize();

    this->records.erase(this->records.begin() + position);
    this->free_space    += size;
    this->total_records -= 1;

  }else{
    registro = NULL;
  }

  return registro;
}

void Bucket::serialize(Block *block){
  unsigned int i;
  RegistroGenerico *primero;
	
	// Dejamos el status del bloque
  block->setStatus(this->status);

  // Primer registro con el tam de dispersion
  primero = new RegistroGenerico();
  this->serializeFirstReg(primero);
  block->setRegistroGenerico(primero);

  for(i = 0; i < this->records.size(); i++){
    // serializamos los registros de hash a un registro generico
    RegistroGenerico *generico = new RegistroGenerico();
    RegistroId  *registro = this->records.at(i);
    registro->serialize(generico);
    block->setRegistroGenerico(generico);
  }
}

void Bucket::deserialize(Block *block){
  unsigned int i;

  this->id     = block->getNumber();
  this->status = block->getStatus();
		
	 if(block->getTotalRecords() > 0)
    this->deserializeFirstReg(block->getRegistroGenerico(0));

  for (i = 1; i < block->getTotalRecords(); i++){ // empezamos desde el segundo registro
    RegistroId *registro = new RegistroId();
    RegistroGenerico *generico = block->getRegistroGenerico(i);
    if(generico){
      registro->deserialize(generico);
      this->setRegistro(registro);
    }
  }
}

bool Bucket::isEmpty(){
  return this->records.empty();
}

void Bucket::deserializeFirstReg(RegistroGenerico *registro){
  memcpy(&(this->dispersion), registro->dato, sizeof(int));
  this->free_space -= registro->getTamanio();
}

void Bucket::serializeFirstReg(RegistroGenerico *registro){
  char *dato = new char[sizeof(int)];
  memcpy(dato, &(this->dispersion), sizeof(int));

  registro->setTamanio(sizeof(int)*2);
  registro->setDato(dato);
}
