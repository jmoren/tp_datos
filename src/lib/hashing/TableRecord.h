#ifndef TABLERECORD_H_
#define TABLERECORD_H_

#include "../Constantes.h"

class TableRecord{
  public:
    unsigned int bucket_id;

    TableRecord();
    virtual ~TableRecord();

    void setBucketId(int number);
    int  getBucketId();

    void serialize(char *registro);
    void deserialize(char *registro);
};

#endif
