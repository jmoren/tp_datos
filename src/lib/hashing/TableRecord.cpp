#include "TableRecord.h"

TableRecord::TableRecord(){
  this->bucket_id = 0;
}

TableRecord::~TableRecord(){}

void TableRecord::setBucketId(int number){
  this->bucket_id = number;
}

int TableRecord::getBucketId(){
  return this->bucket_id;
}

void TableRecord::serialize(char *buffer){
  memcpy(buffer, &(this->bucket_id), sizeof(this->bucket_id));
}

void TableRecord::deserialize(char *buffer){
  memcpy(&(this->bucket_id), buffer, sizeof(this->bucket_id));
}
