#ifndef BUCKET_H_
#define BUCKET_H_

#include "../Constantes.h"
#include "../Block.h"
#include "../RegistroId.h"

class Bucket{
	public:
	  int id;
    int free_space;
	  int total_records;
	  int status;
    int dispersion;
    vector<RegistroId*> records;

    Bucket();
  	virtual ~Bucket();

    int getFreeSpace();
    int getId();

    RegistroId *getRegistro(int key);
    RegistroId *deleteRegistro(int key);
    int setRegistro(RegistroId *registro);
    int findRegistro(int key);

    bool isEmpty();

		void serialize(Block *block);
		void deserialize(Block *block);

    void deserializeFirstReg(RegistroGenerico *registro);
    void serializeFirstReg(RegistroGenerico *registro);
};

#endif
