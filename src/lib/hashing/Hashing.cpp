#include "Hashing.h"

void Hashing::print_table(){
  unsigned int i;
  cout << "Tam table: " << this->table->tam_table << endl;
  for(i = 0; i < this->table->rows.size(); i++){
    cout << "\tBucket: " << this->table->rows.at(i)->bucket_id << endl;
  }
}

Hashing::Hashing(const char *db_name, int size){
  this->block_table = new FileBlock(db_name, TAMANIO_BLOQUE);
  if(this->block_table){
    this->table = readTable();
  }
}

Hashing::Hashing(const char *db_name){
  this->block_table = new FileBlock(db_name);
  if(this->block_table){
    this->table = readTable();
  }
}

Hashing::~Hashing(){
  delete(this->table);
  delete(this->block_table);
}

// public methods

int Hashing::getNextId(){
  int current_id = this->block_table->header->getLastRecordId() + 1;
  return current_id;
}

void Hashing::setNextId(int id){
  this->block_table->header->setLastRecordId(id);
}

RegistroId *Hashing::getRegistro(int key){
  RegistroId *registro, *copy;
  Bucket *bucket;
  TableRecord *row;
  int pos;

  pos = this->getPosition(key);
  if(pos == -1)
    copy = NULL;
  else{
    row = this->table->getRow(pos);
    if(row){
      bucket = this->getBucket(row->getBucketId());

      registro = bucket->getRegistro(key);
      if(registro){
        copy = new RegistroId();
        registro->clone(copy);
      }else{
        copy = NULL;
      }
      delete(bucket);
    }else{
      copy = NULL;
    }
  }

  return copy;
}

int Hashing::addRegistro(RegistroId *registro){
  Bucket *bucket;
  TableRecord *row;
  int pos, res;
  int status = -1;
  bool complete = false;

  while(!complete){
    pos = this->getPosition(registro->getKey());
    if(pos == -1){
      res = pos; // error con la funcion de hashing
      complete = true;
    }else{
      row = this->table->getRow(pos);
      if(row){
        bucket = this->getBucket(row->getBucketId()); // crea un bucket
        status = bucket->setRegistro(registro);
        switch(status){
          case ERROR_SAVE_RECORD:
            reHashBucket(bucket, pos);
            break;
          case INVALID_RECORD:
            cout << "\033[1;31mFallo... No se puede guardar de ninguna manera.\033[0m" << endl;
            complete = true;
            res = INVALID_RECORD;
            this->saveBucket(bucket); // lo guardo como estaba
            break;
          case SUCCESS_SAVE_RECORD:
            complete = true;
            res = SUCCESS_SAVE_RECORD;
            this->setNextId(registro->getKey());
            this->saveBucket(bucket);
            break;
        }
      }else{
        complete = true;
        res = ERROR_READ_TABLE; // error leyendo la tabla
      }
    }
  }

  return res;
}

int Hashing::deleteRegistro(int key){
  Bucket *bucket, *reemp_bucket;
  RegistroId *registro;
  TableRecord *row;
  int row_ahead, row_back;
  int pos, status, result, pos_back, pos_ahead, steps;

  pos = this->getPosition(key);
  if(pos == -1)
    result = pos; // Error getting bucket_id
  else{
    row  = this->table->getRow(pos);
    if(row){
      bucket   = this->getBucket(row->getBucketId());
      registro = bucket->deleteRegistro(key);
      if(registro){
        delete(registro);
        result = SUCCESS_DELETE_RECORD;
      }else{
        result = RECORD_NOT_FOUND;
      }
      if(result == SUCCESS_DELETE_RECORD){
        if(bucket->isEmpty()){
          steps = bucket->dispersion/2;
          pos_ahead = (pos + steps)%this->table->tam_table;

          if(pos < steps)
            pos_back = (pos + this->table->tam_table - steps)%this->table->tam_table;
          else
            pos_back = (pos - steps)%this->table->tam_table;

          row_back  = this->table->getRow(pos_back)->getBucketId();
          row_ahead = this->table->getRow(pos_ahead)->getBucketId();

          if(row_back == row_ahead){
            this->table->update(pos, row_back);
            status = this->table->truncate();
            if(status == SUCCESS_TRUNC_TABLE){
              this->deleteBucket(bucket);
              reemp_bucket = this->getBucket(row_back);
              reemp_bucket->dispersion = reemp_bucket->dispersion/2;
              this->saveBucket(reemp_bucket);
            }// si no entra aca es porque no se trunco la tabla

          }// si no entra aca es porque no hay con que reemplazar

        }// si no entra aca es porque no esta vacio

      }// si no entra aca es porque no se elimino nada

      this->saveBucket(bucket); // se guardo el bucket
    }else{
      result = ERROR_READ_TABLE;
    }
  }

  return result;
}

int Hashing::updateRegistro(RegistroId *registro){
  int pos, result, status;
  Bucket *bucket;
  TableRecord *row;
  RegistroId *old_registro, *copy;

  pos = this->getPosition(registro->getKey());
  if(pos == -1)
    result = pos; // Error getting bucket_id
  else{
    row = this->table->getRow(pos);
    if(row){
      bucket = this->getBucket(row->getBucketId());

      // guardo el viejo registro en un registro totalmente diferente
      copy = new RegistroId();
      old_registro = bucket->deleteRegistro(registro->getKey());
      old_registro->clone(copy);
      delete(old_registro);
      this->saveBucket(bucket);

      status = this->addRegistro(registro);

      switch(status){
        case ERROR_SAVE_RECORD:
          delete(registro);
          registro = NULL;
          result = this->addRegistro(copy);
          break;
        case INVALID_RECORD:
          delete(registro);
          registro = NULL;
          result = this->addRegistro(copy);
          break;
        case SUCCESS_SAVE_RECORD:
          result = SUCCESS_SAVE_RECORD;
          delete(copy);
          break;
      };
    }
  }

  return result;
}

// Manejo de la tabla
Table *Hashing::readTable(){
  Block *block = this->block_table->getFirstBlock();

  this->table = new Table();
  table->deserialize(block);

  delete(block);
  return table;
}

void Hashing::writeTable(){
  Block *block = this->block_table->getFirstBlock();
  block->cleanup();

  this->table->serialize(block);
  this->block_table->writeBlock(block);
  delete(block);
}

// private methods
Bucket *Hashing::getBucket(int id){
  Block *block;
  Bucket *bucket;

  block = this->block_table->readBlock(id);
  if(block){
    bucket = new Bucket();
    bucket->deserialize(block);
    delete(block);
  }else{
    bucket = NULL;
  }

  return bucket;
}

Bucket *Hashing::getNewBucket(){
  Block *block;
  Bucket *bucket;

  block = this->block_table->getNewBlock();
  bucket = new Bucket();
  bucket->deserialize(block);

  bucket->dispersion = this->table->tam_table;
	bucket->status     = STATUS_OCCU;
  // clean
  delete(block);

  return bucket;
}

void Hashing::saveBucket(Bucket *bucket){
  Block *block;
  int id, size;

  id     = bucket->id;
  size   = this->block_table->header->getBlockSize();
  block  = new Block(id, size);

  bucket->serialize(block);

  this->block_table->writeBlock(block);

  if(block->getStatus() == STATUS_FREE){
    this->block_table->deleteBlock(id);
  }

  delete(bucket);
  delete(block);
}

void Hashing::reHashBucket(Bucket *current_bucket, int current_pos){
  Bucket *bucket;
  int steps, new_pos;

  if(current_bucket->dispersion == this->table->tam_table){
    this->table->duplicate();
    bucket = this->getNewBucket();
    current_bucket->dispersion = current_bucket->dispersion*2;
    bucket->dispersion = current_bucket->dispersion;
    this->table->update(current_pos, bucket->id);
  }else{
    bucket = this->getNewBucket();
    current_bucket->dispersion = current_bucket->dispersion*2;
    bucket->dispersion = current_bucket->dispersion;
    steps = current_bucket->dispersion;
    new_pos = (current_pos + steps)%this->table->tam_table;
    while(current_pos != new_pos){
      this->table->update(new_pos, bucket->id);
      new_pos = (new_pos + steps)%this->table->tam_table;
    }
  }

  this->redistributeRecords(current_bucket, bucket);
}

void Hashing::redistributeRecords(Bucket *current_bucket, Bucket *bucket){
  RegistroId *registro, *copy;
  unsigned int i;
  int pos;
  TableRecord *row;

  for(i = 0; i < current_bucket->records.size(); i++){
    registro = current_bucket->records.at(i);
    pos = this->getPosition(registro->getKey());
    row = this->table->getRow(pos);
    if(row){
      if(current_bucket->id != row->getBucketId()){
        copy = new RegistroId();
        registro = current_bucket->deleteRegistro(registro->getKey());
        registro->clone(copy);
        bucket->setRegistro(copy);
        delete(registro);
      }
    }
  }

  this->saveBucket(current_bucket);
  this->saveBucket(bucket);
}

void Hashing::deleteBucket(Bucket *bucket){
  bucket->status = STATUS_FREE;
}

int Hashing::getPosition(int key){
  int res;
  if(this->table->tam_table == 0)
    res = -1;
  else
    res = (key % this->table->tam_table);

  return res;
}

vector<RegistroId*> Hashing::getAll(){
  vector<RegistroId*> all;
  Bucket *bucket;
  RegistroId *registro;
  RegistroId *copy;
  int i, offset = 1;
  unsigned int j;

  for(i = 1; i < this->block_table->getTotalBlocks(); i++){
    bucket = this->getBucket(offset+i);
    if(bucket->status == STATUS_OCCU){
      for( j = 0; j < bucket->records.size(); j++){
        copy     = new RegistroId();
        registro = bucket->records.at(j);
        registro->clone(copy);
        all.push_back(copy);
      }
    }
    delete(bucket);
  }

  return all;
}
