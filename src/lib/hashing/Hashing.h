#ifndef HASHING_H_
#define HASHING_H_

#include "../Constantes.h"
#include "../FileBlock.h"

#include "Table.h"
#include "Bucket.h"


class Hashing{

  public:
    FileBlock *block_table;
    Table *table;

    Hashing(const char *db_name, int size);
    Hashing(const char *db_name);
    virtual ~Hashing();

    Table *readTable();
    void  writeTable();

    RegistroId *getRegistro(int key);
    int addRegistro(RegistroId *registro);
    int deleteRegistro(int key);
    int updateRegistro(RegistroId *registro);

    Bucket *getBucket(int id);
    Bucket *getNewBucket();
    void setNextId(int id);
    int  getNextId();
    void saveBucket(Bucket *bucket);
    void deleteBucket(Bucket *bucket);
    void reHashBucket(Bucket *current_bucket, int pos);
    void redistributeRecords(Bucket *current, Bucket *bucket);
    int getPosition(int key);
    vector<RegistroId*> getAll();

    void print_table();
};

#endif
