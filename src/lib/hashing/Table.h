#ifndef TABLE_H_
#define TABLE_H_

#include "../Constantes.h"
#include "../Block.h"
#include "TableRecord.h"

class Table{
  public:
    int tam_table;
    vector<TableRecord*> rows;

    Table();
    virtual ~Table();

    void serialize(Block *block);
    void deserialize(Block *block);
    void addRow(int number);

    TableRecord *getRow(int position);
    int update(int position, int number);
    int duplicate();
    int truncate();
};

#endif
