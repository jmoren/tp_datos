#include "Table.h"

Table::Table(){
  this->tam_table = 0;
}

Table::~Table(){
  while(!this->rows.empty()){
    TableRecord *row = this->rows.back();
    delete(row);
    this->rows.pop_back();
  }
}

void Table::addRow(int number){
  TableRecord *row = new TableRecord();
  row->setBucketId(number);

  this->rows.push_back(row);
  this->tam_table += 1;
}

TableRecord *Table::getRow(int position){
  TableRecord *row;
  if((position >= 0) && position <= (int)this->rows.size())
    row = this->rows.at(position);
  else
    row = NULL;

  return row;
}

int Table::update(int position, int number){
  int status;
  if((position >= 0) && position <= (int)this->rows.size()){
    this->rows.at(position)->setBucketId(number);
    status = 0;
  }else{
    status = -1;
  }

  return status;
}

int Table::duplicate(){
  TableRecord *row;
  int size, i, res;
  size = this->tam_table;

  for(i = 0; i < size; i++){
    row = this->rows.at(i);
    addRow(row->getBucketId());
  }

  if(size*2 == this->tam_table){
    res = 0;
  }else
    res = -1;

  return res;
}

int Table::truncate(){
  TableRecord *row;
  int size;
  int i = 0;
  int res = SUCCESS_TRUNC_TABLE;
  bool must_truncate = true;

  size = this->tam_table/2;

  while(must_truncate && (i < size)){
    if(this->rows.at(i)->getBucketId() != this->rows.at(i+size)->getBucketId()){
      must_truncate = false;
    }
    i++;
  }

  if(must_truncate){
    for(i=0; i < size; i++){
      row = this->rows.at(size);
      delete(row);
      this->rows.erase(this->rows.begin() + size);
      this->tam_table -= 1;
    }
    res = SUCCESS_TRUNC_TABLE;
  }else{
    res = ERROR_TRUNC_TABLE;
  }

  return res;
}

void Table::deserialize(Block *block){
  int total;
  int i;

  total = block->getTotalRecords();
  this->tam_table = total;
  for(i = 0; i < total; i++){
    RegistroGenerico *registro = block->getRegistroGenerico(i);
    TableRecord *row = new TableRecord();
    row->deserialize(registro->getDato());
    this->rows.push_back(row);
  }
}

void Table::serialize(Block *block){
  int total;
  int i;
  RegistroGenerico *registro;
  // resetamos el bloque donde vamos a escribir la tabla.
  block->cleanup();
  total = this->tam_table;
  for(i = 0; i < total; i++){
    registro = new RegistroGenerico();
    char *buffer = new char[sizeof(int)]();

    this->rows.at(i)->serialize(buffer);

    registro->setTamanio(sizeof(int)*2);
    registro->setDato(buffer);

    block->setRegistroGenerico(registro);
  }
}
