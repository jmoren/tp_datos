#include "BlockHeader.h"

BlockHeader::BlockHeader(unsigned int sizeBlock){
  this->blockSize    = sizeBlock;
  this->totalBlocks  = 0;
  this->freeBlock    = -1;
  this->lastRecordId = 0;
}

BlockHeader::BlockHeader(char* memBlock) {
  this->deserialize(memBlock);
}

BlockHeader::~BlockHeader() {}

void BlockHeader::serialize(char *block){
  char *offset = block;
  memcpy(offset, &(this->blockSize), sizeof(this->blockSize));
  offset += sizeof(this->blockSize);

  memcpy(offset, &(this->totalBlocks),sizeof(this->totalBlocks));
  offset += sizeof(this->totalBlocks);

  memcpy(offset, &(this->freeBlock),sizeof(this->freeBlock));
  offset += sizeof(this->freeBlock);

  memcpy(offset, &(this->lastRecordId),sizeof(this->lastRecordId));
}

void BlockHeader::deserialize(char* memBlock){
  memcpy(&(this->blockSize), memBlock,sizeof(this->blockSize));
  memBlock += sizeof(this->blockSize);

  memcpy(&(this->totalBlocks), memBlock,sizeof(this->totalBlocks));
  memBlock += sizeof(this->totalBlocks);

  memcpy(&(this->freeBlock), memBlock,sizeof(this->freeBlock));
  memBlock += sizeof(this->freeBlock);

  memcpy(&(this->lastRecordId), memBlock,sizeof(this->lastRecordId));
}

unsigned int BlockHeader::getSize(){
  unsigned int size = 0;
  size += sizeof(this->blockSize);
  size += sizeof(this->totalBlocks);
  size += sizeof(this->freeBlock);
  size += sizeof(this->lastRecordId);
  return size;
}

unsigned int BlockHeader::getTotalBlocks(){
  return this->totalBlocks;
}

unsigned int BlockHeader::getBlockSize(){
  return this->blockSize;
}

unsigned int BlockHeader::getLastRecordId(){
  return this->lastRecordId;
}

void BlockHeader::setLastRecordId(int id){
  this->lastRecordId = id;
}

int BlockHeader::getFreeBlock(){
  return this->freeBlock;
}

void BlockHeader::setFreeBlock(int number){
  this->freeBlock = number;
}

void BlockHeader::addBlock(){
  this->totalBlocks += 1;
}

void BlockHeader::removeBlock(){
  this->totalBlocks -= 1;
}
