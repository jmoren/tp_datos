#ifndef __CONSTANTES__
#define __CONSTANTES__

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <fstream>

#include <cstring>
#include <ctype.h>
#include <stdlib.h>
#include <ctime>
#include <getopt.h>
#include <vector>
#include <map>
#include <list>

enum TipoRegistro { TipoUsuarioDNI, ProvinciaDNI, NombreCategoriaIDServicio, IDServicioIDUsuarioIDConsulta, IDServicioIDUsuarioIDPCotizacion, IDServicioFechaCHoraCIDConsulta, IDServicioFechaHoraIDPCotizacion, TipoUsuarioIDServ };
enum OpenMode{read,write,append,rw};
enum TipoNodoHno {DERECHO,IZQUIERDO};
enum ModoConsulta {unionT,interseccion};

const unsigned int underflow = 20;
const unsigned int overflow = 20;

/* Constantes Cli */
#define COMMAND_NOT_FOUND    (-10)
#define NOT_AUTHORIZED       (-11)
#define COMMAND_CANCEL       (-3)
#define LOG_OUT              (-100)
#define CLI_OFF              (-70)
#define CLI_ON               (-50)
#define SUCCESS_LOGIN        (0)
#define SUCCES_NEW_USER      (0)


/* Constantes de la aplicacion */
#define TAMANIO_HEADER          (16)
#define TAMANIO_BLOQUE          (2048)
#define TAMANIO_BLOQUE_USER     (2048)
#define TAMANIO_BLOQUE_SERVICE  (2048)
#define TAMANIO_BLOQUE_CATEGORY (2048)
#define TAMANIO_BLOQUE_MSG      (2048)
#define TAMANIO_BLOQUE_QUOTE    (2048)
#define BLOCK_SIZE_OCURRENCIAS  (512)
#define BLOCK_SIZE_TERMINOS_ORDEN (512)
#define BLOCK_SIZE_LISTAS_INVERTIDAS (512)

/* Block status */
#define STATUS_FREE (0)
#define STATUS_OCCU (1)

/* Definiciones de record */
#define SUCCESS_SAVE_RECORD    (0)
#define SUCCESS_DELETE_RECORD  (0)
#define RECORD_NOT_FOUND    (-7)
#define INVALID_RECORD      (-8)
#define ERROR_READ_RECORD   (-9)
#define ERROR_DELETE_RECORD (-10)
#define ERROR_SAVE_RECORD   (-11)
#define ERROR_UNKNON_RECORD (-12)

/* Definiciones de table */
#define SUCCESS_TRUNC_TABLE (0)
#define ERROR_TRUNC_TABLE   (-1)
#define ERROR_READ_TABLE    (-2)

/* Definiciones de user */
#define SUCCESS_USER_SAVE (0)
#define ERROR_USER_SAVE   (-1)
#define NO_ROLE           (13)
#define ADMIN_ROLE        (12)
#define PROVEEDOR_ROLE    (11)
#define USER_ROLE         (10)

/* Definiciones de servicio */
#define GRATIS            (15)
#define SUBASTA           (16)
#define PRECIO_FIJO       (17)
#define NOT_OWNER         (18)

using namespace std;

#endif
