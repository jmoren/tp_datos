/*
 * FreeSpaceTable.h
 *
 *  Created on: 18/10/2013
 *      Author: pablo
 */

#ifndef FREESPACETABLE_H_
#define FREESPACETABLE_H_

#include <fstream>
#include <iostream>
#include <vector>
#include <cstring>
#include "Constantes.h"
#include "ISerialize.h"

using namespace std;

class FreeSpaceTable : ISerialize{

public:
	FreeSpaceTable(const char* path);
	FreeSpaceTable(const char* path,OpenMode mode);
	virtual ~FreeSpaceTable();

	/**
	 * Agrega una entrada en la tabla de libres, indicando posicion,
	 * y cantidad de bytes que se liberan
	 */
	void addFreeSpace(unsigned int,unsigned int);

	/**
	 * Busca un espacio libre de tamanio mayor al pasado por parametro
	 * Si no encuentra ninguno devuelve -1 en la 1ra pos del vector
	 */
	vector<int> getFreeSpace(int);

	vector<int> getFreePos(unsigned int pos);

	unsigned int getTotalEntries();

	bool isFree(int);

	/**
	 *
	 */
	void close();

	void serialize(char*);
	void deserialize(char*);

private:
	fstream file;
	unsigned int totalEntries;
	vector< vector<int> > freeSpaceTable; //Fila: Pos | Size

	void readTable();
	void writeTable();

};


#endif /* FREESPACETABLE_H_ */
