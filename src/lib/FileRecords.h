/*
 * FileRecords.h
 *
 *  Created on: 18/10/2013
 *      Author: pablo
 */

#ifndef FILERECORDS_H_
#define FILERECORDS_H_

#include <fstream>
#include <map>
#include <exception>
#include <vector>
#include <cstring>
#include "Constantes.h"
#include "RegistroVariable.h"
#include "BlockHeader.h"
#include "FreeSpaceTable.h"

using namespace std;

class FileRecords {

public:

	FileRecords(const char* path);
	FileRecords(const char* path,OpenMode mode);
	virtual ~FileRecords();
	void close();
	unsigned int getTotalRecords();

	/**
	 * Obtiene el registro ubicado en la posicion pasada por parametro
	 */
	RegistroVariable* getRec(unsigned int pos);

	/**
	 * Se posiciona al principio del registro buscado.
	 * Si no lo encuentra lanza una excepcion
	 */
	void seekRec(unsigned int number);

	/**
	 * Busca un registro libre donde entre el registro. Si no encuentra, lo agrega
	 * al final
	 */
	void write(RegistroVariable* reg);

	/**
	 * Actualiza un registro. Si es necesario lo reacomoda en el archivo
	 */
	void update(RegistroVariable* reg);

	/**
	 * Borra el registro donde esta posicionado y lo agrega a la tabla de libres
	 */
	void deleteCurrent();

	/**
	 * Devuelve el siguiente registro a partir de donde esta posicionado
	 */
	RegistroVariable* readNext();


private:

	fstream file;
	unsigned int totalRecords; //header
	unsigned int posActual;
	FreeSpaceTable* freeSpace;

	void readHeader();
	void writeHeader();
	unsigned int getSizeNextRecord();
	unsigned int getIdNextRecord();
	bool recordExists(RegistroVariable* rec);

};

#endif /* FILERECORDS_H_ */
