#include "FileRecords.h"

FileRecords::FileRecords(const char* path){

	string pathFreeSpace(path);
	pathFreeSpace.append(".fr");

	try{
		this->file.open(path,ios::out | ios::binary);
	}catch(exception& e){
		throw exception();
	}
	this->totalRecords = 0;
	this->posActual = 0;
	writeHeader();
	this->freeSpace = new FreeSpaceTable(pathFreeSpace.c_str());
}

FileRecords::FileRecords(const char *path,OpenMode mode){
	string pathFreeSpace(path);
	pathFreeSpace.append(".fr");
	this->totalRecords = 0;

	switch (mode){

	case rw:
		this->file.open(path,ios::in | ios::out | ios::binary);
		if (file.fail())
			throw exception();
		break;

	default:
		this->file.open(path,ios::in | ios::binary);
		if (file.fail())
			throw exception();
		break;
		}
	this->posActual = 0;
	this->freeSpace = new FreeSpaceTable(pathFreeSpace.c_str(),rw);
	this->readHeader();
}

FileRecords::~FileRecords() {
	this->freeSpace->close();
	delete this->freeSpace;
	if(this->file.is_open())
		this->file.close();
}

// Implementacion de metodos publicos

void FileRecords::close(){
	this->freeSpace->close();
	this->file.close();
}

unsigned int FileRecords::getTotalRecords(){
	return this->totalRecords;
}

RegistroVariable* FileRecords::getRec(unsigned int pos){
	unsigned int size;
	char* buffer;
	RegistroVariable* reg = new RegistroVariable();

	this->posActual = pos;
	this->file.seekg(pos,ios::beg);
	size = this->getSizeNextRecord();
	buffer = new char[size];
	this->file.read(buffer,size);
	reg->deserialize(buffer);

	delete[] buffer;
	return reg;
}

void FileRecords::seekRec(unsigned int key){
	RegistroVariable* rec;
	bool found = false;
	unsigned int seek = 0;
	unsigned int i = 0;

	// me muevo al primer registro y recorro
	this->posActual = sizeof(this->totalRecords); //header
	this->file.seekg(this->posActual,ios::beg);

	while (!found && i < this->totalRecords){
		rec = this->readNext();
		i++;
		if (rec->getId() == key){
			found = true;
			seek = rec ->getTamanio();
		}
		delete rec;
	}

	if (!found)
		throw exception();

	// Cuando lo encuentra se posiciona al principio de ese reg
	this->posActual -= seek;
	this->file.seekg(this->posActual,ios::beg);
}

void FileRecords::write(RegistroVariable* rec){

	unsigned int recordSize;
	char* recordSerialized;
	vector<int> freeSpace;

	// Controlo unicidad
	if(this->recordExists(rec))
		throw exception(); // Todo: definir la exception

	recordSize = rec->getTamanio();
	freeSpace = this->freeSpace->getFreeSpace(rec->getTamanio());

	if (freeSpace[0] > 0){
		this->file.seekp(freeSpace[0],ios::beg);
		this->freeSpace->addFreeSpace(freeSpace[0] + rec->getTamanio(),freeSpace[1] - rec->getTamanio());
	}
	else{
		this->file.seekp(0,ios::end);
	}
	// escribo donde quedo posicionado
	recordSerialized = new char[recordSize];
	rec->serialize(recordSerialized);
	this->file.write(recordSerialized, recordSize); //Todo: Contemplar exception
	this->posActual += recordSize;
	this->totalRecords ++;
	this->writeHeader();

	delete[] recordSerialized;
}

void FileRecords::update(RegistroVariable* recActualizado){
	RegistroVariable* recAActualizar;
	char* recActualizadoSerialized;
	unsigned int freeSpace = 0;

	try{
		this->seekRec(recActualizado->getId());
	}catch(exception& e){
		throw exception(); //Todo: Definir la excepcion
	}
	recAActualizar = this->readNext();

	//Si el tamanio del registro actualizado el mayor al que esta persistido
	//elimino este ultimo y lo reescribo
	if (recActualizado->getTamanio() > recAActualizar->getTamanio()){
		this->seekRec(recActualizado->getId());
		this->deleteCurrent();
		this->write(recActualizado);
	}else{
		this->seekRec(recActualizado->getId());
		freeSpace = recAActualizar->getTamanio()- recActualizado->getTamanio();
		recActualizadoSerialized = new char[recActualizado->getTamanio()];
		recActualizado->serialize(recActualizadoSerialized);
		this->file.write(recActualizadoSerialized,recActualizado->getTamanio());
		this->posActual += recActualizado->getTamanio();
		this->freeSpace->addFreeSpace(this->posActual,freeSpace);
		this->posActual += freeSpace;
		delete[] recActualizadoSerialized;
	}

	delete recAActualizar;
}

void FileRecords::deleteCurrent(){
	RegistroVariable* reg;

	reg = readNext();
	this->freeSpace->addFreeSpace(this->posActual - reg->getTamanio(),reg->getTamanio());
	this->posActual += reg->getTamanio();
	this->totalRecords --;
	this->writeHeader();
}

RegistroVariable *FileRecords::readNext(){
  RegistroVariable *rec = new RegistroVariable();
  char* buffer;
  int recordSize = 0;
  vector<int> freeSpace;

  //Mientras este parado en un espacio libre avanzo
  while (this->freeSpace->isFree(this->posActual)){
	  freeSpace = this->freeSpace->getFreePos(this->posActual);
	  this->posActual += freeSpace[1];
	  this->file.seekg(this->posActual,ios::beg);
  }
  recordSize = getSizeNextRecord();
  buffer = new char[recordSize];
  this->file.seekg(this->posActual,ios::beg);
  this->file.read(buffer,recordSize);
  this->posActual += recordSize;
  rec->deserialize(buffer);

  delete[] buffer;

  return rec;
}

// Implementacion de metodos privados

void FileRecords::readHeader(){
	char* memBlock = new char[sizeof(this->totalRecords)];
	this->file.read(memBlock,sizeof(this->totalRecords));
	memcpy(&this->totalRecords,memBlock,sizeof(this->totalRecords));
	this->posActual += sizeof(this->totalRecords);

	delete[] memBlock;
}

void FileRecords::writeHeader(){
  unsigned int size = sizeof(this->totalRecords);
  char *buffer = new char[size];
  memset(buffer, 0, size);

  memcpy(buffer,&this->totalRecords,sizeof(this->totalRecords));

  this->file.seekg(0,ios::beg);
  this->file.write(buffer, size);
  this->posActual += size;
  delete[] buffer;
}

unsigned int FileRecords::getSizeNextRecord(){
	char* bufAux;
	unsigned int recordSize = 0;

	bufAux = new char[sizeof(unsigned int)*2];
	//obtengo tamanio del reg
	this->file.seekg(this->posActual,ios::beg);
	this->file.read(bufAux,sizeof(unsigned int)*2);
	char* iterator = bufAux;
	iterator += sizeof(unsigned int); //Salteo el numero de registro
	memcpy(&recordSize,iterator,sizeof(unsigned int));
	iterator += sizeof(unsigned int);

	delete[] bufAux;
	return recordSize;
}

unsigned int FileRecords::getIdNextRecord(){
	char* bufAux;
	unsigned int id = 0;

	bufAux = new char[sizeof(unsigned int)*2];
	this->file.read(bufAux,sizeof(unsigned int)*2);

	delete[] bufAux;
	return id;
}

bool FileRecords::recordExists(RegistroVariable* rec){
	RegistroVariable* recRead;
	bool found = false;
	unsigned int i = 0;
	unsigned int totalRecords = this->totalRecords;

	//Me muevo al principio y recorro secuencialmente
	this->posActual =  sizeof(this->totalRecords); //Header
	while (!found && i < totalRecords){
		recRead = this->readNext();
		i ++;
		if (rec->getId() == recRead->getId() )
			found = true;
		delete recRead;
	}
	return found;
}
