#ifndef REGISTROGENERICO_H_
#define REGISTROGENERICO_H_

#include "Constantes.h"

class RegistroGenerico{

public:
  RegistroGenerico();
  RegistroGenerico(unsigned int size,char* dato);
  virtual ~RegistroGenerico();

  unsigned int getTamanio();
  char* getDato();

  void setTamanio(unsigned int);
  void setDato(char* dato);
  void clone(RegistroGenerico *copy);

  void serialize(char*);
  void deserialize(char*);

  unsigned int tamanio;
  char* dato;

};

#endif
