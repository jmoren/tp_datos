#include "RegistroId.h"

RegistroId::RegistroId(){
  this->size = 0;
  this->key  = 0;
  this->dato = NULL;
}

RegistroId::~RegistroId(){
  delete[] this->dato;
}

int RegistroId::getKey(){
  return this->key;
}

int RegistroId::getSize(){
  return this->size;
}

char *RegistroId::getDato(){
  return this->dato;
}

void RegistroId::setSize(int size){
  this->size = size;
}

void RegistroId::setKey(int key){
  this->key = key;
}

void RegistroId::setDato(char *dato){
  this->dato = dato;
}

void RegistroId::clone(RegistroId *copy){
  copy->size = this->size;
  copy->key  = this->key;
  copy->dato = new char[this->size - sizeof(int)*2]();
  memcpy(copy->dato, this->dato, this->size - sizeof(int)*2);
}

void RegistroId::serialize(RegistroGenerico *registro){
  int  size    = this->size;
  char *pos;
  registro->setTamanio(size);

  registro->dato = new char[size]();
  pos = registro->dato;

  memcpy(pos, &(this->key), sizeof(int));
  pos += sizeof(int);
  memcpy(pos, this->dato, size - sizeof(int)*2);

}

void RegistroId::deserialize(RegistroGenerico *registro){
  int size_dato;
  char *buffer = registro->dato;

  // tamanio del dato usuario: sizeof(dato) - sizeof(key)
  this->size = registro->getTamanio();

  // dato del registro: dni, (name_size)name, (last_name_size)last_name
  memcpy(&(this->key), buffer, sizeof(int));
  size_dato = this->size - sizeof(int)*2;
  this->dato = new char[size_dato];
  memcpy(this->dato, buffer + sizeof(int), size_dato);
 }
