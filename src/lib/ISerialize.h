/*
 * ISerialize.h
 * Todas las clases persistibles deben heredar de esta interfaz
 */

#ifndef ISERIALIZE_H_
#define ISERIALIZE_H_

class ISerialize{

protected:
	ISerialize();
	virtual ~ISerialize();

	virtual void serialize(char*) = 0;
    virtual void deserialize(char*) = 0;

};


#endif /* ISERIALIZE_H_ */
