/*
 * IndexManager.h
 *
 *  Created on: 02/11/2013
 *      Author: pablo
 */

#ifndef INDEXMANAGER_H_
#define INDEXMANAGER_H_

#include "../../models/category/category.h"
#include "IndiceInvertido.h"

class IndexManager{

public:
	IndexManager(string entidadCampo);
	IndexManager(const char* path,string entidadCampo);
	~IndexManager();

	void indexar(unsigned int idDoc,char* doc);
	vector<RegistroId*> consultar(vector<string> consulta,ModoConsulta mode);


private:
	IndiceInvertido* index;
	Hashing* indicePrimario;

};


#endif /* INDEXMANAGER_H_ */
