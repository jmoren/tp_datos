#include "IndiceInvertido.h"

IndiceInvertido::IndiceInvertido(){
	this->vocabulario = NULL;
	this->ocurrencias = NULL;
	this->terminosXOrden = NULL;
	this->listasInvertidas = NULL;
	this->stopwords = NULL;
}

IndiceInvertido::IndiceInvertido(string entidadCampo){

	string pathVocabulario(entidadCampo + "_v.ind");
	string pathOcurrencias(entidadCampo + "_oc.ind");
	string pathTerminosXOrden(entidadCampo + "_or.ind");
	string pathListasInvertidas(entidadCampo + "_li.ind");

	this->vocabulario = new Vocabulario(pathVocabulario.c_str(),rw);
	this->ocurrencias = new Ocurrencias(pathOcurrencias.c_str(),rw);
	this->terminosXOrden = new TerminosXOrden(pathTerminosXOrden.c_str(),rw);
	this->listasInvertidas = new ListasInvertidas(pathListasInvertidas.c_str(),rw);
	this->stopwords = new StopWords("stop_words.txt");
}

IndiceInvertido::~IndiceInvertido(){

	delete this->vocabulario;
	delete this->ocurrencias;
	delete this->terminosXOrden;
	delete this->listasInvertidas;
	delete this->stopwords;

}

void IndiceInvertido::indexar(unsigned int idDoc,char* doc){
	vector<string> words;
	int idTermino = -1, res = 0;
	Termino* termino;
	ListaInvertida* li;
	string pathOcurrenciasOrdenadas;
	this->stopwords->init();
	words = this->parser(doc);

	for (unsigned int i = 0; i < words.size(); i++){

		// si no es stopword proceso
		if (!this->stopwords->es_stopWord(words[i])){
			// obtengo el id del termino
			idTermino = this->getIdTermino(words[i]);
			// si no esta en la lista de terminos x orden de aparicion lo agrego
			// a esa lista y al vocabulario
			if (idTermino < 0){

				this->terminosXOrden->newTermino(words[i]);
				idTermino = this->getIdTermino(words[i]);
				if(idTermino < 0){
					cout << "Error al crear un nuevo id de termino" << endl;

				}else{
					termino = new Termino(idTermino,words[i]);
					this->vocabulario->agregarTermino(termino);
					li = new ListaInvertida(idTermino);
					li->agregarDocumento(idDoc);
					res = this->listasInvertidas->agregarLista(li);
					if(res != 0)
						cout << "WARNING: revisar cuando se agrega una lista al indice invertido" << endl;
					this->ocurrencias->addOcurrencia(idTermino,idDoc);
				}

			}else{
				this->ocurrencias->addOcurrencia(idTermino,idDoc);
				this->listasInvertidas->actualizarLista(idTermino,idDoc);
			}

		}
	}
}

vector<unsigned int> IndiceInvertido::consultar(vector<string> words,ModoConsulta modo){
	vector<unsigned int> result;
	vector<ListaInvertida*> listas;
	Termino* termino;
	ListaInvertida* li;

	for (unsigned int i = 0; i < words.size(); i++){

		if (!this->stopwords->es_stopWord(words[i])){

			termino = this->vocabulario->getTermino(words[i]);

			if (termino != NULL){
				li = this->listasInvertidas->getLista(termino->getId());
				listas.push_back(li);
			}
		}
	}

	switch (modo){
	case unionT: result = unionListas(listas);
			break;
	case interseccion: result = interseccionListas(listas);
			break;
	}

	// Libero memoria
	while (!listas.empty()){
		li = listas.back();
		listas.pop_back();
		if (li)
			delete li;
	}

	return result;
}


// Implementacion de metodos privados

vector<string> IndiceInvertido::parser(char* cadena){
	vector<string> words;
	string word;
	char espacio = ' ';
	char coma = ',';
	char punto = '.';
	char puntoComa = ';';
	char eol = '\0';
	char* it = cadena;

	for (unsigned int i = 0; i <= strlen(cadena);i++){
		word.clear();
		while(*it != espacio && *it != coma && *it != punto && *it != puntoComa && *it != eol && i <= strlen(cadena)){
			word.push_back(*it);
			i++;
			it++;
		}
		words.push_back(word);
		it++;
	}
	return words;
}

int IndiceInvertido::getIdTermino(string unTermino){
	Termino* t;
	t = this->terminosXOrden->getTermino(unTermino);
	if(t)
		return (t->getId());
	else
		return -1;
}

/**
 * Inicializa los archivos necesarios para indexar
 */
void IndiceInvertido::init(string entidadCampo){
	string pathVocabulario(entidadCampo + "_v.ind");
	string pathOcurrencias(entidadCampo + "_oc.ind");
	string pathTerminosXOrden(entidadCampo + "_or.ind");
	string pathListasInvertidas(entidadCampo + "_li.ind");

	this->vocabulario = new Vocabulario(pathVocabulario.c_str());
	this->ocurrencias = new Ocurrencias(pathOcurrencias.c_str());
	this->terminosXOrden = new TerminosXOrden(pathTerminosXOrden.c_str());
	this->listasInvertidas = new ListasInvertidas(pathListasInvertidas.c_str());
	this->stopwords = new StopWords("stop_words.txt");
}

vector<unsigned int> IndiceInvertido::unionListas(vector<ListaInvertida*> listas){
	vector<unsigned int> res;
	vector<unsigned int> documentos;
	ListaInvertida* li;

	for (unsigned int i = 0; i < listas.size();i++){
		li = listas[i];
		documentos = li->getDocumentos();
		for (unsigned int j = 0; j < documentos.size();j++){

			if (!existeDoc(res,documentos[j])){
				res.push_back(documentos[j]);
			}

		}
	}
	return res;
}

vector<unsigned int> IndiceInvertido::interseccionListas(vector<ListaInvertida*> listas){
	vector<unsigned int> ids;

	return ids;
}


bool IndiceInvertido::existeDoc(vector<unsigned int> vec,unsigned int doc){
	bool found = false;
	unsigned int i = 0;

	while (!found && i < vec.size()){

		if(vec[i] == doc)
			found = true;
		i++;
	}
	return found;
}
