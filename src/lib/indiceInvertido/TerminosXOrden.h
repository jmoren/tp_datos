/*
 * TerminosXOrden.h
 *
 *  Created on: 02/11/2013
 *      Author: pablo
 */

#ifndef TERMINOSXORDEN_H_
#define TERMINOSXORDEN_H_

#include <list>
#include "Termino.h"
#include "../FileRecords.h"

using namespace std;

class TerminosXOrden{

public:
	TerminosXOrden(const char* path);
	TerminosXOrden(const char* path,OpenMode mode);
	virtual ~TerminosXOrden();

	/**
	 * devuelve el termino correspondiente al string recibido.
	 */
	Termino* getTermino(string word);

	/**
	 * Crea un nuevo termino asignandole un id
	 */
	void newTermino(string word);

	void persistir();
	void hidratar();

private:
	list<Termino*> terminos;
	list<Termino*> bufTerminos; //terminos no persistidos aun
	FileRecords* f;
	unsigned int ultimoIdAsignado;
};

#endif /* TERMINOSXORDEN_H_ */
