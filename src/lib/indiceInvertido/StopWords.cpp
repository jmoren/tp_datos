#include "StopWords.h"
#include <fstream>


StopWords::StopWords(string nombreArch) {
        this->nomArch = nombreArch;
}

StopWords::~StopWords(){ }

bool StopWords::init(){
        ifstream fin(this->nomArch.c_str());
        if (!fin) return false;

        // Recorre todo el archivo e inserta las stop words
        while (!fin.eof()){
                string termino = "";
                getline(fin, termino);

                // Esta insercion no permite terminos duplicados pero no falla
                this->words.insert(termino);
        }

        fin.close();
        return true;
}


bool StopWords::es_stopWord(string palabra) const{
        // Busca la palabra en el "set"
        set<string>::const_iterator it = this->words.find(palabra);

        if (it == this->words.end()){
                // No existe la palabra en el "set"
                return false;

        } else {
                // La palabra esta en el "set"
                return true;
        }
}
