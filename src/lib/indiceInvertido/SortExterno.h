/*
 * SortExterno.h
 *
 *  Created on: 05/11/2013
 *      Author: pablo
 */

#ifndef SORTEXTERNO_H_
#define SORTEXTERNO_H_

#include "../FileBlock.h"
#include "../Block.h"

using namespace std;

class SortExterno{

public:

	SortExterno(const char* path);
	virtual ~SortExterno();

	/**
	 * Devuelve el path del archivo ordenado
	 */
	string sort();

private:
	FileBlock* fileToOrder;
};


#endif /* SORTEXTERNO_H_ */
