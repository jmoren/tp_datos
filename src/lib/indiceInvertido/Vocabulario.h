#ifndef VOCABULARIO_H_
#define VOCABULARIO_H_

#include <list>
#include <string>
#include "../Constantes.h"
#include "../FileRecords.h"
#include "../RegistroVariable.h"
#include "Termino.h"


using namespace std;

class Vocabulario{

public:
	Vocabulario(const char* path);
	Vocabulario(const char* path,OpenMode mode);
	virtual ~Vocabulario();

	void agregarTermino(Termino* t);
	list<Termino*> getTerminos();
	Termino* getTermino(string word);
	void setListaInvertida(unsigned int idTermino,unsigned int idLista);
	bool existeTermino(Termino t);

	void persistir();
	void hidratar();

private:
	list<Termino*> nuevosTerminos;
	list<Termino*> terminosIndice;
	FileRecords* file;
};

#endif /* VOCABULARIO_H_ */
