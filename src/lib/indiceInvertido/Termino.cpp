#include "Termino.h"

Termino::Termino() : RegistroVariable(){
}

Termino::Termino(unsigned int id,string word){
	this->dato = new char[word.length() + 1];
	strcpy(this->dato,word.c_str());
	this->id = id;
	this->tamanio = word.length() + 1 + sizeof(this->id) + sizeof(this->tamanio);
}

Termino::~Termino(){
}

string Termino::getWord(){
	string aux(this->dato);
	return aux;
}

