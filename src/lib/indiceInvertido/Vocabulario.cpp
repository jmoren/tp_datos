#include "Vocabulario.h"

Vocabulario::Vocabulario(const char* path){
	this->file = new FileRecords(path);
	this->terminosIndice.clear();
}

Vocabulario::Vocabulario(const char* path,OpenMode mode){
	this->file = new FileRecords(path,mode);
	this->terminosIndice.clear();
	this->hidratar();
}

Vocabulario::~Vocabulario(){
	Termino* t;
	this->persistir();

	while (!this->terminosIndice.empty()){
		t = this->terminosIndice.back();
		this->terminosIndice.pop_back();
		if(t)
			delete t;
	}
	this->file->close();
	delete this->file;

}

void Vocabulario::agregarTermino(Termino* t){
	this->nuevosTerminos.push_back(t);
	this->terminosIndice.push_back(t);
}


list<Termino*> Vocabulario::getTerminos(){
	return this->terminosIndice;
}

Termino* Vocabulario::getTermino(string word){

	list<Termino*>::iterator it = this->terminosIndice.begin();
	for (unsigned int i = 0; i < this->terminosIndice.size();i++){
		if ((*it)->getWord().compare(word) == 0){
			return (*it);
		}
		it++;
	}
	return NULL;
}

void Vocabulario::persistir(){
	char* buffer;
	RegistroVariable* reg;

	list<Termino*>::iterator it = this->nuevosTerminos.begin();
	for (unsigned int i = 0;i < this->nuevosTerminos.size();i++){
		buffer = new char[(*it)->getTamanio()];
		(*it)->serialize(buffer);
		reg = new RegistroVariable();
		reg->deserialize(buffer);

		try{
			this->file->write(reg);
		}catch (exception& e){
			this->file->update(reg);
		}

		it++;
		delete reg;
		delete[] buffer;
	}
}

void Vocabulario::hidratar(){

	Termino* termino;
	RegistroVariable* regLeido;
	char* buffer;
	for (unsigned int i = 0; i < this->file->getTotalRecords();i++){
		termino = new Termino();
		regLeido = this->file->readNext();
		buffer = new char[regLeido->getTamanio()];
		regLeido->serialize(buffer);
		termino->deserialize(buffer);
		this->terminosIndice.push_back(termino);

		delete regLeido;
		delete[] buffer;
	}
}
