#include "ListasInvertidas.h"

ListasInvertidas::ListasInvertidas(const char* path){
	this->hashing = new Hashing(path,BLOCK_SIZE_LISTAS_INVERTIDAS);
	this->listasInv.clear();
	Bucket *bucket;

	this->hashing->writeTable();
	delete(this->hashing);

	this->hashing = new Hashing(path);
	bucket = this->hashing->getNewBucket();
	bucket->dispersion = 1;
	this->hashing->table->addRow(bucket->id);
	this->hashing->saveBucket(bucket);
	this->hashing->writeTable();
}

ListasInvertidas::ListasInvertidas(const char* path,OpenMode mode){
	this->hashing = new Hashing(path);
}

ListasInvertidas::~ListasInvertidas(){
	while (!this->listasInv.empty()){
		ListaInvertida *li = this->listasInv.back();
		this->listasInv.pop_back();
		if(li)
			delete(li);
	}
	delete this->hashing;
}

int ListasInvertidas::agregarLista(ListaInvertida* lista){
	int res = 0;

	RegistroId* reg = new RegistroId();
	lista->serialize(reg);
	res = this->hashing->addRegistro(reg);
	if (res != 0 ){
		cout << "No se agrego el registro al hashing" << endl;
	}
	this->listasInv.push_back(lista);
	return res;
}

ListaInvertida* ListasInvertidas::getLista(unsigned int idTermino){
	RegistroId* rec;
	ListaInvertida* li = new ListaInvertida();

	rec = this->hashing->getRegistro(idTermino);
	li->deserialize(rec);
  delete rec;
	return li;
}

int ListasInvertidas::actualizarLista(unsigned int idTermino,unsigned int idDoc){
	unsigned int res = 0;
	ListaInvertida* li;
	RegistroId* reg = new RegistroId();

	li = this->getLista(idTermino);

	li->agregarDocumento(idDoc);
	li->serialize(reg);
	res = this->hashing->updateRegistro(reg);

	delete li;
	return res;
}

unsigned int ListasInvertidas::getSize(){
	return this->listasInv.size();
}


//void ListasInvertidas::persistirBuffer(){
//	char* buffer;
//	Block* blockBuf = this->file->getNewBlock();
//	unsigned int totalListas = this->listasInv.size();
//	RegistroGenerico* rec;
//	unsigned int size = 0;
//
//	for(unsigned int i = 0;i < totalListas;i++){
//		size = this->listasInv[i]->getSize();
//		buffer = new char[size];
//		this->listasInv[1]->serialize(buffer);
//		rec = new RegistroGenerico();
//		rec->deserialize(buffer);
//		// Buscar lista invertida en el archivo
//		// borrarla e insertarla de nuevo
//		if(blockBuf->getFreeSpace() > rec->getTamanio())
//			blockBuf->setRegistroGenerico(rec);
//		else{
//			this->file->writeBlock(blockBuf);
//			delete blockBuf;
//			blockBuf = this->file->getNewBlock();
//		}
//		delete rec;
//		delete[] buffer;
//	}
//}
//
//void ListasInvertidas::hidratarBuffer(Block* buffer){
//	unsigned int totalRecords = buffer->getTotalRecords();
//	RegistroGenerico* rec;
//	char* buf;
//	ListaInvertida* li;
//
//	for (unsigned int i = 0; i < totalRecords; i++){
//		rec = buffer->getRegistroGenerico(i);
//		buf = new char[rec->getTamanio()];
//		rec->serialize(buf);
//		li = new ListaInvertida();
//		li->deserialize(buf);
//		this->listasInv.push_back(li);
//
//		delete rec;
//		delete buf;
//	}
//}
