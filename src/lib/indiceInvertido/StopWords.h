#ifndef STOPWORDS_H_
#define STOPWORDS_H_

#include <set>
#include <string>

using namespace std;

class StopWords {
        private:
                set<string> words;
                string nomArch;

        public:
                StopWords(string nombreArch);
                virtual ~StopWords();

                // Inicializa la clase e indica si se tuvo exito
                bool init();

                // Señala si la palabra indicada es o no una stop-word
                bool es_stopWord(string palabra) const;
};

#endif /* STOPWORDS_H_ */

