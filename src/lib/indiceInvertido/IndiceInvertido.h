/*
 * IndiceInvertido.h
 *
 *  Created on: 30/10/2013
 *      Author: pablo
 */

#ifndef INDICEINVERTIDO_H_
#define INDICEINVERTIDO_H_

#include "../FileBlock.h"
#include "../RegistroVariable.h"
#include "Vocabulario.h"
#include "TerminosXOrden.h"
#include "Ocurrencias.h"
#include "ListasInvertidas.h"
#include "StopWords.h"


using namespace std;

class IndiceInvertido{

public:

	IndiceInvertido();

	/**
	 * A partir del parametro "entidad" se abren los archivos necesarios para
	 * la indexacion. Se suponen creados.
	 * entidadCampo: nombre de la entidad + campo por el cual se indexa - Sirve para
	 * ubicar los archivos involucrados
	 */
	IndiceInvertido(string entidadCampo);
	virtual ~IndiceInvertido();

	Vocabulario* getVocabulario();

	void init(string entidadCampo);
	void indexar(unsigned int  idDoc,char* doc);
	//void update(RegistroGenerico* documento);

	/**
	 * Parsea la consulta y retorna la lista invertida resultante.
	 */
	vector<unsigned int> consultar(vector<string> consulta,ModoConsulta mode);

	int actualizarListaInvertida(unsigned int refList,unsigned int idTermino,unsigned int idDoc,unsigned int refDoc);

private:
	Vocabulario* vocabulario;
	Ocurrencias* ocurrencias;
	TerminosXOrden* terminosXOrden;
	ListasInvertidas* listasInvertidas;
	StopWords* stopwords;

	vector<string> parser(char* documento);
	int getIdTermino(string unTermino);
	void agregarOcurrencia(unsigned int idTermino,unsigned int idDocumento);
	vector<unsigned int> unionListas(vector<ListaInvertida*> listas);
	vector<unsigned int> interseccionListas(vector<ListaInvertida*> listas);
	bool existeDoc(vector<unsigned int> vec,unsigned int doc);

};

#endif /* INDICEINVERTIDO_H_ */
