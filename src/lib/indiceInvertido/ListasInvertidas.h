/*
 * ListasInvertidas.h
 *
 *  Created on: 07/11/2013
 *      Author: pablo
 */

#ifndef LISTASINVERTIDAS_H_
#define LISTASINVERTIDAS_H_

#include "../hashing/Hashing.h"
#include "../hashing/Bucket.h"
#include "../RegistroId.h"
#include "ListaInvertida.h"

class ListasInvertidas{

public:
	ListasInvertidas(const char* path);
	ListasInvertidas(const char* path,OpenMode mode);
	virtual ~ListasInvertidas();

	int agregarLista(ListaInvertida*);
	ListaInvertida* getLista(unsigned int idTermino);
	unsigned int getSize();
	int actualizarLista(unsigned int idTermino,unsigned int idDoc);

private:
	Hashing* hashing;
	vector<ListaInvertida*> listasInv;



};

#endif /* LISTASINVERTIDAS_H_ */
