#include "Ocurrencias.h"


Ocurrencias::Ocurrencias(const char* path){
	this->f = new FileBlock(path,BLOCK_SIZE_OCURRENCIAS);
	this->buffer = NULL;
	this->oc.clear();
}

Ocurrencias::Ocurrencias(const char* path,OpenMode mode){
	this->f = new FileBlock(path);
	this->buffer = this->f->getFirstBlock();
	this->oc.clear();
	this->hidratar();
}

Ocurrencias::~Ocurrencias(){
	if(buffer)
		delete buffer;
	delete f;
	this->oc.clear();
}

void Ocurrencias::addOcurrencia(int idTer,int idDoc){
	vector<int> aux;
	aux.push_back(idTer);
	aux.push_back(idDoc);
	this->oc.push_back(aux);
	this->escribirBuffer(idTer,idDoc);
}

vector<int> Ocurrencias::getOcurrencia(unsigned int i){
	return this->oc[i];
}

unsigned int Ocurrencias::getTotalOcurrencias(){
	return this->oc.size();
}

void Ocurrencias::hidratar(){
	Block* buffer;
	RegistroGenerico* reg;
	for(int i = 0; i < this->f->getTotalBlocks(); i++){
		buffer = this->f->readBlock(i);
		for( unsigned int j = 0; j < buffer->getTotalRecords();j++){
			reg = buffer->getRegistroGenerico(j);
			this->deserialize(reg);
			delete reg;
		}
		delete buffer;
	}
}

// Implementacion de metodos privados
void Ocurrencias::escribirBuffer(int idTer,int idDoc){
	unsigned int idOc = this->oc.size() + 1;
	char* dato = new char[sizeof(unsigned int)*3];
	char* iterator = dato;

	memcpy(iterator,&idTer,sizeof(idTer));
	iterator += sizeof(idTer);

	memcpy(iterator,&idDoc,sizeof(idDoc));
	iterator += sizeof(idDoc);

	RegistroGenerico* reg = new RegistroGenerico(idOc,dato);

	if (this->buffer->getFreeSpace() >= reg->getTamanio())
		this->buffer->setRegistroGenerico(reg);
	else{
		this->f->writeBlock(this->buffer);
		delete this->buffer;
		this->buffer = this->f->getNewBlock();
	}
}

void Ocurrencias::deserialize(RegistroGenerico* reg){
	vector<int> aux;
	int idTerm;
	int idDoc;
	char* iterator = reg->getDato();

	memcpy(&idTerm,iterator,sizeof(idTerm));
	iterator += sizeof(idTerm);

	memcpy(&idDoc,iterator,sizeof(idDoc));
	iterator += sizeof(idDoc);

	aux.push_back(idTerm);
	aux.push_back(idDoc);
	this->oc.push_back(aux);
}
