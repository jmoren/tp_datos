#include "IndexManager.h"

IndexManager::IndexManager(const char* path,string entidadCampo){
	this->index = new IndiceInvertido(entidadCampo);
	this->indicePrimario = new Hashing(path);

}

IndexManager::IndexManager(string entidadCampo){
	this->indicePrimario = NULL;
	this->index = new IndiceInvertido();
	this->index->init(entidadCampo);
}

IndexManager::~IndexManager(){
	if (this->indicePrimario)
		delete this->indicePrimario;
	if (this->index)
		delete index;
}


void IndexManager::indexar(unsigned int id,char* descripcion){
	string docAIndexar(descripcion);

	this->index->indexar(id,descripcion);
}

vector<RegistroId*> IndexManager::consultar(vector<string> consulta,ModoConsulta mode){
	vector<RegistroId*> result;
	vector<unsigned int > docs;
	RegistroId* rec;

	docs = this->index->consultar(consulta,mode);

	for (unsigned int i=0; i< docs.size();i++){

		rec = this->indicePrimario->getRegistro(docs[i]);
		if (rec){
			result.push_back(rec);
		}

	}
	return result;
}
