#include "ListaInvertida.h"

ListaInvertida::ListaInvertida(){
	this->idTermino = 0;
	this->documentos.clear();
}

ListaInvertida::ListaInvertida(unsigned int idTermino){
	this->idTermino = idTermino;
	this->documentos.clear();
}

ListaInvertida::~ListaInvertida(){
	this->documentos.clear();
}

void ListaInvertida::agregarDocumento(unsigned int idDoc){

	this->documentos.push_back(idDoc);
}

unsigned int ListaInvertida::getIdTermino(){
	return this->idTermino;
}

unsigned int ListaInvertida::getSize(){
	unsigned int size = 0;
	size = sizeof (size) + sizeof(this->idTermino) + sizeof(this->documentos.size()) +  this->documentos.size() * sizeof(unsigned int);
	return size;
}

vector<unsigned int> ListaInvertida::getDocumentos(){
	return this->documentos;
}

void ListaInvertida::serialize(RegistroId* regId){
	char* buffer = new char[this->getSize() - sizeof(unsigned int)*2];
	char* it = buffer;
	unsigned int cantDocs = this->documentos.size();

	regId->setSize(this->getSize());
	regId->setKey(this->getIdTermino());

	memcpy(it, &cantDocs, sizeof(cantDocs));
	it += sizeof(cantDocs);

	for (unsigned int i = 0; i < this->documentos.size(); i++){
		memcpy(it, &(this->documentos[i]), sizeof(unsigned int));
		it += sizeof(unsigned int);
	}
	regId->setDato(buffer);
}

void ListaInvertida::deserialize(RegistroId* regId){
	char *buffer;
	unsigned int cantDocs = 0;
	unsigned int auxDoc;

	this->idTermino = regId->getKey();
	buffer = regId->getDato();

	memcpy(&cantDocs, buffer, sizeof(cantDocs));
	buffer += sizeof(cantDocs);

	for (unsigned int i = 0; i < cantDocs; i++){
		memcpy(&(auxDoc),buffer, sizeof(auxDoc));
		buffer += sizeof(auxDoc);
		this->documentos.push_back(auxDoc);
	}
}
