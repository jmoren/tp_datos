/*
 * ListaInvertida.h
 *
 *  Created on: 30/10/2013
 *      Author: pablo
 */

#ifndef LISTAINVERTIDA_H_
#define LISTAINVERTIDA_H_

#include <cstring>
#include <vector>
#include "../RegistroId.h"

using namespace std;

class ListaInvertida{

public:
	ListaInvertida();
	ListaInvertida(unsigned int idTermino);
	virtual ~ListaInvertida();

	void agregarDocumento(unsigned int idDoc);
	unsigned int getIdTermino();
	unsigned int getSize();
	vector<unsigned int>getDocumentos();

	void serialize(RegistroId*);
	void deserialize(RegistroId*);

private:
	unsigned int idTermino;
	vector<unsigned int> documentos;
};

#endif /* LISTAINVERTIDA_H_ */
