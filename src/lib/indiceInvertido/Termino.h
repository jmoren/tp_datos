#ifndef TERMINO_H_
#define TERMINO_H_

#include "../RegistroVariable.h"

using namespace std;

class Termino : public RegistroVariable{

public:
	Termino();
	Termino(unsigned int id,string word);
	virtual ~Termino();

	string getWord();


private:

};


#endif /* TERMINO_H_ */
