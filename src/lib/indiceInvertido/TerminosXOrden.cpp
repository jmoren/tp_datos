#include "TerminosXOrden.h"


TerminosXOrden::TerminosXOrden(const char* path,OpenMode mode){
	this->f = new FileRecords(path,mode);
	this->terminos.clear();
	this->ultimoIdAsignado = 0;
	this->hidratar();
}

TerminosXOrden::TerminosXOrden(const char* path){
	this->f = new FileRecords(path);
	this->terminos.clear();
	this->ultimoIdAsignado = 0;
}

TerminosXOrden::~TerminosXOrden(){
	Termino* t;
	this->persistir();

	while(!this->terminos.empty()){
		t = this->terminos.back();
		this->terminos.pop_back();
		if(t)
			delete t;
	}

	if (this->f)
		delete f;
}

Termino* TerminosXOrden::getTermino(string word){
	Termino* terminoBuscado = NULL;
	bool found = false;
	unsigned int i = 0;

	//Busco Primero en el buffer
	list<Termino*>::iterator itBuf = this->bufTerminos.begin();
	while (!found && i < this->bufTerminos.size()){
		if ((*itBuf)->getWord().compare(word) == 0){
			terminoBuscado = (*itBuf);
			found = true;
		}
		itBuf++;
		i++;
	}
	list<Termino*>::iterator it = this->terminos.begin();
	while (!found && i < this->terminos.size()){
		if ((*it)->getWord().compare(word) == 0){
			terminoBuscado = (*it);
			found = true;
		}
		it++;
		i++;
	}
	return terminoBuscado;
}

void TerminosXOrden::newTermino(string word){
	Termino* newTermino;
	unsigned int id = this->ultimoIdAsignado + 1;
	this->ultimoIdAsignado++;
	newTermino = new Termino(id,word);
	this->bufTerminos.push_back(newTermino);
}

void TerminosXOrden::persistir(){

	list<Termino*>::iterator it = this->bufTerminos.begin();
	for (unsigned int i = 0;i < this->bufTerminos.size();i++){
		this->f->write(*it);
		delete (*it);
		it++;
	}
}

void TerminosXOrden::hidratar(){

	Termino* unTermino;
	RegistroVariable* regLeido;
	char* buffer;

	for (unsigned int i = 0; i < this->f->getTotalRecords();i++){
		regLeido = this->f->readNext();
		buffer = new char[regLeido->getTamanio()];
		regLeido->serialize(buffer);
		unTermino = new Termino();
		unTermino->deserialize(buffer);
		this->terminos.push_back(unTermino);
		if(unTermino->getId() > this->ultimoIdAsignado)
			this->ultimoIdAsignado = unTermino->getId();

		delete regLeido;
		delete[] buffer;
	}
}
