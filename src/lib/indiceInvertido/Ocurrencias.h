/*
 * Ocurrencias.h
 *
 *  Created on: 02/11/2013
 *      Author: pablo
 */

#ifndef OCURRENCIAS_H_
#define OCURRENCIAS_H_

#include <vector>
#include "../FileBlock.h"
#include "../Block.h"
#include "../Constantes.h"
#include "../RegistroGenerico.h"

class Ocurrencias{

public:
	Ocurrencias(const char* path);
	Ocurrencias(const char* path,OpenMode mode);
	virtual ~Ocurrencias();

	void addOcurrencia(int,int);
	vector<int> getOcurrencia(unsigned int i);
	unsigned int getTotalOcurrencias();

	string sort();
	void persistir();
	void hidratar();

private:
	vector<vector <int> > oc;
	FileBlock* f;
	Block* buffer;

	void escribirBuffer(int,int);
	void deserialize(RegistroGenerico* reg);
};

#endif /* OCURRENCIAS_H_ */
