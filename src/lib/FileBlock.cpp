#include "FileBlock.h"

// Constructor: crea el archivo e inicializa el header
FileBlock::FileBlock(const char *path, unsigned int block_size){
  try{
    this->file.open(path,ios::out | ios::binary);
  }catch(exception& e){
    throw new exception();
  }
  this->header = new BlockHeader(block_size);
  this->writeHeaderBlock();
}

// constructor: Abre el archivo y lee el header
FileBlock::FileBlock(const char *path){
  this->file.open(path,ios::in | ios::out | ios::binary);
  this->header = NULL;
  if (this->file.fail()){
    throw exception();
  }else
    this->readHeaderBlock();
}

// destructor
FileBlock::~FileBlock() {
  delete(this->header);
  if(this->file.is_open())
    this->file.close();
}

// Implementacion de metodos privados

void FileBlock::readHeaderBlock(){
  char* memBlock = new char[TAMANIO_HEADER];
  this->file.read(memBlock,TAMANIO_HEADER);
  this->header = new BlockHeader(memBlock);

  delete[] memBlock;
}

bool FileBlock::hasFreeBlock(){
  if (this->header->getFreeBlock() >= 0)
    return true;
  else
    return false;
}

void FileBlock::writeHeaderBlock(){
  unsigned int size = this->header->getSize();
  char *buffer = new char[size];
  memset(buffer, 0, size);

  this->header->serialize(buffer);
  this->file.seekg(0,ios::beg);
  this->file.write(buffer, size);

  delete[] buffer;
}

void FileBlock::addFreeBlock(Block *block){
  int block_number;
  int current_free_block;

  block_number = block->getNumber();
  current_free_block = this->header->getFreeBlock();
  block->cleanup();

  if( current_free_block == -1){
    this->header->setFreeBlock(block_number);
  }else{
    block->setNextFreeBlock(current_free_block);
    this->header->setFreeBlock(block_number);
  }

  // write updates
  this->writeBlock(block);
  this->writeHeaderBlock();
}


// Implementacion de metodos publicos

void FileBlock::writeBlock(Block* block){
  unsigned int pos;
  unsigned int block_size;
  unsigned int number;
  char* blockSerialized;

  number = block->getNumber();

  // get block size
  block_size = this->header->getBlockSize();
  blockSerialized = new char[block_size];
  memset(blockSerialized, 0, block_size);

  // get position and move there to write
  pos = TAMANIO_HEADER + (number - 1) * block_size;
  this->file.seekg(pos,ios::beg);

  // write block into file
  block->serialize(blockSerialized);
  this->file.write(blockSerialized, block_size);

  // update header if is new block
  if(block->getNumber() > this->header->getTotalBlocks()){
    this->header->addBlock();
  }

  this->writeHeaderBlock();
  delete[] blockSerialized;
}

int FileBlock::deleteBlock(unsigned int number){
  int res;
  Block *block = readBlock(number);
  addFreeBlock(block);

  if(this->header->getFreeBlock() == (int) block->getNumber())
    res = 0;
  else
    res = -1;

  delete(block);
  return res;
}

Block *FileBlock::readBlock(unsigned int number){
  unsigned int pos;
  unsigned int block_size;
  char *buffer;
  Block *block = new Block();

  // get block size
  block_size = this->header->getBlockSize();
  buffer = new char[block_size];

  // get position and move there to write
  pos = TAMANIO_HEADER + (number - 1) * block_size;
  this->file.seekg(pos,ios::beg);

  // read file
  this->file.read(buffer,block_size);
  block->deserialize(buffer);
	block->setStatus(STATUS_OCCU);

  delete[] buffer;

  return block;
}

Block *FileBlock::getNewBlock(){
  int total_blocks, free_block;
  Block *block;

  // get control data
  total_blocks = this->header->getTotalBlocks();
  free_block   = this->header->getFreeBlock();

  // get (new) block
  if (free_block ==  -1){
    block = new Block(total_blocks + 1,this->header->getBlockSize());
    block->setStatus(STATUS_OCCU);
  }else{
    block = readBlock(free_block);
    this->header->setFreeBlock(block->getNextFreeBlock());
    this->writeHeaderBlock();

    block->setNextFreeBlock(-1);
    block->setStatus(STATUS_OCCU);
  }

  return block;
}

Block *FileBlock::getFirstBlock(){
  int total_blocks;

  total_blocks = this->header->getTotalBlocks();
  if(total_blocks == 0){
    return getNewBlock();
  }else{
    return readBlock(1);
  }
}

int FileBlock::getTotalBlocks(){
  return this->header->getTotalBlocks();
}
