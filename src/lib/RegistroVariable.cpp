/*
 * RegistroVariable.cpp
 *
 *  Created on: 18/10/2013
 *      Author: pablo
 */
#include "RegistroVariable.h"

RegistroVariable::RegistroVariable(){
	this->id = 0;
	this->dato = NULL;
}

RegistroVariable::RegistroVariable(unsigned int key,unsigned int size,char* dato)
	: RegistroGenerico(size,dato)
{
	this->id = key;
}

RegistroVariable::~RegistroVariable(){
}

unsigned int RegistroVariable::getId(){
	return this->id;
}

void RegistroVariable::serialize(char* registro){
	int size = this->tamanio  - sizeof(this->id) - sizeof(this->tamanio);
	char* iterator = registro;

	memcpy(iterator, &(this->id), sizeof(this->id));
	iterator += sizeof(this->id);

	memcpy(iterator, &(this->tamanio), sizeof(this->tamanio));
	iterator += sizeof(this->tamanio);

	memcpy(iterator, this->dato, size);
	iterator += size;

}


void RegistroVariable::deserialize(char* registro){
	char *pos = registro;
	int size;

	memcpy(&(this->id), pos, sizeof(this->id));
	pos += sizeof(this->id);

	memcpy(&(this->tamanio), pos, sizeof(this->tamanio));
	pos += sizeof(this->tamanio);

	size = this->tamanio  - sizeof(this->id) - sizeof(this->tamanio);
	this->dato = new char[size];

	memcpy(this->dato, pos, size);
	pos += size;

}



