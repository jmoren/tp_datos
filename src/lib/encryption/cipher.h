#ifndef _CIPHER_H_
#define _CIPHER_H_

#include "../Constantes.h"
#include "../../utils/Funciones.h"

class Cipher{
  public:
    string key;
    string alphabet;
    int det_inv;
    int space;

    Cipher(string key);
    virtual ~Cipher();

    int euclides(int q, int x1, int x2, int b, int y1, int y2,int a);

    string encrypt_block(string block);
    string decrypt_block(string block);

    int getCode(char symbol);
    int *getMatrix();
    int *getInverse();
    string parser(string text_plain);

    int getDeterminant(int matrix[]);

    bool validateKey();
};

#endif



