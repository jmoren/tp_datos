#include "cipher.h"

Cipher::Cipher(string key){
  this->space    = 41;
  this->det_inv  = 0;
  this->key      = key;
  this->alphabet = "_ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.?,-";
}

Cipher::~Cipher(){}

int Cipher::euclides(int q, int x1, int x2, int x3, int y1, int y2,int y3){
  int x1_swp, x2_swp, x3_swp;
  bool next_step =  true;

  while(next_step){
    x1_swp = x1;
    x2_swp = x2;
    x3_swp = x3;

    q  = (int) (x3/y3);
    x1 = y1;
    x2 = y2;
    x3 = y3;
    y1 = x1_swp - (q*x1);
    y2 = x2_swp - (q*x2);
    y3 = x3_swp - (q*x3);

    if((y3 == 1) || (y3 == 0 )){
      next_step = false;
    }
  }

  if(y3 == 1)
    if(y2 < 0)
      return (y2 + this->space);
    else
      return y2;
  else
    return 0;
}

int Cipher::getCode(char symbol){
	int symbol_code = -1;
	bool is_present;
	int pos;

	is_present = false;
	pos  = 0;
	while(!is_present && pos < 41){
	  if(this->alphabet[pos] == symbol){
		symbol_code = pos;
		is_present = true;
	  }
	  pos += 1;
	}
  return symbol_code;
}

int *Cipher::getMatrix(){
  int *matrix = new int[9];
  bool is_present;
  int pos;

  for (int i = 0; i < 9; i++) {
    is_present = false;
    pos  = 0;
    while(!is_present && pos < 41){
      if(this->alphabet[pos] == toupper(this->key[i])){
        matrix[i] = pos;
        is_present = true;
      }
      pos += 1;
    }
  }

  return matrix;
}

int *Cipher::getInverse(){
  int *adj    = new int[9];
  int *inv    = new int[9];
  int inv_det = 0;
  int *matrix;

  matrix  = this->getMatrix(); // leemos la key (puede ser la general o la del usuario)
  inv_det = this->getDeterminant(matrix);

  if(inv_det != -1){
    adj[0] = (matrix[4]*matrix[8] - matrix[5]*matrix[7]) % this->space; // 0
    adj[1] = (matrix[1]*matrix[8] - matrix[2]*matrix[7]) % this->space; // 3
    adj[2] = (matrix[1]*matrix[5] - matrix[2]*matrix[4]) % this->space; // 6

    adj[3] = (matrix[3]*matrix[8] - matrix[5]*matrix[6]) % this->space; // 1
    adj[4] = (matrix[0]*matrix[8] - matrix[2]*matrix[6]) % this->space; // 4
    adj[5] = (matrix[0]*matrix[5] - matrix[2]*matrix[3]) % this->space; // 7

    adj[6] = (matrix[3]*matrix[7] - matrix[4]*matrix[6]) % this->space; // 2
    adj[7] = (matrix[0]*matrix[7] - matrix[1]*matrix[6]) % this->space; // 5
    adj[8] = (matrix[0]*matrix[4] - matrix[1]*matrix[3]) % this->space; // 8

    for(int i = 0; i < 9; i++) {
      if(i%2 == 0)
        inv[i] = ( adj[i] * inv_det ) % this->space;
      else{
        inv[i] = ( (-1) * adj[i] * inv_det ) % this->space;
      }

    }
  }

  delete[] adj;
  delete[] matrix;

  return inv;
}

string Cipher::parser(string plain_text){
	unsigned int i = 0;
	int res_aux = 0;
	string text_parsed;

	while (i < plain_text.length()){
		if (plain_text[i] == ' ')
			text_parsed += '_';
		else {
			res_aux = getCode(toupper(plain_text[i]));
			if(res_aux >= 0)
				text_parsed += plain_text[i];
		}
		i++;
	}
	return text_parsed;
}

int Cipher::getDeterminant(int matrix[]){
  int det_1, det_2, det;
  int inv_det = 0;

  det_1 = matrix[0]*matrix[4]*matrix[8] + matrix[1]*matrix[5]*matrix[6] + matrix[2]*matrix[3]*matrix[7];
  det_2 = matrix[2]*matrix[4]*matrix[6] + matrix[5]*matrix[7]*matrix[0] + matrix[8]*matrix[1]*matrix[3];
  det   = (det_1 - det_2);


  if(det < 0){
    det = (det % this->space) + this->space;
  }else{
    det = det % this->space;
  }
  if(det != 0){
    inv_det = this->euclides(0, 1, 0, this->space, 0, 1, det);

    if(inv_det == 0){
      inv_det = -1; // no tiene inversa en este espacio finito
    }

  }else{
    inv_det = -1; // la matriz no es inversible
  }

  return inv_det;
}

string Cipher::encrypt_block(string plain_text){
  string parsed_text = "";
  string string_encoded = "";
  int *result = new int[3];
  int *matrix;
  unsigned int j = 0;
  unsigned int i = 0;
  int padding_size;
  // un parseador
  // minusculas a mayusculas y buscar en el alfabeto
  // si es " " -> reemplazamos por 0 (_),
  // si no esta, se pasa de largo
  int *code   = new int[3];

  matrix = this->getMatrix();    // leemos la key (puede ser la general o del usuario)

  //Parser
  parsed_text = this->parser(plain_text);

  // padding
  if (parsed_text.length()%3 > 0){
	padding_size = 3 - parsed_text.length()%3;
	for (int h = 0; h < padding_size; h++){
	  parsed_text += '_';
	}
  }
  i = 0;
  while (i < parsed_text.length()){
	  j = 0;
	  while(j < 3 && i < parsed_text.length()){
		  code[j] = getCode(toupper(parsed_text[i]));
	  	  j++;
	  	  i++;
	  }
	  result[0] = (matrix[0]*code[0]+matrix[1]*code[1]+matrix[2]*code[2]) % this->space;
	  result[1] = (matrix[3]*code[0]+matrix[4]*code[1]+matrix[5]*code[2]) % this->space;
	  result[2] = (matrix[6]*code[0]+matrix[7]*code[1]+matrix[8]*code[2]) % this->space;

	  result[0] = result[0] < 0 ? result[0] + this->space : result[0];
	  result[1] = result[1] < 0 ? result[1] + this->space : result[1];
	  result[2] = result[2] < 0 ? result[2] + this->space : result[2];

	  for (int i = 0; i < 3; i++) {
		string_encoded += this->alphabet[result[i]];
	  }
  }

  delete[] result;
  delete[] matrix;
  delete[] code;

  return string_encoded;
}

string Cipher::decrypt_block(string encoded_text){
  string string_decoded = "";
  int *result  = new int[3];
  int *code    = new int[3];
  int *inverse;
  unsigned int i = 0;
  unsigned int j = 0;
  int res_aux;
  char symbol;

  inverse = this->getInverse();

  i = 0;
  while (i < encoded_text.length()){
	  j = 0;
	  while(j < 3 &&  i < encoded_text.length()){

		  res_aux = getCode(toupper(encoded_text[i]));
		  if(res_aux >= 0)
			  code[j] = res_aux;
	  	  j++;
	  	  i++;
	  }

	  result[0] = (inverse[0]*code[0]+inverse[1]*code[1]+inverse[2]*code[2]) % this->space;
	  result[1] = (inverse[3]*code[0]+inverse[4]*code[1]+inverse[5]*code[2]) % this->space;
	  result[2] = (inverse[6]*code[0]+inverse[7]*code[1]+inverse[8]*code[2]) % this->space;

	  result[0] = result[0] < 0 ? result[0] + this->space : result[0];
	  result[1] = result[1] < 0 ? result[1] + this->space : result[1];
	  result[2] = result[2] < 0 ? result[2] + this->space : result[2];

	  for ( int i = 0; i < 3; i++) {
		  symbol = this->alphabet[result[i]];
		  if (symbol == '_')
			  string_decoded += ' ';
		  else
			  string_decoded += tolower(symbol);
	  }
  }

  delete[] result;
  delete[] inverse;
  delete[] code;
  return string_decoded;
}

bool Cipher::validateKey(){
  int *matrix;
  int inv_det = 0, pos = 0;
  bool valid = false, is_present = false;
  int total_codes = 0;

  for (int i = 0; i < 9; i++) {
    is_present = false;
    pos  = 0;
    while(!is_present && pos < 41){
      if(this->alphabet[pos] == toupper(this->key[i])){
        total_codes += 1;
        is_present = true;
      }
      pos += 1;
    }
  }

  matrix  = this->getMatrix(); // leemos la key (puede ser la general o la del usuario)
  inv_det = this->getDeterminant(matrix);

  if((inv_det > 0) && (total_codes == 9)){
    valid = true;
  }

  delete[] matrix;
  return valid;
}
