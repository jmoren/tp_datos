/*
 * FreeSpaceTable.cpp
 *
 *  Created on: 18/10/2013
 *      Author: pablo
 */
#include "FreeSpaceTable.h"

FreeSpaceTable::FreeSpaceTable(const char* path){
	this->file.open(path,ios::out | ios::binary);
	if(!this->file)
		throw exception();
	else{
		this->totalEntries = 0;
		writeTable();
	}
}

FreeSpaceTable::FreeSpaceTable(const char* path,OpenMode mode){

	switch (mode){
	case rw: this->file.open(path,ios::in | ios::out | ios::binary);
		break;

	default: this->file.open(path,ios::in | ios::binary);
		break;
	}
	if(!this->file)
		throw exception();
	else
		readTable();
}


FreeSpaceTable::~FreeSpaceTable(){

	this->freeSpaceTable.clear();
	if (this->file.is_open()){
		this->writeTable();
		this->file.close();
	}
}

void FreeSpaceTable::addFreeSpace(unsigned int pos,unsigned int size){
	vector<int> auxRow;
	auxRow.push_back(pos);
	auxRow.push_back(size);
	this->freeSpaceTable.push_back(auxRow);
	this->totalEntries ++;
}

vector<int> FreeSpaceTable::getFreeSpace(int size){
	vector<int> freeSpace;

	for (unsigned int i = 0; i < this->freeSpaceTable.size();i++){
		if(this->freeSpaceTable[i][1] > size){
			freeSpace = freeSpaceTable[i];
			freeSpaceTable.erase(freeSpaceTable.begin() + i);
			this->totalEntries --;
			return freeSpace;
		}
	}
	// Todo: Agregar mejora para que devuelva el mas optimo
	freeSpace.push_back(-1);
	return freeSpace;
}

vector<int> FreeSpaceTable::getFreePos(unsigned int pos){
	vector<int> freeSpace;

	for (unsigned int i = 0; i < this->freeSpaceTable.size();i++){
		if(this->freeSpaceTable[i][0] == (int) pos){
			freeSpace = freeSpaceTable[i];

			return freeSpace;
		}
	}
	freeSpace.push_back(-1);
	return freeSpace;
}

unsigned int FreeSpaceTable::getTotalEntries(){
	return this->freeSpaceTable.size();
}

bool FreeSpaceTable::isFree(int pos){
	for (unsigned int i = 0; i < this->freeSpaceTable.size();i++){
		if (freeSpaceTable[i][0] == pos)
			return true;
	}
	return false;
}

void FreeSpaceTable::close(){
	if (this->file.is_open()){
		this->writeTable();
		this->file.close();
	}
}

void FreeSpaceTable::serialize(char* memBlock){

	char* currentPos = memBlock;
	unsigned int pos;
	unsigned int freeSpace;

	memcpy(currentPos,&this->totalEntries,sizeof(this->totalEntries));
	currentPos += sizeof(this->totalEntries);

	for (unsigned int i = 0; i < this->totalEntries; i++){
		pos = this->freeSpaceTable[i][0];
		freeSpace = this->freeSpaceTable[i][1];

		memcpy(currentPos,&pos,sizeof(pos));
		currentPos += sizeof(pos);

		memcpy(currentPos,&freeSpace,sizeof(freeSpace));
		currentPos += sizeof(freeSpace);
	}
}

void FreeSpaceTable::deserialize(char* memBlock){

	char* currentPos = memBlock;
	unsigned int pos;
	unsigned int freeSpace;
	vector <int> aux;

	for (unsigned int i = 0; i < this->totalEntries; i++){
		memcpy(&pos,currentPos,sizeof(pos));
		currentPos += sizeof(pos);

		memcpy(&freeSpace,currentPos,sizeof(freeSpace));
		currentPos += sizeof(freeSpace);

		aux.push_back(pos);
		aux.push_back(freeSpace);
		this->freeSpaceTable.push_back(aux);
		aux.clear();
	}
}

// Implementacion de metodos privados

void FreeSpaceTable::readTable(){
	char* buffer;
	unsigned int size;

	// leo total de registros de la tabla
	try{
		this->file.read((char*)&this->totalEntries,sizeof(this->totalEntries));
	}catch (exception& e){
		throw exception(); //Todo: Definir excepcion
	}
	// leo tabla
	size = this->totalEntries * sizeof(unsigned int ) * 2;
	buffer = new char[size];
	this->file.read(buffer,size);
	this->deserialize(buffer);
  delete[] buffer;
}

void FreeSpaceTable::writeTable(){

	char* buffer;
	unsigned int size = sizeof(this->totalEntries) + this->totalEntries * sizeof(unsigned int) * 2;

	buffer = new char[size];
	this->serialize(buffer);
	this->file.seekp(0,ios::beg);
	this->file.write(buffer,size);

	delete[] buffer;
}
