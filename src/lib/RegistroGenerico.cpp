#include "RegistroGenerico.h"

RegistroGenerico::RegistroGenerico(){
  this->tamanio = 0;
  this->dato    = NULL;
}

RegistroGenerico::RegistroGenerico(unsigned int size, char* dato){
  this->tamanio = size;
  this->dato = dato;
}

RegistroGenerico::~RegistroGenerico(){
  if(this->dato)
    delete[] this->dato;
}

void RegistroGenerico::setTamanio(unsigned int tam){
  this->tamanio = tam;
}
unsigned int RegistroGenerico::getTamanio(){
  return this->tamanio;
}

char* RegistroGenerico::getDato(){
  return this->dato;
}

void RegistroGenerico::setDato(char* dato){
  this->dato = dato;
}

void RegistroGenerico::clone(RegistroGenerico *copy){
  copy->tamanio = this->tamanio;
  copy->dato = new char[this->tamanio]();
  memcpy(copy->dato, this->dato, this->tamanio - sizeof(int));
}

void RegistroGenerico::serialize(char* registro){
  int size = this->tamanio  - sizeof(this->tamanio);
  memcpy(registro, &(this->tamanio), sizeof(this->tamanio));
  registro += sizeof(this->tamanio);
  memcpy(registro, this->dato, size);
}

void RegistroGenerico::deserialize(char* dato){
  char *pos = dato;
  int size;

  memcpy(&(this->tamanio), pos, sizeof(this->tamanio));
  pos += sizeof(this->tamanio);
  size = this->tamanio  - sizeof(this->tamanio);
  this->dato = new char[size]();

  memcpy(this->dato, pos, size);
  pos += size;
}
