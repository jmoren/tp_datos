#include "Block.h"

Block::Block(){
  this->number        = 0;
  this->blockSize     = 0;
  this->totalRecords  = 0;
  this->freeSpace     = 0;
  this->status        = STATUS_OCCU;
  this->nextFreeBlock = 0;
}

/* Crea un nuevo bloque con parametros:
** number: numero de bloque dentro del archivo (determina su posicion en el)
** size: tamanio del bloque en bytes
*/
Block::Block(unsigned int number,unsigned int size) {
  this->number        = number;
  this->blockSize     = size;
  this->totalRecords  = 0;
  this->freeSpace     = this->blockSize - sizeof(int) * 6;
  this->status        = STATUS_OCCU;
  this->nextFreeBlock = -1;
}

/* Free Bloque:
** Libera la memoria de un bloque, y los registros contenidos en el
*/
Block::~Block() {
  while (!this->records.empty()){
    RegistroGenerico *reg = this->records.back();
    this->records.pop_back();
    if(reg)
      delete(reg);
  }
}

void Block::cleanup(){
  this->totalRecords = 0;
  this->freeSpace    = this->blockSize - sizeof(int) * 5;
  this->status       = STATUS_FREE;

  while (!this->records.empty()){
    RegistroGenerico *reg = this->records.back();
    delete(reg);
    this->records.pop_back();
  }
}

unsigned int Block::getNumber(){
  return this->number;
}

unsigned int Block::getBlockSize(){
  return this->blockSize;
}

unsigned int Block::getFreeSpace(){
  return this->freeSpace;
}

unsigned int Block::getTotalRecords(){
  return this->totalRecords;
}

unsigned int Block::getStatus(){
  return this->status;
}

int Block::getNextFreeBlock(){
  return this->nextFreeBlock;
}

void Block::setNumber(unsigned int number){
  this->number = number;
}

void Block::setNextFreeBlock(int number){
  this->nextFreeBlock = number;
}

void Block::setStatus(unsigned int status){
  this->status = status;
}

bool Block::isEmpty(){
  return this->records.empty();
}

/* setRegistroGenerico:
** Agrega un registro nuevo al vector de registros en memoria.
** Tambien actualiza el header del bloque.
** Si no hay espacio para guardar, devuelve ERROR_SAVE_RECORD
** Si pudo guardar, devuelve SUCCESS_SAVE_RECORD
*/
int Block::setRegistroGenerico(RegistroGenerico* record){
  if(record->getTamanio() < this->getFreeSpace()){
    this->records.push_back(record);
    this->freeSpace -= record->getTamanio();
    this->totalRecords++;
    return SUCCESS_SAVE_RECORD;
  }else
    return ERROR_SAVE_RECORD;
}

/* getRegistroGenerico:
** Recibe como parametro el numero de registro a leer
** Devuelve un registro si la posicion esta dentro del rango
** si no, devuelve null
*/
RegistroGenerico * Block::getRegistroGenerico(unsigned int pos){
  if (pos < this->records.size()){
    return this->records.at(pos);
  }
  else{
    return NULL;
  }
}

/* getRegistroGenerico:
** Recibe como parametro el numero de registro a eliminar
** Devuelve 0 si el registro pudo eliminarse (la posicion esta dentro del rango)
** o devuelve ERROR_DELETE_RECORD si no se pudo leer
*/
int Block::eliminarRegistroGenerico(unsigned int pos){
  RegistroGenerico *reg;
  int tam;

  if ((pos) < this->records.size()){
    reg = this->getRegistroGenerico(pos);
    tam = reg->getTamanio();
    delete(reg);

    this->freeSpace += tam;
    this->records.erase(this->records.begin() + pos);
    this->totalRecords--;
    return SUCCESS_DELETE_RECORD;
  }else
    return ERROR_DELETE_RECORD;
}

/* Serializacion de bloque
** Parametros: buffer del tamanio del bloque.
** Convierte el bloque en una tira de bytes para ser guardado en el archivo
** Se serializa el header. Tambien se serializa cada registro
*/
void Block::serialize(char* memBlock){
  char *buffer;
  unsigned int tam;

  memcpy(memBlock, &(this->number), sizeof(this->number));
  memBlock += sizeof(this->number);

  memcpy(memBlock, &(this->blockSize), sizeof(this->blockSize));
  memBlock += sizeof(this->blockSize);

  memcpy(memBlock, &(this->totalRecords), sizeof(this->totalRecords));
  memBlock += sizeof(this->totalRecords);

  memcpy(memBlock, &(this->freeSpace), sizeof(this->freeSpace));
  memBlock += sizeof(this->freeSpace);

  memcpy(memBlock, &(this->status), sizeof(this->status));
  memBlock += sizeof(this->status);

  memcpy(memBlock, &(this->nextFreeBlock), sizeof(this->nextFreeBlock));
  memBlock += sizeof(this->nextFreeBlock);

  for(unsigned int i = 0;i < this->records.size();i++){
    RegistroGenerico *rec = this->getRegistroGenerico(i);
    tam = rec->getTamanio();
    buffer = new char[tam];
    this->records[i]->serialize(buffer);
    memcpy(memBlock,buffer, tam);
    memBlock += tam;
    delete[] buffer;
  }
}

/* Deserializacion de bloque
** Parametros: un buffer del tamanio de un bloque
** Lee una tira de bytes leida de un archivo para guardar la informacion en un bloque
** Se serializa el header. Tambien se serializa cada registro
*/
void Block::deserialize(char* memBlock){
  char *offset = memBlock;

  memcpy(&(this->number), offset, sizeof(this->number));
  offset += sizeof(this->number);

  memcpy(&(this->blockSize), offset, sizeof(this->blockSize));
  offset += sizeof(this->blockSize);

  memcpy(&(this->totalRecords), offset, sizeof(this->totalRecords));
  offset += sizeof(this->totalRecords);

  memcpy(&(this->freeSpace), offset, sizeof(this->freeSpace));
  offset += sizeof(this->freeSpace);

  memcpy(&(this->status), offset, sizeof(this->status));
  offset += sizeof(this->status);

  memcpy(&(this->nextFreeBlock), offset, sizeof(this->nextFreeBlock));
  offset += sizeof(this->nextFreeBlock);

  for (unsigned int i = 0; i < this->totalRecords;i++){
    RegistroGenerico *rec = new RegistroGenerico();
    rec->deserialize(offset);
    this->records.push_back(rec);
    offset += rec->getTamanio();
  }
}
