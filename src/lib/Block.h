#ifndef __BLOCK__H
#define __BLOCK__H

#include "Constantes.h"
#include "RegistroGenerico.h"

class Block{

  private:
    unsigned int number;
    unsigned int blockSize;
    unsigned int freeSpace;
    unsigned int totalRecords;
    unsigned int status;
    int nextFreeBlock;
    vector<RegistroGenerico*> records;

  public:

    Block();
    Block(unsigned int number,unsigned int size);
    virtual ~Block();

    // getters and setters
    unsigned int getNumber();
    unsigned int getBlockSize();
    unsigned int getTotalRecords();
    unsigned int getFreeSpace();
    unsigned int getStatus();

    void setStatus(unsigned int status);
    void setNumber(unsigned int id);

    // next block
    int  getNextFreeBlock();
    void setNextFreeBlock(int number);

    // has records?
    bool isEmpty();
    void cleanup();

    // manage records
    RegistroGenerico * getRegistroGenerico(unsigned int pos);
    int setRegistroGenerico(RegistroGenerico* record);
    int eliminarRegistroGenerico(unsigned int pos);

    // convert and revert into array of bytes
    void serialize(char*);
    void deserialize(char*);
};

#endif
