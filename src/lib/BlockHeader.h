#ifndef BlockHeader_H_
#define BlockHeader_H_

#include "Constantes.h"

class BlockHeader{

public:
  BlockHeader(unsigned int tamBloqueReg);
  BlockHeader(char*);
  virtual ~BlockHeader();

  unsigned int getTotalBlocks();
  unsigned int getBlockSize();
  unsigned int getSize();

  unsigned int getLastRecordId();
  void setLastRecordId(int id);

  int getFreeBlock();
  void setFreeBlock(int number);

  void addBlock();
  void removeBlock();

  void serialize(char *);
  void deserialize(char*);

private:
  unsigned int blockSize;
  unsigned int totalBlocks;
  unsigned int lastRecordId;
  int freeBlock;
};

#endif /* BlockHeader_H_ */
