#ifndef FILEBLOCK_H_
#define FILEBLOCK_H_


#include "Constantes.h"
#include "BlockHeader.h"
#include "Block.h"

class FileBlock {

private:
  void readHeaderBlock();

  bool hasFreeBlock();
	void addFreeBlock(Block *block);

public:
  fstream file;
  BlockHeader *header;
  FileBlock(const char* path, unsigned int sizeBlock);
  FileBlock(const char *path);
  virtual ~FileBlock();
  int getTotalBlocks();
  void writeHeaderBlock();
  void writeBlock(Block* block);
  int deleteBlock(unsigned int number);
  Block *readBlock(unsigned int number);
  Block *getNewBlock();
  Block *getFirstBlock();
};

#endif /* FILEBLOCK_H_ */
