#ifndef REGISTROHAHING_H_
#define REGISTROHAHING_H_

#include "Constantes.h"
#include "RegistroGenerico.h"

class RegistroId {
  private:
    int size;
    int key;
    char *dato;

  public:
    RegistroId();
    virtual ~RegistroId();

    int  getSize();
    int  getKey();
    char *getDato();

    void setKey(int key);
    void setDato(char *dato);
    void setSize(int size);

    void clone(RegistroId *copy);
    void serialize(RegistroGenerico *registro);
    void deserialize(RegistroGenerico *registro);
};

#endif
