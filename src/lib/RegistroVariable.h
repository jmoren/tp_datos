#ifndef REGISTROVARIABLE_H_
#define REGISTROVARIABLE_H_

#include <cstring>
#include "RegistroGenerico.h"

class RegistroVariable : public RegistroGenerico{

public:
  RegistroVariable();
  RegistroVariable(unsigned int key,unsigned int size, char* dato);
  virtual ~RegistroVariable();

  unsigned int getId();

  void serialize(char*);
  void deserialize(char*);

protected:
  unsigned int id;
};


#endif
