#include "../../utils/Funciones.h"
#include "../../utils/validations.h"
#include "message_manager.h"

MessageManager::MessageManager(int size){
  this->path  = new char[15];
  strcpy(this->path,"message_db.bin");
  this->hashing          = new Hashing(this->path, size);
  this->indice_por_servicio = NULL;
  this->indice_por_fecha_hora_servicio = NULL;
  this->user_manager        = NULL;
  this->current_user        = NULL;
  this->indexManager        = new IndexManager("consultasTexto");
}

MessageManager::MessageManager(){
  this->path  = new char[15];
  strcpy(this->path,"message_db.bin");
  this->hashing             = new Hashing(this->path);
  this->user_manager        = NULL;
  this->indice_por_servicio = NULL;
  this->indice_por_fecha_hora_servicio = NULL;
  this->current_user        = NULL;
  this->indexManager        = new IndexManager(path,"consultasTexto");
}

MessageManager::MessageManager(User *user){
  this->path  = new char[15];
  strcpy(this->path,"message_db.bin");
  this->hashing             = new Hashing(this->path);
  this->user_manager        = new UserManager();
  this->indice_por_servicio = new Arbol(IDServicioIDUsuarioIDConsulta);
  this->indice_por_fecha_hora_servicio = new Arbol(IDServicioFechaCHoraCIDConsulta);
  this->current_user        = user;
  this->indexManager        = new IndexManager(path,"consultasTexto");
}

MessageManager::~MessageManager(){
  delete[] this->path;
  delete this->hashing;
  if(this->user_manager)
    delete this->user_manager;
  if(this->indice_por_servicio)
    delete this->indice_por_servicio;
  if(this->indice_por_fecha_hora_servicio)
    delete this->indice_por_fecha_hora_servicio;
  if(this->indexManager)
    delete this->indexManager;

}

void MessageManager::addMessage(int user_id, int service_id){
  Message *message;
  RegistroId *registro;
  string temp;
  char *new_body;
  char *new_response;
  char *message_content;
  int  message_size, size;
  int  res = 0;
  bool body_valid = false;

  message           = new Message(user_id, service_id);
  registro          = new RegistroId();
  message->id       = this->hashing->getNextId();
  message->date     = time(0);

  cout << endl << "   Fecha: ";
  Funciones::printHourTime(message->date);

  cout << " - Usuario: " << message->user_id << endl;
  cout << "   --------------------------------------------" << endl;

  cout << "\033[1;34m   Consulta:\033[0m\n\n   ";
  while(!body_valid){
    getline(cin, temp);
    size = temp.length() + 1 + sizeof(int);
    new_body = new char[temp.length() + 1];
    strcpy(new_body, temp.c_str());
    message->body  = new RegistroGenerico(size, new_body);

    if(!message->validateBody()){
      delete[] new_body;
      body_valid = false;
      cout << "\033[0;31m Consulta demasiada larga...\n\033[1;34m ** Ingrese nombre otra vez:\033[0m    " << endl;
    }else
      body_valid = true;
  }
  cout << endl;

  // guardamos el respuesta vacia.
  size              = sizeof(int);
  new_response      = new char[1]();
  message->response = new RegistroGenerico(size, new_response);

  message_size    = message->getSize();
  message_content = new char[message_size + sizeof(int)]();
  memset(message_content, 0, message_size);
  message->serialize(message_content);

  registro->setSize(message_size + sizeof(int)*2); // datos, key, size
  registro->setKey(message->id);
  registro->setDato(message_content);

  res = this->hashing->addRegistro(registro);
  switch(res){
    case SUCCESS_SAVE_RECORD:
      cout << "\033[1;32m Consulta enviada...\033[0m" << endl;
      this->add_to_index_by_service(message);
      this->add_to_index_by_fecha_hora(message);
      this->indexManager->indexar(message->id,message->getBody());
      res = 0;
      break;
    case ERROR_SAVE_RECORD:
      cout << "\033[1;31m No fue posible realizar la consulta\033[0m" << endl;
      res = ERROR_SAVE_RECORD;
      message = NULL;
      break;
    case INVALID_RECORD:
      res = INVALID_RECORD;
      delete(registro);
      delete(message);
      message = NULL;
      break;
  }

  if(message)
    delete(message);
}

Message *MessageManager::findMessage(int id){
  RegistroId *registro;
  Message *message;

  registro = this->hashing->getRegistro(id);
  if(registro){
    message = new Message(registro->getKey(), registro->getDato());
    delete(registro);
  }else{
    cout << "\033[1;31m Mensaje no encontrado\033[0m" << endl;
    message = NULL;
  }

  return message;
}

void MessageManager::respondMessage(int id){
  Message *message;
  bool response_valid = false;
  char *new_response;
  int size;
  string temp;

  message = this->findMessage(id);
  if(message){
    message->date_response = time(0);
    delete message->response;

    cout << endl << "   Fecha: ";
    Funciones::printHourTime(message->date);

    cout << "\033[1;34m   Respuesta:\033[0m\n\n   ";
    while(!response_valid){
      getline(cin, temp);
      size = temp.length() + 1 + sizeof(int);
      new_response = new char[temp.length() + 1];
      strcpy(new_response, temp.c_str());
      message->response  = new RegistroGenerico(size, new_response);

      if(!message->validateResponse()){
        delete[] new_response;
        response_valid = false;
        cout << "\033[0;31m Respuesta demasiada larga...\n\033[1;34m ** Ingrese respuesta otra vez:\033[0m    " << endl;
      }else
        response_valid = true;
    }
    cout << endl;

    this->updateMessage(message);
  }
}

void MessageManager::updateMessage(Message *message){
  RegistroId *registro;
  char *message_content;
  int message_size, res;

  registro = new RegistroId();

  message_size    = message->getSize();
  message_content = new char[message_size + sizeof(int)]();
  message->serialize(message_content);

  registro->setSize(message_size + sizeof(int)*2); // datos, key, size
  registro->setKey(message->id);
  registro->setDato(message_content);

  res = this->hashing->updateRegistro(registro);
  switch(res){
    case SUCCESS_SAVE_RECORD:
      cout << "\033[1;32m   Consulta actualizada...\033[0m" << endl;
      res = SUCCESS_SAVE_RECORD;
      break;
    case ERROR_SAVE_RECORD:
      cout << "\033[1;31m   Fallo... Error al guardar y no se modifico la consulta\033[0m" << endl;
      res = ERROR_SAVE_RECORD;
      delete(registro);
      break;
    case INVALID_RECORD:
      res = INVALID_RECORD;
      delete(registro);
      break;
  }

  if(message)
    delete(message);

}

void MessageManager::deleteMessage(int id){

  Message *message = this->findMessage(id);
  if(message){
    ClaveRegIDServIDUsIDCons *claveSvUs = new ClaveRegIDServIDUsIDCons(message->service_id,message->user_id,message->id);

    char *date_old = Funciones::dateToString(message->date);
    char *hour_old = Funciones::hourToString(message->date);

    ClaveRegIDServFCHCIDCons *clave_FH = new ClaveRegIDServFCHCIDCons(message->service_id,date_old,hour_old,message->id);

    this->indice_por_fecha_hora_servicio->eliminar(clave_FH);
    this->indice_por_servicio->eliminar(claveSvUs);

    this->hashing->deleteRegistro(id);

    delete[] date_old;
    delete[] hour_old;
    delete(claveSvUs);
    delete(clave_FH);
    delete(message);
  }
}

void MessageManager::hideUnhideMessage(int id){
  Message *message;

  message = this->findMessage(id);
  if(message){
    message->is_hidden = !message->is_hidden;
    this->updateMessage(message);
  }
}

void MessageManager::printMessage(Message *message, bool full_info){
  User *user = this->user_manager->findUser(message->user_id);

  cout << endl;
  cout << "   " << message->id << ") ";
  cout << "\033[1;37m";
  if(user)
    cout << user->getName();
  else
    cout << message->user_id;
  cout << " \033[1;34m pregunto el \033[0m";
  Funciones::printHourTime(message->date);
  cout << " hs" << endl;

  cout << endl;
  cout << "   * " << message->getBody() << endl << endl;

  if(message->response->getTamanio() > 5){
    cout << "\033[0;35m   El proveedor respondio el\033[0m ";
    Funciones::printHourTime(message->date_response);
    cout << endl;

    cout << endl;
    cout << "   * " << message->getResponse() << endl;
  }

  cout << "\n   -------------------------------------------------------------" << endl;

  if(full_info)
    cout << "\033[1;31m   Estado: " << message->getStatus() << " \033[0m" << endl;

  if(user)
    delete user;
}

void MessageManager::listMessages(int service_id){
  int id;
  vector <unsigned long> ids;
  Message *message;
  string temp;
  int option;
  unsigned int i = 1;
  bool valid_option = false, cancel = false;

  while(!valid_option){
    cout << endl;
    cout << "\033[1;37m   1) Ver todo  2) Listar por fecha 3) Buscar por palabra: \033[0m";
    getline(cin, temp);
    option = atoi(temp.c_str());

    switch(option){
      case 1:
        valid_option = true;
        ids = this->search_by_service(service_id);
        break;
      case 2:
        valid_option = true;
        ids = this->search_by_fecha_hora(service_id);
        break;
      case 3:
        valid_option = true;
        ids = this->search_by_words();
        break;
      default:
        valid_option = false;
    }
  }
  cout << endl;
  if(ids.size() == 0){
    cout << "   No se encotraron consultas" << endl << endl;
  }else{
    cout << "\033[1;34m   Consultas: \033[0m\n";
    cout << "   -------------------------------------------------------------" << endl << endl;
    while((i < ids.size()+1) && !cancel){
      if(i%4 == 0){
       cout << "\033[0;33m Continuar viendo? (Y/n): \033[0m";
        getline(cin, temp);
        if(temp.compare("n") == 0)
          cancel = true;
        else
          cancel = false;
      }

      if(!cancel){
        id = ids.at(i-1);
        message = this->findMessage(id);

        if(message && (message->service_id == service_id)){
          if(!message->is_hidden)
           this->printMessage(message, false);
          delete(message);
        }
        cout << endl;
      }
      i++;
    }
  }
}

void MessageManager::respondMessages(int service_id){
  string temp;
  int option, id;
  bool valid_option = false;
  vector <unsigned long> ids;
  Message *message;

  while(!valid_option){
    cout << endl;
    cout << "\033[1;37m   1) Ver todo  2) Listar por fecha : \033[0m";
    getline(cin, temp);
    option = atoi(temp.c_str());

    switch(option){
      case 1:
        valid_option = true;
        ids = this->search_by_service(service_id);
        break;
      case 2:
        valid_option = true;
        ids = this->search_by_fecha_hora(service_id);
        break;
      default:
        valid_option = false;
    }
  }
  cout << endl;
  valid_option = false;
  if(ids.size() == 0){
    cout << "   No se encotraron consultas" << endl << endl;
  }else{
    cout << "\033[1;34m   Consultas: \033[0m\n";
    cout << "   -------------------------------------------------------------" << endl << endl;
    for (unsigned int i = 0; i < ids.size(); ++i){
      valid_option = false;
      id = ids.at(i);
      message = this->findMessage(id);

      if(message){
        if(!message->is_hidden){
          this->printMessage(message, false);
          cout << endl;
          while(!valid_option){
            cout << "\033[1;33m  ";
            cout << " 0) Cancelar ";
            cout << " 1) Borrar ";
            if(message->response->getTamanio() < 6)
              cout << " 2) Responder ";

            cout << ": \033[0m";
            getline(cin, temp);
            option = atoi(temp.c_str());
            switch(option){
              case 0:
                cout << "   Accion cancelada..." << endl;
                valid_option = true;
                break;
              case 1:
                this->deleteMessage(message->id);
                valid_option = true;
                break;
              case 2:
                if(message->response->getTamanio() < 6){
                  this->respondMessage(message->id);
                  valid_option = true;
                }else{
                  valid_option = false;
                  cout << "   Opcion desconocida" << endl;
                }
                break;
              default:
                valid_option = false;
                cout << "   Opcion desconocida" << endl;
            }

          }
          cout << endl;
        }
        delete(message);
      }
    }
  }
}

void MessageManager::moderateMessages(int service_id){
  string temp;
  int option, id;
  bool valid_option = false;
  vector <unsigned long> ids;
  Message *message;

  while(!valid_option){
    cout << endl;
    cout << "\033[1;37m   1) Ver todo  2) Listar por fecha : \033[0m";
    getline(cin, temp);
    option = atoi(temp.c_str());

    switch(option){
      case 1:
        valid_option = true;
        ids = this->search_by_service(service_id);
        break;
      case 2:
        valid_option = true;
        ids = this->search_by_fecha_hora(service_id);
        break;
      default:
        valid_option = false;
    }
  }
  cout << endl;
  valid_option = false;
  if(ids.size() == 0){
    cout << "   No se encotraron consultas" << endl << endl;
  }else{
    cout << "\033[1;34m   Consultas: \033[0m\n";
    cout << "   -------------------------------------------------------------" << endl << endl;
    for (unsigned int i = 0; i < ids.size(); ++i){
      valid_option = false;
      id = ids.at(i);
      message = this->findMessage(id);

      if(message){
        this->printMessage(message, true);
        cout << endl;
        while(!valid_option){
          cout << "\033[1;33m  ";
          cout << " 0) Cancelar ";
          if(message->is_hidden)
            cout <<  " 1) Mostrar ";
          else
            cout <<  " 1) Ocultar ";

          cout << ": \033[0m";
          getline(cin, temp);
          option = atoi(temp.c_str());
          switch(option){
            case 0:
              cout << "   Accion cancelada..." << endl;
              valid_option = true;
              break;
            case 1:
              this->hideUnhideMessage(message->id);
              valid_option = true;
              break;
            default:
              valid_option = false;
              cout << "   Opcion desconocida" << endl;
          }
        }
        cout << endl;
        delete(message);
      }
    }
  }
}

void MessageManager::add_to_index_by_service(Message *message){
  ClaveRegIDServIDUsIDCons *new_clave;
  int result;

  new_clave = new ClaveRegIDServIDUsIDCons(message->service_id,message->user_id, message->id);

  result = this->indice_por_servicio->insertar(new_clave);
  if(result != 0)
  if(result != 0)
    cout << "\033[0;31m   * Hubo un problema indexando al usuario\033[0m" << endl;
}

vector<unsigned long> MessageManager::search_by_words(){
  string temp;
  vector <string> words;
  vector <RegistroId *> recs;
  vector <unsigned long> ids;
  unsigned int key;

  cout << endl << "\033[1;33m   * Ingrese las palabras\033[0m: ";
  getline(cin, temp);
  Funciones::split(temp, " ", words);

  recs = this->indexManager->consultar(words, unionT);
  if(!recs.empty()){
    for (unsigned int i = 0; i < recs.size(); ++i){
      key = recs.at(i)->getKey();
      ids.push_back(key);
      delete recs.at(i);
    }
  }

  return ids;

}


vector<unsigned long> MessageManager::search_by_service(int service_id){
  ClaveRegIDServIDUsIDCons *clave;
  ClaveRegistroArbol *current_clave;
  vector <unsigned long> ids;
  unsigned long consulta_id;
  unsigned long servicio_id;

  clave = new ClaveRegIDServIDUsIDCons(service_id,0,0);
  current_clave = this->indice_por_servicio->buscarAproximado(clave);

  // traemos las claves hasta que cambia la clave de busqueda.
  while(current_clave != NULL){
    servicio_id = ((ClaveRegIDServIDUsIDCons*) current_clave)->getIdServicio();
    if(servicio_id == (unsigned int) service_id){
      consulta_id = ((ClaveRegIDServIDUsIDCons*) current_clave)->getIdConsulta();
      ids.push_back(consulta_id);
      current_clave = this->indice_por_servicio->siguiente();
    }else{
      current_clave = NULL;
    }
  }

  delete clave;
  return ids;
}

void MessageManager::add_to_index_by_fecha_hora(Message *message){
  ClaveRegIDServFCHCIDCons *new_clave;
  int result;
  char *date, *hour;

  date = Funciones::dateToString(message->date);
  hour = Funciones::hourToString(message->date);

  new_clave = new ClaveRegIDServFCHCIDCons(message->service_id, date, hour, message->id);
  result  = this->indice_por_fecha_hora_servicio->insertar(new_clave);
  if(result != 0)
    cout << "\033[0;31m   * Hubo un problema indexando al usuario\033[0m" << endl;

  delete[] date;
  delete[] hour;

}

vector <unsigned long> MessageManager::search_by_fecha_hora(int service_id){
  ClaveRegIDServFCHCIDCons *clave;
  ClaveRegistroArbol *current_clave;
  vector <unsigned long> ids;
  unsigned long consulta_id, servicio_id;
  string date, hour, fecha, hora, temp;
  vector <string> range;
  vector <string> date_split;
  vector <string> hour_split;

  cout << endl << "\033[1;33m   * Ingrese fecha hora [dd/mm/yyyy hh:mm] \033[0m: ";
  getline(cin, temp);

  if(!temp.empty()){

    Funciones::split(temp, " ", range);

    if(range.size() == 2){
      fecha = range.at(0);
      hora = range.at(1);
      Funciones::split(fecha, "/", date_split);
      Funciones::split(hora, ":", hour_split);
    }else{
      fecha = range.at(0);
      Funciones::split(fecha, "/", date_split);
      Funciones::split("00:00", "/", hour_split);
    }

    // DD/MM/YYYY
    for (unsigned int i = 0; i < date_split.size(); ++i){
      date.append(date_split.at(date_split.size() - 1 - i));
    }

    // HH:MM
    for (unsigned int i = 0; i < hour_split.size(); ++i){
      hour.append(hour_split.at(i));
    }

    clave = new ClaveRegIDServFCHCIDCons(service_id, date, hour, 0);
    current_clave = this->indice_por_fecha_hora_servicio->buscarAproximado(clave);

    // traemos las claves hasta que cambia la clave de busqueda. Traemos todos los mensajes de la fecha
    while(current_clave != NULL){
      servicio_id = ((ClaveRegIDServFCHCIDCons*) current_clave)->getIdServicio();
      if(date.compare(((ClaveRegIDServFCHCIDCons*) current_clave)->getFechaConsulta()) == 0){
        if(servicio_id == (unsigned int) service_id){
          consulta_id = ((ClaveRegIDServFCHCIDCons*) current_clave)->getIdConsulta();
          ids.push_back(consulta_id);
          current_clave = this->indice_por_fecha_hora_servicio->siguiente();
        }else{
          current_clave = NULL;
        }
      }else{
        current_clave = NULL;
      }
    }

    delete clave;
  }else{
    cout << endl << "\033[0;31m   * No ingreso ninguna fecha \033[0m" << endl;
  }

  return ids;
}


void MessageManager::update_index(Message *new_message, Message *message){
  ClaveRegIDServFCHCIDCons *old_clave_FH;
  ClaveRegIDServFCHCIDCons *new_clave_FH;

  char *date_old = Funciones::dateToString(message->date);
  char *hour_old = Funciones::hourToString(message->date);

  char *date = Funciones::dateToString(new_message->date);
  char *hour = Funciones::hourToString(new_message->date);

  old_clave_FH = new ClaveRegIDServFCHCIDCons(message->service_id,date_old,hour_old,message->id);
  new_clave_FH = new ClaveRegIDServFCHCIDCons(new_message->service_id,date,hour,new_message->id);

  this->indice_por_fecha_hora_servicio->modificar(old_clave_FH, new_clave_FH);

  delete[] date_old;
  delete[] hour_old;
}

