#include "message.h"

Message::Message(){
  this->id            = 0;
  this->user_id       = 0;
  this->service_id    = 0;
  this->date          = 0;
  this->date_response = 0;
  this->is_hidden     = false;
  this->body          = NULL;
  this->response      = NULL;
}

Message::Message(int user_id, int service_id){
  this->id       = 0;
  this->body     = NULL;
  this->response = NULL;

  this->user_id       = user_id;
  this->service_id    = service_id;
  this->is_hidden     = false;
  this->date_response = 0;
  this->date          = 0;
}

Message::Message(int key, char* datos){
  this->id            = key;
  this->user_id       = 0;
  this->service_id    = 0;
  this->date          = 0;
  this->date_response = 0;
  this->is_hidden     = false;
  this->body          = new RegistroGenerico();
  this->response      = new RegistroGenerico();

  this->deserialize(datos);
}

Message::~Message(){
  if(this->body)
    delete this->body;
  if(this->response)
    delete this->response;
}

// todos los datos menos la clave primaria
int Message::getSize(){
  int size = 0;
  size += sizeof(this->user_id);
  size += sizeof(this->service_id);
  size += sizeof(this->is_hidden);
  size += sizeof(this->date);
  size += sizeof(this->date_response);
  size += this->body->getTamanio();
  size += this->response->getTamanio();
  return size;
}

char* Message::getBody(){
  return this->body->getDato();
}

char* Message::getResponse(){
  return this->response->getDato();
}

bool Message::validateBody(){
  if(this->body->getTamanio() > 140)
    return false;
  else
    return true;
}

bool Message::validateResponse(){
  if(this->response->getTamanio() > 300)
    return false;
  else
    return true;
}

string Message::getStatus(){
  if(this->is_hidden)
    return "Oculto";
  else
    return "Visible";
}

void Message::serialize(char *buffer){

  memcpy(buffer, &(this->user_id), sizeof(this->user_id));
  buffer += sizeof(this->user_id);

  memcpy(buffer, &(this->service_id), sizeof(this->service_id));
  buffer += sizeof(this->service_id);

  memcpy(buffer, &(this->is_hidden), sizeof(this->is_hidden));
  buffer += sizeof(this->is_hidden);

  memcpy(buffer, &(this->date), sizeof(this->date));
  buffer += sizeof(this->date);

  memcpy(buffer, &(this->date_response), sizeof(this->date_response));
  buffer += sizeof(this->date_response);

  this->body->serialize(buffer);
  buffer += this->body->getTamanio();

  this->response->serialize(buffer);
}

void Message::deserialize(char *buffer){

  memcpy(&(this->user_id), buffer, sizeof(this->user_id));
  buffer += sizeof(this->user_id);

  memcpy(&(this->service_id), buffer, sizeof(this->service_id));
  buffer += sizeof(this->service_id);

  memcpy(&(this->is_hidden), buffer, sizeof(this->is_hidden));
  buffer += sizeof(this->is_hidden);

  memcpy(&(this->date), buffer, sizeof(this->date));
  buffer += sizeof(this->date);

  memcpy(&(this->date_response), buffer, sizeof(this->date_response));
  buffer += sizeof(this->date_response);

  this->body->deserialize(buffer);
  buffer += this->body->getTamanio();

  this->response->deserialize(buffer);
}
