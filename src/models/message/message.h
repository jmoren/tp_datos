#ifndef _MESSAGE_H_
#define _MESSAGE_H_

#include "../../lib/RegistroGenerico.h"

class Message{
  public:
    int id;
    int user_id;
    int service_id;
    time_t date;
    time_t date_response;
    bool is_hidden;
    RegistroGenerico *body;
    RegistroGenerico *response;

    Message();
    Message(int user_id, int service_id);
    Message(int key, char* registro);
    virtual ~Message();

    int  getSize();
    char *getBody();
    char *getResponse();
    string getStatus();

    void serialize(char *buffer);
    void deserialize(char *buffer);

    bool validateBody();
    bool validateResponse();
};

#endif
