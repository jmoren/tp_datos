#ifndef _MESSAGEMANAGER_H_
#define _MESSAGEMANAGER_H_

#include "message.h"
#include "../users/user_manager.h"
#include "../../lib/hashing/Hashing.h"
#include "../../lib/indiceInvertido/IndexManager.h"
#include "../../arbol/Arbol.h"

class MessageManager{
  public:
    Hashing *hashing;
    char *path;
    User *current_user;
    UserManager *user_manager;
    Arbol *indice_por_servicio;
    Arbol *indice_por_fecha_hora_servicio;
    IndexManager* indexManager;

    MessageManager(int size);
    MessageManager(User *user);
    MessageManager();
    virtual ~MessageManager();

    // users
    void addMessage(int user_id, int service_id);
    void listMessages(int service_id);

    // admin
    void moderateMessages(int service_id);

    // proveedor
    void respondMessages(int service_id);

    void hideUnhideMessage(int message_id);
    void respondMessage(int message_id);
    void deleteMessage(int message_id);

    Message *findMessage(int message_id);
    void updateMessage(Message *message);
    void printMessage(Message *Message, bool full_info);

    void  add_to_index_by_service(Message *message);
    vector <unsigned long> search_by_service(int service_id);

    void  add_to_index_by_fecha_hora(Message *message);
    vector <unsigned long> search_by_fecha_hora(int service_id);

    vector <unsigned long> search_by_words();

    void update_index(Message *message, Message *old_message);
};

#endif
