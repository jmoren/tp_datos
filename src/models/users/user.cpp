#include "user.h"

User::User(){
  this->name         = NULL;
  this->last_name    = NULL;
  this->total_emails = 0;
  this->password     = NULL;
  this->provincia    = NULL;
  this->role         = USER_ROLE;
  this->dni          = 0;
}

User::User(int key, char* datos){
  this->name         = new RegistroGenerico();
  this->last_name    = new RegistroGenerico();
  this->password     = new RegistroGenerico();
  this->total_emails = 0;
  this->provincia    = new RegistroGenerico();
  this->role         = USER_ROLE;
  this->dni          = key;
  this->deserialize(datos);

}

User::~User(){
  if(this->name)
    delete this->name;
  if(this->last_name)
    delete this->last_name;
  if(this->password)
    delete this->password;
  if(this->provincia)
    delete this->provincia;

  while (!this->emails.empty()){
    RegistroGenerico *email =  this->emails.back();
    this->emails.pop_back();
    if(email)
      delete(email);
  }
}

// todos los datos menos la clave primaria
int User::getSize(){
  int size = 0;
  size += this->name->getTamanio();
  size += this->last_name->getTamanio();
  size += this->password->getTamanio();
  size += this->password->getTamanio();
  size += sizeof(this->role);
  size += this->provincia->getTamanio();
  size += sizeof(this->total_emails);
  for(unsigned int i = 0; i < this->emails.size(); i++){
    size += this->emails.at(i)->getTamanio();
  }

  return size;
}

int User::getDni(){
  return this->dni;
}

char* User::getProvincia(){
  return this->provincia->getDato();
}

int User::getRole(){
  return this->role;
}

char* User::getName(){
  return this->name->getDato();
}

char* User::getLastName(){
  return this->last_name->getDato();
}

char* User::getPassword(){
  return this->password->getDato();
}

char *User::getFullName(){
  int tam = this->name->getTamanio() + this->last_name->getTamanio() + 1 - sizeof(int)*2;
  char *full_name = new char[tam]();
  memset(full_name, 0, tam);

  strcat(full_name, this->getName());
  strcat(full_name, " ");
  strcat(full_name, this->getLastName());

  return full_name;
}

bool User::validateName(){
  if(this->name->getTamanio() > 20)
    return false;
  else
    return true;
}

bool User::validateLastName(){
  if(this->last_name->getTamanio() > 20)
    return false;
  else
    return true;
}

bool User::validateEmail(){
  int size = 0;
  for(int i = 0; i < this->total_emails; i++){
    size += this->emails.at(i)->getTamanio();
  }

  if(size > 300)
    return false;
  else
    return true;
}

void User::addEmail(int size, char* new_email){
  RegistroGenerico* email;

  email = new RegistroGenerico(size, new_email);
  this->emails.push_back(email);
}

bool User::validateUsefulPassword(){
  bool valid = false;
  Cipher *cipher, *cipher_user;
  string masterKey;
  string encrypted;
  string decrypted;
  char *encrypted_password;

  masterKey = Funciones::read_master_key();
  cipher = new Cipher(masterKey);
  encrypted  = cipher->encrypt_block(this->getPassword());
  decrypted = cipher->decrypt_block(encrypted);

  if((decrypted.length() != 9) || (decrypted.compare(this->getPassword()) != 0)){
    valid = false;
  }else{
    cipher_user = new Cipher(encrypted);
    valid = cipher_user->validateKey();
    if(valid){ // guardamos la encriptada
      delete this->password;
      encrypted_password  = new char[encrypted.length() + 1]();
      strcpy(encrypted_password, encrypted.c_str());
      this->password = new RegistroGenerico(encrypted.length()  + 1 + sizeof(int), encrypted_password);
    }
    delete cipher_user;
  }

  delete cipher;
  return valid;
}

void User::serialize(char *buffer){
  // name
  this->name->serialize(buffer);
  buffer += this->name->getTamanio();
  // last name
  this->last_name->serialize(buffer);
  buffer += this->last_name->getTamanio();
  // total emails
  memcpy(buffer, &(this->total_emails), sizeof(this->total_emails));
  buffer += sizeof(this->total_emails);
  // emails
  for(int i = 0; i < this->total_emails; i++){
    RegistroGenerico * email = this->emails.at(i);
    email->serialize(buffer);
    buffer += email->getTamanio();
  }
  // password
  this->password->serialize(buffer);
  buffer += this->password->getTamanio();
  // provincia
  this->provincia->serialize(buffer);
  buffer += this->provincia->getTamanio();
  // role
  memcpy(buffer, &(this->role), sizeof(this->role));
}

void User::deserialize(char *buffer){
  // name
  this->name->deserialize(buffer);
  buffer += this->name->getTamanio();
  // last_name
  this->last_name->deserialize(buffer);
  buffer += this->last_name->getTamanio();
  // total_emails
  memcpy(&(this->total_emails), buffer, sizeof(this->total_emails));
  buffer += sizeof(this->total_emails);
  // emails
  for(int i = 0; i < this->total_emails; i++){
    RegistroGenerico *email = new RegistroGenerico();
    email->deserialize(buffer);
    this->emails.push_back(email);
    buffer += email->getTamanio();
  }
  // password
  this->password->deserialize(buffer);
  buffer += this->password->getTamanio();
  // provincia
  this->provincia->deserialize(buffer);
  buffer += this->provincia->getTamanio();
  // role
  memcpy(&(this->role), buffer, sizeof(this->role));
}

string User::mapRole(){
  string role;
  switch(this->role){
    case ADMIN_ROLE:
      role = "A";
      break;
    case PROVEEDOR_ROLE:
      role = "P";
      break;
    case USER_ROLE:
      role = "U";
      break;
  }

  return role;
}
