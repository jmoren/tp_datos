#ifndef USERMANAGER_H_
#define USERMANAGER_H_

#include "user.h"
#include "../../utils/validations.h"
#include "../../lib/hashing/Hashing.h"
#include "../../arbol/Arbol.h"

class UserManager{
  public:
    char *path;
    Hashing *hashing;
    Arbol   *indice_por_provincias;
    Arbol   *indice_por_role;

    UserManager(int size);
    UserManager();
    virtual ~UserManager();

    // acciones sobre usuarios
    User *addUser(int role);
    User *findUser(int id);
    User *editUser(User *user, int role);
    User *login_user();
    int deleteUser(int id);

    // listados
    void listUsers();
    void list_by_provincia();
    void list_by_role(string role);

    void printUser(User *user);
    void printUserRow(User *user);

    // admin actions
    void editUser(int id);
    void showUser(int id);

    // validaciones
    bool validate_user(int id);
    string mapProvincia(int id);
    void printProvincias();

    // indices
    void add_to_index_by_provincia(User *user);
    vector<unsigned long> search_by_provincia(string provincia);

    void add_to_index_by_role(User *user);
    vector<unsigned long> search_by_role(string role);

    void update_index(User *user, User *old_user);
    // varios
    int mapRole(string role);
    User *createAdmin();
};


#endif
