#ifndef USER_H_
#define USER_H_

#include "../../lib/RegistroGenerico.h"
#include "../../lib/encryption/cipher.h"

class User{
  public:
    RegistroGenerico *name;
    RegistroGenerico *last_name;
    RegistroGenerico *password;
    int total_emails;
    vector<RegistroGenerico *> emails;
    RegistroGenerico *provincia;
    int role;
    int dni;

    User();
    User(int key, char* regisro);
    virtual ~User();

    int getSize();
    int getDni();
    int getRole();
    char *getProvincia();
    char *getName();
    char *getLastName();
    char *getPassword();
    char *getFullName();

    void addEmail(int size, char* email);
    void serialize(char *buffer);
    void deserialize(char *buffer);

    string mapRole();
    bool validateName();
    bool validateLastName();
    bool validateUsefulPassword();
    bool validateEmail();
};

#endif

