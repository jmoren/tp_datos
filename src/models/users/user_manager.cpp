#include "user_manager.h"

string PROVINCIAS[] = {
    "CABA",
    "Buenos Aires",
    "Catamarca",
    "Chaco",
    "Chubut",
    "Cordoba",
    "Corrientes",
    "Entre Rios",
    "Formosa",
    "Jujuy",
    "La Pampa",
    "La Rioja",
    "Mendoza",
    "Misiones",
    "Neuquen",
    "Rio Negro",
    "Salta",
    "San Juan",
    "San Luis",
    "Santa Cruz",
    "Santa Fe",
    "S. del Estero",
    "Ta del Fuego",
    "Tucuman"
};

void UserManager::printProvincias(){
  cout << "   1) CABA         2) Buenos Aires     3) Catamarca        4) Chaco	   " << endl;
  cout << "   5) Chubut       6) Cordoba          7) Corrientes       8) Entre Rios" << endl;
  cout << "   9) Formosa     10) Jujuy           11) La Pampa        12) La Rioja  " << endl;
  cout << "  13) Mendoza     14) Misiones        15) Neuquen         16) Rio Negro " << endl;
  cout << "  17) Salta       18) San Juan        19) San Luis        20) Santa Cruz" << endl;
  cout << "  21) Santa Fe    22) S. del Estero   23) Ta del Fuego    24) Tucuman   " << endl;
}

string UserManager::mapProvincia(int id){
  if(id < 25)
    return PROVINCIAS[id];
  else
    return "";
}

UserManager::UserManager(int size){
  this->path  = new char[12];
  strcpy(this->path,"user_db.bin");
  this->hashing               = new Hashing(this->path, size);
  this->indice_por_provincias = NULL;
  this->indice_por_role       = NULL;
}

UserManager::UserManager(){
  this->path  = new char[12];
  strcpy(this->path,"user_db.bin");
  this->hashing               = new Hashing(this->path);
  this->indice_por_provincias = new Arbol(ProvinciaDNI);
  this->indice_por_role       = new Arbol(TipoUsuarioDNI);
}

UserManager::~UserManager(){
  delete[] this->path;
  delete this->hashing;
  if(this->indice_por_provincias)
    delete this->indice_por_provincias;
  if(this->indice_por_role)
    delete this->indice_por_role;
}

User *UserManager::login_user(){
  User *user;
  Cipher *cipher;
  int key, chances = 0;
  string temp;
  string encrypted_password;
  string masterKey;
  char *password;
  bool logged = false;

  while(!logged && (chances < 3)){
    cout << "\033[1;34m Ingrese DNI:\033[0m ";
    getline(cin, temp);
    key = atoi(temp.c_str());

    user = findUser(key);

    // si se encuentra el usuario, intentamos 3 veces con el password
    if(user){
      masterKey = Funciones::read_master_key();
      cipher  = new Cipher(masterKey);
      chances = 0;
      while(!logged && (chances < 3)){
        cout << "\033[1;34m Ingrese password:\033[0m ";
        getline(cin, temp);
        encrypted_password = cipher->encrypt_block(temp);
        password = new char[encrypted_password.length() + 1];
        strcpy(password, encrypted_password.c_str());
        if(strcmp(user->getPassword(),password) == 0){
          logged = true;
        }else{
          cout << " Password incorrecto!, intente de nuevo" << endl;
          chances += 1;
        }
        delete[] password;
      }
      delete cipher;
    }else{
      cout << " DNI incorrecto!, intente de nuevo" << endl;
      chances += 1;
    }
  }

  if(user && !logged){
    cout << " Password o DNI incorrecto! Verifique sus datos" << endl;
    delete(user);
    user = NULL; // mas attemps login
  }

  return user;
}

User *UserManager::addUser(int role){
  User *user;
  User *temp_user;
  RegistroId *registro;
  string temp;
  char *new_name;
  char *new_last_name;
  char *new_email;
  char *new_password;
  char *new_provincia;
  char *user_content;
  int new_dni, user_size, size;
  int res = 0, option = 0;
  bool valid = false, is_number = false;
  bool province_valid = false, role_valid = false;
  bool name_valid = false, last_name_valid = false;
  bool password_valid = false;

  user       = new User();
  registro   = new RegistroId();
  user->role = role;

  cout << "\n\033[0;32m";
  cout << " Registracion                                   \n";
  cout << " -----------------------------------------------\n";
  cout << "\033[0m\n";

  // Ingresar DNI
  do{
    do{
      cout << "\033[1;34m ** Ingrese DNI:\033[0m ";
      getline(cin, temp);
      new_dni = atoi(temp.c_str());
      is_number = validate_number(new_dni);
    }while(!is_number);

    if(is_number){
      user->dni = new_dni;
      temp_user = this->findUser(new_dni);
    }

    if(!temp_user)
      valid = true;
    else{
      valid = false;
      is_number = false;
      cout << "   Ya existe un usuario con ese id, ingrese otro..." << endl;
      delete(temp_user);
    }
  }while(!valid);
  cout << endl;

  if(valid){

    cout << "\033[1;34m ** Ingrese nombre:\033[0m ";
    while(!name_valid){
      getline(cin, temp);
      size = temp.length() + 1 + sizeof(int);
      new_name = new char[temp.length() + 1];
      strcpy(new_name, temp.c_str());
      user->name  = new RegistroGenerico(size, new_name);

      if(!user->validateName()){
        delete[] new_name;
        name_valid = false;
        cout << "\033[0;31m Nombre demasiado largo...\n\033[1;34m ** Ingrese nombre otra vez:\033[0m ";
      }else
        name_valid = true;
    }

    cout << endl;

    // Ingresar Apellido
    cout << "\033[1;34m ** Ingrese apellido:\033[0m ";
    while(!last_name_valid){
      getline(cin, temp);
      size = temp.length() + 1 + sizeof(int);
      new_last_name  = new char[temp.length() + 1]();
      strcpy(new_last_name, temp.c_str());
      user->last_name = new RegistroGenerico(size, new_last_name);

      if(!user->validateLastName()){
        delete[] new_last_name;
        last_name_valid = false;
        cout << "\033[0;31m Apellido demasiado largo...\n\033[1;34m ** Ingrese apellido otra vez:\033[0m ";
      }else
        last_name_valid = true;
    }
    cout << endl;

    // Ingresar emails
    cout << "\033[1;34m ** Ingresar emails:\033[0m" << endl;
    for(int i = 0; i < 3; i++){
      cout << "\033[0;31m + Nuveo Email ("<< i  +1 <<"): \033[0m";
      getline(cin, temp);
      if(temp.length() > 1){
        size = temp.length() + 1 + sizeof(int);
        new_email  = new char[temp.length() + 1]();
        strcpy(new_email, temp.c_str());
        user->addEmail(size, new_email);
        user->total_emails += 1;
      }
    }
    cout << endl;

    // Ingresar provincia
    cout << "\033[1;34m ** Seleccione una provincia:\033[0m\n";
    do{
      this->printProvincias();
      cout << "\033[0;33m  Opcion: \033[0m";
      getline(cin, temp);
      option = atoi(temp.c_str());

      if((option < 1) || (option > 24)){
        province_valid = false;
        cout << "\033[0;31m   Opcion invalida \033[0m" << endl;
      }else{
        string provincia = this->mapProvincia(option - 1);
        size = provincia.length()+1+sizeof(int);
        new_provincia    = new char[provincia.length()+1];
        strcpy(new_provincia, provincia.c_str());
        user->provincia = new RegistroGenerico(size, new_provincia);
        province_valid = true;
      }

    }while(!province_valid);
    cout << endl;

    // Ingresar role
    if(user->getRole() == NO_ROLE){
      cout << "\033[1;34m ** Seleccione un rol: \033[0m"<<  endl;
      do{
        cout << "    1) Admin 2) Proveedor 3) Usuario " << endl;
        cout << "\033[0;33m    Opcion: \033[0m";
        getline(cin, temp);
        option = atoi(temp.c_str());

        switch(option){
          case 1:
            user->role = ADMIN_ROLE;
            role_valid = true;
            break;
          case 2:
            user->role = PROVEEDOR_ROLE;
            role_valid = true;
            break;
          case 3:
            user->role = USER_ROLE;
            role_valid = true;
            break;
          default:
            role_valid = false;
            cout << "\033[0;31m    Opcion invalida \033[0m" << endl;
        }
      }while(!role_valid);
    }
    cout << endl;

    // Ingresar Password
    cout << "\033[1;34m ** Ingrese password:\033[0m ";
    while(!password_valid){
      getline(cin, temp);
      size = temp.length() + 1 + sizeof(int);
      new_password  = new char[temp.length() + 1]();
      strcpy(new_password, temp.c_str());
      user->password = new RegistroGenerico(size, new_password);
      if(!user->validateUsefulPassword()){
        delete user->password;
        password_valid = false;
        cout << "\033[0;31m Password invalido...\n\033[1;34m ** Ingrese password otra vez:\033[0m ";
      }else
        password_valid = true;
    }
    cout << endl;

    user_size    = user->getSize();
    user_content = new char[user_size + sizeof(int)]();
    user->serialize(user_content);

    registro->setSize(user_size + sizeof(int)*2); // datos, key, size
    registro->setKey(user->dni);
    registro->setDato(user_content);

    // Guardamos user
    res = this->hashing->addRegistro(registro);
    switch(res){
      case SUCCESS_SAVE_RECORD:
        cout << "\033[1;32m   OK...\033[0m" << endl;
        this->add_to_index_by_provincia(user);
        this->add_to_index_by_role(user);
        res = 0;
        break;
      case ERROR_SAVE_RECORD:
        cout << "\033[1;31m   Fallo... Error al guardar\033[0m" << endl;
        res = ERROR_SAVE_RECORD;
        user = NULL;
        break;
      case INVALID_RECORD:
        res = INVALID_RECORD;
        delete(registro);
        delete(user);
        user = NULL;
        break;
    }
  }else{
    res = -3;
    user = NULL;
  }

  return user;
}

User *UserManager::findUser(int id){
  RegistroId *registro;
  User *user;

  registro = this->hashing->getRegistro(id);
  if(registro){
    user = new User(registro->getKey(), registro->getDato());
    delete(registro);
  }else{
    user = NULL;
  }
  return user;
}

// admin actions
void UserManager::showUser(int id){
  User *user = this->findUser(id);
  if(user){
    this->printUser(user);
    delete(user);
  }else
    cout << "\033[0;31m Ususario no encontrado!\033[0m" << endl;
}

void UserManager::editUser(int id){
  User *user = this->findUser(id);
  if(user){
    user = this->editUser(user, ADMIN_ROLE);
    delete(user);
  }else
    cout << "\033[0;31m Ususario no encontrado!\033[0m" << endl;
}

int UserManager::deleteUser(int id){
  int res = -1;
  User *user;
  string temp;
  ClaveRegTipoUsuarioDNI * clave_tipo;
  ClaveRegProvinciaDNI   * clave_provincia;
  user = this->findUser(id);
  if(user){
    cout << endl << "\033[1;31m ** Esta seguro de borrar el usuario? (y/N):\033[0m ";
    getline(cin, temp);
    if((temp.compare("Y") == 0) || (temp.compare("y") == 0)){
      res = this->hashing->deleteRegistro(id);
      if(res == 0){
        clave_provincia = new ClaveRegProvinciaDNI(user->dni, user->getProvincia());
        clave_tipo      = new ClaveRegTipoUsuarioDNI(user->dni, user->role);
        this->indice_por_provincias->eliminar(clave_provincia);
        this->indice_por_role->eliminar(clave_tipo);
        delete clave_provincia;
        delete clave_tipo;
      }
    }
    delete(user);
    cout << endl;
  }else{
    cout << "\033[0;31m Ususario no encontrado!\033[0m" << endl;
    res = -2;
  }

  return res;
}

User *UserManager::editUser(User *user, int role){
  User *new_user;
  RegistroId *registro;
  RegistroGenerico *updated_email;
  char *new_name, *new_last_name;
  char *new_password, *new_email;
  char *new_provincia;
  char *user_content;
  int size, user_size, res, id, option;
  string temp;
  bool province_valid = false;
  bool role_valid     = false;
  bool password_valid = false;

  id = user->dni;
  new_user = new User();
  registro = new RegistroId();
  new_user->dni  = user->dni;
  new_user->role = user->role;

  cout << "\n\033[0;32m";
  cout << " Actualizacion de datos:" << endl;
  cout << " ---------------------- " << endl;
  cout << "\033[0m\n";

    // Ingresar nombre
  cout << "\033[1;34m ** Actualizar nombre:\033[0m ";
  cout << user->getName();
  cout << "\033[1;34m (y/N):\033[0m ";
  getline(cin, temp);
  if((temp.compare("Y") == 0) || (temp.compare("y") == 0)){
    cout << "\033[0;31m + Nuveo Nombre: \033[0m";
    getline(cin, temp);
    size = temp.length() + 1 + sizeof(int);
    new_name = new char[temp.length() + 1];
    strcpy(new_name, temp.c_str());
    new_user->name  = new RegistroGenerico(size, new_name);
  }else{
    new_user->name  = new RegistroGenerico();
    user->name->clone(new_user->name);
  }
  cout <<  endl;

  // Ingresar Apellido
  cout << "\033[1;34m ** Actualizar apellido:\033[0m ";
  cout << user->getLastName();
  cout << "\033[1;34m (y/N):\033[0m ";
  getline(cin, temp);
  if((temp.compare("Y") == 0) || (temp.compare("y") == 0)){
    cout << "\033[0;31m + Nuveo Apellido: \033[0m";
    getline(cin, temp);
    size = temp.length() + 1 + sizeof(int);
    new_last_name  = new char[temp.length() + 1]();
    strcpy(new_last_name, temp.c_str());
    new_user->last_name = new RegistroGenerico(size, new_last_name);
  }else{
    new_user->last_name = new RegistroGenerico();
    user->last_name->clone(new_user->last_name);
  }
  cout << endl;

  // Ingresar emails
  cout << "\033[1;34m ** Actualizar emails:\033[0m\n";
  for (int i = 0; i < user->total_emails; i++) {
    cout << " * "<< user->emails.at(i)->getDato() << " (y/N): ";
    getline(cin, temp);

    if((temp.compare("Y") == 0) || (temp.compare("y") == 0)){
      cout << "\033[0;31m + Nuveo Email: \033[0m";
      getline(cin, temp);
      if(temp.length() > 1){
        size = temp.length() + 1 + sizeof(int);
        new_email  = new char[temp.length() + 1]();
        strcpy(new_email, temp.c_str());
        updated_email = new RegistroGenerico(size, new_email);
        new_user->emails.push_back(updated_email);
        new_user->total_emails += 1;
      }
    }else{
      updated_email = new RegistroGenerico();
      user->emails.at(i)->clone(updated_email);
      new_user->emails.push_back(updated_email);
      new_user->total_emails += 1;
    }
  }
  cout << endl;

  // los emails que faltan
  int pending_emails = 3 - user->total_emails;
  for (int i = 0; i < pending_emails; i++) {
    cout << " * Adicionar nuevo email? (y/N): ";
    getline(cin, temp);

    if((temp.compare("Y") == 0) || (temp.compare("y") == 0)){
      cout << "\033[0;31m + Nuveo Email: \033[0m";
      getline(cin, temp);
      if(temp.length() > 1){
        size = temp.length() + 1 + sizeof(int);
        new_email  = new char[temp.length() + 1]();
        strcpy(new_email, temp.c_str());
        updated_email = new RegistroGenerico(size, new_email);
        new_user->emails.push_back(updated_email);
        new_user->total_emails += 1;
      }
    }
  }
  cout << endl;

  // Ingresar provincia
  cout << "\033[1;34m ** Actualizar provincia:\033[0m ";
  cout << user->getProvincia();
  cout << "\033[1;34m (y/N):\033[0m";
  getline(cin, temp);
  if((temp.compare("Y") == 0) || (temp.compare("y") == 0)){
    do{
      this->printProvincias();
      cout << "\033[0;33m  Opcion: \033[0m";
      getline(cin, temp);
      option = atoi(temp.c_str());

      if((option < 1) || (option > 24)){
        province_valid = false;
        cout << "\033[0;31m Opcion invalida\033[0m" << endl;
      }else{
        string provincia = this->mapProvincia(option-1);
        size = provincia.length()+1+sizeof(int);
        new_provincia = new char[provincia.length()+1];
        strcpy(new_provincia, provincia.c_str());
        new_user->provincia = new RegistroGenerico(size, new_provincia);
        province_valid = true;
      }
    }while(!province_valid);

  }else{
    new_user->provincia  = new RegistroGenerico();
    user->provincia->clone(new_user->provincia);
  }
  cout << endl;

  // Ingresar Password
  cout << "\033[1;34m** Actualizar password (y/N):\033[0m";
  getline(cin, temp);

  if((temp.compare("Y") == 0) || (temp.compare("y") == 0)){
    while(!password_valid){
      cout << "\033[0;31m + Nuveo Password: \033[0m";
      getline(cin, temp);
      size = temp.length() + 1 + sizeof(int);
      new_password  = new char[temp.length() + 1]();
      strcpy(new_password, temp.c_str());
      new_user->password = new RegistroGenerico(size, new_password);
      if(!new_user->validateUsefulPassword()){
        delete[] new_password;
        password_valid = false;
        cout << "\033[0;31m Password invalido...\n\033[1;34m ** Ingrese password otra vez:\033[0m ";
      }else
        password_valid = true;
    }

  }else{
    new_user->password = new RegistroGenerico();
    user->password->clone(new_user->password);
  }
  cout << endl;

  // Actualizar role
  cout << "\033[1;34m** Actualizar role (y/N):\033[0m";
  getline(cin, temp);

  if((temp.compare("Y") == 0) || (temp.compare("y") == 0)){
    do{
      if(role == ADMIN_ROLE)
        cout << "   1) Proveedor 2) Usuario 3) Admin " << endl;
      else
        cout << "   1) Proveedor 2) Usuario " << endl;

      cout << "\033[0;33m  Opcion: \033[0m";
      getline(cin, temp);
      option = atoi(temp.c_str());
      switch(option){
        case 1:
          new_user->role = PROVEEDOR_ROLE;
          role_valid = true;
          break;
        case 2:
          new_user->role = USER_ROLE;
          role_valid = true;
          break;
        case 3:
          if(role == ADMIN_ROLE){
            new_user->role = ADMIN_ROLE;
            role_valid = true;
          }else{
            role_valid = false;
            cout << "\033[0;31m   Opcion invalida \033[0m" << endl;
          }
          break;
        default:
          role_valid = false;
          cout << "\033[0;31m   Opcion invalida \033[0m" << endl;
      }
    }while(!role_valid);
  }

  cout << endl;

  user_size    = new_user->getSize();
  user_content = new char[user_size + sizeof(int)]();
  new_user->serialize(user_content);

  registro->setSize(user_size + sizeof(int)*2); // datos, key, size
  registro->setKey(new_user->dni);
  registro->setDato(user_content);

  cout << "\033[1;33m ** Esta seguro de actualizar el usuario? (y/N):\033[0m ";
  getline(cin, temp);

  if((temp.compare("Y") == 0) || (temp.compare("y") == 0)){
    res = this->hashing->updateRegistro(registro);
    switch(res){
      case SUCCESS_SAVE_RECORD:
        cout << "\033[1;32m OK...\033[0m\n" << endl;
        this->update_index(new_user, user);
        res = SUCCESS_SAVE_RECORD;
        break;
      case ERROR_SAVE_RECORD:
        cout << "\033[1;31m Fallo... Error al guardar y no se modifico el usuario\033[0m" << endl;
        res = ERROR_SAVE_RECORD;
        break;
      case INVALID_RECORD:
        res = INVALID_RECORD;
        break;
    }
  }else{
    res = -5;
    delete(registro);
  }

  if(user)
    delete(user);
  if(new_user)
    delete(new_user);

  new_user = this->findUser(id);

  return new_user;
}

void UserManager::printUser(User *user){
  cout << endl;
  cout << " Perfil del usuario:" << endl;
  cout << " -------------------" << endl;
  cout << endl;
  cout << "\033[0;34m DNI:        \033[0m" << user->dni            << endl;
  cout << "\033[0;34m Nombre:     \033[0m" << user->getName()      << endl;
  cout << "\033[0;34m Apellido:   \033[0m" << user->getLastName()  << endl;
  cout << "\033[0;34m Provincia:  \033[0m" << user->getProvincia() << endl;
  cout << "\033[0;34m Role:       \033[0m" << user->mapRole()      << endl;
  for(int i = 0; i < user->total_emails; i++){
    if(i==0)
      cout << "\033[0;34m Emails:    \033[0m * " << user->emails.at(i)->getDato() << endl;
    else
      cout << "             * " << user->emails.at(i)->getDato() << endl;
  }
  cout << endl;
}

void UserManager::printUserRow(User *user){
  char *full_name;
  full_name = user->getFullName();

  cout << " |  " << setw (5) << left;
  cout  << user->mapRole();

  cout << " |  " << setw (10) << left;
  cout << user->getDni();

  cout << " |  " << setw (15) << left;
  cout << user->getProvincia();

  cout << " |  " << setw (30) << left;
  cout << full_name << "|" << endl;

  cout << " |--------+-------------+------------------+--------------------------------|" << endl;

  delete[] full_name;
}

void UserManager::list_by_provincia(){
  vector<unsigned long> ids;
  User *user;
  string provincia, temp;
  int option;

  cout << endl;
  this->printProvincias();
  cout << "\033[0;33m  Opcion: \033[0m";
  getline(cin, temp);
  option = atoi(temp.c_str());

  provincia = this->mapProvincia(option-1);

  ids = this->search_by_provincia(provincia);
  if(ids.size() == 0){
    cout << endl << "\033[1;33m  No hay usuarios de la provincia \033[0m" << provincia << endl;
  }else{
    cout << "\033[1;33m\n Usuarios ("<< ids.size() << "):\033[0m \n" << endl;

    cout << " |  ROLE  |  DNI        |  Provincia       |  Nombre Completo               |" << endl;
    cout << " |--------+-------------+------------------+--------------------------------|" << endl;

    for (int unsigned i = 0; i < ids.size(); ++i){
      user = this->findUser(ids.at(i));
      if(user){
        this->printUserRow(user);
        delete(user);
      }
    }
  }
  cout << endl;
  ids.clear();
}

void UserManager::list_by_role(string role){
  vector<unsigned long> ids;
  User *user;

  ids = this->search_by_role(role);
  if(ids.size() == 0){
    cout << endl << "\033[1;33m  No hay usuarios con role \033[0m" << role << endl;
  }else{
    cout << "\033[1;33m\n Usuarios ("<< ids.size() << "):\033[0m \n" << endl;

    cout << " |  ROLE  |  DNI        |  Provincia       |  Nombre Completo               |" << endl;
    cout << " |--------+-------------+------------------+--------------------------------|" << endl;

    for (int unsigned i = 0; i < ids.size(); ++i){
      user = this->findUser(ids.at(i));
      if(user){
        this->printUserRow(user);
        delete(user);
      }
    }
  }
  cout << endl;
  ids.clear();
}


void UserManager::listUsers(){
  User *user;
  vector<RegistroId*> registros;
  RegistroId *registro;
  unsigned int i;

  registros = this->hashing->getAll();
  cout << "\033[1;33m\n Usuarios ("<< registros.size() << "):\033[0m \n" << endl;

  cout << " |  ROLE  |  DNI        |  Provincia       |  Nombre Completo               |" << endl;
  cout << " |--------+-------------+------------------+--------------------------------|" << endl;

  for(i = 0; i < registros.size(); i++){
    registro = registros.at(i);
    user = new User(registro->getKey(), registro->getDato());
    this->printUserRow(user);
    delete(user);
  }
  cout << endl;

  for(i = 0; i < registros.size(); i++){
    registro = registros.at(i);
    delete(registro);
  }

  registros.clear();
}

void UserManager::add_to_index_by_role(User *user){
  ClaveRegTipoUsuarioDNI *new_clave_usuario;
  int result;

  new_clave_usuario = new ClaveRegTipoUsuarioDNI(user->dni, user->role);

  result = this->indice_por_role->insertar(new_clave_usuario);

  if(result != 0)
    cout << "  Hubo un problema indexando al usuario" << endl;
}

vector<unsigned long> UserManager::search_by_role(string role){
  ClaveRegTipoUsuarioDNI *clave;
  ClaveRegistroArbol *current_clave;
  vector <unsigned long> ids;
  unsigned long dni, rol;

  rol = this->mapRole(role);
  clave = new ClaveRegTipoUsuarioDNI(0, rol);
  current_clave = this->indice_por_role->buscarAproximado(clave);
  while(current_clave != NULL){
    if(rol == ((ClaveRegTipoUsuarioDNI*) current_clave)->getTipoUsuario()){
      dni = ((ClaveRegTipoUsuarioDNI*) current_clave)->getDni();
      ids.push_back(dni);
      current_clave = this->indice_por_role->siguiente();
    }else{
      current_clave = NULL;
    }
  }

  delete clave;
  return ids;
}

void UserManager::add_to_index_by_provincia(User *user){
  ClaveRegProvinciaDNI *new_clave_usuario;
  string provincia;
  int result;

  provincia = user->getProvincia();
  new_clave_usuario = new ClaveRegProvinciaDNI(user->dni, provincia);

  result = this->indice_por_provincias->insertar(new_clave_usuario);

  if(result != 0)
    cout << "  Hubo un problema indexando al usuario" << endl;
}

void UserManager::update_index(User *new_user, User *user){
  ClaveRegProvinciaDNI *old_clave_provincia;
  ClaveRegProvinciaDNI *new_clave_provincia;
  ClaveRegTipoUsuarioDNI *old_clave_tipo;
  ClaveRegTipoUsuarioDNI *new_clave_tipo;


  old_clave_provincia = new ClaveRegProvinciaDNI(user->dni, user->getProvincia());
  new_clave_provincia = new ClaveRegProvinciaDNI(new_user->dni, new_user->getProvincia());

  old_clave_tipo = new ClaveRegTipoUsuarioDNI(user->dni, user->role);
  new_clave_tipo = new ClaveRegTipoUsuarioDNI(new_user->dni, new_user->role);

  this->indice_por_provincias->modificar(old_clave_provincia, new_clave_provincia);
  this->indice_por_role->modificar(old_clave_tipo, new_clave_tipo);
  delete(old_clave_tipo);
  delete(old_clave_provincia);

 }


vector<unsigned long> UserManager::search_by_provincia(string provincia){
  ClaveRegProvinciaDNI *clave;
  ClaveRegistroArbol *current_clave;
  vector <unsigned long> ids;
  unsigned long dni;

  clave = new ClaveRegProvinciaDNI(0, provincia);
  current_clave = this->indice_por_provincias->buscarAproximado(clave);
  while(current_clave != NULL){
    if(provincia.compare( ( (ClaveRegProvinciaDNI*) current_clave)->getProvincia()) == 0){
      dni = ((ClaveRegProvinciaDNI*) current_clave)->getDni();
      ids.push_back(dni);
      current_clave = this->indice_por_provincias->siguiente();
    }else{
      current_clave = NULL;
    }
  }

  delete clave;
  return ids;
}

int UserManager::mapRole(string role){
  int rol = USER_ROLE;

  if((role.compare("Admin") == 0) || (role.compare("admin") == 0))
    rol = ADMIN_ROLE;
  else if((role.compare("Proveedor") == 0) || (role.compare("proveedor") == 0))
    rol = PROVEEDOR_ROLE;
  else if((role.compare("Usuario") == 0) || (role.compare("usuario") == 0))
    rol = USER_ROLE;

  return rol;
}
