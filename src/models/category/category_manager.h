#ifndef CATEGORYMANAGER_H_
#define CATEGORYMANAGER_H_

#include "category.h"
#include "../../utils/Funciones.h"
#include "../../lib/hashing/Hashing.h"
#include "../../lib/indiceInvertido/IndexManager.h"

class CategoryManager{
public:
	IndexManager* indexManager;
    Hashing *hashing;
    char *path;

    CategoryManager(int size);
    CategoryManager();
    virtual ~CategoryManager();

    Category *addCategory();
    Category *findCategory(int id);
    Category *editCategory(int id);
    int deleteCategory(int id);

    void listCategories();
    void listCategoriesByName();
    void printCategory(Category *cateogry);
    void printCategoryByName(Category *cateogry);

    void search_by_words();
    int bulk_load(string path);
};

#endif
