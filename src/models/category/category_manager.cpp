#include "../../utils/validations.h"
#include "category_manager.h"

CategoryManager::CategoryManager(int size){
  this->path  = new char[16];
  strcpy(this->path,"category_db.bin");
  this->hashing  = new Hashing(this->path, size);
  this->indexManager = new IndexManager("categoriaDescripcion");
}

CategoryManager::CategoryManager(){
  this->path  = new char[16];
  strcpy(this->path,"category_db.bin");
  this->hashing = new Hashing(this->path);
  this->indexManager = new IndexManager(path,"categoriaDescripcion");
}

CategoryManager::~CategoryManager(){
  delete[] path;
  delete hashing;
  delete indexManager;
}

Category *CategoryManager::addCategory(){
  Category *category;
  RegistroId *registro;
  string temp;
  char *new_name;
  char *new_description;
  char *category_content;
  int  category_size, size;
  int  res = 0;
  bool name_valid = false, description_valid = false;

  category  = new Category();
  registro  = new RegistroId();
  category->id = this->hashing->getNextId();

  cout << endl;
  cout << " Ingreso de categoria" << endl;
  cout << endl;

  cout << "\033[1;34m ** Ingrese nombre:\033[0m ";
  while(!name_valid){
    getline(cin, temp);
    size = temp.length() + 1 + sizeof(int);
    new_name = new char[temp.length() + 1];
    strcpy(new_name, temp.c_str());
    category->name  = new RegistroGenerico(size, new_name);

    if(!category->validateName()){
      delete[] new_name;
      name_valid = false;
      cout << "\033[0;31m Nombre demasiado largo...\n\033[1;34m ** Ingrese nombre otra vez:\033[0m ";
    }else
      name_valid = true;
  }
  cout << endl;

  cout << "\033[1;34m ** Ingrese Descripcion:\033[0m ";
  while(!description_valid){
    getline(cin, temp);
    size = temp.length() + 1 + sizeof(int);
    new_description  = new char[temp.length() + 1]();
    strcpy(new_description, temp.c_str());
    category->description = new RegistroGenerico(size, new_description);

    if(!category->validateDescription()){
      delete[] new_description;
      name_valid = false;
      cout << "\033[0;31m  Descripcion demasiada larga...\n\033[1;34m ** Ingrese descipcion otra vez:\033[0m ";
    }else
      description_valid = true;

  }
  cout << endl;

  category_size    = category->getSize();
  category_content = new char[category_size + sizeof(int)]();

  category->serialize(category_content);

  registro->setSize(category_size + sizeof(int)*2); // datos, key, size
  registro->setKey(category->id);
  registro->setDato(category_content);

  res = this->hashing->addRegistro(registro);
  switch(res){
    case SUCCESS_SAVE_RECORD:
    	//Se agrego con exito ---> indexo
    	this->indexManager->indexar(category->id,category->getDescription());
      cout << "\033[1;32m OK...\033[0m" << endl;
      res = 0;
      break;
    case ERROR_SAVE_RECORD:
      cout << "\033[1;31m Fallo... Error al guardar\033[0m" << endl;
      res = ERROR_SAVE_RECORD;
      category = NULL;
      break;
    case INVALID_RECORD:
      res = INVALID_RECORD;
      delete(registro);
      delete(category);
      category = NULL;
      break;
  }

  return category;
}

Category *CategoryManager::findCategory(int id){
  RegistroId *registro;
  Category *category;

  registro = this->hashing->getRegistro(id);
  if(registro){
    category = new Category(registro->getKey(), registro->getDato());
    delete(registro);
  }else{
    category = NULL;
  }

  return category;
}

Category *CategoryManager::editCategory(int id){
  Category *new_category;
  Category *category;
  RegistroId *registro;
  char *new_name, *new_description;
  char *category_content;
  int size, category_size, res;
  bool name_valid = false, description_valid = false;
  string temp;

  cout << endl;
  cout << "\n\033[0;32m";
  cout << " Actualizacion de categoria:" << endl;
  cout << " ------------------------- " << endl;
  cout << "\033[0m" << endl;

  category    = this->findCategory(id);

  if(category){

  	registro         = new RegistroId();
    new_category     = new Category();
  	new_category->id = category->id;

    // Ingresar nombre
    cout << "\033[1;34m ** Actualizar nombre:\033[0m ";
    cout << category->getName();
    cout << "\033[1;34m (y/N):\033[0m ";
    getline(cin, temp);
    if((temp.compare("Y") == 0) || (temp.compare("y") == 0)){
      cout << "\033[0;31m + Nuveo Nombre: \033[0m";
      while(!name_valid){
        getline(cin, temp);
        size = temp.length() + 1 + sizeof(int);
        new_name = new char[temp.length() + 1];
        strcpy(new_name, temp.c_str());
        new_category->name  = new RegistroGenerico(size, new_name);

       if(!category->validateName()){
          delete[] new_name;
          name_valid = false;
          cout << "\033[0;31m Nombre demasiado largo...\n\033[1;34m ** Ingrese apellido otra vez:\033[0m ";
        }else
          name_valid = true;
      }
    }else{
      new_category->name  = new RegistroGenerico();
      category->name->clone(new_category->name);
    }
    cout << endl;

    // Ingresar Descripcion
    cout << "* ";
    cout << category->getDescription();
    cout << " * " << endl;
    cout << "\033[1;34m * Actualizar descipcion: (y/N):\033[0m ";
    getline(cin, temp);
    if((temp.compare("Y") == 0) || (temp.compare("y") == 0)){
      cout << "\033[0;31m + Nuvea descripcion: \033[0m\n * ";
      while(!description_valid){
        getline(cin, temp);
        size = temp.length() + 1 + sizeof(int);
        new_description = new char[temp.length() + 1];
        strcpy(new_description, temp.c_str());
        new_category->description = new RegistroGenerico(size, new_description);

        if(!category->validateDescription()){
          delete[] new_description;
          description_valid = false;
          cout << "\033[0;31m Descripcion demasiada larga...\n\033[1;34m ** Ingrese la descripcion otra vez:\033[0m ";
        }else
          description_valid = true;
      }
    }else{
      new_category->description  = new RegistroGenerico();
      category->description->clone(new_category->description);
    }
    cout << endl;

    category_size    = new_category->getSize();
    category_content = new char[category_size + sizeof(int)]();
    new_category->serialize(category_content);

    registro->setSize(category_size + sizeof(int)*2); // datos, key, size
    registro->setKey(new_category->id);
    registro->setDato(category_content);

    cout << "\033[1;33m ** Esta seguro de actualizar el servicio? (y/N):\033[0m ";
    getline(cin, temp);

    if((temp.compare("Y") == 0) || (temp.compare("y") == 0)){
      res = this->hashing->updateRegistro(registro);

      switch(res){
        case SUCCESS_SAVE_RECORD:
          cout << "\033[1;32m OK...\033[0m" << endl;
          res = SUCCESS_SAVE_RECORD;
          break;
        case ERROR_SAVE_RECORD:
          cout << "\033[1;31m Fallo... Error al guardar y no se modifico el servicio\033[0m" << endl;
          res = ERROR_SAVE_RECORD;
          break;
        case INVALID_RECORD:
          res = INVALID_RECORD;
          break;
      }
    }else{
      res = -5;
      delete(registro);
    }

    if(new_category)
      delete(new_category);
    if(category)
      delete(category);

  }else{
    cout << "\033[1;31m Categoria no encontrada\033[0m" << endl;
  }

  new_category = this->findCategory(id);

  return new_category;
}

int CategoryManager::deleteCategory(int id){
  int res;

  res = this->hashing->deleteRegistro(id);

  return res;
}

void CategoryManager::printCategory(Category *category){
  cout << endl;
  cout << "\033[1;34m * Id:          \033[0m"  << category->id           << endl;
  cout << "\033[1;34m * Nombre:      \033[0m"  << category->getName()    << endl;
  cout << "\033[1;34m * Descripcion:\033[0m "  << endl;
  cout << "\n   " << category->getDescription() << endl;
  cout << "   -------------------------------------------------------------" << endl << endl;
}

void CategoryManager::printCategoryByName(Category *category){
  cout  << "    " << category->id << ") "   << setw (15) << left << category->getName() << endl;
}

void CategoryManager::listCategoriesByName(){
  Category *category;
  vector<RegistroId*> registros;
  RegistroId *registro;
  unsigned int i = 1;

  registros = this->hashing->getAll();

  while(i < registros.size() + 1){
    registro = registros.at(i-1);
    category = new Category(registro->getKey(), registro->getDato());
    this->printCategoryByName(category);
    delete(category);
    if(i%4 == 0)
      cout << endl;
    i++;
  }

  for(i = 0; i < registros.size(); i++){
    registro = registros.at(i);
    delete(registro);
  }

  registros.clear();
}

void CategoryManager::search_by_words(){
  string temp;
  vector <string> words;
  vector <RegistroId *> recs;
  Category *category;
  RegistroId *registro;
  bool cancel = false;
  unsigned int i = 1;

  cout << endl << "\033[1;33m   * Ingrese las palabras\033[0m: ";
  getline(cin, temp);
  Funciones::split(temp, " ", words);

  recs = this->indexManager->consultar(words, unionT);
  if(recs.size() == 0){

  }else{
    while((i < recs.size() + 1) && !cancel){
      if(i%4 == 0){
        cout << "\033[0;33m Continuar viendo? (Y/n): \033[0m";
        getline(cin, temp);
        if(temp.compare("n") == 0)
          cancel = true;
        else
          cancel = false;
      }

      if(!cancel){
        registro = recs.at(i-1);
        category = new Category(registro->getKey(), registro->getDato());
        this->printCategory(category);
        delete(category);
        i++;
      }
    }

    for(i = 0; i < recs.size(); i++){
      registro = recs.at(i);
      delete(registro);
    }
    recs.clear();

  }

}
void CategoryManager::listCategories(){
  Category *category;
  vector<RegistroId*> registros;
  RegistroId *registro;
  unsigned int i = 1;
  bool cancel = false;
  string temp;

  registros = this->hashing->getAll();
  cout << "\033[1;33m\n Categorias ("<< registros.size() << "):\033[0m \n" << endl;

  while((i < registros.size() + 1) && !cancel){
    if(i%4 == 0){
      cout << "\033[0;33m Continuar viendo? (Y/n): \033[0m";
      getline(cin, temp);
      if(temp.compare("n") == 0)
        cancel = true;
      else
        cancel = false;
    }
    if(!cancel){
      registro = registros.at(i-1);
      category = new Category(registro->getKey(), registro->getDato());
      this->printCategory(category);
      delete(category);
      i++;
    }
  }
  cout <<  endl;

  for(i = 0; i < registros.size(); i++){
    registro = registros.at(i);
    delete(registro);
  }

  registros.clear();
}

int CategoryManager::bulk_load(string path){
  string str;
  vector<string> data;
  Category *category;
  RegistroId *registro;
  char* category_content;
  int category_size, res;
  int count = 0;

  ifstream file(path.c_str());
  if(file.good()){
    while (getline(file, str)){
      if(!str.empty()){
        Funciones::split(str, ";", data);
        category     = new Category();

        category->id = this->hashing->getNextId();
        category->setContent(data.at(0), data.at(1));

        registro = new RegistroId();

        category_size    = category->getSize();
        category_content = new char[category_size + sizeof(int)]();

        category->serialize(category_content);

        registro->setSize(category_size + sizeof(int)*2); // datos, key, size
        registro->setKey(category->id);
        registro->setDato(category_content);

        // Guardamos Category
        res = this->hashing->addRegistro(registro);
        switch(res){
          case SUCCESS_SAVE_RECORD:
            count += 1;
            this->indexManager->indexar(category->id,category->getDescription());
            res = 0;
            break;
          case ERROR_SAVE_RECORD:
            res = ERROR_SAVE_RECORD;
            category = NULL;
            break;
          case INVALID_RECORD:
            res = INVALID_RECORD;
            delete(registro);
            delete(category);
            category = NULL;
            break;
        }

        if(category)
          delete category;

        data.clear();
      }
    }
  }else{
    cout << "\033[1;31m El archivo no fue encontrado, revise el path \033[0m" << endl;
  }

  return count;
}
