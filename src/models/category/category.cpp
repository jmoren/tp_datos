#include "category.h"

Category::Category(){
  this->id           = 0;
  this->name         = NULL;
  this->description  = NULL;
}

Category::Category(int key, char* datos){
  this->id           = key;
  this->name         = new RegistroGenerico();
  this->description  = new RegistroGenerico();

  this->deserialize(datos);
}

Category::~Category(){
  if(this->name)
    delete this->name;
  if(this->description)
    delete this->description;
}

void Category::setContent(string name, string description){
  char * c_name, *c_description;
  int name_size, description_size;
  this->id = id;

  name_size  = sizeof(int) + name.length() + 1;
  c_name     = new char[name.length() + 1];
  strcpy(c_name, name.c_str());
  this->name = new RegistroGenerico(name_size, c_name);

  description_size  = sizeof(int) + description.length() + 1;
  c_description     = new char[description.length() + 1];
  strcpy(c_description, description.c_str());
  this->description = new RegistroGenerico(description_size, c_description);
}

// todos los datos menos la clave primaria
int Category::getSize(){
	int size = 0;
	size += this->name->getTamanio();
	size += this->description->getTamanio();
	return size;
}

char* Category::getName(){
  return this->name->getDato();
}

char* Category::getDescription(){
  return this->description->getDato();
}

bool Category::validateName(){
  if(this->name->getTamanio() > 100)
    return false;
  else
    return true;
}

bool Category::validateDescription(){
  if(this->description->getTamanio() > 300)
    return false;
  else
    return true;
}

void Category::deserialize(char *buffer){
	this->name->deserialize(buffer);
  buffer += this->name->getTamanio();
  this->description->deserialize(buffer);
}

void Category::serialize(char *buffer){
  this->name->serialize(buffer);
  buffer += this->name->getTamanio();
  this->description->serialize(buffer);
}
