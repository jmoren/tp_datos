#ifndef _CATEGORY_H_
#define _CATEGORY_H_

#include "../../lib/RegistroGenerico.h"

class Category{
	public:
	int id;
  RegistroGenerico *name;
	RegistroGenerico *description;

  Category();
  Category(int key, char* regisro);
  virtual ~Category();

  int  getSize();
  char *getName();
  char *getDescription();

  void setContent(string name, string description);
  void serialize(char *buffer);
  void deserialize(char *buffer);

  bool validateName();
  bool validateDescription();
};

#endif
