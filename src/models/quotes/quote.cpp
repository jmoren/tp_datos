#include "quote.h"

Quote::Quote(){
  this->id         = 0;
  this->user_id    = 0;
  this->service_id = 0;
  this->date       = 0;
  this->body       = NULL;
}

Quote::Quote(int user_id, int service_id){
  this->id   = 0;
  this->date = 0;
  this->body = NULL;
  this->user_id    = user_id;
  this->service_id = service_id;
}

Quote::Quote(int key, char* content){
  this->id          = 0;
  this->date        = 0;
  this->user_id     = 0;
  this->service_id  = 0;
  this->body        = new RegistroGenerico();

  this->deserialize(content);
}

Quote::~Quote(){
  if(this->body)
    delete this->body;
}

int Quote::getSize(){
  int size = 0;
  size += sizeof(this->user_id);
  size += sizeof(this->service_id);
  size += sizeof(this->date);
  size += this->body->getTamanio();
  return size;
}

char* Quote::getBody(){
  return this->body->getDato();
}

bool Quote::validateBody(){
  if(this->body->getTamanio() > 140)
    return false;
  else
    return true;
}

void Quote::serialize(char *buffer){

  memcpy(buffer, &(this->user_id), sizeof(this->user_id));
  buffer += sizeof(this->user_id);

  memcpy(buffer, &(this->service_id), sizeof(this->service_id));
  buffer += sizeof(this->service_id);

  memcpy(buffer, &(this->date), sizeof(this->date));
  buffer += sizeof(this->date);

  this->body->serialize(buffer);
}

void Quote::deserialize(char *buffer){

  memcpy(&(this->user_id), buffer, sizeof(this->user_id));
  buffer += sizeof(this->user_id);

  memcpy(&(this->service_id), buffer, sizeof(this->service_id));
  buffer += sizeof(this->service_id);

  memcpy(&(this->date), buffer, sizeof(this->date));
  buffer += sizeof(this->date);

  this->body->deserialize(buffer);
}
