#ifndef _QUOTE_H
#define _QUOTE_H

#include "../../lib/RegistroGenerico.h"

class Quote{
  public:
    int id;
    int user_id;
    int service_id;
    time_t date;
    RegistroGenerico *body;

    Quote();
    Quote(int user_id, int service_id);
    Quote(int key, char *registro);
    virtual ~Quote();

    int getSize();
    char *getBody();

    void serialize(char *buffer);
    void deserialize(char *buffer);

    bool validateBody();
};
#endif
