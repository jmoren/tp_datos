#include "../../utils/Funciones.h"
#include "../../utils/validations.h"
#include "quote_manager.h"

QuoteManager::QuoteManager(int size){
  this->path  = new char[15];
  strcpy(this->path,"quote_db.bin");
  this->hashing               = new Hashing(this->path, size);
  this->indice_por_servicio   = NULL;
  this->indice_por_fecha_hora = NULL;
  this->user_manager          = NULL;
  this->current_user          = NULL;
}

QuoteManager::QuoteManager(){
  this->path  = new char[15];
  strcpy(this->path,"quote_db.bin");
  this->hashing               = new Hashing(this->path);
  this->user_manager          = NULL;
  this->indice_por_servicio   = NULL;
  this->indice_por_fecha_hora = NULL;
  this->current_user          = NULL;
}

QuoteManager::QuoteManager(User *user){
  this->path  = new char[15];
  strcpy(this->path,"quote_db.bin");
  this->hashing               = new Hashing(this->path);
  this->user_manager          = new UserManager();
  this->indice_por_servicio   = new Arbol(IDServicioIDUsuarioIDPCotizacion);
  this->indice_por_fecha_hora = new Arbol(IDServicioFechaHoraIDPCotizacion);
  this->current_user          = user;
}

QuoteManager::~QuoteManager(){
  delete[] this->path;
  if(this->hashing)
    delete this->hashing;
  if(this->user_manager)
    delete this->user_manager;
  if(this->indice_por_servicio)
    delete this->indice_por_servicio;
  if(this->indice_por_fecha_hora)
    delete this->indice_por_fecha_hora;
}

void QuoteManager::addQuote(User *user, int service_id, User *proveedor){
  Quote *quote;
  RegistroId *registro;
  string temp;
  Cipher *cipher;
  string encrypted_body;
  char *new_body;
  char *quote_content;
  int  quote_size, size;
  int  res = 0;
  bool body_valid = false;

  quote           = new Quote(user->dni, service_id);
  registro        = new RegistroId();
  quote->id       = this->hashing->getNextId();
  quote->date     = time(0);
  quote->user_id  = user->dni;

  cout << endl << "   Fecha: ";
  Funciones::printHourTime(quote->date);

  cout << " - Usuario: " << user->getName() << " " << user->getLastName() << endl;
  cout << "   --------------------------------------------" << endl;

  cout << "\033[1;34m   Pedido de cotizacion:\033[0m\n\n   ";
  while(!body_valid){
    getline(cin, temp);
    size = temp.length() + 1 + sizeof(int);
    new_body = new char[temp.length() + 1];
    strcpy(new_body, temp.c_str());
    quote->body  = new RegistroGenerico(size, new_body);

    if(!quote->validateBody()){
      delete[] new_body;
      body_valid = false;
      cout << "\033[0;31m Pedido demasiado largo...\n\033[1;34m ** Ingrese el pedido otra vez:\033[0m    " << endl;
    }else{
      cipher = new Cipher(proveedor->getPassword());
      encrypted_body = cipher->encrypt_block(quote->getBody());

      delete quote->body;
      // reemplazamos el texto por el encriptado
      size = encrypted_body.length() + 1 + sizeof(int);
      new_body = new char[encrypted_body.length() + 1];
      strcpy(new_body, encrypted_body.c_str());
      quote->body  = new RegistroGenerico(size, new_body);

      delete cipher;
      body_valid = true;
    }
  }
  cout << endl;

  quote_size    = quote->getSize();
  quote_content = new char[quote_size + sizeof(int)]();
  memset(quote_content, 0, quote_size);
  quote->serialize(quote_content);

  registro->setSize(quote_size + sizeof(int)*2); // datos, key, size
  registro->setKey(quote->id);
  registro->setDato(quote_content);

  res = this->hashing->addRegistro(registro);
  switch(res){
    case SUCCESS_SAVE_RECORD:
      cout << "\033[1;32m Consulta enviada...\033[0m" << endl;
      this->add_to_index_by_service(quote);
      this->add_to_index_by_fecha_hora(quote);
      res = 0;
      break;
    case ERROR_SAVE_RECORD:
      cout << "\033[1;31m No fue posible realizar la consulta\033[0m" << endl;
      res = ERROR_SAVE_RECORD;
      quote = NULL;
      break;
    case INVALID_RECORD:
      res = INVALID_RECORD;
      delete(registro);
      delete(quote);
      quote = NULL;
      break;
  }

  if(quote)
    delete(quote);
}

Quote *QuoteManager::findQuote(int id){
  RegistroId *registro;
  Quote *quote;

  registro = this->hashing->getRegistro(id);
  if(registro){
    quote = new Quote(registro->getKey(), registro->getDato());
    delete(registro);
  }else{
    cout << "\033[1;31m Pedido de cotizacion no encontrado\033[0m" << endl;
    quote = NULL;
  }

  return quote;
}

void QuoteManager::deleteQuote(int id){
  this->hashing->deleteRegistro(id);
}

void QuoteManager::printQuote(Quote *quote){
  Cipher *cipher;

  cipher = new Cipher(this->current_user->getPassword());
  User *user = this->user_manager->findUser(quote->user_id);
  cout << endl;
  cout << "   " << quote->id << ") ";
  cout << "\033[1;37m";
  if(user)
    cout << user->getName();
  else
    cout << quote->user_id;
  cout << " \033[1;34m realizo el siguiente pedido \033[0m";
  Funciones::printHourTime(quote->date);
  cout << endl;

  cout << endl;
  cout << "   * " << cipher->decrypt_block(quote->getBody()) << endl << endl;

  cout << "\n   -------------------------------------------------------------" << endl;

  delete cipher;
  if(user)
    delete user;
}

void QuoteManager::listQuotes(int service_id){
  int id;
  vector <unsigned long> ids;
  Quote *quote;
  string temp;
  int option;
  unsigned int i = 1;
  bool valid_option = false, cancel = false;

  while(!valid_option){
    cout << endl;
    cout << "\033[1;37m   1) Listar por usuario 2) Listar por fecha  \033[0m";
    getline(cin, temp);
    option = atoi(temp.c_str());

    switch(option){
      case 1:
        valid_option = true;
        ids = this->search_by_service(service_id);
        break;
      case 2:
        valid_option = true;
        ids = this->search_by_fecha_hora(service_id);
        break;
      default:
        valid_option = false;
    }
  }
  cout << endl;
  cout << "\033[1;34m   Cotizaciones: \033[0m\n";
  cout << "   -------------------------------------------------------------" << endl << endl;
  if(ids.size() == 0){
    cout << "   No se encotraron pedidos de cotizacion" << endl << endl;
  }else{
    while((i < ids.size()+1) && !cancel){
      if(i%4 == 0){
       cout << "\033[0;33m Continuar viendo? (Y/n): \033[0m";
        getline(cin, temp);
        if(temp.compare("n") == 0)
          cancel = true;
        else
          cancel = false;
      }

      if(!cancel){
        id = ids.at(i-1);
        quote = this->findQuote(id);

        if(quote && (quote->service_id == service_id)){
          this->printQuote(quote);
          delete(quote);
        }
        cout << endl;
      }
      i++;
    }
  }
}

void QuoteManager::add_to_index_by_service(Quote *quote){
  ClaveRegIDServIDUsPC *new_clave;
  int result;

  new_clave = new ClaveRegIDServIDUsPC(quote->service_id, quote->user_id, quote->id);

  result = this->indice_por_servicio->insertar(new_clave);
  if(result != 0)
    cout << "Hubo un problema indexando al usuario" << endl;
}

void QuoteManager::add_to_index_by_fecha_hora(Quote *quote ){
  ClaveRegIDServFHPCotizacion *new_clave;
  int result;
  char *date, *hour;

  date = Funciones::dateToString(quote->date);
  hour = Funciones::hourToString(quote->date);

  new_clave = new ClaveRegIDServFHPCotizacion(quote->service_id, date, hour, quote->id);
  result  = this->indice_por_fecha_hora->insertar(new_clave);
  if(result != 0)
    cout << "Hubo un problema indexando al usuario" << endl;
  delete[] date;
  delete[] hour;
}

vector<unsigned long> QuoteManager::search_by_service(int service_id){
  ClaveRegIDServIDUsPC *clave;
  ClaveRegistroArbol *current_clave;
  vector <unsigned long> ids;
  unsigned long quote_id;
  unsigned long servicio_id;
  unsigned long user_id, usuario_id;
  string temp;

  cout << endl;
  cout << "\033[1;33m   Ingrese DNI (ingrese 0 para ver todas las cotizaciones): ";
  cout << ": \033[0m";
  getline(cin, temp);
  user_id = atoi(temp.c_str());

  clave = new ClaveRegIDServIDUsPC(service_id, user_id, 0);
  current_clave = this->indice_por_servicio->buscarAproximado(clave);

  // traemos las claves hasta que cambia la clave de busqueda.
  while(current_clave != NULL){
    servicio_id = ((ClaveRegIDServIDUsPC*) current_clave)->getIdServicio();
    usuario_id  = ((ClaveRegIDServIDUsPC*) current_clave)->getIdUsuario();
    if(servicio_id == (unsigned int) service_id ){
      if(user_id != 0){
        if(user_id == usuario_id){
          quote_id = ((ClaveRegIDServIDUsPC*) current_clave)->getIdPedidoCotizacion();
          ids.push_back(quote_id);
          current_clave = this->indice_por_servicio->siguiente();
        }else{
          current_clave = NULL;
        }
      }else{
        quote_id = ((ClaveRegIDServIDUsPC*) current_clave)->getIdPedidoCotizacion();
        ids.push_back(quote_id);
        current_clave = this->indice_por_servicio->siguiente();
      }
    }else{
      current_clave = NULL;
    }
  }

  delete clave;
  return ids;
}

vector <unsigned long> QuoteManager::search_by_fecha_hora(int service_id){
  ClaveRegIDServFHPCotizacion *clave;
  ClaveRegistroArbol *current_clave;
  vector <unsigned long> ids;
  unsigned long quote_id, servicio_id;
  string date, hour, fecha, hora, temp;
  vector <string> range;
  vector <string> date_split;
  vector <string> hour_split;

  cout << endl << "\033[1;33m   * Ingrese fecha hora [dd/mm/yyyy hh:mm] \033[0m: ";
  getline(cin, temp);

  if(!temp.empty()){

    Funciones::split(temp, " ", range);

    if(range.size() == 2){
      fecha = range.at(0);
      hora = range.at(1);
      Funciones::split(fecha, "/", date_split);
      Funciones::split(hora, ":", hour_split);
    }else{
      fecha = range.at(0);
      Funciones::split(fecha, "/", date_split);
      Funciones::split("00:00", "/", hour_split);
    }

    // DD/MM/YYYY
    for (unsigned int i = 0; i < date_split.size(); ++i){
      date.append(date_split.at(date_split.size() - 1 - i));
    }

    // HH:MM
    for (unsigned int i = 0; i < hour_split.size(); ++i){
      hour.append(hour_split.at(i));
    }

    clave = new ClaveRegIDServFHPCotizacion(service_id, date, hour, 0);
    current_clave = this->indice_por_fecha_hora->buscarAproximado(clave);

    // traemos las claves hasta que cambia la clave de busqueda. Traemos todos los mensajes de la fecha
    while(current_clave != NULL){
      servicio_id = ((ClaveRegIDServFHPCotizacion*) current_clave)->getIdServicio();
      if(date.compare(((ClaveRegIDServFHPCotizacion*) current_clave)->getFecha()) == 0){
        if(servicio_id == (unsigned int) service_id){
          quote_id = ((ClaveRegIDServFHPCotizacion*) current_clave)->getIdCotizacion();
          ids.push_back(quote_id);
          current_clave = this->indice_por_fecha_hora->siguiente();
        }else{
          current_clave = NULL;
        }
      }else{
        current_clave = NULL;
      }
    }

    delete clave;
  }else{
    cout << endl << "\033[0;31m   * No ingreso ninguna fecha \033[0m" << endl;
  }
  return ids;
}
