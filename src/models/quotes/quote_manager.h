#ifndef _QUOTEMANAGER_H_
#define _QUOTEMANAGER_H_


#include "quote.h"
#include "../users/user_manager.h"
#include "../../lib/hashing/Hashing.h"
#include "../../arbol/Arbol.h"
#include "../../lib/encryption/cipher.h"

class QuoteManager{
	public:
    Hashing *hashing;
    char *path;
    UserManager *user_manager;
    User *current_user;
    Arbol *indice_por_servicio;
    Arbol *indice_por_fecha_hora;

    QuoteManager(int size);
    QuoteManager(User *user);
    QuoteManager();
    virtual ~QuoteManager();

    void addQuote(User *user, int service_id, User *proveedor);
    void listQuotes(int service_id);
    void deleteQuote(int id);
    void printQuote(Quote *quote);

    Quote *findQuote(int id);

    void  add_to_index_by_service(Quote *quote);
    vector <unsigned long> search_by_service(int service_id);
    void  add_to_index_by_fecha_hora(Quote *quote);
    vector <unsigned long> search_by_fecha_hora(int service_id);
};
#endif
