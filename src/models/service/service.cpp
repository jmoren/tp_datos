#include "service.h"

Service::Service(int proveedor_id){
  this->id           = 0;
  this->type         = 0;
  this->proveedor_id = proveedor_id;
  this->name         = NULL;
  this->description  = NULL;
  this->category_ids = NULL;
}

Service::Service(int key, char* datos){
  this->id           = key;
  this->proveedor_id = 0;
  this->type         = 0;
  this->name         = new RegistroGenerico();
  this->description  = new RegistroGenerico();
  this->category_ids = new RegistroGenerico();

  this->deserialize(datos);
}

Service::~Service(){
  if(this->name)
    delete this->name;
  if(this->description)
    delete this->description;
  if(this->category_ids)
    delete this->category_ids;
}

// todos los datos menos la clave primaria
int Service::getSize(){
	int size = 0;
	size += this->name->getTamanio();
	size += this->description->getTamanio();
	size += this->category_ids->getTamanio();
  size += sizeof(this->type);
	size += sizeof(this-proveedor_id);
  return size;
}

char* Service::getName(){
  return this->name->getDato();
}

char* Service::getDescription(){
  return this->description->getDato();
}

int Service::getProveedorId(){
  return this->proveedor_id;
}

int Service::getType(){
  return this->type;
}

bool Service::validateName(){
  if(this->name->getTamanio() > 100)
    return false;
  else
    return true;
}

bool Service::validateDescription(){
  if(this->description->getTamanio() > 300)
    return false;
  else
    return true;
}

string Service::mapType(){
  string type;

  switch(this->type){
    case GRATIS:
      type = "Gratis";
      break;
    case SUBASTA:
      type = "Subasta";
      break;
    case PRECIO_FIJO:
      type = "Precio fijo";
      break;
  }

  return type;
}

void Service::deserialize(char *buffer){
	memcpy(&(this->proveedor_id), buffer, sizeof(this->proveedor_id));
	buffer += sizeof(this->proveedor_id);
  this->name->deserialize(buffer);
  buffer += this->name->getTamanio();
  this->description->deserialize(buffer);
  buffer += this->description->getTamanio();
  this->category_ids->deserialize(buffer);
  buffer += this->category_ids->getTamanio();
  memcpy(&(this->type), buffer, sizeof(this->type));

}

void Service::serialize(char *buffer){
  memcpy(buffer, &(this->proveedor_id), sizeof(this->proveedor_id));
  buffer += sizeof(this->proveedor_id);
  this->name->serialize(buffer);
  buffer += this->name->getTamanio();
  this->description->serialize(buffer);
  buffer += this->description->getTamanio();
  this->category_ids->serialize(buffer);
  buffer += this->category_ids->getTamanio();
  memcpy(buffer, &(this->type), sizeof(this->type));
}
