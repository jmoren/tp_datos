#include "../../utils/validations.h"
#include "service_manager.h"

ServiceManager::ServiceManager(int size){
  this->path  = new char[15];
  strcpy(this->path,"service_db.bin");
  this->hashing               = new Hashing(this->path, size);
  this->current_user          = NULL;
  this->message_manager       = NULL;
  this->quote_manager         = NULL;
  this->category_manager      = NULL;
  this->user_manager          = NULL;
  this->indice_por_proveedor  = NULL;
  this->indice_por_categoria  = NULL;
}

ServiceManager::ServiceManager(){
  this->path  = new char[15];
  strcpy(this->path,"service_db.bin");
  this->hashing               = new Hashing(this->path);
  this->message_manager       = NULL;
  this->quote_manager         = NULL;
  this->category_manager      = NULL;
  this->user_manager          = NULL;
  this->indice_por_proveedor  = NULL;
  this->indice_por_categoria  = NULL;
  this->current_user          = NULL;
}

ServiceManager::ServiceManager(User* current_user){
  this->path  = new char[15];
  strcpy(this->path,"service_db.bin");
  this->hashing               = new Hashing(this->path);
  this->message_manager       = new MessageManager(current_user);
  this->quote_manager         = new QuoteManager(current_user);
  this->category_manager      = new CategoryManager();
  this->user_manager          = new UserManager();
  this->indice_por_proveedor  = new Arbol(TipoUsuarioIDServ);
  this->indice_por_categoria  = new Arbol(NombreCategoriaIDServicio);
  this->current_user          = current_user;
}

ServiceManager::~ServiceManager(){
  delete[] this->path;
  delete this->hashing;
  if(this->message_manager)
    delete this->message_manager;
  if(this->quote_manager)
    delete this->quote_manager;
  if(this->category_manager)
    delete this->category_manager;
  if(this->indice_por_categoria)
    delete this->indice_por_categoria;
  if(this->indice_por_proveedor)
    delete this->indice_por_proveedor;
  if(this->user_manager)
    delete this->user_manager;
}

Service *ServiceManager::addService(int proveedor_id){
  Service *service;
  RegistroId *registro;
  string temp;
  char *new_name;
  char *new_description;
  char *new_category_ids;
  char *service_content;
  int  service_size, size;
  int  res = 0, option;
  bool valid_option = false, name_valid = false, description_valid = false;

  service  = new Service(proveedor_id);
  registro = new RegistroId();
  service->id = this->hashing->getNextId();

  cout << endl;
  cout << " Ingreso de servicios" << endl;
  cout << endl;

  cout << "\033[1;34m ** Ingrese nombre:\033[0m ";
  while(!name_valid){
    getline(cin, temp);
    size = temp.length() + 1 + sizeof(int);
    new_name = new char[temp.length() + 1];
    strcpy(new_name, temp.c_str());
    service->name  = new RegistroGenerico(size, new_name);

    if(!service->validateName()){
      delete[] new_name;
      name_valid = false;
      cout << "\033[0;31m Nombre demasiado largo...\n\033[1;34m ** Ingrese nombre otra vez:\033[0m ";
    }else
      name_valid = true;
  }
  cout << endl;

  cout << "\033[1;34m ** Ingrese Descripcion:\033[0m\n    ";
  while(!description_valid){
    getline(cin, temp);
    size = temp.length() + 1 + sizeof(int);
    new_description  = new char[temp.length() + 1]();
    strcpy(new_description, temp.c_str());
    service->description = new RegistroGenerico(size, new_description);

    if(!service->validateDescription()){
      delete[] new_description;
      name_valid = false;
      cout << "\033[0;31m Descripcion demasiada larga...\n\033[1;34m ** Ingrese descipcion otra vez:\033[0m\n    ";
    }else
      description_valid = true;

  }
  cout << endl;

  // Ingresar categorias
  cout << "\033[1;34m ** Categorias: \033[0m" << endl;
  this->category_manager->listCategoriesByName();
  cout << "\033[0;33m    Seleccione categorias: \033[0m";
  getline(cin, temp);

  size = temp.length() + 1 + sizeof(int);
  new_category_ids = new char[temp.length() + 1];
  strcpy(new_category_ids, temp.c_str());
  service->category_ids = new RegistroGenerico(size, new_category_ids);

  cout << endl;

  cout << "\033[1;34m ** Ingrese una opcion: \n\033[0m    1) Gratis 2) Precio Fijo 3) Subasta " << endl;
  while(!valid_option){
    cout << "\033[0;33m    Opcion: \033[0m";
    getline(cin, temp);
    option = atoi(temp.c_str());
    switch(option){
      case 1:
        service->type   = GRATIS;
        valid_option = true;
        break;
      case 2:
        service->type   = PRECIO_FIJO;
        valid_option = true;
        break;
      case 3:
        service->type   = SUBASTA;
        valid_option = true;
        break;
      default:
        cout << "    Opcion invalida" << endl;
        valid_option = false;
    }
  }

  service_size    = service->getSize();
  service_content = new char[service_size + sizeof(int)]();

  service->serialize(service_content);

  registro->setSize(service_size + sizeof(int)*2); // datos, key, size
  registro->setKey(service->id);
  registro->setDato(service_content);

  // Guardamos service
  res = this->hashing->addRegistro(registro);
  switch(res){
    case SUCCESS_SAVE_RECORD:
      cout << "\033[1;32m OK...\033[0m" << endl;
      this->add_to_index_by_category(service);
      this->add_to_index_by_user(service);
      res = 0;
      break;
    case ERROR_SAVE_RECORD:
      cout << "\033[1;31m Fallo... Error al guardar\033[0m" << endl;
      res = ERROR_SAVE_RECORD;
      service = NULL;
      break;
    case INVALID_RECORD:
      res = INVALID_RECORD;
      delete(registro);
      delete(service);
      service = NULL;
      break;
  }

  return service;
}

Service *ServiceManager::findService(int id){
  RegistroId *registro;
  Service *service;

  registro = this->hashing->getRegistro(id);
  if(registro){
    service = new Service(registro->getKey(), registro->getDato());
    delete(registro);
  }else{
    cout << "\033[1;31m No existe el servicio con id \033[0m" << id << endl;
    service = NULL;
  }

  return service;
}

Service *ServiceManager::editService(int id){
  Service *new_service;
  Service *service;
  RegistroId *registro;
  char *new_name, *new_description, *new_category_ids;
  char *service_content;
  int size, service_size, res;
  int option;
  bool valid_option = false, name_valid = false, description_valid = false;
  string temp;

  cout << endl;
  cout << "\n\033[0;32m";
  cout << " Actualizacion de servicio:" << endl;
  cout << " ------------------------- " << endl;
  cout << "\033[0m" << endl;

  service    = this->findService(id);

  if(service){
    if(service->proveedor_id == this->current_user->dni){
    	registro        = new RegistroId();
    	new_service     = new Service(service->proveedor_id);
    	new_service->id = service->id;

      // Ingresar nombre
      cout << "\033[1;34m ** Actualizar nombre:\033[0m ";
      cout << service->getName();
      cout << "\033[1;34m (y/N):\033[0m ";
      getline(cin, temp);
      if((temp.compare("Y") == 0) || (temp.compare("y") == 0)){
        cout << "\033[0;31m + Nuveo Nombre: \033[0m";
        while(!name_valid){
          getline(cin, temp);
          size = temp.length() + 1 + sizeof(int);
          new_name = new char[temp.length() + 1];
          strcpy(new_name, temp.c_str());
          new_service->name  = new RegistroGenerico(size, new_name);

         if(!service->validateName()){
            delete[] new_name;
            name_valid = false;
            cout << "\033[0;31m Nombre demasiado largo...\n\033[1;34m ** Ingrese apellido otra vez:\033[0m ";
          }else
            name_valid = true;
        }
      }else{
        new_service->name  = new RegistroGenerico();
        service->name->clone(new_service->name);
      }
      cout << endl;

      // Ingresar Descripcion
      cout << "\033[1;34m * Actualizar descipcion: (y/N):\033[0m ";
      getline(cin, temp);
      if((temp.compare("Y") == 0) || (temp.compare("y") == 0)){
        cout << "\033[0;31m + Nuvea descripcion: \033[0m\n * ";
        while(!description_valid){
          getline(cin, temp);
          size = temp.length() + 1 + sizeof(int);
          new_description = new char[temp.length() + 1];
          strcpy(new_description, temp.c_str());
          new_service->description = new RegistroGenerico(size, new_description);

          if(!service->validateDescription()){
            delete[] new_description;
            description_valid = false;
            cout << "\033[0;31m Descripcion demasiada larga...\n\033[1;34m ** Ingrese la descripcion otra vez:\033[0m ";
          }else
            description_valid = true;
        }
      }else{
        new_service->description  = new RegistroGenerico();
        service->description->clone(new_service->description);
      }
      cout << endl;

      // Ingresar categorias
      cout << "\033[1;34m ** Actualizar categorias: \033[1;37m";
      this->showCategories(service);
      cout << "\033[1;34m (y/N): \033[0m ";
      getline(cin, temp);
      if((temp.compare("Y") == 0) || (temp.compare("y") == 0)){
        cout << "\033[1;34m ** Categorias: \033[0m" << endl;
        this->category_manager->listCategoriesByName();
        cout << "\033[0;33m    Seleccione categorias: \033[0m";
        getline(cin, temp);

        size = temp.length() + 1 + sizeof(int);
        new_category_ids = new char[temp.length() + 1];
        strcpy(new_category_ids, temp.c_str());
        new_service->category_ids = new RegistroGenerico(size, new_category_ids);
      }else{
        new_service->category_ids  = new RegistroGenerico();
        service->category_ids->clone(new_service->category_ids);
      }
      cout << endl;

      cout << "\033[1;34m ** Actualizar tipo de servicio \033[1;37m"<< service->mapType() <<"\033[1;34m (y/N): \033[0m ";
      getline(cin, temp);
      if((temp.compare("Y") == 0) || (temp.compare("y") == 0)){
        cout << "\033[1;37m  1) Gratis 2) Precio Fijo 3) Subasta \033[0m" << endl;
        while(!valid_option){
          cout << "\033[0;33m  Opcion: \033[0m";
          getline(cin, temp);
          option = atoi(temp.c_str());
          switch(option){
            case 1:
              new_service->type   = GRATIS;
              valid_option = true;
              break;
            case 2:
              new_service->type   = PRECIO_FIJO;
              valid_option = true;
              break;
            case 3:
              new_service->type   = SUBASTA;
              valid_option = true;
              break;
            default:
              cout << " Opcion invalida" << endl;
              valid_option = false;
          }
        }
      }else{
        new_service->type = service->type;
      }
      cout << endl;

      service_size    = new_service->getSize();
      service_content = new char[service_size + sizeof(int)]();
      new_service->serialize(service_content);

      registro->setSize(service_size + sizeof(int)*2); // datos, key, size
      registro->setKey(new_service->id);
      registro->setDato(service_content);

      cout << "\033[1;33m ** Esta seguro de actualizar el servicio? (y/N):\033[0m ";
      getline(cin, temp);

      if((temp.compare("Y") == 0) || (temp.compare("y") == 0)){
        res = this->hashing->updateRegistro(registro);


        switch(res){
          case SUCCESS_SAVE_RECORD:
            cout << "\033[1;32m OK...\033[0m" << endl;
            this->update_index(new_service, service);
            res = SUCCESS_SAVE_RECORD;
            break;
          case ERROR_SAVE_RECORD:
            cout << "\033[1;31m Fallo... Error al guardar y no se modifico el servicio\033[0m" << endl;
            res = ERROR_SAVE_RECORD;
            break;
          case INVALID_RECORD:
            res = INVALID_RECORD;
            break;
        }
      }else{
        res = -5;
        delete(registro);
      }
      if(new_service)
        delete(new_service);

    }else
      res = NOT_OWNER;

    if(service)
      delete(service);

  }else
    cout << "\033[1;31m No existe el servicio con id \033[0m" << id << endl;

  new_service = this->findService(id);

  return new_service;
}

void ServiceManager::showService(int id){
  User *proveedor;
  string option;
  int action;
  bool valid_option = false;
  Service *service = this->findService(id);

  if(service){
    this->printService(service);
    proveedor = this->user_manager->findUser(service->proveedor_id);
    while(!valid_option){
      if(this->current_user->dni == service->proveedor_id)
        cout << "\033[1;36m   0) Cancelar 1) Ver preguntas 2) Responder o Borrar preguntas 3) Ver cotizaciones: \033[0m";
      else if(this->current_user->getRole() == ADMIN_ROLE)
        cout << "\033[1;36m   0) Cancelar 1) Ver preguntas 2) Moderar preguntas : \033[0m";
      else
        cout << "\033[1;36m   0) Cancelar 1) Ver preguntas 2) Hacer una preguntar 3) Pedir cotizacion: \033[0m";

      getline(cin, option);
      action = atoi(option.c_str());
      switch(action){
        case 0:
          valid_option = true;
          cout << "   Accion cancelada..." << endl;
          break;
        case 1:
          valid_option = true;
          this->message_manager->listMessages(service->id);
          break;
        case 2:
          valid_option = true;
          if(this->current_user->getRole() == ADMIN_ROLE)
            this->message_manager->moderateMessages(service->id);
          else if(this->current_user->dni == service->proveedor_id)
            this->message_manager->respondMessages(service->id);
          else
            this->message_manager->addMessage(this->current_user->dni, service->id);
          break;
        case 3:
          if(this->current_user->getRole() == ADMIN_ROLE){
            valid_option = false;
          }else{
            valid_option = true;
            if(this->current_user->dni == service->proveedor_id)
              this->quote_manager->listQuotes(service->id);
            else
              this->quote_manager->addQuote(this->current_user, service->id, proveedor);
          }
          break;
        default:
          valid_option = false;
      }
    }
    delete service;
    delete proveedor;
  }
}

int ServiceManager::deleteService(int id){
  int res;
  Service *service;
  bool can_delete = false;

  service  = this->findService(id);
  if(service){
    if(service->proveedor_id == this->current_user->dni)
      can_delete = true;
    else
      can_delete = false;

 }

  if(can_delete){
    res = this->hashing->deleteRegistro(id);
    if(res == 0){
    	ClaveRegIDUsIDServ *clave_IdProv= new ClaveRegIDUsIDServ(service->proveedor_id, service->id);
		this->indice_por_proveedor->eliminar(clave_IdProv);
		delete(clave_IdProv);
	}

    vector<string> s_ids;
    Category *category;
    ClaveRegCatIDServ *clave;
    string category_name;
    int result;

    Funciones::split(service->category_ids->getDato(), ",", s_ids);
    //elimino las claves del indice
    if(!s_ids.empty()){
    	for (unsigned int i = 0; i < s_ids.size(); ++i){
    		category = this->category_manager->findCategory( atoi(s_ids.at(i).c_str()) );
    		if(category){
			category_name = category->getName();
			clave     = new ClaveRegCatIDServ(service->id, category_name);
			result        = this->indice_por_categoria->eliminar(clave);
			if(result != 0)
			  cout << "Error al indexar categoria: " << category_name << endl;
			delete(category);
			delete(clave);
    		}
    	}
    }




  }
  else{
    res = NOT_OWNER;
  }

  delete service;

  return res;
}

void ServiceManager::printService(Service *service){
  cout << endl;
  cout << "\033[1;34m * Id:          \033[0m " << service->id           << endl;
  cout << "\033[1;34m * Proveedor:   \033[0m " << service->proveedor_id << endl;
  cout << "\033[1;34m * Nombre:      \033[0m " << service->getName()    << endl;
  cout << "\033[1;34m * Tipo:        \033[0;32m " << service->mapType() << "\033[0m" << endl;
  cout << "\033[1;34m * Categorias:  \033[1;33m ";
  this->showCategories(service);
  cout << "\033[0m" << endl;
  cout << "\033[1;34m * Descripcion: \033[0m" << endl;
  cout << "\n   " << service->getDescription() << endl;
  cout << "   -------------------------------------------------------------" << endl << endl;

}

void ServiceManager::listServices(){
  Service *service;
  vector<RegistroId*> registros;
  RegistroId *registro;
  unsigned int i = 1;
  bool cancel = false;
  string temp;

  registros = this->hashing->getAll();
  cout << "\033[1;33m\n Servicios ("<< registros.size() << "):\033[0m" << endl;

  while((i < registros.size() + 1) && !cancel){
    if(i%4 == 0){
      cout << "\033[0;33m Continuar viendo? (Y/n): \033[0m";
      getline(cin, temp);
      if(temp.compare("n") == 0)
        cancel = true;
      else
        cancel = false;
    }
    if(!cancel){
      registro = registros.at(i-1);
      service = new Service(registro->getKey(), registro->getDato());
      this->printService(service);
      delete(service);
      i++;
    }
  }
  cout <<  endl;

  // clean vector
  for(i = 0; i < registros.size(); i++){
    registro = registros.at(i);
    delete(registro);
  }

  registros.clear();
}

void ServiceManager::listServicesByUser(string user_id){
  Service *service;
  vector <unsigned long> ids;
  int id;

  id  = atoi(user_id.c_str());
  ids = this->search_by_user(id);

  if(ids.size() > 0){
    cout << "\033[1;33m\n Servicios ("<< ids.size() << "):\033[0m" << endl;

    for(unsigned int i = 0; i < ids.size(); i++){
      service = this->findService(ids.at(i));
      if(service){
        this->printService(service);
        delete(service);
      }
    }
    cout <<  endl;
    ids.clear();
  }else{
    cout << "\033[1;33m No hay servicios creados por "<< user_id <<"\033[0m" << endl;
  }
}

void ServiceManager::listServicesByCategory(){
  Service *service;
  vector <unsigned long> ids;
  string category_name, temp;
  Category *category;
  int cat_id;

  cout << endl;
  this->category_manager->listCategoriesByName();
  cout << "\033[0;33m    Seleccione categorias: \033[0m";
  getline(cin, temp);
  cat_id = atoi(temp.c_str());
  category = this->category_manager->findCategory(cat_id);

  if(category){
    ids = this->search_by_category(category->getName());

    if(ids.size() > 0){
      cout << "\033[1;33m\n Servicios ("<< ids.size() << "):\033[0m" << endl;

      for(unsigned int i = 0; i < ids.size(); i++){
        service = this->findService(ids.at(i));
        if(service){
          this->printService(service);
          delete(service);
        }
      }
      ids.clear();
    }else{
      cout << endl << "\033[1;33m    No hay servicios con esta categoria\033[0m" << endl;
    }

    delete(category);

  }else{
    cout << endl << "    Catgoria no encontrada..." << endl;
  }

  cout <<  endl;
}

void ServiceManager::showCategories(Service *service){
  Category *category;
  vector<string> s_ids;

  Funciones::split(service->category_ids->getDato(), ",", s_ids);
  if(!s_ids.empty()){
    for (unsigned int i = 0; i < s_ids.size(); ++i){
      category = this->category_manager->findCategory( atoi(s_ids.at(i).c_str()) );
      if(category){
        cout << category->getName();
        delete(category);
      }

      if(i+1 != s_ids.size())
        cout << ", ";
    }
  }else{
    cout << "\033[1;33m Sin categoria ";
  }
}

// indice por categoria
void ServiceManager::add_to_index_by_category(Service *service){
  vector<string> s_ids;
  Category *category;
  ClaveRegCatIDServ *new_clave;
  string category_name;
  int result;

  Funciones::split(service->category_ids->getDato(), ",", s_ids);

  if(!s_ids.empty()){
    for (unsigned int i = 0; i < s_ids.size(); ++i){
      category = this->category_manager->findCategory( atoi(s_ids.at(i).c_str()) );
      if(category){
        category_name = category->getName();
        new_clave     = new ClaveRegCatIDServ(service->id, category_name);
        result        = this->indice_por_categoria->insertar(new_clave);
        if(result != 0)
          cout << "Error al indexar categoria: " << category_name << endl;
        delete(category);
      }
    }
  }
}

vector<unsigned long> ServiceManager::search_by_category(string category_name){
  ClaveRegCatIDServ *clave;
  ClaveRegistroArbol *current_clave;
  vector <unsigned long> ids;
  string categoria;
  unsigned long servicio_id;

  clave = new ClaveRegCatIDServ(0, category_name);
  current_clave = this->indice_por_categoria->buscarAproximado(clave);

  // traemos las claves hasta que cambia la clave de busqueda.
  while(current_clave != NULL){
    categoria = ((ClaveRegCatIDServ*) current_clave)->getNombreCategoria();
    if(category_name.compare(categoria) ==  0){
      servicio_id   = ((ClaveRegCatIDServ*) current_clave)->getIdServicio();
      ids.push_back(servicio_id);
      current_clave = this->indice_por_categoria->siguiente();
    }else{
      current_clave = NULL;
    }
  }

  delete clave;
  return ids;
}

// indice por usuario
void ServiceManager::add_to_index_by_user(Service *service){
  ClaveRegIDUsIDServ *new_clave;
  int result;

  new_clave = new ClaveRegIDUsIDServ(service->id, service->proveedor_id);

  result = this->indice_por_proveedor->insertar(new_clave);
  if(result != 0)
    cout << "Hubo un problema indexando al usuario" << endl;
}

vector<unsigned long> ServiceManager::search_by_user(int user_id){
  ClaveRegIDUsIDServ *clave;
  ClaveRegistroArbol *current_clave;
  vector <unsigned long> ids;
  unsigned long servicio_id;

  clave = new ClaveRegIDUsIDServ(0, user_id);
  current_clave = this->indice_por_proveedor->buscarAproximado(clave);

  while(current_clave != NULL){
    if(user_id == (int)((ClaveRegIDUsIDServ*) current_clave)->getIdUsuario()){
      servicio_id = ((ClaveRegIDUsIDServ*) current_clave)->getIdServicio();
      ids.push_back(servicio_id);
      current_clave = this->indice_por_proveedor->siguiente();
    }else{
     current_clave = NULL;
    }
  }

  delete clave;
  return ids;
}

void ServiceManager::update_index(Service *new_service, Service *service){
  vector<string> s_ids;
  Category *category;
  ClaveRegCatIDServ *old_clave;
  string category_name;
  int result;
  vector<string> new_s_ids;
  ClaveRegCatIDServ *new_clave;

  Funciones::split(service->category_ids->getDato(), ",", s_ids);
  //elimino las claves viejas del indice
  if(!s_ids.empty()){
    for (unsigned int i = 0; i < s_ids.size(); ++i){
      category = this->category_manager->findCategory( atoi(s_ids.at(i).c_str()) );
      if(category){
        category_name = category->getName();
        old_clave     = new ClaveRegCatIDServ(service->id, category_name);
        result        = this->indice_por_categoria->eliminar(old_clave);
        if(result != 0)
          cout << "Error al indexar categoria: " << category_name << endl;
        delete(category);
        delete(old_clave);
      }
    }
  }

  Funciones::split(new_service->category_ids->getDato(), ",", new_s_ids);

  //inserto las claves nuevas en el indice
  if(!new_s_ids.empty()){
    for (unsigned int i = 0; i < new_s_ids.size(); ++i){
      category = this->category_manager->findCategory( atoi(new_s_ids.at(i).c_str()) );
      if(category){
        category_name = category->getName();
        new_clave     = new ClaveRegCatIDServ(new_service->id, category_name);
        result        = this->indice_por_categoria->insertar(new_clave);
        if(result != 0)
          cout << "Error al indexar categoria: " << category_name << endl;
        delete(category);
      }
    }
  }
}

