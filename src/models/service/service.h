#ifndef _SERVICE_H_
#define _SERVICE_H_

#include "../../lib/RegistroGenerico.h"
#include "../category/category.h"

class Service{
	public:
		int id;
		int proveedor_id;
		int type;
    RegistroGenerico *name;
		RegistroGenerico *description;
    RegistroGenerico *category_ids;

		Service(int proveedor_id);
    Service(int key, char* regisro);
    virtual ~Service();

    int  getSize();
    int  getProveedorId();
    int  getType();
    char *getName();
    char *getDescription();

    string mapType();

    void serialize(char *buffer);
    void deserialize(char *buffer);

    bool validateName();
    bool validateDescription();
};

#endif
