#ifndef SERVICEMANAGER_H_
#define SERVICEMANAGER_H_

#include "service.h"
#include "../users/user.h"
#include "../category/category_manager.h"
#include "../message/message_manager.h"
#include "../quotes/quote_manager.h"
#include "../../lib/hashing/Hashing.h"
#include "../../arbol/Arbol.h"

class ServiceManager{
  public:
    Hashing *hashing;
    char *path;
    User *current_user;
    UserManager    *user_manager;
    MessageManager *message_manager;
    QuoteManager *quote_manager;
    CategoryManager *category_manager;
    Arbol *indice_por_proveedor;
    Arbol *indice_por_categoria;

    ServiceManager(int size);
    ServiceManager(User *current_user);
    ServiceManager();
    virtual ~ServiceManager();

    Service *addService(int proveedor_id);
    Service *findService(int id);
    Service *editService(int id);
    void showService(int id);
    int deleteService(int id);

    void listServices();
    void listServicesByCategory();
    void listServicesByUser(string user_id);

    void printService(Service *service);
    void printServiceRow(Service *service);
    void showCategories(Service *service);

    // indices
    void  add_to_index_by_category(Service *service);
    vector <unsigned long> search_by_category(string category);
    void  add_to_index_by_user(Service *service);
    vector <unsigned long> search_by_user(int user_id);

    void update_index(Service *service, Service *old_service);
};

#endif
