#ifndef   _CLI_H_
#define   _CLI_H_

#include "../lib/Constantes.h"
#include "../utils/Funciones.h"
#include "../models/users/user_manager.h"
#include "../models/service/service_manager.h"
#include "../models/category/category_manager.h"

class Cli{
  public:
    UserManager *user_manager;
    ServiceManager *service_manager;
    CategoryManager *category_manager;
    User *current_user;
    int status;

    // constructor
    Cli();
    virtual ~Cli();

    // shell init
    int shell();
    int login();
    void logout();

    // command parser and execution
    void split(const string& str, const string& delim, vector<string>& parts);
    int search_command(string command);
    void process(std::string const &line);
    void string_prompt(User *current_user);

    // manage actions
    void user_actions(vector<string> argv);
    void service_actions(vector<string> argv);
    void category_actions(vector<string> argv);

    // help commands
    void help_users();
    void help_categories();
    void help_messages();
    void help_services();
    int print_help(vector<string> argv);

    void printHour(tm *local_time);
    void printHourTime(tm *local_time);

};

#endif
