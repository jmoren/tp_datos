#include "Funciones.h"

char* Funciones::getStringToChar(string cadena){
  char * charCadena = new char [cadena.length()+1];
  strcpy (charCadena, cadena.c_str());
  return charCadena;
}

void Funciones::split(const string& str, const string& delim, vector<string>& parts) {
  size_t start, end = 0;
  while (end < str.size()) {
    start = end;
    while (start < str.size() && (delim.find(str[start]) != string::npos)) {
      start++;
    }
    end = start;
    while (end < str.size() && (delim.find(str[end]) == string::npos)) {
      end++;
    }
    if (end-start != 0) {
      parts.push_back(string(str, start, end-start));
    }
  }
}

bool Funciones::fileExists(string name) {
  ifstream f(name.c_str());
  if (f.good()) {
    f.close();
    return true;
  } else {
    f.close();
    return false;
  }
}

tm *Funciones::datetimeToLocal(time_t my_time){
  tm* local_time = localtime(&my_time);
  return local_time;
}

void Funciones::printHour(time_t my_time){
  tm *local_time = datetimeToLocal(my_time);
  char buffer[20];

  strftime (buffer,20,"%R hs ",local_time);

  cout << buffer;
}

void Funciones::printHourTime(time_t my_time){
  tm *local_time = datetimeToLocal(my_time);
  char buffer[80];

  strftime (buffer,80,"%d/%m/%Y - %R hs",local_time);

  cout << buffer;
}

char* Funciones::dateToString(time_t my_time){
  tm *local_time = datetimeToLocal(my_time);
  string date;
  char *buffer = new char[9];

  strftime (buffer,9,"%Y%m%d",local_time);

  return buffer;
}

char* Funciones::hourToString(time_t my_time){
  tm *local_time = datetimeToLocal(my_time);
  string date;
  char *buffer = new char[5];

  strftime (buffer,5,"%H%M",local_time);

  return buffer;
}

string Funciones::read_master_key(){
  ifstream config;
  string key;
  config.open ("config.txt");
  if (config.is_open()) {
    while (!config.eof()) {
      config >> key;
    }
    config.close();
  }

  return key;
}
