#include "cli.h"

Cli::Cli(){
  this->user_manager     = NULL;
  this->current_user     = NULL;
  this->service_manager  = NULL;
  this->category_manager = NULL;
  this->status           = CLI_ON;
}

Cli::~Cli(){
  if(this->user_manager)
    delete this->user_manager;
  if(this->current_user)
    delete this->current_user;
}


void Cli::string_prompt(User *current_user){
  time_t now = time(0);
  cout << "\033[1;37m [" <<  this->current_user->getName();
  cout << "]\033[1;34m :: [ paginas_doradas ]\033[0m - ";
  Funciones::printHour(now);
  cout << ": ";
}

void Cli::logout(){
  delete this->current_user;
  this->current_user = NULL;
}

int Cli::login(){
  int result;
  int option   = 0;
  bool cancel  = false;
  string temp;
  time_t now;

  while((this->current_user == NULL) && (!cancel)){
    cout << "\033[1;34m Ingrese una opcion: \n\033[0m 1) Ingresar 2) Registrarse 0) Salir " << endl;
    cout << " Opcion: " ;
    getline(cin, temp);
    option = atoi(temp.c_str());
    switch(option){
      case 0:
        cancel = true;
        this->status = CLI_OFF;
        result = COMMAND_CANCEL;
        cout << "\n Adios!" << endl << endl;
        break;
      case 1:
        this->current_user = this->user_manager->login_user();
        if(this->current_user){
          now = time(0);
          this->status = CLI_ON;
          cout << "\n\033[0;32m Bienvenido nuevamente " << this->current_user->getName() << " ";
          cout << this->current_user->getLastName() << " - \033[1;37m";
          Funciones::printHourTime(now);
          cout << "\033[0m" << endl << endl;
          result = SUCCESS_LOGIN;
        }
        break;
      case 2:
        this->current_user = this->user_manager->addUser(USER_ROLE);
        if(this->current_user){
          this->status = CLI_ON;
          result = SUCCES_NEW_USER;
        }
        break;
      default:
        cout << "\033[0;31m Opcion invalida \033[0m" << endl;
    }
  }
  return result;
}

int Cli::shell(){
  string line;
  int result;
  this->user_manager = new UserManager();

  while(this->status == CLI_ON){
    if(this->current_user == NULL){
      result = this->login();
    }else{
      string_prompt(this->current_user);
      while(getline(cin, line) && (line != "exit") && (this->current_user != NULL)){
        if (!line.empty()) {
          this->process(line);
        }
        if(this->current_user != NULL)
          string_prompt(this->current_user);
      }
      if(line == "exit"){
        cout << " Adios " << this->current_user->getName() << " " << this->current_user->getLastName() << endl;
        this->logout();
        this->status = CLI_OFF;
      }
      result = 0;
    }
  }

  return result;
}

void Cli::category_actions(vector<string> argv){
  string action, path;
  int id;
  this->category_manager = new CategoryManager();

  switch(argv.size()){
    case 2:
      action = argv[1];
      if(action == "-l"){
        this->category_manager->listCategories();
      }else if(action == "-s"){
        this->category_manager->search_by_words();
      }else if(action == "-a"){

        if(this->current_user->getRole() == ADMIN_ROLE){
          Category *category;
          category = this->category_manager->addCategory();
          if(category){
            this->category_manager->printCategory(category);
            delete category;
          }
        }else
          cout << "\033[0;31m No tenes permisos para hacer esta accion\033[0m" << endl;

      }else{
        cout << "\033[1;33m Opcion desconocida...\033[0m" << endl << endl;
        this->help_categories();
      }
      break;
    case 3:
      action = argv[1];
      if(action == "-f"){

        if(this->current_user->getRole() == ADMIN_ROLE){
          path   = argv[2];
          int total;
          total = this->category_manager->bulk_load(path);
          cout << "\033[1;32m Se cargaron\033[1;37m " << total << "\033[1;32m categorias\033[0m" << endl;
        }else
          cout << "\033[0;31m No tenes permisos para hacer esta accion\033[0m" << endl;

      }else if(action == "-v"){

        id = atoi(argv[2].c_str());
        Category *category;
        category = this->category_manager->findCategory(id);
        if(category){
          this->category_manager->printCategory(category);
          delete(category);
        }

      }else if(action == "-e"){

        if(this->current_user->getRole() == ADMIN_ROLE){
          id = atoi(argv[2].c_str());
          Category *category;
          category = this->category_manager->editCategory(id);
          if(category){
            this->category_manager->printCategory(category);
            delete(category);
          }
        }else
          cout << "\033[0;31m No tenes permisos para hacer esta accion\033[0m" << endl;

      }else if(action == "-d"){

        if(this->current_user->getRole() == ADMIN_ROLE){
          id = atoi(argv[2].c_str());
          this->category_manager->deleteCategory(id);
        }else
          cout << "\033[0;31m No tenes permisos para hacer esta accion\033[0m" << endl;

      }else{
        cout << "\033[1;33m Opcion desconocida...\033[0m" << endl << endl;
        this->help_categories();
      }
      break;
    default:
      cout << "\033[0;31m Cantidad de opciones invalida.\033[0m" << endl;
  }

  delete this->category_manager;
}

void Cli::user_actions(vector<string> argv){
  string action, value, field;
  int id, res;

  switch(argv.size()){
    case 2:
      action = argv[1];
      if(action == "-e")
        this->current_user = this->user_manager->editUser(this->current_user, this->current_user->getRole());
      else if(action == "-v")
        this->user_manager->printUser(this->current_user);
      else if(action == "-d"){
        res = this->user_manager->deleteUser(this->current_user->dni);
        if(res == 0){
          this->logout();
          cout << "\033[1;33m Se ha eliminado del sistema. Vuelva a registrarse para ingresar nuevamente...\033[0m" << endl << endl;
        }
      }else if(action == "-l")
        this->user_manager->listUsers();
      else if(action == "-a"){
        if(this->current_user->getRole() == ADMIN_ROLE){
          User *new_user;
          new_user = this->user_manager->addUser(NO_ROLE);
          this->user_manager->printUser(new_user);
          delete(new_user);
        }else{
          cout << "\033[0;31m No tenes permisos para hacer esta accion\033[0m" << endl;
        }
      }else{
        cout << "\033[1;33m Opcion desconocida...\033[0m" << endl << endl;
        this->help_users();
      }
      break;
    case 3:
      action = argv[1];

      if(action == "-s"){
        field = argv[2];
        if(field == "provincia"){
          this->user_manager->list_by_provincia();
        }else{
          cout << "\033[1;33m Campo desconocida...\033[0m" << endl << endl;
        }
      }else if(current_user->getRole() == ADMIN_ROLE){
        id = atoi(argv[2].c_str());
        if(action == "-e"){
          this->user_manager->editUser(id);
          this->user_manager->showUser(id);
        }else if(action == "-v")
          this->user_manager->showUser(id);
        else if(action == "-d")
          this->user_manager->deleteUser(id);
        else if(action == "-a")
          this->user_manager->addUser(NO_ROLE);
        else{
          cout << "\033[1;33m Opcion desconocida...\033[0m" << endl << endl;
          this->help_users();
        }
      }else{
        cout << "\033[0;31m No tenes permisos para hacer esta accion\033[0m" << endl;
      }
      break;
    case 4:
      action = argv[1];
      field  = argv[2];
      value  = argv[3];
      if(current_user->getRole() == ADMIN_ROLE){
        if(action == "-s"){
          if(field == "provincia"){
            this->user_manager->list_by_provincia();
          }else if(field == "rol"){
            this->user_manager->list_by_role(value);
          }else{
            cout << "\033[1;31m Campo desconocido.\033[0m" << endl;
          }
        }else{
          cout << "\033[1;33m Opcion desconocida...\033[0m" << endl << endl;
          this->help_users();
        }
      }else{
        cout << "\033[0;31m No tenes permisos para hacer esta accion\033[0m" << endl;
      }
      break;
    default:
      cout << "\033[1;31m Cantidad de opciones invalida.\033[0m" << endl;
  }
}

void Cli::service_actions(vector<string> argv){
  this->service_manager = new ServiceManager(this->current_user);
  string action, value, field;
  int id, res = 0;

  switch(argv.size()){
    case 2:
      action = argv[1];
      if(action == "-a"){
        if(current_user->getRole() == PROVEEDOR_ROLE){
          Service *service;
          service = this->service_manager->addService(this->current_user->dni);
          delete(service);
        }else
          cout << "\033[0;31m No tenes permisos para hacer esta accion\033[0m" << endl;
      }else if(action == "-l")
        this->service_manager->listServices();
      else{
        cout << "\033[1;33m Opcion desconocida...\033[0m" << endl << endl;
        this->help_services();
      }
      break;
    case 3:
      action = argv[1];
      if(action == "-s"){
        field = argv[2];
        if(field == "categoria")
          this->service_manager->listServicesByCategory();
        else
          cout << "\033[1;31m Campo desconocido.\033[0m" << endl;
      }else{
        id = atoi(argv[2].c_str());

        if(action == "-e"){
          if(current_user->getRole() == PROVEEDOR_ROLE){
            Service *service;
            service = this->service_manager->editService(id);
            this->service_manager->printService(service);
            delete(service);
          }else
            cout << "\033[0;31m No tenes permisos para hacer esta accion\033[0m" << endl;
        }else if(action == "-d"){
          if(current_user->getRole() == PROVEEDOR_ROLE){
            res = this->service_manager->deleteService(id);
            if(res == NOT_OWNER)
              cout << "\033[1;31m No puede eliminar el servicio porque le pertenece a otro proveedor\033[0m" << endl;

          }else
            cout << "\033[0;31m No tenes permisos para hacer esta accion\033[0m" << endl;

        }else if(action == "-v")
          this->service_manager->showService(id);

        else{
          cout << "\033[1;33m Opcion desconocida...\033[0m" << endl << endl;
          this->help_services();
        }
      }
      break;
    case 4:
      action = argv[1];
      field  = argv[2];
      value  = argv[3];
      if(action == "-s"){
        if(field == "usuario")
          this->service_manager->listServicesByUser(value);
        else
          cout << "\033[1;31m Campo desconocido.\033[0m" << endl;
      }else{
        cout << "\033[1;33m Opcion desconocida...\033[0m" << endl << endl;
        this->help_users();
      }
      break;
    default:
      cout << " Cantidad de opciones invalida." << endl;
  }

  delete service_manager;
}

void Cli::help_users(){
  if(current_user->getRole() == ADMIN_ROLE){
    cout << setw(40) << left << "\033[1;37m > usuario -a      " << "\033[0;37m# Agregar usuario al sistema\033[0m\n";
    cout << setw(40) << left << "\033[1;37m > usuario -v <id> " << "\033[0;37m# Ver un usuario\033[0m\n";
    cout << setw(40) << left << "\033[1;37m > usuario -e <id> " << "\033[0;37m# Editar un usuario\033[0m\n";
    cout << setw(40) << left << "\033[1;37m > usuario -d <id> " << "\033[0;37m# Eliminar un usuario del sistema\033[0m\n";
  }
  cout << setw(40) << left << "\033[1;37m > usuario -v " << "\033[0;37m# Ver perfil\033[0m\n";
  cout << setw(40) << left << "\033[1;37m > usuario -e " << "\033[0;37m# Editar perfil\033[0m\n";
  cout << setw(40) << left << "\033[1;37m > usuario -d " << "\033[0;37m# Eliminar perfil\033[0m\n";
  cout << setw(40) << left << "\033[1;37m > usuario -l " << "\033[0;37m# Listar usuarios del sistema\033[0m\n";
    cout << setw(40) << left << "\033[1;37m > usuario -s <campo> <valor> " << "\033[0;37m# Buscar usuarios [provincia|rol] [Admin|Proveedor|Usuario]\033[0m\n";
  cout << endl;
}

void Cli::help_categories(){
  if(current_user->getRole() == ADMIN_ROLE){
    cout << setw(40) << left << "\033[1;37m > categoria -a        "  << "\033[0;37m# Agregar una categoria\033[0m\n";
    cout << setw(40) << left << "\033[1;37m > categoria -e <id>   "  << "\033[0;37m# Editar categoria\033[0m\n";
    cout << setw(40) << left << "\033[1;37m > categoria -d <id>   "  << "\033[0;37m# Eliminar categoria\033[0m\n";
    cout << setw(40) << left << "\033[1;37m > categoria -f <path> "  << "\033[0;37m# Cargar categorias desde un archivo\033[0m\n";
  }
  cout << setw(40) << left << "\033[1;37m > categoria -v  <id> " << "\033[0;37m# Ver una categoria\033[0m\n";
  cout << setw(40) << left << "\033[1;37m > categoria -l       " << "\033[0;37m# Listar categorias\033[0m\n";
  cout << setw(40) << left << "\033[1;37m > categoria -s       " << "\033[0;37m# Filtrar categorias\033[0m\n";
  cout << endl;
}

void Cli::help_services(){
  if(current_user->getRole() == PROVEEDOR_ROLE){
    cout << setw(40) << left << "\033[1;37m > servicio -a "      << "\033[0;37m# Agregar servicio  \033[0m\n";
    cout << setw(40) << left << "\033[1;37m > servicio -e <id> " << "\033[0;37m# Editar servicio   \033[0m\n";
    cout << setw(40) << left << "\033[1;37m > servicio -d <id> " << "\033[0;37m# Eliminar servicio \033[0m\n";
  }
  cout << setw(40) << left << "\033[1;37m > servicio -v <id> " << "\033[0;37m# Ver servicio \033[0m\n";
  cout << setw(40) << left << "\033[1;37m > servicio -l      " << "\033[0;37m# Listar servicios \033[0m\n";
  cout << setw(40) << left << "\033[1;37m > servicio -s <campo> <valor> " << "\033[0;37m# Buscar servicio [categoria|usuario valaor]\033[0m\n";
  cout << endl;
}

int Cli::print_help(vector<string> argv){
  int res = 0;
  int argc;
  string option;

  argc = argv.size();
  switch(argc){
    case 1:
      cout << endl;
      cout << setw(40) << left << "\033[1;37m > salir " << "\033[0;37m# Desloguearse del sistema.\033[0m\n";
      cout << setw(40) << left << "\033[1;37m > exit  " << "\033[0;37m# Desloguearse y salir del sistema.\033[0m\n";
      cout << endl;
      this->help_users();
      this->help_categories();
      this->help_services();
      break;
    case 2:
      option = argv.back();
      if(option == "usuario")
        this->help_users();
      else if(option == "categoria")
        this->help_categories();
      else if(option == "servicio" )
        this->help_services();
      else{
        cout << "\033[1;33m Opcion desconocida...\033[0m" << endl;
        res = -1;
      }
      break;
    default:
      cout << " Cantidad de opciones invalida." << endl;
      res = -2;
  }

  return res;
}

int Cli::search_command(string command){
  int i = 0;
  int found = COMMAND_NOT_FOUND;
  string commands[6] = { "ayuda", "usuario","categoria", "servicio", "salir" };

  while((found == COMMAND_NOT_FOUND) && (i < 6)){
    if(command == commands[i]){
      found = 0;
    }
    i++;
  };
  return found;
}


void Cli::process(std::string const &line){
  string command;
  vector<string> argv;

  Funciones::split(line, " ", argv);
  command = argv[0];

  if(command == "ayuda")
    this->print_help(argv);
  else if(command == "salir"){
    cout << " Te has deslogueado "<< this->current_user->getName() << " " << this->current_user->getLastName() << "!.(Enter para continuar)\n";
    this->logout();
  }else if(command == "usuario")
    this->user_actions(argv);
  else if(command == "categoria")
    this->category_actions(argv);
  else if(command == "servicio")
    this->service_actions(argv);
  else
    cout << "\033[1;31m Comando desconocido... Para ver los comandos disponibles, utilice el comando 'ayuda'.\033[0m\n";
}
