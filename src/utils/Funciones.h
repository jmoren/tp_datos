#ifndef __FUNCIONES_H__
#define __FUNCIONES_H__

#include "../lib/Constantes.h"

class Funciones {

public:
	static char* getStringToChar(string cadena);
	static void split(const string& str, const string& delim, vector<string>& parts);
  static bool fileExists(string name);
  static tm *datetimeToLocal(time_t my_time);
  static void printHourTime(time_t my_time);
  static void printHour(time_t my_time);

  static char* dateToString(time_t my_time);
  static char* hourToString(time_t my_time);
  static string read_master_key();
};

#endif
