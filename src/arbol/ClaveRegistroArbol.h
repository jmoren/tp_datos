#ifndef CLAVEREGISTROARBOL_H_
#define CLAVEREGISTROARBOL_H_

#include "Persistente.h"

class ClaveRegistroArbol: public Persistente {
public:
	ClaveRegistroArbol();
	virtual ~ClaveRegistroArbol();

	//devuelve 0 si son iguales, 1 si es mayor y -1 si es menor
	virtual short int comparar(ClaveRegistroArbol* claveParaComparar)=0;
	virtual unsigned int getTamanio()=0;
	virtual char* serializar()=0;
	virtual void deserializar(char*)=0;

	// Sobrecarga del operador <<
	friend std::ostream& operator<< (std::ostream& stream, ClaveRegistroArbol& clave){
		stream << clave.getDatosImportantes();
	    return stream;
	}
	virtual string getDatosImportantes()=0;

};

#endif /* CLAVEREGISTROARBOL_H_ */
