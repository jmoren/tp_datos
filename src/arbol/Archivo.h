/*
 * Archivo.h
 *
 *  Created on: 05/10/2013
 *      Author: natalia
 */

#ifndef ARCHIVO_H_
#define ARCHIVO_H_

#include "Bloque.h"
#include "../lib/RegistroGenerico.h"
#include "../lib/FileBlock.h"


/**
 * Se considera que archivo sera un archivo de bloques de longitud fija con una lista de RegistroGenericos que guardaran
 * la cantidad de byte sizeof(unsigned int) = 4bytes para guardar la longitud del dato del registro y luego tantos caracteres
 * como la longitud del dato del registro que sera un char*
 */
class Archivo {

protected:
	unsigned int tamanioBloque;
	unsigned int tamanioDatosDeControlBloque; // Es la cantidad de bytes que ocupan los datos de control de cada bloque del archivo
	unsigned int tamanioDatosDeControlRegistro; // Es la cantidad de bytes que ocupan los datos de control de cada registro
	unsigned int cantidadTotalBloques;
	TipoRegistro tipoRegistro;

public:
	Archivo(); // No usar
	Archivo(TipoRegistro tipoRegistro, unsigned int tamanioBloque);
	virtual ~Archivo();

	virtual bool existeArchivoFisico();
	virtual void crearArchivo();

	virtual unsigned int getTamanioBloque();
	virtual unsigned int getTamanioDatosDeControlBloque();
	virtual unsigned int getTamanioDatosDeControlRegistro();
	unsigned int getTamanioTotalDisponibleParaRegistros();
	virtual unsigned int getCantidadTotalBloques();
	virtual unsigned int getIdBloqueLibre();
	virtual bool existenBloquesLibres();

	virtual Bloque* getBloque(unsigned int nroBloque);
	virtual Bloque* getRaiz();

	virtual void borrarBloque(unsigned int nroBloque);
	virtual void grabarBloque(Bloque* bloque);

	virtual void imprimir();

};

#endif /* ARCHIVO_H_ */
