/*
 * RegistroNodoInterno.cpp
 *
 *  Created on: 06/10/2013
 *      Author: natalia
 */

#include "RegistroNodoInterno.h"

RegistroNodoInterno::RegistroNodoInterno(ClaveRegistroArbol* clave): RegistroNodo(clave){
	this->idNodoIzq = 0;
	this->idNodoDer = 0;
}

/*
 * En este metodo no se setea el idNodoIzquierdo dado que la idea es que se haga desde el NodoInterno.
 * Y el idNodoDerecho se hace en el deserializar que llama el constructor padre.
 */
RegistroNodoInterno::RegistroNodoInterno(TipoRegistro tipoRegistro, char *dato, unsigned int tamanioDato): RegistroNodo(){
	// Creo la clave sin deserializarla porque no tengo el dato preprado para ello aun.
	this->clave = FabricaClaveRegistro::crearClaveRegistro(tipoRegistro, NULL);
	this->deserializar(dato, tamanioDato);
	this->idNodoIzq = 0;
}

RegistroNodoInterno::~RegistroNodoInterno() {
	//cout << "INFO: Destructor RegistroNodoInterno" << endl;
	this->idNodoIzq = 0;
	this->idNodoDer = 0;
}

unsigned int RegistroNodoInterno::getIdNodoIzq(){
	return this->idNodoIzq;
}

unsigned int RegistroNodoInterno::getIdNodoDer(){
	return this->idNodoDer;
}


void RegistroNodoInterno::setIdNodoIzq(unsigned int idNodoIzq) {
	this->idNodoIzq = idNodoIzq;
}

void RegistroNodoInterno::setIdNodoDer(unsigned int idNodoDer) {
	this->idNodoDer = idNodoDer;
}

unsigned int RegistroNodoInterno::getTamanio(){
	return  sizeof(unsigned int) + this->clave->getTamanio(); // idNodoDer + Clave
}

char* RegistroNodoInterno::serializar(){
	if(this->clave != NULL){
		char* cadenaEscritura = new char[getTamanio()];
		unsigned int pos = 0;

		// idNodoDer
		this->almacenarEntero(cadenaEscritura, pos, getIdNodoDer());
		// Clave
		char *claveSerializada = this->clave->serializar();
		this->almacenarCharAsterisco(cadenaEscritura, pos, claveSerializada, this->clave->getTamanio());
		delete[] claveSerializada;

		return cadenaEscritura;
	}else{
		cout << "ERROR: al querer serializar un RegistroNodo que no tiene clave instanciada" << endl;
		return NULL;
	}
}

void RegistroNodoInterno::deserializar(char *dato, unsigned int tamanioDato){
	if(this->clave != NULL){
		unsigned int pos = 0;

		// idNodoDer
		this->setIdNodoDer(this->recuperarEntero(dato, pos));
		// tamanioClave
		unsigned int tamanioClave = tamanioDato - sizeof(unsigned int);
		// Clave
		char* datoClave = this->recuperarCharAsterisco(dato, pos,tamanioClave);
		this->clave->deserializar(datoClave);
		delete[] datoClave;
	}else{
		//cout << "ERROR: al querer deserializar un RegistroNodo que no tiene clave instanciada" << endl;
	}
}

string RegistroNodoInterno::getDatosImportantes(){
	std::stringstream datosImportantes;
	datosImportantes << "( RegistroNodoInterno: "<< "Tamanio: " << (sizeof(unsigned int) + getTamanio()) << ", clave" << *(this->clave) << ", idNodoIzq: " << getIdNodoIzq() << ", idNodoDer: " << getIdNodoDer()<<")";

    return  datosImportantes.str();
}
