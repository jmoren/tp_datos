#ifndef REGISTRONODOINTERNO_H_
#define REGISTRONODOINTERNO_H_

#include "RegistroNodo.h"

class RegistroNodoInterno: public RegistroNodo {
private:
	unsigned int idNodoIzq;// Representa al hijo izquierdo del registroNodoInterno
	unsigned int idNodoDer;// Representa al hijo derecho del registroNodoInterno

public:
	RegistroNodoInterno(ClaveRegistroArbol* clave);
	RegistroNodoInterno(TipoRegistro tipoRegistro, char *dato, unsigned int tamanioDato);
	virtual ~RegistroNodoInterno();

	unsigned int getIdNodoIzq();
	unsigned int getIdNodoDer();

	void setIdNodoIzq(unsigned int idNodoIzq);
	void setIdNodoDer(unsigned int idNodoDer);

	unsigned int getTamanio();
	char* serializar();
	void deserializar(char *dato,  unsigned int tamanioDato);

	virtual string getDatosImportantes();
};

#endif /* REGISTRONODOINTERNO_H_ */
