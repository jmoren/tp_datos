/*
 * NodoHoja.cpp
 *
 *  Created on: 06/10/2013
 *      Author: natalia
 */

#include "NodoHoja.h"

using namespace std;

//constructores y destructores ------------------------------------------------------------------------------
NodoHoja::NodoHoja(TipoRegistro tipoRegistro, Bloque *bloque, unsigned int tamanioTotalDisponibleParaRegistros) : NodoInterno(){
	this->tipoRegistro = tipoRegistro;
	this->tamanioBloque = bloque->getTamanioBloque();
	this->tamanioTotalDisponibleParaRegistros = tamanioTotalDisponibleParaRegistros;
	unsigned int tamanioCompletoRegistroGenericoCabecera = sizeof(unsigned int) +  getTamanioRegistroCabeceraNodo();
	if(tamanioCompletoRegistroGenericoCabecera <= tamanioTotalDisponibleParaRegistros){
		this->espacioLibre = tamanioTotalDisponibleParaRegistros - tamanioCompletoRegistroGenericoCabecera;
	}else{
		cout << "ERROR: El espacio libre insuficiente, no se pueden guardar los datos minimos de la cabecera del nodo." << endl;
		this->espacioLibre = 0;
	}

	list<RegistroGenericoArbol*> registrosGenericos = bloque->getRegistros();

	for (std::list<RegistroGenericoArbol*>::iterator it = registrosGenericos.begin(); it != registrosGenericos.end(); it++){
		if(it == registrosGenericos.begin()){
			// Recupero el primer registro que tiene los datos de la cabecera
			recuperarDatosCabeceraNodo(*it);
			// Chequeo que la cantidad de registros sea igual a la cantidad de registros del bloque -1 (la cabecera)
			if(this->cantRegistros != (registrosGenericos.size() - 1)){
				cout << "ERROR: NodoInterno constructor: La cantidad de nodos obtenida del bloque no es igual a la cantidad guardada en la cabecera." << endl;
				//cout << "this->cantRegistros = " << this->cantRegistros << endl;
				//cout << "registrosGenericos.size() = " << registrosGenericos.size() << endl;
				break;
			}
			this->cantRegistros = 0;
		}else{
			// Recupero todos los registroNodoInterno
			RegistroNodo* registroNodo = recuperarRegistroNodo(*it);
			unsigned short resultado =  this->insertarRegistroNodo(registroNodo);

			// Si el registro no se inserta en el arbol entonces liberamos su memoria.
			if((resultado == 0) || (resultado == 3)){
				delete(registroNodo);
			}
		}
	}
}

NodoHoja::NodoHoja(TipoRegistro tipoRegistro, unsigned int idNodo, unsigned int tamanioBloque, unsigned int tamanioTotalDisponibleParaRegistros) : NodoInterno(){
	this->tipoRegistro = tipoRegistro;
	this->idNodo = idNodo;
	this->nivel = 0;
	this->tamanioBloque = tamanioBloque;
	this->tamanioTotalDisponibleParaRegistros = tamanioTotalDisponibleParaRegistros;
	unsigned int tamanioCompletoRegistroGenericoCabecera = sizeof(unsigned int) +  getTamanioRegistroCabeceraNodo(); // tamanio longitud registroGenerico + longitud datos registroGenerico
	if(tamanioCompletoRegistroGenericoCabecera <= tamanioTotalDisponibleParaRegistros){
		this->espacioLibre = tamanioTotalDisponibleParaRegistros - tamanioCompletoRegistroGenericoCabecera;
	}else{
		cout << "ERROR: El espacio libre insuficiente, no se pueden guardar los datos minimos de la cabecera del nodo." << endl;
		this->espacioLibre = 0;
	}
	this->cantRegistros = 0;
	this->idNodoIzq = 0;
	this->idNodoSiguiente=0;
}

NodoHoja::~NodoHoja(){
	this->idNodoSiguiente=0;
}

// Metodos Protected  ---------------------------------------------------------------------------------------
unsigned int NodoHoja::getTamanioRegistroCabeceraNodo(){
	// nivel + idNodo + cantidad registros + idNodoIzquierdo o idNodoAnterior + idNodoSiguiente
	unsigned int tamanioRegistroCabeceraNodo = sizeof(unsigned int) + sizeof(unsigned int) + sizeof(unsigned int) + sizeof(unsigned int) + sizeof(unsigned int);
	return tamanioRegistroCabeceraNodo;
}

/**
 *  Devuelve de forma serializada y en un RegistroGenerico los
 *  atributos de la cabecera:
 *   nivel + idNodo + cantidad registros + idNodoIzquierdo o idNodoAnterior + idNodoSiguiente
 */
RegistroGenericoArbol* NodoHoja::crearRegistroCabeceraNodo(){
	unsigned int tamanio = getTamanioRegistroCabeceraNodo();
	char* dato = new char[tamanio];
	unsigned int pos = 0;

	// nivel
	this->almacenarEntero(dato, pos, this->getNivel());

	// idNodo
	this->almacenarEntero(dato, pos, this->getIdNodo());

	// cantidad registros
	this->almacenarEntero(dato, pos, this->getCantRegistros());

	// idNodoIzquierdo o idNodoAnterior
	this->almacenarEntero(dato, pos, this->getIdNodoAnterior());

	// idNodoSiguiente
	this->almacenarEntero(dato, pos, this->getIdNodoSiguiente());

	return new RegistroGenericoArbol(dato, tamanio);
}

RegistroGenericoArbol* NodoHoja::crearRegistroGenerico(RegistroNodo* registroNodo){
	unsigned int tamanio = registroNodo->getTamanio();
	char *dato = registroNodo->serializar();

	return new RegistroGenericoArbol(dato, tamanio);
}

void NodoHoja::recuperarDatosCabeceraNodo(RegistroGenericoArbol *registroGenerico){
	if(registroGenerico->getTamanio() == this->getTamanioRegistroCabeceraNodo()){
		char* datoLectura = registroGenerico->getDato();
		unsigned int pos = 0;

		// nivel
		this->setNivel(this->recuperarEntero(datoLectura, pos));

		// idNodo
		this->setIdNodo(this->recuperarEntero(datoLectura, pos));

		// cantidad registros
		this->cantRegistros = this->recuperarEntero(datoLectura, pos);

		// idNodoIzquierdo o idNodoAnterior -> obtengo el id nodo anterior del secuence set
		this->setIdNodoAnterior(this->recuperarEntero(datoLectura, pos));

		// idNodoSiguiente
		this->setIdNodoSiguiente(this->recuperarEntero(datoLectura, pos));

	}else{
		cout << "ERROR: NodoInterno -> recuperarDatosCabeceraNodo: El tamanio del registroGenerico no coincide con el tamanio de la cabecera" << endl;
	}

}

RegistroNodo* NodoHoja::recuperarRegistroNodo(RegistroGenericoArbol *registroGenerico){
	RegistroNodo *registroNodoHoja = new RegistroNodo(this->tipoRegistro, registroGenerico->getDato(), registroGenerico->getTamanio());
	return registroNodoHoja;
}

void NodoHoja::actualizarRegistroSiguiente(RegistroNodo *registroNodoInsertado, RegistroNodo *registroNodoSiguiente){
	// No se hace nada en este caso.
	//cout << "****************************** NodoHoja::actualizarRegistroSiguiente: No se hace nada en este caso" << endl;
}

//geters y seters -------------------------------------------------------------------------------------------

unsigned int NodoHoja::getIdNodoAnterior() {
	return idNodoIzq;
}

unsigned int NodoHoja::getIdNodoSiguiente(){
	return idNodoSiguiente;
}

void NodoHoja::setIdNodoAnterior(unsigned int idNodoAnterior) {
	this->idNodoIzq = idNodoAnterior;
}

void NodoHoja::setIdNodoSiguiente(unsigned int idNodoSiguiente) {
	this->idNodoSiguiente = idNodoSiguiente;
}

NodoHoja* NodoHoja::dividir(unsigned int idNodo){
	//le paso el id del nodo nuevo

	ClaveRegistroArbol* clave = this->getDatosClaveMedio();
	list<RegistroNodo*> registrosNodoNuevo;

	NodoHoja *nuevoNodo = new NodoHoja(this->getTipoRegistro(),idNodo,this->getTamanioBloque(), this->tamanioTotalDisponibleParaRegistros);
	RegistroNodo *reg;

	if(!this->registrosNodo.empty()){
		// Inserto en el nodoNuevo la mitad superior de la lista de registroNodo del nodoHoja actual
		reg = this->registrosNodo.back();
		while ((reg->getClave())->comparar(clave)>=0) {
			nuevoNodo->insertarRegistroNodo(reg);

			this->registrosNodo.pop_back();

			cout << "--- dividir : this->registrosNodo.size() = " << this->registrosNodo.size() << endl;
			reg = this->registrosNodo.back();
		}

		// Actualizo espacio libre y cantidad de registros
		this->cantRegistros = 0;
		unsigned int tamanioCompletoRegistroGenericoCabecera = sizeof(unsigned int) +  getTamanioRegistroCabeceraNodo();
		this->espacioLibre = this->tamanioTotalDisponibleParaRegistros - tamanioCompletoRegistroGenericoCabecera;

		for (std::list<RegistroNodo*>::iterator it = this->registrosNodo.begin(); it != this->registrosNodo.end(); it++){
			this->cantRegistros++;
			this->espacioLibre = this->espacioLibre - sizeof(unsigned int) - ((*it)->getTamanio()); // espacio libre - espacio del valor del tamanio registroNodo o registroGenerico - espacio de los datos del registroNodo
		}

	}

	return nuevoNodo;
}
// Metodos --------------------------------------------------------------------------------------------------
/*
 *  Devuelve:
 * 				0	Si el registroNodo recibido no se actualizo
 * 				1	El nodo se actualizo insertanto el registroNodo
 * 				2	El nodo se desbordo al intentar insertar y no lo inserto
 * 				3	Ya existe un registro con el identificador igual al que se quiso insertar
 */
unsigned short NodoHoja::insertarClaveArbol(ClaveRegistroArbol *clave, TipoRegistro tipoRegistro){
	char* claveSerializada = clave->serializar();
	ClaveRegistroArbol *claveClonada = FabricaClaveRegistro::crearClaveRegistro(tipoRegistro, claveSerializada);
	delete[] claveSerializada; // Liberamos la memoria ya que no se usara mas.
	RegistroNodo *registro = new RegistroNodo(claveClonada);
	unsigned short resultado =  this->insertarRegistroNodo(registro);

	// Si el registro no se inserta en el arbol entonces liberamos su memoria.
	if((resultado == 0) || (resultado == 3)){
		delete(registro);
	}

	return resultado;

}

/*
 *  Devuelve:
 * 				0	Si el registroNodo recibido no se actualizo
 * 				1	Elimino el registro con el identificador pasado.
 * 				2   Si hay underflow
 * 				3	No existe un registro con el identificador igual al que se quiso eliminar
 */
unsigned short NodoHoja::eliminarClaveArbol(ClaveRegistroArbol* clave, TipoRegistro tipoRegistro){
	char* claveSerializada = clave->serializar();
	ClaveRegistroArbol *claveClonada = FabricaClaveRegistro::crearClaveRegistro(tipoRegistro, claveSerializada);
	delete[] claveSerializada; // Liberamos la memoria ya que no se usara mas.

	RegistroNodo *registro = new RegistroNodo(claveClonada);
	unsigned int idNodo = 0;
	unsigned short respuesta = this->eliminarRegistroNodo(registro, idNodo); // Se manda idNodo = 0 dado que para una hora nunca se va a usar en ese metodo.
	delete(registro);
	return respuesta;
}

string NodoHoja::getDatosImportantes(){
	std::stringstream datosImportantes;
	datosImportantes << "( NodoHoja:";
	datosImportantes << "\n idNodo: " << this->idNodo << ", nivel: " << this->nivel << ", tamanioBloque: " << this->tamanioBloque << ", tamanioTotalDisponible: " << this->tamanioTotalDisponibleParaRegistros << ", espacioLibre: " << this->espacioLibre << ", cantRegistros: " << this->cantRegistros << ", idNodoAnterior: " << this->getIdNodoAnterior() << ", idNodoSiguiente: " << this->getIdNodoSiguiente() ;

	unsigned int pos = 0;
	for (std::list<RegistroNodo*>::iterator it = registrosNodo.begin(); it != registrosNodo.end(); it++){
		datosImportantes << "\n " << pos << " = " << *(*it);
		pos++;
	}

	datosImportantes << "\n)";

    return  datosImportantes.str();

}

void NodoHoja::unir(NodoHoja * nodoHijo){

	cout<<*this<<endl;
	cout<<*nodoHijo<<endl;
	RegistroNodo* reg;
	//le asigno al objeto todos los registros del nodoHijo
	while (nodoHijo->cantRegistros > 0) {
		reg = nodoHijo->registrosNodo.front();
		nodoHijo->espacioLibre = nodoHijo->espacioLibre + sizeof(unsigned int) + (reg)->getTamanio(); // espacio libre - espacio del valor del tamanio registroNodo o registroGenerico - espacio de los datos del registroNodo
		nodoHijo->cantRegistros--;
		nodoHijo->registrosNodo.pop_front();
		this->espacioLibre = this->espacioLibre - sizeof(unsigned int) - (reg)->getTamanio(); // espacio libre - espacio del valor del tamanio registroNodo o registroGenerico - espacio de los datos del registroNodo
		this->registrosNodo.push_back(reg);
		this->cantRegistros++;
		cout<<*reg<<endl;
	}
	this->setIdNodoSiguiente(nodoHijo->getIdNodoSiguiente());
}

void NodoHoja::balancear(NodoHoja * nodoHno,NodoInterno * nodoPadre,TipoNodoHno tipoHno, TipoRegistro tipoRegistro){
	cout << "INFO: NodoHoja::balancear" << endl;
	if (tipoHno == DERECHO) {
		//leo el primer registro del nodoHno
		RegistroNodo * regHnoABajar = (nodoHno->registrosNodo).front();
		// Actualizo el espacio libre en el nododelete(reg);
		unsigned int nuevoEspacioLibreHno = nodoHno->getEspacioLibre() + sizeof(unsigned int) + regHnoABajar->getTamanio();
		nodoHno->setEspacioLibre(nuevoEspacioLibreHno);
		nodoHno->cantRegistros--;
		nodoHno->registrosNodo.pop_front();

		//inserto el elemento nuevo en mi lista de registros
		this->registrosNodo.push_back(regHnoABajar);
		unsigned int nuevoEspacioLibre = this->getEspacioLibre() - sizeof(unsigned int) - regHnoABajar->getTamanio();
		this->setEspacioLibre(nuevoEspacioLibre);
		this->cantRegistros++;

		//Elimino la clave en el padre e inserto la nueva clave que le sigue a la que elimine
		unsigned int idNodo = 0; //No se va a usar si es que todo anda bien.
		RegistroNodoInterno* reg = (RegistroNodoInterno*)nodoPadre->obtenerRegistroClaveNodo(this->getIdNodo(),nodoHno->getIdNodo());
		if(nodoPadre->eliminarRegistroNodo(reg, idNodo) != 1){
			cout << "ERROR: NodoHoja::balancear der -> fallo nodoPadre->eliminarClaveArbol" << endl;
		}

		// Obtengo el siguiente registro al regHno para subir como nueva clave al nodoPadre
		if(nodoPadre->insertarClaveArbol((nodoHno->registrosNodo).front()->getClave(), tipoRegistro, this->getIdNodo(), nodoHno->getIdNodo()) != 1){
			cout << "ERROR: NodoHoja::balancear der -> fallo nodoPadre->insertarClaveArbol" << endl;
		}
	}else if (tipoHno == IZQUIERDO) {
		//leo el primer registro mio
		//RegistroNodo * regHnoABajar = (this->registrosNodo).front();

		RegistroNodoInterno* regHnoABajar =(RegistroNodoInterno*)nodoPadre->obtenerRegistroClaveNodo(nodoHno->getIdNodo(),this->getIdNodo());

		// Obtengo el anterior registro al menor mio para subir como nueva clave al nodoPadre, osea el ultimo reg de mi hermano izquierdo
		RegistroNodo * regHnoSiguienteASubir = (nodoHno->registrosNodo).back();

		// Actualizo el espacio libre de mi hermano porque le saque el ultimo registro
		unsigned int nuevoEspacioLibreHno = nodoHno->getEspacioLibre() + sizeof(unsigned int) + regHnoSiguienteASubir->getTamanio();
		nodoHno->setEspacioLibre(nuevoEspacioLibreHno);
		nodoHno->cantRegistros--;
		nodoHno->registrosNodo.pop_back();

		// Inserto el elemento nuevo en mi lista de registros
		this->registrosNodo.push_front(regHnoSiguienteASubir);
		unsigned int nuevoEspacioLibre = this->getEspacioLibre() - sizeof(unsigned int) - regHnoSiguienteASubir->getTamanio();
		this->setEspacioLibre(nuevoEspacioLibre);
		this->cantRegistros++;

		//Elimino la clave en el padre e inserto la nueva clave que le sigue a la que elimine
		unsigned int idNodo = 0; //No se va a usar si es que todo anda bien.
		if(nodoPadre->eliminarRegistroNodo(regHnoABajar, idNodo) != 1){
			cout << "ERROR: NodoHoja::balancear izq -> fallo nodoPadre->eliminarClaveArbol" << endl;
		}

		// Obtengo el siguiente registro al regHno para subir como nueva clave al nodoPadre
		if(nodoPadre->insertarClaveArbol((nodoHno->registrosNodo).back()->getClave(), tipoRegistro, nodoHno->getIdNodo(),this->getIdNodo()) != 1){
			cout << "ERROR: NodoHoja::balancear izq -> fallo nodoPadre->insertarClaveArbol" << endl;
		}

	}

}



