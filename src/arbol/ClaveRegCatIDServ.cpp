/*
 * ClaveRegNombreCategoriaIDServicio.cpp
 *
 *  Created on: 08/10/2013
 *      Author: natalia
 */

#include "ClaveRegCatIDServ.h"

ClaveRegCatIDServ::ClaveRegCatIDServ() {
	this->idServicio      =0;
	this->nombreCategoria =" ";
	this->longitud        =this->nombreCategoria.length();
}

ClaveRegCatIDServ::ClaveRegCatIDServ(unsigned long idservicio,string nombreCategoria) {
	this->idServicio=idservicio;
	this->nombreCategoria=nombreCategoria;
	this->longitud=this->nombreCategoria.length();

}
unsigned long ClaveRegCatIDServ::getIdServicio(){
	return idServicio;
}

void ClaveRegCatIDServ::setIdServicio(unsigned long idServicio) {
	this->idServicio = idServicio;
}

string ClaveRegCatIDServ::getNombreCategoria(){
	return nombreCategoria;
}

void ClaveRegCatIDServ::setNombreCategoria(string nombreCategoria) {
	this->nombreCategoria = nombreCategoria;
}

unsigned long ClaveRegCatIDServ::getLongitud(){
	return longitud;
}
void ClaveRegCatIDServ::setLongitud(unsigned long longitud){

	this->longitud = longitud;
}

ClaveRegCatIDServ::~ClaveRegCatIDServ() {
	// TODO Auto-generated destructor stub
}

short int ClaveRegCatIDServ::comparar(ClaveRegistroArbol* claveParaComparar){
	if((claveParaComparar != NULL) ){
		ClaveRegCatIDServ *clave2 = (ClaveRegCatIDServ*)claveParaComparar;

		if(this->getNombreCategoria() == clave2->getNombreCategoria()){
			if(this->getIdServicio() == clave2->getIdServicio()){
				return 0;
			}else if(this->getIdServicio()  > clave2->getIdServicio() ){
				return 1;
			}else{
				return -1;
			}
		}else if(this->getNombreCategoria() > clave2->getNombreCategoria()){
			return 1;
		}else{
			return -1;
		}
	}else{
		return 1; // Si la clave con la que comparo es NULL entonces siempre esta clave sera mayor a NULL
	}
}

unsigned int ClaveRegCatIDServ::getTamanio(){

	return (unsigned int)(sizeof(unsigned long)*2 + (this->nombreCategoria).length());

}

char* ClaveRegCatIDServ::serializar(){

	char* cadenaEscritura = new char[getTamanio()];
	unsigned int pos = 0;
	this->almacenarEnteroLargo(cadenaEscritura, pos, getLongitud());
	this->almacenarString(cadenaEscritura, pos, getNombreCategoria());
	this->almacenarEnteroLargo(cadenaEscritura, pos, getIdServicio());
	return cadenaEscritura;
}

void ClaveRegCatIDServ::deserializar(char* cadena){

	unsigned int pos = 0;

	this->setLongitud(this->recuperarEnteroLargo(cadena, pos));
	this->setNombreCategoria(this->recuperarString(cadena, pos, getLongitud()));
	this->setIdServicio(this->recuperarEnteroLargo(cadena, pos));

}

string ClaveRegCatIDServ::getDatosImportantes(){
	std::stringstream datosImportantes;
	datosImportantes << "ClaveRegCatIDServ: "<< "Tamanio: " << getTamanio() << " (" << this->getNombreCategoria() << ", " << this->getIdServicio() << ")";

    return  datosImportantes.str();
}
