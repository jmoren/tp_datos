/*
 * Bloque.cpp
 *
 *  Created on: 05/10/2013
 *      Author: natalia
 */

#include "Bloque.h"

// Constructores -------------------------------------------------------------------------------
Bloque::Bloque() {
	nroBloque = 0;
	tamanioBloque = 0;
	tamanioEspacioLibre = 0;
}

Bloque::~Bloque() {
	vaciarRegistros();
	nroBloque = 0;
	tamanioBloque = 0;
	tamanioEspacioLibre = 0;
}

// Setter -------------------------------------------------------------------------------------
void Bloque::setRegistros(list<RegistroGenericoArbol*> registrosGenericos){
	vaciarRegistros();
	this->registrosGenericos = registrosGenericos;
}

void Bloque::push_back(RegistroGenericoArbol* registroGenerico){
	this->registrosGenericos.push_back(registroGenerico);
}

void Bloque::setNroBloque(unsigned int nroBloque){
	this->nroBloque = nroBloque;
}

void Bloque::setTamanioBloque(unsigned int tamanioBloque){
	this->tamanioBloque = tamanioBloque;
}

void Bloque::setTamanioEspacioLibre(unsigned int tamanioEspacioLibre){
	this->tamanioEspacioLibre = tamanioEspacioLibre;
}

// Getter -------------------------------------------------------------------------------------
list<RegistroGenericoArbol*> Bloque::getRegistros(){
	return this->registrosGenericos;
}

unsigned int Bloque::getNroBloque(){
	return this->nroBloque;
}

unsigned int Bloque::getTamanioBloque(){
	return this->tamanioBloque;
}

unsigned int Bloque::getTamanioEspacioLibre(){
	return this->tamanioEspacioLibre;
}

// --------------------------------------------------------------------------------------------
void Bloque::vaciarRegistros(){
	while (!this->registrosGenericos.empty()){
		RegistroGenericoArbol *reg = registrosGenericos.front();
		registrosGenericos.pop_front();
		delete(reg);
	}
}
