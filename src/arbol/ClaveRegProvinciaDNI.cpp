/*
 * ClaveRegProvinciaDNI.cpp
 *
 *  Created on: 06/10/2013
 *      Author: natalia
 */

#include "ClaveRegProvinciaDNI.h"

using namespace std;

ClaveRegProvinciaDNI::ClaveRegProvinciaDNI() {
	this->dni=0;
	this->provincia="";
	this->longitud=0;
}

ClaveRegProvinciaDNI::ClaveRegProvinciaDNI(unsigned long dni,string provincia) {

	this->dni=dni;
	this->provincia=provincia;
	this->longitud=provincia.length();
}


ClaveRegProvinciaDNI::~ClaveRegProvinciaDNI() {

}

//devuelve 0 si son iguales, 1 si es mayor y -1 si es menor
short int ClaveRegProvinciaDNI::comparar(ClaveRegistroArbol* claveParaComparar){
	if((claveParaComparar != NULL) ){
		ClaveRegProvinciaDNI *clave2 = (ClaveRegProvinciaDNI*)claveParaComparar;

		if(this->getProvincia() == clave2->getProvincia()){
			if(this->getDni() == clave2->getDni()){
				return 0;
			}else if(this->getDni() > clave2->getDni()){
				return 1;
			}else{
				return -1;
			}
		}else if(this->getProvincia() > clave2->getProvincia()){
			return 1;
		}else{
			return -1;
		}
	}else{
		return 1; // Si la clave con la que comparo es NULL entonces siempre esta clave sera mayor a NULL
	}
}

unsigned int ClaveRegProvinciaDNI::getTamanio(){
	unsigned int tamanioTotal = (unsigned int)((sizeof(unsigned long)*2) + (this->provincia).length());
	//cout << "---- tamanioTotal = " <<  tamanioTotal << endl;
	return tamanioTotal;

}

char* ClaveRegProvinciaDNI::serializar(){

	char* cadenaEscritura = new char[getTamanio()];
	unsigned int pos = 0;
	//cout<<"serializo ---------"<<endl;
	this->almacenarEnteroLargo(cadenaEscritura, pos, getLongitud());
	this->almacenarString(cadenaEscritura, pos, getProvincia());
	this->almacenarEnteroLargo(cadenaEscritura, pos, getDni());
	return cadenaEscritura;
}

void ClaveRegProvinciaDNI::deserializar(char* cadena){
	unsigned int pos = 0;

	unsigned long longitud = this->recuperarEnteroLargo(cadena, pos);
	this->setLongitud(longitud);

	string provincia = this->recuperarString(cadena, pos, longitud);
	this->setProvincia(provincia);

	unsigned long dni = this->recuperarEnteroLargo(cadena, pos);
	this->setDni(dni);

	/*
	cout << endl << "deserializar: " << endl;
	cout << endl << "longitud: " << longitud;
	cout << ", provincia: " << provincia;
	cout << ", dni: " << dni << endl << endl;
	 */
}

unsigned long ClaveRegProvinciaDNI::getDni()  {
	return dni;
}

void ClaveRegProvinciaDNI::setDni(unsigned long dni) {
	this->dni = dni;
}

string ClaveRegProvinciaDNI::getProvincia()  {
	return provincia;
}

void ClaveRegProvinciaDNI::setProvincia(string provincia) {
	//cout << endl << "setProvincia(" << provincia << ");" << endl;
	this->provincia = provincia;
	//cout << endl << "this->provincia = " << this->provincia << endl;
}

void ClaveRegProvinciaDNI::setLongitud(unsigned long longitud){

	this->longitud=longitud;
}

unsigned long ClaveRegProvinciaDNI::getLongitud(){

	return longitud;
}

string ClaveRegProvinciaDNI::getDatosImportantes(){
	std::stringstream datosImportantes;
	datosImportantes << "ClaveRegProvinciaDNI: "<< "Tamanio: " << getTamanio() << " (" << getProvincia() << ", " << getDni() << ")";

    return  datosImportantes.str();

}
