#ifndef REGISTRONODO_H_
#define REGISTRONODO_H_

#include "ClaveRegistroArbol.h"
#include "Persistente.h"
#include "FabricaClaveRegistro.h"

class RegistroNodo: public Persistente {
protected:
	ClaveRegistroArbol* clave;

public:
	RegistroNodo();
	RegistroNodo(ClaveRegistroArbol* clave);
	RegistroNodo(TipoRegistro tipoRegistro, char *dato, unsigned int tamanioDato);
	virtual ~RegistroNodo();

	void setClave(ClaveRegistroArbol* clave);
	ClaveRegistroArbol* getClave();
	virtual unsigned int getTamanio();
	short int comparar(RegistroNodo *registroNodoComparar);
	virtual char* serializar();
	virtual void deserializar(char *dato,  unsigned int tamanioDato);

	// Sobrecarga del operador <<
	friend std::ostream& operator<< (std::ostream& stream, RegistroNodo& reg){
		stream << reg.getDatosImportantes();
	    return stream;
	}
	virtual string getDatosImportantes();
};

#endif /* REGISTRONODO_H_ */
