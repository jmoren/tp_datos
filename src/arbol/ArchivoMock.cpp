/*
 * ArchivoMock.cpp
 *
 *  Created on: 06/10/2013
 *      Author: Natalia
 */

#include "ArchivoMock.h"

// Constructores -------------------------------------------------------------------------------
ArchivoMock::ArchivoMock(TipoRegistro tipoRegistro, unsigned int tamanioBloque){
	this->tamanioBloque = tamanioBloque;
	this->tamanioDatosDeControlBloque = 24;
	this->tamanioDatosDeControlRegistro = 0;
	this->cantidadTotalBloques = 0;
	this->tipoRegistro = tipoRegistro; // Asignar default;
	primerLlamado = true;
}

ArchivoMock::~ArchivoMock(){
	this->tamanioBloque = 0;
	this->tamanioDatosDeControlBloque = 0;
	this->tamanioDatosDeControlRegistro = 0;
	this->cantidadTotalBloques = 0;
	this->tipoRegistro = TipoUsuarioDNI; // Asignar default;
	primerLlamado = false;
	vaciarBloques();
}

// Funciones ----------------------------------------------------------------------------------
bool ArchivoMock::existeArchivoFisico(){
	if(this->primerLlamado){
		this->primerLlamado = false;
		return false;
	}else{
		return true;
	}
}

void ArchivoMock::crearArchivo(){
	cout << "ArchivoMock::crearArchivo()" << endl;
	this->cantidadTotalBloques = 0;
	vaciarBloques();
}


unsigned int ArchivoMock::getTamanioBloque(){
	return this->tamanioBloque;
}


unsigned int ArchivoMock::getTamanioDatosDeControlBloque(){
	return this->tamanioDatosDeControlBloque;
}

unsigned int ArchivoMock::getTamanioDatosDeControlRegistro(){
	return this->tamanioDatosDeControlRegistro;
}

unsigned int ArchivoMock::getCantidadTotalBloques(){
	return this->cantidadTotalBloques;
}

/**
 * La numeracion de nodos arranca en 0 con lo cual, para esta implementacion,
 * para obtener el siguiente idBloqueLibre con devolver la cantidad total de bloques alcanza
 */
unsigned int ArchivoMock::getIdBloqueLibre(){
	if(existenBloquesLibres()){
		//TODO
		return (this->cantidadTotalBloques);
	}else{
		//Se devuelve el siguiente al ultimo bloque para agregar al final del archivo
		return (this->cantidadTotalBloques);
	}
}

bool ArchivoMock::existenBloquesLibres(){
	//TODO: Se devuelve false hasta que este implementada la administracion de bloques libres.
	return false;
}

Bloque* ArchivoMock::getBloque(unsigned int nroBloque){
	map<unsigned int,Bloque*>::iterator it = bloques.find(nroBloque);
	if(it != bloques.end()){
		return it->second;
	}else{
		cout << "Error: getBloque no encuentra el numero de bloque: " << nroBloque << endl;
		return NULL;
	}
}
Bloque* ArchivoMock::getRaiz(){
	return getBloque(0);
}

void ArchivoMock::borrarBloque(unsigned int nroBloque){
	map<unsigned int,Bloque*>::iterator it = bloques.find(nroBloque);
	if(it != bloques.end()){
		delete(it->second);
		bloques.erase (it);
		this->cantidadTotalBloques--;
	}else{
		cout << "Error: borrarBloque no encuentra el numero de bloque: " << nroBloque << endl;
	}
}

void ArchivoMock::grabarBloque(Bloque* bloque){
	unsigned int nroBloque = bloque->getNroBloque();
	map<unsigned int,Bloque*>::iterator it = bloques.find(nroBloque);
	if(it != bloques.end()){
		cout << "INFO: grabarBloque actualizara los datos del numero de bloque: " << nroBloque << endl;
		delete(bloques[nroBloque]);
	}else{
		this->cantidadTotalBloques++;
		cout << "INFO: grabarBloque creara los datos del numero de bloque: " << nroBloque << endl;
	}
	bloques[nroBloque] = bloque;
	cout << "--------------------------------------------------------------" << endl;
	cout << "ArchivoMock::grabarBloque -> nroBloque = " << nroBloque << endl;
	cout << "--------------------------------------------------------------" << endl;
}

void ArchivoMock::vaciarBloques(){
	if(!bloques.empty()){
		for( map<unsigned int,Bloque*>::iterator it=bloques.begin(); it!=bloques.end(); ++it){
			//cout << "--------------------------------------------------------------" << endl;
			//cout << "ArchivoMock::vaciarBloques -> nroBloque = " << it->second->getNroBloque() << endl;
			//cout << "--------------------------------------------------------------" << endl;
			delete(it->second); // Borro el puntero del bloque
			this->cantidadTotalBloques--;
		}
		bloques.erase(bloques.begin(), bloques.end());
	}
}

void ArchivoMock::imprimir(){
	if(!bloques.empty()){
		for( map<unsigned int,Bloque*>::iterator it=bloques.begin(); it!=bloques.end(); ++it){
			Bloque *bloque = it->second;

			if (bloque->getRegistros().size()>0){
				NodoInterno *nodoTemp = new NodoInterno();
				unsigned int nivel = nodoTemp->recuperarNivel(bloque->getRegistros().front());
				delete(nodoTemp);
				if (nivel==0){
					NodoHoja *hoja = new NodoHoja(this->tipoRegistro,bloque, this->getTamanioTotalDisponibleParaRegistros());
					cout << *hoja << endl;
					delete(hoja);
				} else {
					NodoInterno *nodo = new NodoInterno(this->tipoRegistro,bloque, this->getTamanioTotalDisponibleParaRegistros());
					cout << *nodo << endl;
					delete(nodo);
				}
			} else {
				cout<<"Error:El bloque "<< it->first <<" no se pudo imprimir."<<endl;
			}
		}
	}else{
		cout << "Archivo Vacio!!!" << endl;
	}
}

