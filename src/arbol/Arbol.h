#ifndef ARBOL_H_
#define ARBOL_H_

#include "RegistroGenericoArbol.h"
#include "Archivo.h"
#include "ArchivoFisico.h"
#include "ArchivoMock.h"
#include "NodoInterno.h"
#include "NodoHoja.h"
#include "ClaveRegistroArbol.h"

using namespace std;

class Arbol {
private:
	Archivo *archivo;
	TipoRegistro tipoRegistro;
	NodoInterno *raiz;

	short eliminarRecursivo(NodoInterno* nodo,ClaveRegistroArbol* clave,unsigned int &idNodo);

	ClaveRegistroArbol *ultimaClaveEncontrada;
	NodoHoja *ultimoNodoHojaEncontrado;

	short insertarRecursivo(NodoInterno* nodo,ClaveRegistroArbol* &clave);

	void leerRaiz();
	bool raizVacia();

	void enorden(NodoInterno *nodo);

	ClaveRegistroArbol* buscar(NodoInterno *nodo, ClaveRegistroArbol* clave, bool busquedaExacta);
	ClaveRegistroArbol* guardarUltimaClaveYHojaBuscada(RegistroNodo *registroEncontrado, NodoHoja *ultimaHojaLeida);

	ClaveRegistroArbol* getPrimeraClave();
	short unirNodosHermanos(NodoInterno *nodoHijo, NodoInterno *nodoHno, NodoInterno *nodo, TipoNodoHno tipoHno, unsigned int &idNodo);
public:
	static unsigned int TAMANIO_BLOQUE_ARBOL;

	//static unsigned int ARBOL_UNDERFLOW;

	Arbol();
	Arbol(TipoRegistro tipoRegistro);
	virtual ~Arbol();

	Archivo* getArchivo();
	TipoRegistro getTipoRegistro();

	// Devuelve 0 si fue exitoso o -1 si hubo un error
	short insertar(ClaveRegistroArbol* clave);

	// Devuelve 0 si fue exitoso o -1 si hubo un error o -2 no se encontro el registro
	short eliminar(ClaveRegistroArbol* clave);

	// Elimina el registroViejo y luego inserta el registroNuevo. Devuelve 0 si fue exitoso o -1 si hubo un error
	short actualizar(ClaveRegistroArbol* clave, ClaveRegistroArbol* claveNueva);

	// Devuelve el registro buscado o NULL si no lo encuentra
	ClaveRegistroArbol* buscar(ClaveRegistroArbol* clave);
	ClaveRegistroArbol* buscarAproximado(ClaveRegistroArbol* clave);

	/* Previamente se debe llamar a la funcion buscar.
	 * Devuelve el siguiente registro del ultimo buscado o
	 * NULL si se terminan los registros del secuence set
	 */
	ClaveRegistroArbol* siguiente();
	//Lista por pantalla el arbol completo.
	void listar();
	//devuelve los registros mayores a r1 y menores a r2 (incluidos r1 y r2)
	void listarRango(ClaveRegistroArbol* r1, ClaveRegistroArbol* r2);

	NodoInterno* getNodo(unsigned int nroNodo);

	unsigned int getIdNodoLibre();

	void guardarNodo(NodoInterno* nodoSig);

	unsigned int modificar(ClaveRegistroArbol* claveVieja,ClaveRegistroArbol* claveNueva);

};



#endif /* ARBOL_H_ */
