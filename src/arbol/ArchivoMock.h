/*
 * ArchivoMock.h
 *
 *  Created on: 06/10/2013
 *      Author: Natalia
 */

#ifndef ARCHIVOMOCK_H_
#define ARCHIVOMOCK_H_

#include "Archivo.h"
#include <map>
#include <iostream>
#include "../utils/Constantes.h"
#include "NodoInterno.h"
#include "NodoHoja.h"

using namespace std;

class ArchivoMock: public Archivo {

private:
	// Redefino nuevamente
	bool primerLlamado;
	map<unsigned int,Bloque*> bloques;

public:
	ArchivoMock(TipoRegistro tipoRegistro, unsigned int tamanioBloque);
	virtual ~ArchivoMock();

	virtual bool existeArchivoFisico();
	virtual void crearArchivo();

	virtual unsigned int getTamanioBloque();
	virtual unsigned int getTamanioDatosDeControlBloque();
	virtual unsigned int getTamanioDatosDeControlRegistro();
	virtual unsigned int getCantidadTotalBloques();
	virtual unsigned int getIdBloqueLibre();
	virtual bool existenBloquesLibres();

	virtual Bloque* getBloque(unsigned int nroBloque);
	virtual Bloque* getRaiz();

	virtual void borrarBloque(unsigned int nroBloque);
	virtual void grabarBloque(Bloque* bloque);

	void vaciarBloques();

	void imprimir();

};

#endif /* ARCHIVOMOCK_H_ */
