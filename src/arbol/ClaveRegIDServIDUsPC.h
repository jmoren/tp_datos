#ifndef CLAVEREGIDSERVIDUSPC_H_
#define CLAVEREGIDSERVIDUSPC_H_

#include "ClaveRegistroArbol.h"
#include "../utils/Funciones.h"

class ClaveRegIDServIDUsPC: public ClaveRegistroArbol {
private: unsigned long idServicio;
		 unsigned long idUsuario;
		 unsigned long idPedidoCotizacion;

public:
	ClaveRegIDServIDUsPC();
	virtual ~ClaveRegIDServIDUsPC();
	ClaveRegIDServIDUsPC(unsigned long idServicio,unsigned long idUsuario,unsigned long idPedidoCotizacion);

	unsigned long getIdPedidoCotizacion();
	void setIdPedidoCotizacion(unsigned long idPedidoCotizacion);
	unsigned long getIdServicio();
	void setIdServicio(unsigned long idServicio);
	unsigned long getIdUsuario();
	void setIdUsuario(unsigned long idUsuario);

	short int comparar(ClaveRegistroArbol* claveParaComparar);
	unsigned int getTamanio();
	char* serializar();
	void deserializar(char*);

	string getDatosImportantes();


};

#endif /* CLAVEREGIDSERVIDUSPC_H_ */
