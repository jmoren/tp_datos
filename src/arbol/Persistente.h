#ifndef PERSISTENTE_H_
#define PERSISTENTE_H_

#include "../lib/Constantes.h"

class Persistente {
public:
	Persistente();
	virtual ~Persistente();
	virtual char* serializar();
	virtual void deserializar(char* dato);

public:
	void almacenarEnteroLargo(char* datoEscritura, unsigned int &pos, unsigned long enteroLargo);
	unsigned long recuperarEnteroLargo(char* datoLectura, unsigned int &pos);

	void almacenarEntero(char* datoEscritura, unsigned int &pos, unsigned int entero);
	unsigned int recuperarEntero(char* datoLectura, unsigned int &pos);

	void almacenarEnteroCorto(char* datoEscritura, unsigned int &pos, unsigned short enteroCorto);
	unsigned short int recuperarEnteroCorto(char* datoLectura, unsigned int &pos);

	void almacenarString(char* datoEscritura, unsigned int &pos, string cadena);
	string recuperarString(char* datoLectura, unsigned int &pos,unsigned int lon);

	void almacenarChar(char* datoEscritura, unsigned int &pos, char caracter);
	char recuperarChar(char* datoLectura, unsigned int &pos);

	void almacenarCharAsterisco(char* datoEscritura, unsigned int &pos, char *charAsterisco, unsigned int longitudCharAsterisco);
	char* recuperarCharAsterisco(char* datoLectura, unsigned int &pos, unsigned int longitudCharAsterisco);

};

#endif /* PERSISTENTE_H_ */
