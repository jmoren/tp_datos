/*
 * Arbol.cpp
 *
 *  Created on: 12/10/2013
 *      Author: natalia
 */

#include "Arbol.h"

unsigned int Arbol::TAMANIO_BLOQUE_ARBOL = 1024; //150;//144;//150;//1024;

Arbol::Arbol() {
	this->archivo = NULL;
	this->tipoRegistro = TipoUsuarioDNI; // Asignar default;
	this->raiz = NULL;
	this->ultimaClaveEncontrada = NULL;
	this->ultimoNodoHojaEncontrado = NULL;
}

Arbol::Arbol(TipoRegistro tipoRegistro){
	this->tipoRegistro = tipoRegistro;
	this->raiz = NULL;
	this->ultimaClaveEncontrada = NULL;
	this->ultimoNodoHojaEncontrado = NULL;

	this->archivo = new ArchivoFisico(tipoRegistro, Arbol::TAMANIO_BLOQUE_ARBOL); // TODO: NDL cambiar a Archivo

	//this->archivo = new ArchivoMock(tipoRegistro, Arbol::TAMANIO_BLOQUE_ARBOL); // TODO: NDL cambiar a Archivo

	if(!this->archivo->existeArchivoFisico()){
		// Creo el archivo fisico
		this->archivo->crearArchivo();

		// Creo el primer NodoInterno que sera la raiz vacia
		raiz = new NodoInterno(tipoRegistro, 0, 1, this->archivo->getTamanioBloque(), this->archivo->getTamanioTotalDisponibleParaRegistros());

		// Le pido al NodoInterno que cree un bloque y lo grabo en el archivo
		Bloque *bloqueNuevo = raiz->crearBloque();
		this->archivo->grabarBloque(bloqueNuevo);
	} else {
		leerRaiz();
	}
	//if(raiz != NULL){  // TODO sacar
	//	delete(raiz);   // TODO sacar
	//}
	//leerRaiz();       // TODO: NDL descomentar el else y sacar esta linea.
}

Arbol::~Arbol() {
	if(this->archivo != NULL){
		delete(this->archivo);
		this->archivo = NULL;
	}

	//this->tipoRegistro = TipoUsuarioDNI;

	if(this->raiz != NULL){
		//cout << "delete(this->raiz); " << endl;
		//cout << "*_*_*_*_*_*_* this->raiz 3= " << this->raiz << endl;
		//cout << "*_*_*_*_*_*_* this->raiz 3= " << *(this->raiz) << endl;
		delete(this->raiz);
		this->raiz = NULL;
	}

	if(this->ultimoNodoHojaEncontrado != NULL){
		delete(this->ultimoNodoHojaEncontrado);
		this->ultimoNodoHojaEncontrado = NULL;
	}

	if(this->ultimaClaveEncontrada != NULL){
		// No se hace delete de este objeto porque se hace en el destructor del ultimoNodoHojaEncontrado
		this->ultimaClaveEncontrada = NULL;
	}

}

// Metodos privados -----------------------------------------------------------------------------------------
void Arbol::leerRaiz(){
	Bloque *bloqueRaiz = this->archivo->getRaiz();
	if(bloqueRaiz != NULL){
		this->raiz = new NodoInterno(this->tipoRegistro, bloqueRaiz, this->archivo->getTamanioTotalDisponibleParaRegistros());
		//cout << "Raiz leida : " << *raiz << endl;
	}else{
		//cout << "ERROR: leerRaiz no pudo obtener la raiz desde el archivo.";
	}
	ArchivoMock *archivoMock = dynamic_cast<ArchivoMock*>(this->archivo);
	// Si el archivo no es del tipo ArchivoMock entonces libero la memoria del bloque ya que no se volvera a usar.
	if(archivoMock == NULL){
		delete(bloqueRaiz);
	}
}

bool Arbol::raizVacia(){
	if(this->raiz->getRegistrosNodo().size()==0){
		return true;
	}else{
		return false;
	}
}

// Getters --------------------------------------------------------------------------------------------------
Archivo* Arbol::getArchivo(){
	return this->archivo;
}

TipoRegistro Arbol::getTipoRegistro(){
	return this->tipoRegistro;
}

// Devuelve 0 si fue exitoso o -1 si hubo un error
short Arbol::insertar(ClaveRegistroArbol* clave){
	short respuesta = -1;
	ClaveRegistroArbol* claveOriginal = clave;

	if(clave == NULL){
		return -1;
	}

	if(raizVacia()){ // El arbol esta vacio y no tiene ningun nodo hojas
		//Creo el primer nodo hoja
		NodoHoja *primerHoja = new NodoHoja(this->getTipoRegistro(), getIdNodoLibre(), Arbol::TAMANIO_BLOQUE_ARBOL, this->archivo->getTamanioTotalDisponibleParaRegistros());

		//Inserto el primer elemento en la primer hoja
		unsigned short resultado = primerHoja->insertarClaveArbol(clave, this->tipoRegistro);

		// Si se inserto correctamente la primer clave en el arbol entonces la guardamos.
		if(resultado == 1){

			//Inserto el primer elemento en la raiz y sus idNodoHijo izquierdo y derecho seran iguales apuntando a la primer hoja
			unsigned short resultadoRaiz = this->raiz->insertarClaveArbol(clave, this->tipoRegistro, primerHoja->getIdNodo(), primerHoja->getIdNodo());

			if(resultadoRaiz == 1){
				//Grabo la raiz
				this->archivo->grabarBloque(this->raiz->crearBloque());

				// Grabo la primer hoja en el archivo
				this->archivo->grabarBloque(primerHoja->crearBloque());

				//cout << "Raiz: " << *(this->raiz) << endl;
				//cout << "Primer hoja creada : " << *primerHoja << endl;

				//cout << endl << "Imprimir archivo 3---------------------------------------------" << endl;
				//this->getArchivo()->imprimir();
			}
		}
		delete(primerHoja);
		respuesta = 0;

	}else{
		short resultado = this->insertarRecursivo(this->raiz,clave);
		switch (resultado) {
			case 0:
				respuesta = 0;
				break;
			case 1: {
				this->guardarNodo(this->raiz);
				respuesta = 0;
				break;
			}
			case 2: {
				NodoInterno *nodoHijoDer = (this->raiz)->dividir(this->getIdNodoLibre());
				this->guardarNodo(nodoHijoDer);

//				cout << "*_*_*_*_*_*_* this->raiz = " << this->raiz << endl;
//				cout << "*_*_*_*_*_*_* this->raiz = " << *(this->raiz) << endl;

				NodoInterno *nodoHijoIzq = this->raiz;
				nodoHijoIzq->setIdNodo(this->getIdNodoLibre());

				NodoInterno *nodoRaiz = new NodoInterno(this->tipoRegistro,0,nodoHijoIzq->getNivel() +1,this->TAMANIO_BLOQUE_ARBOL, this->archivo->getTamanioTotalDisponibleParaRegistros());
				//cambio la raiz por el nuevo nodo
				this->raiz = nodoRaiz;
				nodoRaiz->insertarClaveArbol(clave, this->tipoRegistro, nodoHijoIzq->getIdNodo(), nodoHijoDer->getIdNodo());

				// Hay que liberar la memoria de la clave que se inserta clonada en el nodoRaiz y que surge de dividir un nodoInterno.
				if(clave == claveOriginal){
					cout << "ERROR: No puede ocurrir que la claveOriginal sea la misma que se inserta en la raiz" << endl;
				}
				delete(clave);

				this->guardarNodo(this->raiz);
				this->guardarNodo(nodoHijoIzq);

				//cout << endl << "Imprimir archivo 4---------------------------------------------" << endl;
				//this->getArchivo()->imprimir();
				delete(nodoHijoDer);
				delete(nodoHijoIzq);

//				cout << "*_*_*_*_*_*_* this->raiz 2= " << this->raiz << endl;
//				cout << "*_*_*_*_*_*_* this->raiz 2= " << *(this->raiz) << endl;

				respuesta = 0;
				break;
			}
			case 3:
				respuesta = -1;
				break;
		}

	}

	//TODO: Ver de sacar esto y que el delete lo haga la funcion que llama al arbol.
	if(claveOriginal != NULL){
		delete(claveOriginal); // Si no se quiere que el arbol elimine la clave pasada, entonces sacar esta linea.
	}
	return respuesta;
}

// Devuelve 0 si fue exitoso o -1 si hubo un error o -2 no se encontro el registro
/*
 *  Devuelve:
 * 				0	Si se elimino bien o no se modifico la raiz
 * 				3	No existe un registro con el identificador igual al que se quiso eliminar
 */
short Arbol::eliminar(ClaveRegistroArbol* clave){
	unsigned int idNodo = 0;
	unsigned int resultado = this->eliminarRecursivo(this->raiz,clave,idNodo);

	switch(resultado){
	case 0: break;
	case 1: this->guardarNodo(this->raiz);
			return 0;
	case 2: {
				if (this->raizVacia()==true){
					NodoInterno* nodo = this->getNodo(idNodo);
					if (nodo->getNivel() > 0){
						nodo->setIdNodo(0);
						delete(this->raiz);
						this->raiz = nodo;
						this->archivo->borrarBloque(idNodo);
					}
					else {//si el nodo es una hoja es la unica hoja que tiene el arbol
						if (nodo->getCantRegistros() > 0){
							RegistroNodo* reg = nodo->registrosNodo.front();
							if(this->raiz->insertarClaveArbol(reg->getClave(), this->tipoRegistro, nodo->getIdNodo(), nodo->getIdNodo()) != 1){
								cout << "ERROR: NodoHoja:fallo nodoRaiz->insertarClaveArbol" << endl;
							}
						} else {
							this->archivo->borrarBloque(idNodo);
							this->raiz->setIdNodoIzq(0);
						}
						delete(nodo);
					}
				}
				this->guardarNodo(this->raiz);
				return 0;
			}
	case 3: break;
	}
	return resultado;
}

// Elimina el registroViejo y luego inserta el registroNuevo. Devuelve 0 si fue exitoso o -1 si hubo un error
short Arbol::actualizar(ClaveRegistroArbol* claveVieja, ClaveRegistroArbol* claveNueva){

	return 0;
}

// Devuelve el registro buscado o
ClaveRegistroArbol* Arbol::buscar(ClaveRegistroArbol* clave){
	return buscar(this->raiz, clave, true);
}

/**
 * nodo					Nodo a partir del cual se va a buscar
 * clave				Clave buscada
 * busquedaAproximada	Cuando sea true se devolvera la clave buscada y de no encontrarla se devolvera el siguiente si es que existe.
 */
ClaveRegistroArbol* Arbol::buscar(NodoInterno *nodo, ClaveRegistroArbol* clave, bool busquedaExacta){
	if (this->raizVacia() == true){
		return NULL;
	}
	else {	//Si es un nodo HOJA
		if (nodo->getNivel() == 0){
			std::list<RegistroNodo*> registrosNodo = nodo->getRegistrosNodo();
			RegistroNodo *ultimoRegistroLeido = NULL;

			for (std::list<RegistroNodo*>::iterator it = registrosNodo.begin(); it != registrosNodo.end(); it++){
				ultimoRegistroLeido = (*it);

				short int comparacion = ultimoRegistroLeido->getClave()->comparar(clave);
				if(comparacion == 0){
					// Busqueda Exacta
					return guardarUltimaClaveYHojaBuscada(ultimoRegistroLeido, dynamic_cast<NodoHoja*>(nodo));

				} else if(comparacion > 0){
					if(busquedaExacta){// Si es es busqueda exacta entonces devolvemos NULL para indicar que la clave no esta en el arbol
						ClaveRegistroArbol* claveEncontrada = guardarUltimaClaveYHojaBuscada(NULL, NULL);
						return claveEncontrada;
					}else{ // Si es busquedaAproxima entonces al no estar la clave en el arbol devolvemos la siguiente clave buscada si es que existe una siguiente porque la clave podria ser mayor.
						return guardarUltimaClaveYHojaBuscada(ultimoRegistroLeido, dynamic_cast<NodoHoja*>(nodo));
					}
				}
			}

			// Si no se encontro ningun registro igual o mayor entonces la clave es mayor a la mas grande de este nodo, entonces para busqueda exacta no se encuentra esta clave en el arbol.
			if(busquedaExacta){
				ClaveRegistroArbol* claveEncontrada = guardarUltimaClaveYHojaBuscada(NULL, NULL);
				return claveEncontrada;
			}else{
				/* Si es busqueda aproximada entonces al ser la clave buscada mayor a las claves del nodo hoja y menor que el separador del nodo interno,
				 * se busca la primer clave del siguiente nodo hoja para devolver como siguiente clave a la buscada.
				 */
				NodoHoja *hoja = (NodoHoja*) nodo;
				if(hoja->getIdNodoSiguiente() > 0){
					NodoHoja *hojaSiguiente = dynamic_cast<NodoHoja*>(this->getNodo(hoja->getIdNodoSiguiente()));
					// Si la siguiente hoja esta bien recuperada y tiene registros, entonces devolvemos su primer registro que sera el siguiente al buscado que no se encontro.
					if((hojaSiguiente != NULL) && (hojaSiguiente->getRegistrosNodo().size() > 0)){
						return guardarUltimaClaveYHojaBuscada(hojaSiguiente->getRegistrosNodo().front(), hojaSiguiente);
					}else{ // La hoja siguiente esta vacia, este caso no deberia ocurrir nunca entonces tambien devolveria NULL
						ClaveRegistroArbol* claveEncontrada = guardarUltimaClaveYHojaBuscada(NULL, NULL);
						return claveEncontrada;
					}
				}else{ // Estamos en la ultima hoja entonces no existe registro siguiente al buscado y devolvemos NULL para el caso de busqueda aproximada
					ClaveRegistroArbol* claveEncontrada = guardarUltimaClaveYHojaBuscada(NULL, NULL);
					return claveEncontrada;
				}
			}

		} else if (nodo->getNivel() > 0) { //si el nodo no es hoja, es nodo INTERNO
			std::list<RegistroNodo*> registrosNodo = nodo->getRegistrosNodo();
			RegistroNodoInterno *ultimoRegistroLeido = NULL;
			for (std::list<RegistroNodo*>::iterator it = registrosNodo.begin(); it != registrosNodo.end(); it++){
				ultimoRegistroLeido = (RegistroNodoInterno*) (*it);

				short int comparacion = ultimoRegistroLeido->getClave()->comparar(clave);
				if(comparacion == 0){
					NodoInterno *nodoSiguiente = this->getNodo(ultimoRegistroLeido->getIdNodoDer());
					ClaveRegistroArbol* claveEncontrada = buscar(nodoSiguiente, clave, busquedaExacta);
					/* Si el nodo siguiente es interno y no es la raiz entonces debe liberar su memoria dado que no se va a volver a usar
					 * O si el nodoSiguiente fue una hoja y la clave no se encontro entonces hay que liberar su memoria ya que no fue guardada
					 * por el arbol como  this->ultimoNodoHojaEncontrado
					 */
					if( ((nodoSiguiente->getNivel() > 0) && (nodoSiguiente->getIdNodo() > 0)) ||
						((nodoSiguiente->getNivel() == 0) && (claveEncontrada == NULL )) ){
						delete(nodoSiguiente); // Liberamos la memoria del nodoSiguiente que ya se utilizo y no se guardo como ultimoNodoHoja
					}
					return claveEncontrada;
				} else if(comparacion > 0){
					NodoInterno *nodoSiguiente = this->getNodo(ultimoRegistroLeido->getIdNodoIzq());
					ClaveRegistroArbol* claveEncontrada = buscar(nodoSiguiente, clave, busquedaExacta);
					/* Si el nodo siguiente es interno y no es la raiz entonces debe liberar su memoria dado que no se va a volver a usar
					 * O si el nodoSiguiente fue una hoja y la clave no se encontro entonces hay que liberar su memoria ya que no fue guardada
					 * por el arbol como  this->ultimoNodoHojaEncontrado
					 */
					if( ((nodoSiguiente->getNivel() > 0) && (nodoSiguiente->getIdNodo() > 0)) ||
						((nodoSiguiente->getNivel() == 0) && (claveEncontrada == NULL )) ){
						delete(nodoSiguiente); // Liberamos la memoria del nodoSiguiente que ya se utilizo y no se guardo como ultimoNodoHoja
					}
					return claveEncontrada;
				}
			}
			// Si no se encontro ningun registro igual o mayor entonces la clave es mayor a la mas grande, entonces vamos al hijo derecho del ultimo registro leido, el del final del nodo.
			NodoInterno *nodoSiguiente = this->getNodo(ultimoRegistroLeido->getIdNodoDer());
			ClaveRegistroArbol* claveEncontrada = buscar(nodoSiguiente, clave, busquedaExacta);
			/* Si el nodo siguiente es interno y no es la raiz entonces debe liberar su memoria dado que no se va a volver a usar
			 * O si el nodoSiguiente fue una hoja y la clave no se encontro entonces hay que liberar su memoria ya que no fue guardada
			 * por el arbol como  this->ultimoNodoHojaEncontrado
			 */
			if( ((nodoSiguiente->getNivel() > 0) && (nodoSiguiente->getIdNodo() > 0)) ||
				((nodoSiguiente->getNivel() == 0) && (claveEncontrada == NULL )) ){
				delete(nodoSiguiente); // Liberamos la memoria del nodoSiguiente que ya se utilizo y no se guardo como ultimoNodoHoja
			}
			return claveEncontrada;
		}
	}
	return NULL; // No deberia pasar nunca este caso.
}

ClaveRegistroArbol* Arbol::guardarUltimaClaveYHojaBuscada(RegistroNodo *registroEncontrado, NodoHoja *ultimaHojaLeida){
	if(registroEncontrado != NULL){
		this->ultimaClaveEncontrada = registroEncontrado->getClave();
	}else{
		this->ultimaClaveEncontrada = NULL;
	}

	if(this->ultimoNodoHojaEncontrado != NULL){
		delete(this->ultimoNodoHojaEncontrado);
	}
	this->ultimoNodoHojaEncontrado = ultimaHojaLeida;

	return this->ultimaClaveEncontrada;
}

ClaveRegistroArbol* Arbol::buscarAproximado(ClaveRegistroArbol* clave){
	return buscar(this->raiz, clave, false);
}

/* Previamente se debe llamar a la funcion buscar.
 * Devuelve el siguiente registro del ultimo buscado o
 * NULL si se terminan los registros del secuence set
 */

ClaveRegistroArbol* Arbol::siguiente(){
	if((this->ultimaClaveEncontrada != NULL) && (this->ultimoNodoHojaEncontrado != NULL)){
		std::list<RegistroNodo*> registrosNodo = this->ultimoNodoHojaEncontrado->getRegistrosNodo();
		RegistroNodo *ultimoRegistroLeido = NULL;
		for (std::list<RegistroNodo*>::iterator it = registrosNodo.begin(); it != registrosNodo.end(); it++){
			ultimoRegistroLeido = (*it);

			short int comparacion = ultimoRegistroLeido->getClave()->comparar(this->ultimaClaveEncontrada);
			if(comparacion > 0){ // Si es mayor entonces se encontro la siguiente clave
				this->ultimaClaveEncontrada = ultimoRegistroLeido->getClave();
				return this->ultimaClaveEncontrada;
			}
		}
		// No se encontro en el ultimoNodoHojaEncontrado un registro mayor a la ultimaClaveEncontrada entonces hay que buscarla en el proximo nodo hoja
		if(this->ultimoNodoHojaEncontrado->getIdNodoSiguiente() > 0){
			NodoHoja *hojaSiguiente = dynamic_cast<NodoHoja*>(this->getNodo(this->ultimoNodoHojaEncontrado->getIdNodoSiguiente()));
			// Si la siguiente hoja esta bien recuperada y tiene registros, entonces devolvemos su primer registro que sera el siguiente al buscado que no se encontro.
			if((hojaSiguiente != NULL) && (hojaSiguiente->getRegistrosNodo().size() > 0)){
				return guardarUltimaClaveYHojaBuscada(hojaSiguiente->getRegistrosNodo().front(), hojaSiguiente);
			}else{ // La hoja siguiente esta vacia, este caso no deberia ocurrir nunca entonces tambien devolveria NULL
				ClaveRegistroArbol* claveEncontrada = guardarUltimaClaveYHojaBuscada(NULL, NULL);
				return claveEncontrada;
			}
		}else{ // Estamos en la ultima hoja entonces no existe registro siguiente al buscado y devolvemos NULL para el caso de busqueda aproximada
			ClaveRegistroArbol* claveEncontrada = guardarUltimaClaveYHojaBuscada(NULL, NULL);
			return claveEncontrada;
		}
	}
	return NULL;
}

//Lista por pantalla el arbol completo.
void Arbol::listar(){
	/*cout << "INFO: Arbol::listar() -> Raiz: " << endl;
	cout << "----------------------------------------------------------------------------------------" << endl;
	cout << "INFO: Raiz: " << *(this->raiz) << endl;
	cout << "----------------------------------------------------------------------------------------" << endl;*/
	ClaveRegistroArbol *primeraClave = getPrimeraClave();
	if(primeraClave != NULL){
		//cout << "Se encontro la primera clave:   " << *primeraClave << endl;
		while(primeraClave != NULL){
			primeraClave = siguiente();
			if(primeraClave != NULL){
				//cout << "Se encontro la clave siguiente: " << *primeraClave << endl;
			}
		}
		//cout << "Fin Listar." << endl;
	}else{
		//cout << "NO se encontro la primer clave para listar." << endl;
	}
}

ClaveRegistroArbol* Arbol::getPrimeraClave(){
	ClaveRegistroArbol* primeraClave = NULL;
	NodoInterno *nodo = this->raiz;
	bool finRecorrido = false;
	while(!finRecorrido){
		// Si el nodo tiene al menos un elemento;
		if(nodo->getRegistrosNodo().size() > 0){
			if(nodo->getNivel() > 0){ // Nodo interno
				// Obtenemos el primer registro del nodo que es el menor.
				RegistroNodoInterno *registro = dynamic_cast<RegistroNodoInterno*>( nodo->getRegistrosNodo().front() );
				if(registro != NULL){
					// Buscamos el nodo izquierdo que es siempre el menor.
					NodoInterno *nodoSiguiente = getNodo(registro->getIdNodoIzq());

					// Liberamos la memoria del nodoInterno anterior si no es la raiz
					if(nodo->getIdNodo() > 0){
						delete(nodo);
					}

					// Asignamos nodoSiguiente a nodo para empezar el proximo loop
					nodo = nodoSiguiente;
				}else{
					// Liberamos la memoria del nodoInterno anterior si no es la raiz
					if(nodo->getIdNodo() > 0){
						delete(nodo);
					}
					finRecorrido = true; // No se encontro la primer clave
				}
			} else { // Nodo Hoja
				// Obtenemos el primer registro del nodo que es el menor.
				RegistroNodo *registroEncontrado = nodo->getRegistrosNodo().front();
				primeraClave = guardarUltimaClaveYHojaBuscada(registroEncontrado, (NodoHoja*)nodo);
				finRecorrido = true;
			}
		}else{
			// Liberamos la memoria del nodoInterno anterior si no es la raiz
			if(nodo->getIdNodo() > 0){
				delete(nodo);
			}
			finRecorrido = true; // No se encontro la primer clave
		}
	}
	return primeraClave;
}

void Arbol::enorden(NodoInterno *nodo){
 /* if (nodo->getNivel() == 0) { // Es Hoja
      cout << nodo << endl;
  }else{ // es interno
	  NodoInterno hijoIzq;
	  NodoInterno hijoDer;
	  if(nodo->getNivel() == 1){ // Se que los hijos son hojas
		  hijoIzq = new NodoHoja(this->tipoRegistro, this->archivo->getBloque(nodo->getI));
		  hijoDer
	  }else{ // los hijos son internos

	  }
	  enorden(hijoIzq);
	  cout << nodo << endl;
	  enorden(hijoDer);
  }*/
}

//devuelve los registros mayores a r1 y menores a r2 (incluidos r1 y r2)
void Arbol::listarRango(ClaveRegistroArbol* clave1, ClaveRegistroArbol* clave2){
	/*cout << "----------------------------------------------------------------------------------------" << endl;
	cout << "listarRango(ClaveRegistroArbol* clave1, ClaveRegistroArbol* clave2)" << endl;
	cout << "----------------------------------------------------------------------------------------" << endl;
	cout << "INFO: Raiz: " << *(this->raiz) << endl;
	cout << "----------------------------------------------------------------------------------------" << endl;
	cout << "Clave Desde = " << *clave1 << endl;
	cout << "----------------------------------------------------------------------------------------" << endl;
	cout << "Clave Hasta = " << *clave2 << endl;
	cout << "----------------------------------------------------------------------------------------" << endl;*/
	ClaveRegistroArbol *claveEncontrada = this->buscarAproximado(clave1);
	if(claveEncontrada != NULL){
		//cout << "Se encontro la primera clave del listado por Rango: " << *claveEncontrada << endl;
		while((claveEncontrada != NULL) && (claveEncontrada->comparar(clave2) < 0)){
			claveEncontrada = this->siguiente();
			if((claveEncontrada != NULL) && (claveEncontrada->comparar(clave2) <= 0)){
				//cout << "Se encontro la clave siguiente: " << *claveEncontrada << endl;
			}else{
				//cout << "NO se encontro la clave siguiente: " << endl;
			}
		}
	}else{
		//cout << "NO se encontro la clave desde buscada ni ningun valor mayor aproximado: " << endl;
	}
}

//funcion recursiva
/*
 *  Devuelve:
 * 				0	Si el registroNodo recibido no se actualizo
 * 				1	El nodo se actualizo insertanto el registroNodo
 * 				2	El nodo se desbordo al intentar insertar y no lo inserto
 * 				3	Ya existe un registro con el identificador igual al que se quiso insertar
 */
short Arbol::insertarRecursivo(NodoInterno* nodo,ClaveRegistroArbol* &regClave){
	short respuesta = 0;
	//Si es un nodo HOJA
	if (nodo->getNivel() == 0){
		respuesta =((NodoHoja*)nodo)->insertarClaveArbol(regClave, this->tipoRegistro);

		//validar si hay overflow
		if (respuesta==2){
			regClave=((NodoHoja*)nodo)->getDatosClaveMedio();
		}
		return respuesta;

	} else if (nodo->getNivel() > 0) { //si el nodo no es hoja, es nodo INTERNO

		unsigned int idNodoHijo = nodo->buscarNodoHijo(regClave);
		NodoInterno* nodoHijo = this->getNodo(idNodoHijo);
		short result = this->insertarRecursivo(nodoHijo,regClave);
		switch (result) {
			case 1:
					this->guardarNodo(nodoHijo);
					respuesta = 0;
					break;
			case 0:
					respuesta = 0;
					break;
			case 3:
					respuesta = 3;
					break;
			case 2:
					// Nodo HOJA
					if (nodoHijo->getNivel() == 0){
						// Creo un nodo nuevo con la mitad de los registros de la derecha
						NodoHoja* nodoNuevo = ((NodoHoja*)nodoHijo)->dividir(this->getIdNodoLibre());

						// Actualizo las referencias a nodos siguiente y anterior del nodo nuevo
						nodoNuevo->setIdNodoSiguiente(((NodoHoja*)nodoHijo)->getIdNodoSiguiente());
						nodoNuevo->setIdNodoAnterior(((NodoHoja*)nodoHijo)->getIdNodo());

						// Actualizo referencia a nodo siguiente del nodo hijo partido
						((NodoHoja*)nodoHijo)->setIdNodoSiguiente(nodoNuevo->getIdNodo());

						// Actualizo y guardo la referencia a nodo anterior del nodo siguiente al nodoNuevo si existe nodo siguiente, es decir que el idNodo sea mayor a cero.
						if(nodoNuevo->getIdNodoSiguiente() > 0){
							NodoHoja* nodoSig = (NodoHoja*)(this->getNodo(nodoNuevo->getIdNodoSiguiente()));
							nodoSig->setIdNodoAnterior(nodoNuevo->getIdNodo());
							this->guardarNodo(nodoSig);
							// Libera nodoSig
							delete(nodoSig);
						}

						// Guardo los nodo hoja actualizados
						this->guardarNodo(nodoHijo);
						this->guardarNodo(nodoNuevo);

						// caso especial raiz con una sola hoja, entonces vaciar la raiz.
						if( (nodo->getIdNodo()==0) &&  //El nodo es raiz
							(nodo->getRegistrosNodo().size() == 1) && // y tiene un solo elemento
							( ((RegistroNodoInterno*)(nodo->getRegistrosNodo().front()))->getIdNodoIzq() == ((RegistroNodoInterno*)(nodo->getRegistrosNodo().front()))->getIdNodoDer() ) //y sus idnodohijo son iguales
						   ){
							unsigned int idNodo = 0;
							nodo->eliminarRegistroNodo(nodo->getRegistrosNodo().front(),idNodo);
						}

						respuesta = nodo->insertarClaveArbol(regClave, this->tipoRegistro, nodoHijo->getIdNodo(), nodoNuevo->getIdNodo());
						if (respuesta == 2){
							//regClave = NULL;
							regClave = nodo->getDatosClaveMedio();
						}
						if(nodoNuevo != NULL){
							delete(nodoNuevo);
						}

					} else if (nodoHijo->getNivel() > 0){ // Nodo INTERNO
						NodoInterno* nodoNuevo = (NodoInterno*)nodoHijo->dividir(this->getIdNodoLibre());

						this->guardarNodo(nodoHijo);
						this->guardarNodo(nodoNuevo);

						respuesta = nodo->insertarClaveArbol(regClave, this->tipoRegistro, nodoHijo->getIdNodo(), nodoNuevo->getIdNodo());
						delete(regClave);
						regClave = NULL;

						if (respuesta == 2){
							regClave = nodo->getDatosClaveMedio();
						}
						if(nodoNuevo != NULL){
							delete(nodoNuevo);
						}
						//cout << endl << "Imprimir archivo 5---------------------------------------------" << endl;
						//this->getArchivo()->imprimir();
					}
		}
		if(nodoHijo != NULL){
			delete(nodoHijo);
		}
		return respuesta;
	}
	return 0;
}

NodoInterno* Arbol::getNodo(unsigned int idNodo){
	NodoInterno* nuevoNodo = NULL;
	Bloque *bloque = this->archivo->getBloque(idNodo); //obtengo el bloque del archivo

	if(bloque != NULL){
		if (bloque->getRegistros().size() > 0){
			NodoInterno *nodoTemp = new NodoInterno();
			unsigned int nivel = nodoTemp->recuperarNivel(bloque->getRegistros().front());
			delete(nodoTemp);
			if (nivel==0){
				nuevoNodo = new NodoHoja(this->tipoRegistro,bloque, this->archivo->getTamanioTotalDisponibleParaRegistros());

			} else {
				nuevoNodo = new NodoInterno(this->tipoRegistro,bloque, this->archivo->getTamanioTotalDisponibleParaRegistros());
			}
		} else {
			cout<<"Error:El nodo "<<idNodo<<" esta vacio."<<endl;
		}

		ArchivoMock *archivoMock = dynamic_cast<ArchivoMock*>(this->archivo);
		// Si el archivo no es del tipo ArchivoMock entonces libero la memoria del bloque ya que no se volvera a usar.
		if(archivoMock == NULL){
			delete(bloque);
		}
	}
	return nuevoNodo;
}

unsigned int Arbol::getIdNodoLibre(){

	return archivo->getIdBloqueLibre();

}

void Arbol::guardarNodo(NodoInterno* nodoSig){
	Bloque *bloque = nodoSig->crearBloque();
	archivo->grabarBloque(bloque);

}

/*
 *  Devuelve:
 * 				0	Si el registroNodo recibido no se actualizo
 * 				1	Elimino el registro con el identificador pasado.
 * 				2   Si hay underflow
 * 				3	No existe un registro con el identificador igual al que se quiso eliminar
 */
short Arbol::eliminarRecursivo(NodoInterno* nodo,ClaveRegistroArbol* clave,unsigned int &idNodo){

	if (nodo->getNivel() == 0){
		// Estoy en la hoja a borrar
		unsigned short resultado = ((NodoHoja*)nodo)->eliminarClaveArbol(clave, this->tipoRegistro);

		return resultado;

	} else if (nodo->getNivel()>0){ // Es nodo interno
		short respuesta = 0;

		unsigned int idNodoHijo = nodo->buscarNodoHijo(clave);
		NodoInterno* nodoHijo = this->getNodo(idNodoHijo);
		short result = this->eliminarRecursivo(nodoHijo,clave,idNodo);

		bool casoEspecial = false;
		// Verifico caso especial de raiz con una sola hoja
		if ((nodo->getIdNodo() == 0) && (nodoHijo->getNivel() == 0)){ // El hijo era una hoja.
			//lee el hno derecho, si no tiene lee el izquierdo
			TipoNodoHno tipoHno;
			unsigned int idNodoHno = nodoHijo->leerNodoHno(nodo,tipoHno);
			// Si estoy en el caso especial de que es la raiz y tiene una sola hoja
			if (idNodoHno == nodoHijo->getIdNodo()) {
				this->guardarNodo(nodoHijo);
				short respuestaEliminar = nodo->eliminarClaveArbol(clave,this->tipoRegistro,idNodo);
				if(respuestaEliminar == 3){
					respuesta = 0;
				}else{
					respuesta = respuestaEliminar;
				}
				idNodo = nodoHijo->getIdNodo();
				casoEspecial = true;
			}
		}
		if(!casoEspecial){
			switch (result) {
				case 0:
					respuesta = 0;
					break;
				case 1:
					this->guardarNodo(nodoHijo);
					respuesta = 0;
					break;
				case 2:{
					if (nodoHijo->getNivel() == 0){ // El hijo era una hoja.
						//lee el hno derecho, si no tiene lee el izquierdo
						TipoNodoHno tipoHno;
						unsigned int idNodoHno = nodoHijo->leerNodoHno(nodo,tipoHno);
						NodoHoja *nodoHno = (NodoHoja*)(this->getNodo(idNodoHno));
						if (nodoHno->tieneCargaMinima(tipoHno)) {
							// El nodo hermano no puede prestar ningun registro, no puede balancear porque queda en underflow, entonces hay que unirlos.
							respuesta = unirNodosHermanos(nodoHijo, nodoHno, nodo, tipoHno,idNodo);
						} else { // El nodo hermano puede prestar registros sin quedar en underflow.
							// El nodo padre permite cambiar la clave porque no queda en overflow
							if(nodo->permiteCambiarClave(nodoHno, nodoHijo, tipoHno)){
								((NodoHoja*)nodoHijo)->balancear(nodoHno,nodo,tipoHno,this->tipoRegistro);
								cout << *nodoHijo << endl;
								cout << *nodoHno << endl;
								cout << *nodo << endl;
								this->guardarNodo(nodoHijo);
								this->guardarNodo(nodoHno);
								// se modifico el padre con el balanceo de los hijos
								respuesta = 1;
							}else{ // El nodo padre no permite cambiar la clave dado que sino da overflow
								// Si el nodoHijo queda vacio, entonces hay que unir los 3 nodos.
								if(nodoHijo->registrosNodo.size() == 0){
									// Unimos los dos nodos hoja hermanos, y en realidad quedara solo el hermano.
									respuesta = unirNodosHermanos(nodoHijo, nodoHno, nodo, tipoHno,idNodo);
								}else{ // Si el nodoHijo no queda vacio entonces quedara en underflow sin balancear
									respuesta = 0;
								}
							}
						}
						if (nodoHno != NULL){
							delete(nodoHno);
						}

					} else {//nodoHijo es un nodo interno
						//lee el hno derecho, si no tiene lee el izquierdo
						TipoNodoHno tipoHno;
						unsigned int idNodoHno = nodoHijo->leerNodoHno(nodo,tipoHno);
						NodoInterno *nodoHno = this->getNodo(idNodoHno);
						if (nodoHno->tieneCargaMinima(tipoHno) == true) {
							//Tengo que ver si mi hermano tiene espacio para todos los registros mios (menos del 20%) + el registro separador del nodoPadre.
							//TODO: if(nodo->(nodoHno, nodoHijo, tipoHno)){
							// Sino puedo unir los nodos hermanos.
							respuesta = unirNodosHermanos(nodoHijo, nodoHno, nodo, tipoHno,idNodo);
							cout<<*nodo<<endl;
						} else { // El nodo hermano puede prestar registros sin quedar en underflow.
							// El nodo padre permite cambiar la clave porque no queda en overflow
							if(nodo->permiteCambiarClaveInterno(nodoHno, nodoHijo, tipoHno)){
								nodoHijo->balancear(nodoHno,nodo,tipoHno,this->getTipoRegistro());
								cout<<*nodoHijo<<endl;
								cout<<*nodoHno<<endl;
								cout<<*nodo<<endl;
								this->guardarNodo(nodoHijo);
								this->guardarNodo(nodoHno);
								// se modifico el padre con el balanceo de los hijos
								respuesta = 1;
							}else{ // El nodo padre no permite cambiar la clave dado que sino da overflow
								// Si el nodoHijo queda vacio, entonces hay que unir los 3 nodos.
								if(nodoHijo->registrosNodo.size() == 0){
									//En este caso especial
									// Unimos los dos nodos hoja hermanos, y en realidad quedara solo el hermano.
									respuesta = unirNodosHermanos(nodoHijo, nodoHno, nodo, tipoHno,idNodo);
								}else{ // Si el nodoHijo no queda vacio entonces quedara en underflow sin balancear
									respuesta = 0;
								}
							}
						}
						delete(nodoHno);
					}
					break;
				}
				case 3:
					respuesta = 3;
					break;
			}
		}
		if(nodoHijo != NULL){
			delete(nodoHijo);
		}
		return respuesta;
	}
	return 0;
}

/*
 *  Devuelve:
 * 				0	Si el registroNodo del padre no se elimino
 * 				1	Elimino el registro del padre.
 * 				2   Si hay underflow
 * 				3	No existe un registro con el identificador igual al que se quiso eliminar
 */
short Arbol::unirNodosHermanos(NodoInterno *nodoHijo, NodoInterno *nodoHno, NodoInterno *nodo, TipoNodoHno tipoHno, unsigned int &idNodo){
	RegistroNodo *claveMedio = NULL;
	if (tipoHno == DERECHO){
		claveMedio= nodo->obtenerRegistroClaveNodo(nodoHijo->getIdNodo(),nodoHno->getIdNodo());

		// Uno los dos nodos hermanos
		if(nodoHijo->getNivel() == 0 && nodoHno->getNivel() == 0){
			// Uno los dos nodos hermanos
			((NodoHoja*)(nodoHijo))->unir((NodoHoja*)nodoHno);
			//Actualizo el idHermanoIzquierdo del nodo Hoja siguiente
			NodoInterno *hojaSiguiente = this->getNodo(((NodoHoja*)nodoHijo)->getIdNodoSiguiente());
			if (hojaSiguiente->getIdNodo()!=0){
				((NodoHoja*)hojaSiguiente)->setIdNodoAnterior(nodoHijo->getIdNodo());
				this->guardarNodo(hojaSiguiente);
			}
			delete(hojaSiguiente);
			//libero el nodo hermano
			this->archivo->borrarBloque(nodoHno->getIdNodo());
			//La union resulta en este nodo
			this->guardarNodo(nodoHijo);
			cout<<*nodoHijo<<endl;
			//devuelvo a la llamada anterior el nro de nodo con el que me quede en la union
			idNodo = nodoHijo->getIdNodo();
		}else{
			// unir con clave medio
			char* claveSerializada = claveMedio->getClave()->serializar();
			ClaveRegistroArbol *claveClonada = FabricaClaveRegistro::crearClaveRegistro(this->tipoRegistro, claveSerializada);
			delete[] claveSerializada; // Liberamos la memoria ya que no se usara mas.

			nodoHijo->unir(nodoHno,claveClonada,idNodo,tipoHno);
			//libero el nodo
			this->archivo->borrarBloque(nodoHijo->getIdNodo());
			//La union resulta en este nodo
			cout<<*nodoHno<<endl;
			this->guardarNodo(nodoHno);
		}

	}else if (tipoHno == IZQUIERDO) {
		claveMedio= nodo->obtenerRegistroClaveNodo(nodoHno->getIdNodo(),nodoHijo->getIdNodo());

		// Uno los dos nodos hermanos
		if(nodoHijo->getNivel() == 0 && nodoHno->getNivel() == 0){
			// Uno los dos nodos hermanos
			((NodoHoja*)(nodoHno))->unir((NodoHoja*)nodoHijo);
			//Actualizo el idHermanoIzquierdo del nodo Hoja siguiente
			NodoInterno *hojaSiguiente = this->getNodo(((NodoHoja*)nodoHno)->getIdNodoSiguiente());
			if (hojaSiguiente->getIdNodo()!=0){
				((NodoHoja*)hojaSiguiente)->setIdNodoAnterior(nodoHno->getIdNodo());
				this->guardarNodo(hojaSiguiente);
			}
			delete(hojaSiguiente);
			//libero el nodo hijo
			this->archivo->borrarBloque(nodoHijo->getIdNodo());
			//La union resulta en este nodo
			this->guardarNodo(nodoHno);
			cout<<*nodoHno<<endl;
			//devuelvo a la llamada anterior el nro de nodo con el que me quede en la union
			idNodo = nodoHno->getIdNodo();
		}else{
			// unir con clave medio
			char* claveSerializada = claveMedio->getClave()->serializar();
			ClaveRegistroArbol *claveClonada = FabricaClaveRegistro::crearClaveRegistro(this->tipoRegistro, claveSerializada);
			delete[] claveSerializada; // Liberamos la memoria ya que no se usara mas.

			nodoHijo->unir(nodoHno,claveClonada,idNodo,tipoHno);
			//libero el nodo
			this->archivo->borrarBloque(nodoHijo->getIdNodo());
			//La union resulta en este nodo
			cout<<*nodoHno<<endl;
			this->guardarNodo(nodoHno);
		}
	}
	return nodo->eliminarRegistroNodo(claveMedio,idNodo);
}

//Devuelve 0 si fue exitoso y -1 si hubo error
unsigned int Arbol::modificar(ClaveRegistroArbol* claveVieja,ClaveRegistroArbol* claveNueva){

	int error = this->eliminar(claveVieja);

	if (error!=0) {
		return error;
	} else {
		error= this->insertar(claveNueva);
	}

	return error;
}

