/*
 * FabricaClaveRegistro.cpp
 *
 *  Created on: 06/10/2013
 *      Author: Natalia
 */

#include "FabricaClaveRegistro.h"

FabricaClaveRegistro::FabricaClaveRegistro() {
	// TODO Auto-generated constructor stub

}

FabricaClaveRegistro::~FabricaClaveRegistro() {
	// TODO Auto-generated destructor stub
}

ClaveRegistroArbol* FabricaClaveRegistro::crearClaveRegistro(TipoRegistro tipoRegistro, char *dato){
	ClaveRegistroArbol *claveRegistroArbol;
	switch(tipoRegistro){
		case TipoUsuarioDNI:
			claveRegistroArbol = new ClaveRegTipoUsuarioDNI();
			break;
		case ProvinciaDNI:
			claveRegistroArbol = new ClaveRegProvinciaDNI();
			break;
		case TipoUsuarioIDServ:
			claveRegistroArbol = new ClaveRegIDUsIDServ();
			break;
		case NombreCategoriaIDServicio:
			claveRegistroArbol = new ClaveRegCatIDServ();
			break;
		case IDServicioIDUsuarioIDConsulta:
			claveRegistroArbol = new ClaveRegIDServIDUsIDCons();
			break;
		case IDServicioIDUsuarioIDPCotizacion:
			claveRegistroArbol = new ClaveRegIDServIDUsPC();
			break;
		case IDServicioFechaCHoraCIDConsulta:
			claveRegistroArbol = new ClaveRegIDServFCHCIDCons();
			break;
		case IDServicioFechaHoraIDPCotizacion:
			claveRegistroArbol = new ClaveRegIDServFHPCotizacion();
			break;
		default:
			claveRegistroArbol = NULL;
			break;
	}


	if((claveRegistroArbol != NULL) && (dato!=NULL)){
		claveRegistroArbol->deserializar(dato);
	}
	return claveRegistroArbol;
}

bool FabricaClaveRegistro::validarTipoClaveRegistro(TipoRegistro tipoRegistro, ClaveRegistroArbol *clave){
	return true;
}


