#ifndef CLAVEREGPROVINCIADNI_H_
#define CLAVEREGPROVINCIADNI_H_

#include "ClaveRegistroArbol.h"
#include "../utils/Funciones.h"

class ClaveRegProvinciaDNI: public ClaveRegistroArbol {

private:
	unsigned long dni;
	string provincia;
	unsigned long longitud;

public:
	ClaveRegProvinciaDNI();
	virtual ~ClaveRegProvinciaDNI();
	ClaveRegProvinciaDNI(unsigned long dni,string provincia);

	//devuelve 0 si son iguales, 1 si es mayor y -1 si es menor
	short int comparar(ClaveRegistroArbol* claveParaComparar);
	unsigned int getTamanio();
	char* serializar();
	void deserializar(char*);

	unsigned long getDni();
	void setDni(unsigned long dni);
	string getProvincia();
	void setProvincia(string provincia);
	void setLongitud(unsigned long longitud);
	unsigned long getLongitud();
	string getDatosImportantes();

};

#endif /* CLAVEREGPROVINCIADNI_H_ */
