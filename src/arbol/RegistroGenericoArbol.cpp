/*
 * RegistroGenericoArbol.cpp

 *
 *  Created on: 05/10/2013
 *      Author: natalia
 */

#include "RegistroGenericoArbol.h"

// Constructores -------------------------------------------------------------------------------
	RegistroGenericoArbol::RegistroGenericoArbol() {
		//this->dato=(new Funciones())->getStringToChar("");
		this->dato = NULL;
		this->tamanio = 0;
	}

	RegistroGenericoArbol::RegistroGenericoArbol(char* dato, unsigned int tamanio) {
		this->dato    = dato;
		this->tamanio = tamanio;
	}


	RegistroGenericoArbol::~RegistroGenericoArbol() {
		if((this->tamanio > 0) && (this->dato != NULL)){
			delete[] this->dato;
		}
		//this->dato=(new Funciones())->getStringToChar("");
		this->tamanio = 0;
	}

// Setter -------------------------------------------------------------------------------------
	void RegistroGenericoArbol::setTamanio(unsigned int tamanio){
		this->tamanio = tamanio;
	}

	void RegistroGenericoArbol::setDato(char* dato){
		this->dato=dato;
	}

// Getter -------------------------------------------------------------------------------------
	unsigned int RegistroGenericoArbol::getTamanio(){
		return this->tamanio;

	}

	char* RegistroGenericoArbol::getDato(){
		return this->dato;
	}

// Serializacion -----------------------------------------------------------------------------
	char* RegistroGenericoArbol::serializar(){
		//TODO: terminar
		return (new Funciones())->getStringToChar("natalia");
	}

	RegistroGenericoArbol* RegistroGenericoArbol::deserializar(char*){
		//TODO: Terminar
		RegistroGenericoArbol* registroNuevo = new RegistroGenericoArbol();
		return registroNuevo;
	}
