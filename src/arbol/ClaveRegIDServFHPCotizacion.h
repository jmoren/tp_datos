#ifndef CLAVEREGIDSERVFHPCOTIZACION_H_
#define CLAVEREGIDSERVFHPCOTIZACION_H_

#include "ClaveRegistroArbol.h"
#include "../utils/Funciones.h"

class ClaveRegIDServFHPCotizacion: public ClaveRegistroArbol {
private: unsigned long idServicio;
		 string fecha;
		 string hora;
		 unsigned long idCotizacion;

public:
	ClaveRegIDServFHPCotizacion();
	virtual ~ClaveRegIDServFHPCotizacion();
	ClaveRegIDServFHPCotizacion(unsigned long idServicio,string fecha, string hora, unsigned long idCotizacion);

	short int comparar(ClaveRegistroArbol* claveParaComparar);
	unsigned int getTamanio();
	char* serializar();
	void deserializar(char*);

	string getFecha();
	void setFecha(string fechaConsulta);
	string getHora();
	void setHora(string horaConsulta);
	unsigned long getIdCotizacion();
	void setIdCotizacion(unsigned long idCotizacion);
	unsigned long getIdServicio();
	void setIdServicio(unsigned long idServicio);

	string getDatosImportantes();
};

#endif /* CLAVEREGIDSERVFHPCOTIZACION_H_ */
