#ifndef CLAVEREGIDUSIDSERV_H_
#define CLAVEREGIDUSIDSERV_H_

#include "ClaveRegistroArbol.h"
#include "../utils/Funciones.h"

class ClaveRegIDUsIDServ: public ClaveRegistroArbol {

private:
	unsigned long idUsuario;
	unsigned long idServicio;

public:
	ClaveRegIDUsIDServ();
	virtual ~ClaveRegIDUsIDServ();
	ClaveRegIDUsIDServ(unsigned long idUsuario,unsigned long idservicio);
	unsigned long getIdServicio();
	unsigned long getIdUsuario();
	void setIdServicio(unsigned long idServicio);
	void setIdUsuario(unsigned long idUsuario);

	short int comparar(ClaveRegistroArbol* claveParaComparar);
	unsigned int getTamanio();
	char* serializar();
	void deserializar(char*);


	string getDatosImportantes();

};

#endif /* CLAVEREGIDUSIDSERV_H_ */
