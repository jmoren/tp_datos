/*
 * ClaveRegIDServIDUsPC.cpp
 *
 *  Created on: 08/10/2013
 *      Author: natalia
 */

#include "ClaveRegIDServIDUsPC.h"

ClaveRegIDServIDUsPC::ClaveRegIDServIDUsPC() {
	this->idServicio=0;
	this->idUsuario=0;
    this->idPedidoCotizacion=0;
}

ClaveRegIDServIDUsPC::ClaveRegIDServIDUsPC(unsigned long idServicio,unsigned long idUsuario,unsigned long idPedidoCotizacion) {
	this->idServicio=idServicio;
	this->idUsuario=idUsuario;
    this->idPedidoCotizacion=idPedidoCotizacion;
}

unsigned long ClaveRegIDServIDUsPC::getIdPedidoCotizacion() {
	return idPedidoCotizacion;
}

void ClaveRegIDServIDUsPC::setIdPedidoCotizacion(unsigned long idPedidoCotizacion) {
	this->idPedidoCotizacion = idPedidoCotizacion;
}

unsigned long ClaveRegIDServIDUsPC::getIdServicio() {
	return idServicio;
}

void ClaveRegIDServIDUsPC::setIdServicio(unsigned long idServicio) {
	this->idServicio = idServicio;
}

unsigned long ClaveRegIDServIDUsPC::getIdUsuario() {
	return idUsuario;
}

void ClaveRegIDServIDUsPC::setIdUsuario(unsigned long idUsuario) {
	this->idUsuario = idUsuario;
}

ClaveRegIDServIDUsPC::~ClaveRegIDServIDUsPC() {
	// TODO Auto-generated destructor stub
}

//devuelve 0 si son iguales, 1 si es mayor y -1 si es menor
short int ClaveRegIDServIDUsPC::comparar(ClaveRegistroArbol* claveParaComparar){
	if((claveParaComparar != NULL) ){
		if (this->idServicio == ((ClaveRegIDServIDUsPC*)claveParaComparar)->idServicio){
			if (this->idUsuario == ((ClaveRegIDServIDUsPC*)claveParaComparar)->idUsuario) {
				if (this->idPedidoCotizacion == ((ClaveRegIDServIDUsPC*)claveParaComparar)->idPedidoCotizacion) {
					return 0;
				} else if (this->idPedidoCotizacion > ((ClaveRegIDServIDUsPC*)claveParaComparar)->idPedidoCotizacion) {
					return 1;
				} else {
					return (-1);
				}
			} else if (this->idUsuario > ((ClaveRegIDServIDUsPC*)claveParaComparar)->idUsuario) {
				return 1;
			} else {
				return -1;
			}
		} else if (this->idServicio > ((ClaveRegIDServIDUsPC*)claveParaComparar)->idServicio) {
			return 1;
		}else {
			return -1;
		}
	}else{
		return 1; // Si la clave con la que comparo es NULL entonces siempre esta clave sera mayor a NULL
	}


}


unsigned int ClaveRegIDServIDUsPC::getTamanio(){

	return (unsigned int)(sizeof(unsigned long)*3);

}

char* ClaveRegIDServIDUsPC::serializar(){

	char* cadenaEscritura = new char[getTamanio()];
	unsigned int pos = 0;
	this->almacenarEnteroLargo(cadenaEscritura, pos, getIdServicio());
	this->almacenarEnteroLargo(cadenaEscritura, pos, getIdUsuario());
	this->almacenarEnteroLargo(cadenaEscritura, pos, getIdPedidoCotizacion());
	return cadenaEscritura;
}

void ClaveRegIDServIDUsPC::deserializar(char* cadena){

	unsigned int pos=0;
	this->idServicio=recuperarEnteroLargo(cadena,pos);
	this->idUsuario=recuperarEnteroLargo(cadena,pos);
	this->idPedidoCotizacion=recuperarEnteroLargo(cadena,pos);

}

string ClaveRegIDServIDUsPC::getDatosImportantes(){
	std::stringstream datosImportantes;
	datosImportantes << "ClaveRegIDServIDUsPC: "<< "Tamanio: " << getTamanio() << " (" << getIdServicio() << ", " << getIdUsuario() << ", " << getIdPedidoCotizacion() << ")";

    return  datosImportantes.str();
}
