
#ifndef NODOHOJA_H_
#define NODOHOJA_H_

#include "Bloque.h"
#include "NodoInterno.h"

using namespace std;

class NodoHoja: public NodoInterno {

private:
	unsigned int idNodoSiguiente;

protected:
	unsigned int getTamanioRegistroCabeceraNodo();

	RegistroGenericoArbol* crearRegistroCabeceraNodo();
	RegistroGenericoArbol* crearRegistroGenerico(RegistroNodo* registroNodo);

	void recuperarDatosCabeceraNodo(RegistroGenericoArbol *registroGenerico);
	RegistroNodo* recuperarRegistroNodo(RegistroGenericoArbol *registroGenerico);

	void actualizarRegistroSiguiente(RegistroNodo *registroNodoInsertado, RegistroNodo *registroNodoSiguiente);

public:
	NodoHoja(TipoRegistro tipoRegistro, Bloque *bloque, unsigned int tamanioTotalDisponibleParaRegistros);
	NodoHoja(TipoRegistro tipoRegistro, unsigned int idNodo, unsigned int tamanioBloque, unsigned int tamanioTotalDisponibleParaRegistros);
	virtual ~NodoHoja();

	unsigned int getIdNodoAnterior();
	unsigned int getIdNodoSiguiente();

	void setIdNodoAnterior(unsigned int idNodoAnterior);
	void setIdNodoSiguiente(unsigned int idNodoSiguiente);


	NodoHoja* dividir(unsigned int idNodo);

	/*unsigned short NodoHoja::eliminarRegistroNodo(RegistroNodo *registroNodo){
	 *  Devuelve:
	 * 				0	Si el registroNodo recibido no se actualizo
	 * 				1	El nodo se actualizo insertanto el registroNodo
	 * 				2	El nodo se desbordo al intentar insertar y no lo inserto
	 * 				3	Ya existe un registro con el identificador igual al que se quiso insertar
	 */

	unsigned short insertarClaveArbol(ClaveRegistroArbol *clave, TipoRegistro tipoRegistro);

	unsigned short eliminarClaveArbol(ClaveRegistroArbol* clave, TipoRegistro tipoRegistro);

	virtual string getDatosImportantes();

	void unir(NodoHoja * nodoHijo);

	virtual void balancear(NodoHoja * nodoHno,NodoInterno * nodoPadre,TipoNodoHno tipoHno,TipoRegistro tipoRegistro);

};

#endif /* NODOHOJA_H_ */
