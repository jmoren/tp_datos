/*
 * ClaveRegIDServFHPCotizacion.cpp
 *
 *  Created on: 08/10/2013
 *      Author: natalia
 */

#include "ClaveRegIDServFHPCotizacion.h"

ClaveRegIDServFHPCotizacion::ClaveRegIDServFHPCotizacion() {
	this->idServicio=0;
	this->fecha="20130101";
	this->hora="0000";
    this->idCotizacion=0;
}

ClaveRegIDServFHPCotizacion::ClaveRegIDServFHPCotizacion(unsigned long idServicio,string fecha, string hora, unsigned long idCotizacion) {
	this->idServicio=idServicio;
	this->fecha=fecha;
	this->hora=hora;
    this->idCotizacion=idCotizacion;
}

ClaveRegIDServFHPCotizacion::~ClaveRegIDServFHPCotizacion() {
	// TODO Auto-generated destructor stub
}


string ClaveRegIDServFHPCotizacion::getFecha() {
	return fecha;
}

void ClaveRegIDServFHPCotizacion::setFecha(string fecha) {
	this->fecha = fecha;
}

string ClaveRegIDServFHPCotizacion::getHora() {
	return hora;
}

void ClaveRegIDServFHPCotizacion::setHora(string hora) {
	this->hora = hora;
}

unsigned long ClaveRegIDServFHPCotizacion::getIdCotizacion() {
	return idCotizacion;
}

void ClaveRegIDServFHPCotizacion::setIdCotizacion(unsigned long idCotizacion) {
	this->idCotizacion = idCotizacion;
}

unsigned long ClaveRegIDServFHPCotizacion::getIdServicio() {
	return idServicio;
}

void ClaveRegIDServFHPCotizacion::setIdServicio(unsigned long idServicio) {
	this->idServicio = idServicio;
}

//devuelve 0 si son iguales, 1 si es mayor y -1 si es menor
short int ClaveRegIDServFHPCotizacion::comparar(ClaveRegistroArbol* claveParaComparar){
	if((claveParaComparar != NULL) ){
		if (this->idServicio == ((ClaveRegIDServFHPCotizacion*)claveParaComparar)->idServicio){
			if (this->fecha == ((ClaveRegIDServFHPCotizacion*)claveParaComparar)->fecha) {
				if (this->hora == ((ClaveRegIDServFHPCotizacion*)claveParaComparar)->hora) {
					if (this->idCotizacion == ((ClaveRegIDServFHPCotizacion*)claveParaComparar)->idCotizacion){
						return 0;
					} else if (this->idCotizacion > ((ClaveRegIDServFHPCotizacion*)claveParaComparar)->idCotizacion) {
						return 1;
					} else {
						return (-1);
					}
				} else if (this->hora > ((ClaveRegIDServFHPCotizacion*)claveParaComparar)->hora) {
					return 1;
				} else {
					return -1;
				}
			} else if (this->fecha > ((ClaveRegIDServFHPCotizacion*)claveParaComparar)->fecha) {
				return 1;
			} else {
				return -1;
			}
		} else if (this->idServicio > ((ClaveRegIDServFHPCotizacion*)claveParaComparar)->idServicio) {
			return 1;
		}else {
			return -1;
		}
	}else{
		return 1; // Si la clave con la que comparo es NULL entonces siempre esta clave sera mayor a NULL
	}
}


unsigned int ClaveRegIDServFHPCotizacion::getTamanio(){

	return (unsigned int)(sizeof(unsigned long)*2 + this->fecha.length() + this->hora.length());

}

char* ClaveRegIDServFHPCotizacion::serializar(){

	char* cadenaEscritura = new char[getTamanio()];
	unsigned int pos = 0;
	this->almacenarEnteroLargo(cadenaEscritura, pos, getIdServicio());
	this->almacenarString(cadenaEscritura, pos, getFecha());
	this->almacenarString(cadenaEscritura, pos, getHora());
	this->almacenarEnteroLargo(cadenaEscritura, pos, getIdCotizacion());
	return cadenaEscritura;
}

void ClaveRegIDServFHPCotizacion::deserializar(char* cadena){

	unsigned int pos=0;
	this->idServicio=recuperarEnteroLargo(cadena,pos);
	this->fecha=recuperarString(cadena,pos,8);
	this->hora=recuperarString(cadena,pos,4);
	this->idCotizacion=recuperarEnteroLargo(cadena,pos);

}

string ClaveRegIDServFHPCotizacion::getDatosImportantes(){
	std::stringstream datosImportantes;
	datosImportantes << "ClaveRegIDServFHPCotizacion: "<< "Tamanio: " << getTamanio() << " (" << getIdServicio() << ", " << getFecha() << ", " << getHora() << ", " << getIdCotizacion()<< ")";

    return  datosImportantes.str();
}

