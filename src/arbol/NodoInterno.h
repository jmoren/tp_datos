#ifndef NODOINTERNO_H_
#define NODOINTERNO_H_

#include "Bloque.h"
#include "RegistroNodo.h"
#include "RegistroNodoInterno.h"
#include "Persistente.h"

using namespace std;

class NodoInterno : public Persistente {

protected:
	TipoRegistro tipoRegistro;
	unsigned int idNodo;
	unsigned int nivel;
	unsigned int tamanioBloque;
	unsigned int tamanioTotalDisponibleParaRegistros;
	unsigned int espacioLibre;
	unsigned int cantRegistros;
public:
	list<RegistroNodo*> registrosNodo;

	/*
	 * Para NodoInterno: Representa al id nodo hijo izquierdo del primer registroNodoInterno de la lista registrosNodo
	 * Para NodoHoja   : Representa al id nodo anterior del secuence set
	 */
	unsigned int idNodoIzq;

	virtual unsigned int getTamanioRegistroCabeceraNodo();

	virtual RegistroGenericoArbol* crearRegistroCabeceraNodo();
	virtual RegistroGenericoArbol* crearRegistroGenerico(RegistroNodo* registroNodo);

	virtual void recuperarDatosCabeceraNodo(RegistroGenericoArbol *registroGenerico);
	virtual RegistroNodo* recuperarRegistroNodo(RegistroGenericoArbol *registroGenerico);

	void insertarOrdenado(RegistroNodo *registroNodo);
	virtual void actualizarRegistroSiguiente(RegistroNodo *registroNodoInsertado, RegistroNodo *registroNodoSiguiente);

public:
	NodoInterno();
	NodoInterno(TipoRegistro tipoRegistro, Bloque *bloque, unsigned int tamanioTotalDisponibleParaRegistros);
	NodoInterno(TipoRegistro tipoRegistro, unsigned int idNodo, unsigned int nivel, unsigned int tamanioBloque, unsigned int tamanioTotalDisponibleParaRegistros);
	virtual ~NodoInterno();

	TipoRegistro getTipoRegistro();
	unsigned int getIdNodo();
	unsigned int getNivel();
	unsigned int getTamanioBloque();
	unsigned int getTamanioTotalDisponibleParaRegistros();
	unsigned int getEspacioLibre();
	unsigned int getCantRegistros();
	list<RegistroNodo*> getRegistrosNodo();
	unsigned int getIdNodoIzq();

	void setTipoRegistro(TipoRegistro tipo);
	void setIdNodo(unsigned int idNodo);
	void setNivel(unsigned int nivel);
	void setTamanioBloque(unsigned int tamanioBloque);
	void getTamanioTotalDisponibleParaRegistros(unsigned int tamanioTotalDisponibleParaRegistros);
	void setEspacioLibre(unsigned int espacioLibre);
	void setIdNodoIzq(unsigned int idNodoIzq);
	void setCantRegistros(unsigned int cant);

	/*
	 *  Devuelve:
	 * 				0	Si el registroNodo recibido no se actualizo
	 * 				1	El nodo se actualizo insertanto el registroNodo
	 * 				2	El nodo se desbordo al intentar insertar y no lo inserto
	 * 				3	Ya existe un registro con el identificador igual al que se quiso insertar
	 */
	unsigned short insertarClaveArbol(ClaveRegistroArbol *clave, TipoRegistro tipoRegistro, unsigned int idNodoIzq, unsigned int idNodoDer);
	/*
	 *  Devuelve:
	 * 				0	Si el registroNodo recibido no se actualizo
	 * 				1	El nodo se actualizo insertanto el registroNodo
	 * 				2	El nodo se desbordo al intentar insertar y no lo inserto
	 * 				3	Ya existe un registro con el identificador igual al que se quiso insertar
	 */
	unsigned short insertarRegistroNodo(RegistroNodo *registroNodo);


	unsigned short eliminarClaveArbol(ClaveRegistroArbol* clave, TipoRegistro tipoRegistro, unsigned int &idNodo);

	unsigned short eliminarRegistroNodo(RegistroNodo *registroNodo, unsigned int &idNodo);

	bool existeRegistro(RegistroNodo *registroNodo);

	Bloque* crearBloque();
	void vaciarRegistrosNodo();

	ClaveRegistroArbol* getDatosClaveMedio();
	unsigned int buscarNodoHijo(ClaveRegistroArbol* clave);
	NodoInterno* dividir(unsigned int idNodo);

	// Sobrecarga del operador <<
	friend std::ostream& operator<< (std::ostream& stream, NodoInterno& nodo){
		stream << nodo.getDatosImportantes();
	    return stream;
	}
	virtual string getDatosImportantes();

	unsigned int recuperarNivel(RegistroGenericoArbol *registro);

	unsigned int leerNodoHno(NodoInterno *nodo,TipoNodoHno &tipoHno);

	bool tieneCargaMinima(TipoNodoHno tipoHno);

	ClaveRegistroArbol * obtenerClaveNodo(unsigned int idNodoIzq,unsigned int idNodoDer);

	virtual RegistroNodo* obtenerRegistroClaveNodo(unsigned int idNodoIzq,unsigned int idNodoDer);

	void unir(NodoInterno * nodoHijo,ClaveRegistroArbol *claveMedio,unsigned int &idNodo,TipoNodoHno tipoHno);

	virtual void balancear(NodoInterno * nodoHno,NodoInterno * nodoPadre,TipoNodoHno tipoHno,TipoRegistro tipoReg);

	bool permiteCambiarClave(NodoInterno *nodoHno, NodoInterno *nodoHijo, TipoNodoHno tipoHno);
	//void modificarClaveMedio(ClaveRegistroArbol * clave,unsigned int idNodoDer, unsigned int idNodoIzq);

	bool permiteCambiarClaveInterno(NodoInterno *nodoHno, NodoInterno *nodoHijo, TipoNodoHno tipoHno);


};


#endif /* NODOINTERNO_H_ */
