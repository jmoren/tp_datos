/*
 * ClaveRegTipoUsIDServ.cpp
 *
 *  Created on: 08/10/2013
 *      Author: natalia
 */

#include "ClaveRegIDUsIDServ.h"

ClaveRegIDUsIDServ::ClaveRegIDUsIDServ() {
	this->idServicio=0;
	this->idUsuario=0;
}

ClaveRegIDUsIDServ::ClaveRegIDUsIDServ(unsigned long idservicio,unsigned long idUsuario) {
	this->idServicio=idservicio;
	this->idUsuario=idUsuario;

}
unsigned long ClaveRegIDUsIDServ::getIdServicio(){
	return idServicio;
}

unsigned long ClaveRegIDUsIDServ::getIdUsuario()  {
	return idUsuario;
}
void ClaveRegIDUsIDServ::setIdServicio(unsigned long idServicio) {
	this->idServicio = idServicio;
}


void ClaveRegIDUsIDServ::setIdUsuario(unsigned long idUsuario) {
	this->idUsuario=idUsuario;
}


ClaveRegIDUsIDServ::~ClaveRegIDUsIDServ() {
	// TODO Auto-generated destructor stub
}

short int ClaveRegIDUsIDServ::comparar(ClaveRegistroArbol* claveParaComparar){
	if((claveParaComparar != NULL) ){
		ClaveRegIDUsIDServ *clave2 = (ClaveRegIDUsIDServ*)claveParaComparar;

		if(this->getIdUsuario() == clave2->getIdUsuario()){
			if(this->getIdServicio() == clave2->getIdServicio()){
				return 0;
			}else if(this->getIdServicio()  > clave2->getIdServicio() ){
				return 1;
			}else{
				return -1;
			}
		}else if(this->getIdUsuario() > clave2->getIdUsuario()){
			return 1;
		}else{
			return -1;
		}
	}else{
		return 1; // Si la clave con la que comparo es NULL entonces siempre esta clave sera mayor a NULL
	}
}

unsigned int ClaveRegIDUsIDServ::getTamanio(){

	return sizeof(unsigned long) + sizeof(unsigned long);

}

char* ClaveRegIDUsIDServ::serializar(){

	char* cadenaEscritura = new char[getTamanio()];
	unsigned int pos = 0;
	this->almacenarEnteroLargo(cadenaEscritura, pos, getIdUsuario());
	this->almacenarEnteroLargo(cadenaEscritura, pos, getIdServicio());
	return cadenaEscritura;
}

void ClaveRegIDUsIDServ::deserializar(char* cadena){

	unsigned int pos = 0;

	this->setIdUsuario(this->recuperarEnteroLargo(cadena, pos));
	this->setIdServicio(this->recuperarEnteroLargo(cadena, pos));

}

string ClaveRegIDUsIDServ::getDatosImportantes(){
	std::stringstream datosImportantes;
	datosImportantes << "ClaveRegTipoUsIDServ: "<< "Tamanio: " << getTamanio() << " (" << this->getIdUsuario() << ", " << this->getIdServicio() << ")";

    return  datosImportantes.str();
}
