#ifndef CLAVEREGTIPOUSUARIODNI_H_
#define CLAVEREGTIPOUSUARIODNI_H_

#include "ClaveRegistroArbol.h"
#include "../utils/Funciones.h"

class ClaveRegTipoUsuarioDNI: public ClaveRegistroArbol {

private:
	unsigned long dni;
	unsigned int tipoUsuario;

public:
	ClaveRegTipoUsuarioDNI();
	virtual ~ClaveRegTipoUsuarioDNI();
	ClaveRegTipoUsuarioDNI(unsigned long dni,unsigned int tipoUsuario);

	void setDni(unsigned long dni);
	void setTipoUsuario(unsigned int tipoUsuario);

	unsigned long getDni();
	unsigned int getTipoUsuario();

//	char getCharTipoUsuario();
//	unsigned int getEnumTipoUsuario(char caracter);

	//devuelve 0 si son iguales, 1 si es mayor y -1 si es menor
	short int comparar(ClaveRegistroArbol* claveParaComparar);
	unsigned int getTamanio();
	char* serializar();
	void deserializar(char*);

	string getDatosImportantes();

};

#endif /* CLAVEREGTIPOUSUARIODNI_H_ */
