/*
 * Bloque.h

 *
 *  Created on: 05/10/2013
 *      Author: natalia
 */

#ifndef BLOQUE_H_
#define BLOQUE_H_

#include <list>
#include "RegistroGenericoArbol.h"

using namespace std;

class Bloque {

private:
	list<RegistroGenericoArbol*> registrosGenericos;
	unsigned int nroBloque;
	unsigned int tamanioBloque;
	unsigned int tamanioEspacioLibre;

public:
	Bloque();
	virtual ~Bloque();

	void setRegistros(list<RegistroGenericoArbol*> registrosGenericos);
	void push_back(RegistroGenericoArbol* registroGenericos);
	void setNroBloque(unsigned int nroBloque);
	void setTamanioBloque(unsigned int tamanioBloque);
	void setTamanioEspacioLibre(unsigned int tamanioEspacioLibre);

	list<RegistroGenericoArbol*> getRegistros();
	unsigned int getNroBloque();
	unsigned int getTamanioBloque();
	unsigned int getTamanioEspacioLibre();

	void vaciarRegistros();
};

#endif /* BLOQUE_H_ */
