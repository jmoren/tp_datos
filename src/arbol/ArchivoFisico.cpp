/*
 * ArchivoFisico.cpp
 *
 *  Created on: 06/10/2013
 *      Author: Natalia
 */

#include "ArchivoFisico.h"

// Constructores -------------------------------------------------------------------------------
ArchivoFisico::ArchivoFisico(){
	this->tamanioDatosDeControlBloque = 0;
	this->tamanioDatosDeControlRegistro = 0;
	this->cantidadTotalBloques = 0;
	this->tipoRegistro = TipoUsuarioDNI; // Asignar default;
	this->fileBlock = NULL;
}

ArchivoFisico::ArchivoFisico(TipoRegistro tipoRegistro, unsigned int tamanioBloque){
	this->tamanioBloque = tamanioBloque;
	this->tamanioDatosDeControlBloque = 24;
	this->tamanioDatosDeControlRegistro = 0;
	this->cantidadTotalBloques = 0;
	this->tipoRegistro = tipoRegistro; // Asignar default;

	// Intento abrir el archivo para lectura y escritura si es que existe;
	try{
		//cout << "INFO: ArchivoFisico::crearArchivo() -> Abrimos el archivo." << endl;
		this->fileBlock = new FileBlock(getPathFromTipoRegistro(tipoRegistro));
	}catch(exception & e){
		//cout << "INFO: ArchivoFisico::crearArchivo() -> Fallo al abrir el archivo, el archivo no existe." << endl;
		this->fileBlock = NULL; // Si el archivo no existe dejo la referencia en NULL
	}
}

ArchivoFisico::~ArchivoFisico(){
	this->tamanioDatosDeControlBloque = 0;
	this->tamanioDatosDeControlRegistro = 0;
	this->cantidadTotalBloques = 0;
	this->tipoRegistro = TipoUsuarioDNI; // Asignar default;

	if(this->fileBlock != NULL){
		delete(this->fileBlock);
	}
}

// Funciones ----------------------------------------------------------------------------------
const char* ArchivoFisico::getPathFromTipoRegistro(TipoRegistro tipoRegistro){
	switch(tipoRegistro){
		case TipoUsuarioDNI:
			return "arbolUsuarioDNI.dat";
		case ProvinciaDNI:
			return "arbolProvinciaDNI.dat";
		case NombreCategoriaIDServicio:
			return "arbolNombreCategoriaIDServicioI.dat";
		case IDServicioIDUsuarioIDConsulta:
			return "arbolIDServicioIDUsuarioIDConsulta.dat";
		case IDServicioIDUsuarioIDPCotizacion:
			return "arbolIDServIDUsuarioIDPCotizacion.dat";
		case IDServicioFechaCHoraCIDConsulta:
			return "arbolIDServFechaCHoraCIDConsulta.dat";
		case IDServicioFechaHoraIDPCotizacion:
			return "arbolIDServFechaHoraIDPCotizacion.dat";
		case TipoUsuarioIDServ:
			return "arbolIDUsIDServ.dat";
		default:
			return "arbolSinTipoDefinido.dat";
	}
}

bool ArchivoFisico::existeArchivoFisico(){
	if(this->fileBlock != NULL){
		return true;
	}
	return false;
}

void ArchivoFisico::crearArchivo(){
	//cout << "ArchivoFisico::crearArchivo()" << endl;
	try{
		// Creo el archivo con el constructor que inicializa el header
		//cout << "INFO: ArchivoFisico::crearArchivo() -> Creamos el archivo." << endl;
		this->fileBlock = new FileBlock(getPathFromTipoRegistro(tipoRegistro), this->tamanioBloque);
		// Destruyo el fileBlock para asegurarme que se guarde en disco.
		//cout << "INFO: ArchivoFisico::crearArchivo() -> Grabamos el archivo." << endl;
		delete(this->fileBlock);

		// Abro el archivo para tenerlo como de lectura y escritura.
		try{
			//cout << "INFO: ArchivoFisico::crearArchivo() -> Abrimos el archivo para lectura y escritura." << endl;
			this->fileBlock = new FileBlock(getPathFromTipoRegistro(tipoRegistro));
		}catch(exception e){
			this->fileBlock = NULL; // Si el archivo no existe dejo la referencia en NULL
			//cout << "ERROR: ArchivoFisico::crearArchivo() -> No se pudo abrir el archivo para lectura y escritura" << endl;
		}
		this->cantidadTotalBloques = (unsigned int)this->fileBlock->getTotalBlocks();
	}catch(exception & e){
		//cout << "ERROR: ArchivoFisico::crearArchivo() -> No se pudo crear el archivo" << endl;
		this->fileBlock = NULL; // Si el archivo no existe dejo la referencia en NULL
	}
}


unsigned int ArchivoFisico::getTamanioBloque(){
	return this->tamanioBloque;
}


unsigned int ArchivoFisico::getTamanioDatosDeControlBloque(){
	return this->tamanioDatosDeControlBloque;
}

unsigned int ArchivoFisico::getTamanioDatosDeControlRegistro(){
	return this->tamanioDatosDeControlRegistro;
}

unsigned int ArchivoFisico::getCantidadTotalBloques(){
	return this->cantidadTotalBloques;
}

/**
 * La numeracion de nodos arranca en 0 con lo cual, para esta implementacion,
 * para obtener el siguiente idBloqueLibre con devolver la cantidad total de bloques alcanza
 */
unsigned int ArchivoFisico::getIdBloqueLibre(){
	Block * block = this->fileBlock->getNewBlock();
	unsigned int idBloqueLibre = block->getNumber()-1;

	delete(block);

	return idBloqueLibre;
}

bool ArchivoFisico::existenBloquesLibres(){
	// Se devuelve true ya que esta implementada la administracion de bloques libres.
	return true;
}

Bloque* ArchivoFisico::getBloque(unsigned int nroBloque){
	Block * block = this->fileBlock->readBlock(nroBloque+1);

	Bloque *bloqueNuevo = new Bloque();
	// Decremento el numero de bloque porque en el fileBlock parte de 1 y en el arbol parte de 0;
	bloqueNuevo->setNroBloque(block->getNumber() - 1);

	if(block->getBlockSize() != this->tamanioBloque){
		//cout << "ADVERTENCIA: ArchivoFisico::getBloque -> el tamanio bloque guardado en archivoFisico es distinto al configurado en el arbol" << endl;
	}
	bloqueNuevo->setTamanioBloque(block->getBlockSize());

	bloqueNuevo->setTamanioEspacioLibre(block->getFreeSpace());

	for(unsigned int i = 0;i < block->getTotalRecords();i++){
		RegistroGenerico *reg = block->getRegistroGenerico(i);

		unsigned int tamanioDatos = reg->getTamanio() - sizeof(unsigned int);
		char * datos = new char [tamanioDatos];
		for(unsigned int pos = 0; pos < tamanioDatos; pos++){
			datos[pos] = (reg->getDato())[pos];
		}

		bloqueNuevo->push_back(new RegistroGenericoArbol(datos, tamanioDatos));
	}

	delete(block);

	return bloqueNuevo;
}
Bloque* ArchivoFisico::getRaiz(){
	return getBloque(0);
}

void ArchivoFisico::borrarBloque(unsigned int nroBloque){
	this->fileBlock->deleteBlock(nroBloque+1);
}

void ArchivoFisico::grabarBloque(Bloque* bloque){
	Block * block = new Block(bloque->getNroBloque()+1, this->tamanioBloque);

	//RegistroGenericoArbol* cabecera = bloque->getRegistros().front();
	//cout << "cabecera->getTamanio() = " << cabecera->getTamanio() << endl;

	list<RegistroGenericoArbol*> registros = bloque->getRegistros();

	for (std::list<RegistroGenericoArbol*>::iterator it = registros.begin(); it != registros.end(); it++ ){
		//RegistroGenericoArbol* registro = *it;
		//unsigned int tamanioReg = registro->getTamanio();
		//cout << "tamanioReg = " << tamanioReg << endl;
		unsigned int tamanioTotalRegistro = (unsigned int) (((*it)->getTamanio()) + sizeof(unsigned int));

		char* datoClonado = new char[(*it)->getTamanio()];
		for(unsigned int i = 0; i<(*it)->getTamanio();i++){
			datoClonado[i] = ((*it)->getDato())[i];
		}

		RegistroGenerico *registroGenerico = new RegistroGenerico(tamanioTotalRegistro, datoClonado);
		block->setRegistroGenerico(registroGenerico);
	}

	this->fileBlock->writeBlock(block);
	delete(block);
	delete(bloque);
}

void ArchivoFisico::imprimir(){
	//cout << "INFO: ArchivoFisico::imprimir() -> Metodo no implementado." << endl;
}

