/*
 * ArbolTest.cpp
 *
 *  Created on: 06/10/2013
 *      Author: Natalia
 */

#include "ArbolTest.h"

ArbolTest::ArbolTest() {
	// TODO Auto-generated constructor stub

}

ArbolTest::~ArbolTest() {
	// TODO Auto-generated destructor stub
}

Arbol* ArbolTest::creacion(TipoRegistro tipoRegistro){
	return new Arbol(tipoRegistro);
}

void ArbolTest::cargaInicial(Arbol* arbol){
	//Cargo Registros para insertar luego en el arbol
/*
	ClaveRegTipoUsuarioDNI *clave01 = new ClaveRegTipoUsuarioDNI(11222001, ADMINISTRADOR);
	cout << "clave01: " << *clave01 << endl;

	ClaveRegTipoUsuarioDNI *clave02 = new ClaveRegTipoUsuarioDNI(11222002, PROVEEDOR);
	cout << "clave02: " << *clave02 << endl;

	ClaveRegTipoUsuarioDNI *clave03 = new ClaveRegTipoUsuarioDNI();
	cout << "clave03: " << *clave03 << endl;

	char *dato01 = clave01->serializar();
	clave03->deserializar(dato01);
	cout << "clave03 deserializada: " << *clave03 << endl;


	ClaveRegistroArbol *clave04 = FabricaClaveRegistro::crearClaveRegistro(TipoUsuarioDNI, dato01);
	cout << "clave04: " << *clave04 << endl;

	delete(clave01);
	delete(clave02);
	delete(clave03);
	delete(clave04);

	cout << "--------------------------------------------------------------" << endl;
	cout << "cargaInicial" << endl;
	cout << "--------------------------------------------------------------" << endl;
	int tipo = 0;//-1;
	for(unsigned int i = 1; i <= 40; i++){
		//tipo++;
		TipoUsuario tipoUsuario;
		if(tipo == 0 ){
			tipoUsuario = USUARIO_COMUN;
		}else if(tipo == 1 ){
			tipoUsuario = PROVEEDOR;
		}else{
			tipoUsuario = ADMINISTRADOR;
			//tipo = -1;
		}
		ClaveRegTipoUsuarioDNI *clave = new ClaveRegTipoUsuarioDNI(i, tipoUsuario);
		arbol->insertar(clave);
		//cout << endl << "Insert " << i << ", Imprimir archivo2 ---------------------------------------------" << endl;
		//arbol->getArchivo()->imprimir();
	}
	cout << endl << "Imprimir archivo2 ---------------------------------------------" << endl;
	arbol->getArchivo()->imprimir();
*/
	cout << "--------------------------------------------------------------" << endl;
	cout << "cargaInicial" << endl;
	cout << "--------------------------------------------------------------" << endl;
	string categoria;//-1;
	for(unsigned int i = 1; i <= 40; i++){
		//tipo++;
		if(i % 2 == 0 ){
			categoria = "Vehiculos";
		}else if(i%3 == 0 ){
			categoria = "Real State";
		}else{
			categoria = "Mascotas";
		}
		ClaveRegCatIDServ *clave = new ClaveRegCatIDServ(i*i, categoria);
		arbol->insertar(clave);
		//cout << endl << "Insert " << i << ", Imprimir archivo2 ---------------------------------------------" << endl;
		//arbol->getArchivo()->imprimir();
	}
	cout << endl << "Imprimir archivo2 ---------------------------------------------" << endl;
	arbol->getArchivo()->imprimir();

	//NDL: sacar
	/*
	cout << "Creacion de hoja" << endl;
	NodoHoja *hoja = new NodoHoja(arbol->getTipoRegistro(), 1, Arbol::TAMANIO_BLOQUE_ARBOL);
	Bloque *bloqueNuevo2 = hoja->crearBloque();
	cout << "Hoja creada : " << *hoja << endl;
	this->archivo->grabarBloque(bloqueNuevo2);

	//Leer la hoja
	Bloque *bloqueHoja = this->archivo->getBloque(1);
	if(bloqueHoja != NULL){
		NodoHoja *hojaleida = new NodoHoja(this->tipoRegistro, bloqueHoja);
		cout << "Hoja leida : " << *hojaleida << endl;
	}else{
		cout << "ERROR: bloqueHoja no pudo obtener la hoja desde el archivo.";
	}
	*/


}


void ArbolTest::creacionTipoUsuarioDNI(Arbol *arbol){
	cout << "--------------------------------------------------------------" << endl;
	cout << "cargaInicial" << endl;
	cout << "--------------------------------------------------------------" << endl;
	unsigned int tipoUsuario;//-1;
	/*
	for(unsigned int i = 1; i <= 40; i++){
		tipoUsuario = 12;

		ClaveRegTipoUsuarioDNI *clave = new ClaveRegTipoUsuarioDNI(i, tipoUsuario);
		arbol->insertar(clave);
	}
*/
	for(unsigned int i = 100; i >= 1; i--){
		tipoUsuario = 12;

		ClaveRegTipoUsuarioDNI *clave = new ClaveRegTipoUsuarioDNI(i, tipoUsuario);
		arbol->insertar(clave);
	}

	//for(unsigned int i = 200; i <= 151; i--){
	for(unsigned int i = 151; i <= 200; i++){
		tipoUsuario = 12;

		ClaveRegTipoUsuarioDNI *clave = new ClaveRegTipoUsuarioDNI(i, tipoUsuario);
		arbol->insertar(clave);
	}

	for(unsigned int i = 101; i <= 150; i++){
		tipoUsuario = 12;

		ClaveRegTipoUsuarioDNI *clave = new ClaveRegTipoUsuarioDNI(i, tipoUsuario);
		arbol->insertar(clave);
	}

	for(unsigned int i = 1; i <= 1; i++){
		tipoUsuario = 13;

		ClaveRegTipoUsuarioDNI *clave = new ClaveRegTipoUsuarioDNI(i, tipoUsuario);
		arbol->insertar(clave);
	}

}


void ArbolTest::insertRegistrosTest(Arbol* arbol){
	//Cargo Registros para insertar luego en el arbol
	//list<RegistroGenericoArbol*> registrosGenericos;
	//for(int i = 1; i++; i <= 20){

	//}
	ClaveRegTipoUsuarioDNI *clave01 = new ClaveRegTipoUsuarioDNI(11222001, ADMINISTRADOR);
	cout << "clave01: " << *clave01 << endl;

	ClaveRegTipoUsuarioDNI *clave02 = new ClaveRegTipoUsuarioDNI(11222002, PROVEEDOR);
	cout << "clave02: " << *clave02 << endl;


	char *dato01 = clave01->serializar();
	clave02->deserializar(dato01);
	cout << "clave02 cambiada: " << *clave02 << endl;
}

void ArbolTest::deleteRegistrosTest(Arbol* arbol){
/*
	//for(unsigned int i = 24; i >= 19; i--){
	for(unsigned int i = 19; i <= 36; i++){
		ClaveRegTipoUsuarioDNI *claveEliminar01 = new ClaveRegTipoUsuarioDNI(i, 12);

		arbol->eliminar(claveEliminar01);
		delete(claveEliminar01);
	}
*/

	for(unsigned int i = 150; i >= 101; i--){
	//for(unsigned int i = 101; i <= 150; i++){
		ClaveRegTipoUsuarioDNI *claveEliminar01 = new ClaveRegTipoUsuarioDNI(i, 12);

		arbol->eliminar(claveEliminar01);
		delete(claveEliminar01);
	}

	//for(unsigned int i = 200; i >= 151; i--){
	for(unsigned int i = 151; i <= 200; i++){
		ClaveRegTipoUsuarioDNI *claveEliminar01 = new ClaveRegTipoUsuarioDNI(i, 12);

		arbol->eliminar(claveEliminar01);
		delete(claveEliminar01);
	}

	for(unsigned int i = 1; i <= 100; i++){
		ClaveRegTipoUsuarioDNI *claveEliminar01 = new ClaveRegTipoUsuarioDNI(i, 12);

		arbol->eliminar(claveEliminar01);
		delete(claveEliminar01);
	}

	/*
	ClaveRegTipoUsuarioDNI *claveEliminar01 = new ClaveRegTipoUsuarioDNI(37, 12);
	arbol->eliminar(claveEliminar01);
	cout<<"-------------arbol---------------------------------"<<endl;
	arbol->getArchivo()->imprimir();

	arbol->getArchivo()->imprimir();
	ClaveRegTipoUsuarioDNI *claveEliminar02 = new ClaveRegTipoUsuarioDNI(38, 12);
	arbol->eliminar(claveEliminar02);
	cout<<"-------------arbol---------------------------------"<<endl;
	arbol->getArchivo()->imprimir();


	ClaveRegTipoUsuarioDNI *claveEliminar03 = new ClaveRegTipoUsuarioDNI(39, 12);
	arbol->eliminar(claveEliminar03);
	cout<<"-------------arbol---------------------------------"<<endl;
	arbol->getArchivo()->imprimir();


	ClaveRegTipoUsuarioDNI *claveEliminar04 = new ClaveRegTipoUsuarioDNI(40, 12);
	arbol->eliminar(claveEliminar04);
	cout<<"-------------arbol---------------------------------"<<endl;
	arbol->getArchivo()->imprimir();

	delete(claveEliminar01);
	delete(claveEliminar02);
	delete(claveEliminar03);
	delete(claveEliminar04);
*/
}

void ArbolTest::mostrarArchivoMock(Arbol* arbol){

}


void ArbolTest::buscar(Arbol* arbol){
	TipoUsuario tipoUsuario = USUARIO_COMUN;

	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegTipoUsuarioDNI *claveBuscada = new ClaveRegTipoUsuarioDNI(0, tipoUsuario);
	cout << "Busqueda de la clave: " << *claveBuscada << endl;
	ClaveRegistroArbol *claveEncontrada = arbol->buscar(claveBuscada);
	if(claveEncontrada != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}

	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegTipoUsuarioDNI *claveBuscada2 = new ClaveRegTipoUsuarioDNI(1, tipoUsuario);
	cout << "Busqueda de la clave: " << *claveBuscada2 << endl;
	ClaveRegistroArbol *claveEncontrada2 = arbol->buscar(claveBuscada2);
	if(claveEncontrada2 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada2 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}

	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegTipoUsuarioDNI *claveBuscada3 = new ClaveRegTipoUsuarioDNI(45, tipoUsuario);
	cout << "Busqueda de la clave: " << *claveBuscada3 << endl;
	ClaveRegistroArbol *claveEncontrada3 = arbol->buscar(claveBuscada3);
	if(claveEncontrada3 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada3 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}

	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegTipoUsuarioDNI *claveBuscada4 = new ClaveRegTipoUsuarioDNI(91, tipoUsuario);
	cout << "Busqueda de la clave: " << *claveBuscada4 << endl;
	ClaveRegistroArbol *claveEncontrada4 = arbol->buscar(claveBuscada4);
	if(claveEncontrada4 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada4 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}

	// Busqueda proximada
	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegTipoUsuarioDNI *claveBuscada5 = new ClaveRegTipoUsuarioDNI(0, tipoUsuario);
	cout << "Busqueda de la clave: " << *claveBuscada5 << endl;
	ClaveRegistroArbol *claveEncontrada5 = arbol->buscarAproximado(claveBuscada5);
	if(claveEncontrada5 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada5 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}

	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegTipoUsuarioDNI *claveBuscada6 = new ClaveRegTipoUsuarioDNI(45, tipoUsuario);
	cout << "Busqueda de la clave: " << *claveBuscada6 << endl;
	ClaveRegistroArbol *claveEncontrada6 = arbol->buscarAproximado(claveBuscada6);
	if(claveEncontrada6 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada6 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}

	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegTipoUsuarioDNI *claveBuscada7 = new ClaveRegTipoUsuarioDNI(91, tipoUsuario);
	cout << "Busqueda de la clave: " << *claveBuscada7 << endl;
	ClaveRegistroArbol *claveEncontrada7 = arbol->buscarAproximado(claveBuscada7);
	if(claveEncontrada7 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada7 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}

	// Busqueda siguiente
	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegTipoUsuarioDNI *claveBuscada9 = new ClaveRegTipoUsuarioDNI(0, tipoUsuario);
	cout << "Busqueda de la clave: " << *claveBuscada9 << endl;
	ClaveRegistroArbol *claveEncontrada9 = arbol->buscarAproximado(claveBuscada9);
	if(claveEncontrada9 != NULL){
		cout << "Se encontro la clave primera de busqueda siguiente: " << *claveEncontrada9 << endl;
		while(claveEncontrada9 != NULL){
			claveEncontrada9 = arbol->siguiente();
			if(claveEncontrada9 != NULL){
				cout << "Se encontro la clave siguiente: " << *claveEncontrada9 << endl;
			}else{
				cout << "NO se encontro la clave siguiente: " << endl;
			}
		}
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}

}

void ArbolTest::buscarCategoriaServicio(Arbol *arbol){
	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegCatIDServ *claveBuscada = new ClaveRegCatIDServ(0, "Vehiculos");
	cout << "Busqueda de la clave: " << *claveBuscada << endl;
	ClaveRegistroArbol *claveEncontrada = arbol->buscar(claveBuscada);
	if(claveEncontrada != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}

	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegCatIDServ *claveBuscada2 = new ClaveRegCatIDServ(1,"Mascotas");
	cout << "Busqueda de la clave: " << *claveBuscada2 << endl;
	ClaveRegistroArbol *claveEncontrada2 = arbol->buscar(claveBuscada2);
	if(claveEncontrada2 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada2 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}

	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegCatIDServ *claveBuscada3 = new ClaveRegCatIDServ(45, "Real State");
	cout << "Busqueda de la clave: " << *claveBuscada3 << endl;
	ClaveRegistroArbol *claveEncontrada3 = arbol->buscar(claveBuscada3);
	if(claveEncontrada3 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada3 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}

	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegCatIDServ *claveBuscada4 = new ClaveRegCatIDServ(91, "Mascotas");
	cout << "Busqueda de la clave: " << *claveBuscada4 << endl;
	ClaveRegistroArbol *claveEncontrada4 = arbol->buscar(claveBuscada4);
	if(claveEncontrada4 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada4 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}

	// Busqueda proximada
	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegCatIDServ *claveBuscada5 = new ClaveRegCatIDServ(0, "Vehiculos");
	cout << "Busqueda de la clave: " << *claveBuscada5 << endl;
	ClaveRegistroArbol *claveEncontrada5 = arbol->buscarAproximado(claveBuscada5);
	if(claveEncontrada5 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada5 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}

	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegCatIDServ *claveBuscada6 = new ClaveRegCatIDServ(45, "Mascotas");
	cout << "Busqueda de la clave: " << *claveBuscada6 << endl;
	ClaveRegistroArbol *claveEncontrada6 = arbol->buscarAproximado(claveBuscada6);
	if(claveEncontrada6 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada6 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}

	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegCatIDServ *claveBuscada7 = new ClaveRegCatIDServ(91, "Mascotas");
	cout << "Busqueda de la clave: " << *claveBuscada7 << endl;
	ClaveRegistroArbol *claveEncontrada7 = arbol->buscarAproximado(claveBuscada7);
	if(claveEncontrada7 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada7 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}

	// Busqueda siguiente
	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegCatIDServ *claveBuscada9 = new ClaveRegCatIDServ(0, "Real State");
	cout << "Busqueda de la clave: " << *claveBuscada9 << endl;
	ClaveRegistroArbol *claveEncontrada9 = arbol->buscarAproximado(claveBuscada9);
	if(claveEncontrada9 != NULL){
		cout << "Se encontro la clave primera de busqueda siguiente: " << *claveEncontrada9 << endl;
		while(claveEncontrada9 != NULL){
			claveEncontrada9 = arbol->siguiente();
			if(claveEncontrada9 != NULL){
				cout << "Se encontro la clave siguiente: " << *claveEncontrada9 << endl;
			}else{
				cout << "NO se encontro la clave siguiente: " << endl;
			}
		}
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}

}

void ArbolTest::buscar2(Arbol* arbol){
	unsigned int tipoUsuario = 12;

	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegTipoUsuarioDNI *claveBuscada = new ClaveRegTipoUsuarioDNI(0, tipoUsuario);
	cout << "Busqueda de la clave: " << *claveBuscada << endl;
	ClaveRegistroArbol *claveEncontrada = arbol->buscar(claveBuscada);
	if(claveEncontrada != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}
	delete(claveBuscada);

	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegTipoUsuarioDNI *claveBuscada2 = new ClaveRegTipoUsuarioDNI(1, tipoUsuario);
	cout << "Busqueda de la clave: " << *claveBuscada2 << endl;
	ClaveRegistroArbol *claveEncontrada2 = arbol->buscar(claveBuscada2);
	if(claveEncontrada2 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada2 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}
	delete(claveBuscada2);

	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegTipoUsuarioDNI *claveBuscada3 = new ClaveRegTipoUsuarioDNI(45, tipoUsuario);
	cout << "Busqueda de la clave: " << *claveBuscada3 << endl;
	ClaveRegistroArbol *claveEncontrada3 = arbol->buscar(claveBuscada3);
	if(claveEncontrada3 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada3 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}
	delete(claveBuscada3);

	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegTipoUsuarioDNI *claveBuscada4 = new ClaveRegTipoUsuarioDNI(201, tipoUsuario);
	cout << "Busqueda de la clave: " << *claveBuscada4 << endl;
	ClaveRegistroArbol *claveEncontrada4 = arbol->buscar(claveBuscada4);
	if(claveEncontrada4 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada4 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}
	delete(claveBuscada4);


	// Busqueda proximada
	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegTipoUsuarioDNI *claveBuscada5 = new ClaveRegTipoUsuarioDNI(0, tipoUsuario);
	cout << "Busqueda de la clave: " << *claveBuscada5 << endl;
	ClaveRegistroArbol *claveEncontrada5 = arbol->buscarAproximado(claveBuscada5);
	if(claveEncontrada5 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada5 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}
	delete(claveBuscada5);


	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegTipoUsuarioDNI *claveBuscada6 = new ClaveRegTipoUsuarioDNI(45, tipoUsuario);
	cout << "Busqueda de la clave: " << *claveBuscada6 << endl;
	ClaveRegistroArbol *claveEncontrada6 = arbol->buscarAproximado(claveBuscada6);
	if(claveEncontrada6 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada6 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}
	delete(claveBuscada6);

	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegTipoUsuarioDNI *claveBuscada7 = new ClaveRegTipoUsuarioDNI(201, tipoUsuario);
	cout << "Busqueda de la clave: " << *claveBuscada7 << endl;
	ClaveRegistroArbol *claveEncontrada7 = arbol->buscarAproximado(claveBuscada7);
	if(claveEncontrada7 != NULL){
		cout << "Se encontro la clave: " << *claveEncontrada7 << endl;
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}
	delete(claveBuscada7);


	// Busqueda siguiente
	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegTipoUsuarioDNI *claveBuscada9 = new ClaveRegTipoUsuarioDNI(0, tipoUsuario);
	cout << "Busqueda de la clave: " << *claveBuscada9 << endl;
	ClaveRegistroArbol *claveEncontrada9 = arbol->buscarAproximado(claveBuscada9);
	if(claveEncontrada9 != NULL){
		cout << "Se encontro la clave primera de busqueda siguiente: " << *claveEncontrada9 << endl;
		while(claveEncontrada9 != NULL){
			claveEncontrada9 = arbol->siguiente();
			if(claveEncontrada9 != NULL){
				cout << "Se encontro la clave siguiente: " << *claveEncontrada9 << endl;
			}else{
				cout << "NO se encontro la clave siguiente: " << endl;
			}
		}
	}else{
		cout << "NO se encontro la clave buscada: " << endl;
	}
	delete(claveBuscada9);
}

void ArbolTest::listar(Arbol* arbol){
	cout << "----------------------------------------------------------------------------------------" << endl;
	cout << "Listar" << endl;
	cout << "----------------------------------------------------------------------------------------" << endl;
	arbol->listar();
	cout << "----------------------------------------------------------------------------------------" << endl;
	cout << "Listar por rango" << endl;
	cout << "----------------------------------------------------------------------------------------" << endl;
	ClaveRegTipoUsuarioDNI *clave1 = new ClaveRegTipoUsuarioDNI(1, USUARIO_COMUN);
	cout << "clave1: " << *clave1 << endl;
	ClaveRegTipoUsuarioDNI *clave2 = new ClaveRegTipoUsuarioDNI(16, USUARIO_COMUN);
	cout << "clave2: " << *clave2 << endl;
	arbol->listarRango(clave1, clave2);
	cout << "----------------------------------------------------------------------------------------" << endl;
	cout << "Listar por rango" << endl;
	cout << "----------------------------------------------------------------------------------------" << endl;
	clave1 = new ClaveRegTipoUsuarioDNI(16, USUARIO_COMUN);
	cout << "clave1: " << *clave1 << endl;
	clave2 = new ClaveRegTipoUsuarioDNI(61, USUARIO_COMUN);
	cout << "clave2: " << *clave2 << endl;
	arbol->listarRango(clave1, clave2);
	cout << "----------------------------------------------------------------------------------------" << endl;
	cout << "Listar por rango" << endl;
	cout << "----------------------------------------------------------------------------------------" << endl;
	clave1 = new ClaveRegTipoUsuarioDNI(0, USUARIO_COMUN);
	cout << "clave1: " << *clave1 << endl;
	clave2 = new ClaveRegTipoUsuarioDNI(100, USUARIO_COMUN);
	cout << "clave2: " << *clave2 << endl;
	arbol->listarRango(clave1, clave2);
}

void ArbolTest::cargaInicialProvinciaDNI(Arbol* arbol){
	//Cargo Registros para insertar luego en el arbol
	cout << "--------------------------------------------------------------" << endl;
	cout << "cargaInicial" << endl;
	cout << "--------------------------------------------------------------" << endl;
	for(unsigned int i = 1; i <= 1; i++){
		string provincia = "Buen";
		ClaveRegProvinciaDNI *clave = new ClaveRegProvinciaDNI(i, provincia);
		arbol->insertar(clave);
	}

	for(unsigned int i = 2; i <= 90; i++){
		string provincia = "Buenos Aires";
		if(i >= 10 && i <= 30){
			provincia = "CABA";
		}else if(i >= 60 && i <= 70){
			provincia = "GBA";
		}

		ClaveRegProvinciaDNI *clave = new ClaveRegProvinciaDNI(i, provincia);
		arbol->insertar(clave);
	}

	cout << endl << "Imprimir archivo ---------------------------------------------" << endl;
	arbol->getArchivo()->imprimir();
	cout << endl << "Listar archivo2  ---------------------------------------------" << endl;
	arbol->listar();
}


