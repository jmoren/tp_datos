/*
 * ClaveRegistroArbol.cpp
 *
 *  Created on: 06/10/2013
 *      Author: natalia
 */

#include "Persistente.h"

Persistente::Persistente() {
}

Persistente::~Persistente() {
}

char* Persistente::serializar(){
	return NULL;
}

void Persistente::deserializar(char* dato){}

void Persistente::almacenarEnteroLargo(char* datoEscritura, unsigned int &pos, unsigned long enteroLargo){
    //Casteo a char* el entero y luego lo copio al dato
    char* enteroLargoChar = (char*) &enteroLargo;
    for(short i = 0; i<(short)sizeof(unsigned long); i++){
    	datoEscritura[pos] = enteroLargoChar[i];
    	pos++;
    }
}

unsigned long Persistente::recuperarEnteroLargo(char* datoLectura, unsigned int &pos){
	unsigned long enteroLargo;
	char* enteroLargoChar = new char[sizeof(unsigned long)];
	for(short i = 0; i<(short)sizeof(unsigned long); i++){
		enteroLargoChar[i] = datoLectura[pos];
		pos++;
	}
	enteroLargo = *((unsigned long*)(enteroLargoChar));

	delete[] enteroLargoChar;
	return enteroLargo;
}

void Persistente::almacenarEntero(char* datoEscritura, unsigned int &pos, unsigned int entero){
    //Casteo a char* el entero y luego lo copio al dato
    char* enteroChar = (char*) &entero;
    for(short i = 0; i<(short)sizeof(unsigned int); i++){
    	datoEscritura[pos] = enteroChar[i];
    	pos++;
    }
}

unsigned int Persistente::recuperarEntero(char* datoLectura, unsigned int &pos){
	unsigned int entero;
	char* enteroChar = new char[sizeof(unsigned int)];
	for(short i = 0; i<(short)sizeof(unsigned int); i++){
		enteroChar[i] = datoLectura[pos];
		pos++;
	}
	entero = *((unsigned int*)(enteroChar));

	delete[] enteroChar;
	return entero;
}

void Persistente::almacenarEnteroCorto(char* datoEscritura, unsigned int &pos, unsigned short enteroCorto){
    //Casteo a char* el entero y luego lo copio al dato
    char* enteroChar = (char*) &enteroCorto;
    for(short i = 0; i<(short)sizeof(unsigned short); i++){
    	datoEscritura[pos] = enteroChar[i];
    	pos++;
    }
}

unsigned short int Persistente::recuperarEnteroCorto(char* datoLectura, unsigned int &pos){
	unsigned short int enteroCorto;

	char* enteroCortoChar = new char[sizeof(unsigned short int)];
	for(short i = 0; i<(short)sizeof(unsigned short int); i++){
		enteroCortoChar[i] = datoLectura[pos];
		pos++;
	}
	enteroCorto = *((unsigned short int*)(enteroCortoChar));

	//TODO: Chequear que ande todo bien!!!
	delete[] enteroCortoChar;
	return enteroCorto;
}

void Persistente::almacenarString(char* datoEscritura, unsigned int &pos, string cadena){
	//cout << "Almacenar String: ";
	for(unsigned int i = 0; i<cadena.length(); i++){
    	datoEscritura[pos] = cadena[i];
    	//cout << datoEscritura[pos];
    	pos++;
    }
	//cout << endl;
}

string Persistente::recuperarString(char* datoLectura, unsigned int &pos,unsigned int lon){
    stringstream ss;
    unsigned int i;
    //cout << "Recuperar String: ";
	for( i= 0; i<lon; i++){
		//cout << datoLectura[pos];
    	ss << datoLectura[pos];
    	pos++;
    }
	//cout << endl;
    return ss.str();
}

void Persistente::almacenarChar(char* datoEscritura, unsigned int &pos, char caracter){
	datoEscritura[pos] = caracter;
	pos++;
}

char Persistente::recuperarChar(char* datoLectura, unsigned int &pos){
	char caracter = datoLectura[pos];
	pos++;
	return caracter;
}

void Persistente::almacenarCharAsterisco(char* datoEscritura, unsigned int &pos, char *charAsterisco, unsigned int longitudCharAsterisco){
	for(unsigned int i = 0; i < longitudCharAsterisco; i++){
		datoEscritura[pos] = charAsterisco[i];
		pos++;
	}
}

char* Persistente::recuperarCharAsterisco(char* datoLectura, unsigned int &pos, unsigned int longitudCharAsterisco){
	char* charAsterisco = new char[longitudCharAsterisco];
	for(unsigned int i = 0; i < longitudCharAsterisco; i++){
		charAsterisco[i] = datoLectura[pos];
		pos++;
	}
	return charAsterisco;
}
