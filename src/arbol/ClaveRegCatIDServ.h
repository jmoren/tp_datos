#ifndef CLAVEREGCATIDSERV_H_
#define CLAVEREGCATIDSERV_H_

#include "ClaveRegistroArbol.h"
#include "../utils/Funciones.h"

class ClaveRegCatIDServ: public ClaveRegistroArbol {

private:
	unsigned long longitud;
	string nombreCategoria;
	unsigned long idServicio;

public:
	ClaveRegCatIDServ();
	virtual ~ClaveRegCatIDServ();
	ClaveRegCatIDServ(unsigned long idservicio,string nombreCategoria);
	unsigned long getIdServicio();
	void setIdServicio(unsigned long idServicio);
	string getNombreCategoria();
	void setNombreCategoria(string nombreCategoria);
	unsigned long getLongitud();
	void setLongitud(unsigned long idServicio);


	short int comparar(ClaveRegistroArbol* claveParaComparar);
	unsigned int getTamanio();
	char* serializar();
	void deserializar(char*);


	string getDatosImportantes();

};

#endif /* CLAVEREGCATIDSERV_H_ */
