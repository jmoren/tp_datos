/*
 * RegistroNodo.cpp
 *
 *  Created on: 06/10/2013
 *      Author: natalia
 */

#include "RegistroNodo.h"

RegistroNodo::RegistroNodo() {
	this->clave = NULL;
}

RegistroNodo::RegistroNodo(ClaveRegistroArbol* clave) {
	this->clave = clave;
}


RegistroNodo::RegistroNodo(TipoRegistro tipoRegistro, char *dato, unsigned int tamanioDato){
	// Creo la clave sin deserializarla porque no tengo el dato preprado para ello aun.
	this->clave = FabricaClaveRegistro::crearClaveRegistro(tipoRegistro, NULL);
	this->deserializar(dato, tamanioDato);
}

RegistroNodo::~RegistroNodo() {
	//cout << "INFO: Destructor RegistroNodo" << endl;
	if(this->clave != NULL){
		delete(this->clave);
	}
}

void RegistroNodo::setClave(ClaveRegistroArbol* clave){
	this->clave = clave;
}

ClaveRegistroArbol* RegistroNodo::getClave(){
	return this->clave;
}

unsigned int RegistroNodo::getTamanio(){
	return  this->clave->getTamanio(); // Clave
}

short int RegistroNodo::comparar(RegistroNodo *registroNodoComparar){
	if((registroNodoComparar != NULL) && (registroNodoComparar->clave != NULL)){
		return this->clave->comparar(registroNodoComparar->clave);
	}else{
		return 1; // Si el registro con el que comparo es NULL entonces siempre este registro sera mayor a NULL
	}
}

char* RegistroNodo::serializar(){
	if(this->clave != NULL){
		return this->clave->serializar();
	}else{
		cout << "Error al querer serializar un RegistroNodo que no tiene clave instanciada" << endl;
		return NULL;
	}
}

void RegistroNodo::deserializar(char *dato, unsigned int tamanioDato){
	if(this->clave != NULL){
		this->clave->deserializar(dato);
	}else{
		cout << "Error al querer deserializar un RegistroNodo que no tiene clave instanciada" << endl;
	}
}

string RegistroNodo::getDatosImportantes(){
	std::stringstream datosImportantes;
	datosImportantes << "( RegistroNodo: "<< "Tamanio: " << (sizeof(unsigned int) + getTamanio()) << ", clave" << *(this->clave) << ")";

    return  datosImportantes.str();
}
