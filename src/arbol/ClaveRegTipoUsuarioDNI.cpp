/*
 * claveRegTipoUsuarioDNI.cpp
 *
 *  Created on: 06/10/2013
 *      Author: natalia
 */

#include "ClaveRegTipoUsuarioDNI.h"

using namespace std;

// Constructores -------------------------------------------------------------------------------
ClaveRegTipoUsuarioDNI::ClaveRegTipoUsuarioDNI() {
	this->dni         = 0;
	this->tipoUsuario = 0;
}

ClaveRegTipoUsuarioDNI::ClaveRegTipoUsuarioDNI(unsigned long dni, unsigned int tipoUsuario) {
	this->dni         = dni;
	this->tipoUsuario = tipoUsuario;
}

ClaveRegTipoUsuarioDNI::~ClaveRegTipoUsuarioDNI() {
	this->dni         = 0;
	this->tipoUsuario = 0;
}

// Setter -------------------------------------------------------------------------------------
void ClaveRegTipoUsuarioDNI::setDni(unsigned long dni) {
	this->dni = dni;
}

void ClaveRegTipoUsuarioDNI::setTipoUsuario(unsigned int tipoUsuario) {
	this->tipoUsuario = tipoUsuario;
}

// Getter -------------------------------------------------------------------------------------
unsigned long ClaveRegTipoUsuarioDNI::getDni()  {
	return dni;
}

unsigned int ClaveRegTipoUsuarioDNI::getTipoUsuario()  {
	return tipoUsuario;
}

// --------------------------------------------------------------------------------------------

//devuelve 0 si son iguales, 1 si es mayor y -1 si es menor
short int ClaveRegTipoUsuarioDNI::comparar(ClaveRegistroArbol* claveParaComparar){
	if((claveParaComparar != NULL) ){
		ClaveRegTipoUsuarioDNI *clave2 = (ClaveRegTipoUsuarioDNI*)claveParaComparar;

		if(this->getTipoUsuario() == clave2->getTipoUsuario()){
			if(this->getDni() == clave2->getDni()){
				return 0;
			}else if(this->getDni() > clave2->getDni()){
				return 1;
			}else{
				return -1;
			}
		}else if(this->getTipoUsuario() > clave2->getTipoUsuario()){
			return 1;
		}else{
			return -1;
		}
	}else{
		return 1; // Si la clave con la que comparo es NULL entonces siempre esta clave sera mayor a NULL
	}
}

unsigned int ClaveRegTipoUsuarioDNI::getTamanio(){

	return sizeof(unsigned int) + sizeof(unsigned long);

}

char* ClaveRegTipoUsuarioDNI::serializar(){
	char* cadenaEscritura = new char[getTamanio()];
	unsigned int pos = 0;
	this->almacenarEntero(cadenaEscritura, pos, getTipoUsuario());
	this->almacenarEnteroLargo(cadenaEscritura, pos, getDni());

	return cadenaEscritura;
}

void ClaveRegTipoUsuarioDNI::deserializar(char* cadena){
	unsigned int pos = 0;

	this->setTipoUsuario(this->recuperarEntero(cadena, pos));

	this->setDni(this->recuperarEnteroLargo(cadena, pos));
}

string ClaveRegTipoUsuarioDNI::getDatosImportantes(){
	std::stringstream datosImportantes;
	datosImportantes << "ClaveRegTipoUsuarioDNI: "<< "Tamanio: " << getTamanio() << " (tipo: " << getTipoUsuario() << ", DNI:" << getDni() << ")";

    return  datosImportantes.str();
}

