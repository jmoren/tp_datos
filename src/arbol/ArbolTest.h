/*
 * ArbolTest.h
 *
 *  Created on: 06/10/2013
 *      Author: Natalia
 */

#ifndef ARBOLTEST_H_
#define ARBOLTEST_H_

#include "Arbol.h"
#include "ClaveRegTipoUsuarioDNI.h"
#include "FabricaClaveRegistro.h"
#include <list>

class ArbolTest {

public:
	ArbolTest();
	virtual ~ArbolTest();

	static Arbol* creacion(TipoRegistro tipoRegistro);
	static void cargaInicial(Arbol* arbol);
	static void insertRegistrosTest(Arbol* arbol);

	static void deleteRegistrosTest(Arbol* arbol);
	static void mostrarArchivoMock(Arbol* arbol);

	static void buscar(Arbol* arbol);
	static void buscarCategoriaServicio(Arbol *arbol);
	static void buscar2(Arbol* arbol);

	static void listar(Arbol* arbol);

	// Prueba 2
	static void cargaInicialProvinciaDNI(Arbol* arbol);
	static void creacionTipoUsuarioDNI(Arbol* arbol);



};

#endif /* ARBOLTEST_H_ */
