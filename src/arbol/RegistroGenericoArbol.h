#ifndef REGISTROGENERICOARBOL_H_
#define REGISTROGENERICOARBOL_H_

#include "../utils/Funciones.h"

using namespace std;

class RegistroGenericoArbol {

private:
	char* dato;
	unsigned int tamanio;

public:

	RegistroGenericoArbol();
	RegistroGenericoArbol(char* dato, unsigned int tamanio);
	virtual ~RegistroGenericoArbol();

	void setTamanio(unsigned int tamanio);
	void setDato(char* dato);

	unsigned int getTamanio();
	char* getDato();

	char* serializar();
	static RegistroGenericoArbol* deserializar(char*);

};

#endif /* REGISTROGENERICOARBOL_H_ */
