#ifndef CLAVEREGIDSERVFCHCIDCONS_H_
#define CLAVEREGIDSERVFCHCIDCONS_H_

#include "ClaveRegistroArbol.h"
#include "../utils/Funciones.h"

class ClaveRegIDServFCHCIDCons: public ClaveRegistroArbol {
private: unsigned long idServicio;
		 string fechaConsulta;
		 string horaConsulta;
		 unsigned long idConsulta;

public:
	ClaveRegIDServFCHCIDCons();
	virtual ~ClaveRegIDServFCHCIDCons();
	ClaveRegIDServFCHCIDCons(unsigned long idServicio,string fechaConsulta, string horaConsulta, unsigned long idConsulta);

	short int comparar(ClaveRegistroArbol* claveParaComparar);
	unsigned int getTamanio();
	char* serializar();
	void deserializar(char*);

	string getFechaConsulta();
	void setFechaConsulta(string fechaConsulta);
	string getHoraConsulta();
	void setHoraConsulta(string horaConsulta);
	unsigned long getIdConsulta();
	void setIdConsulta(unsigned long idConsulta);
	unsigned long getIdServicio();
	void setIdServicio(unsigned long idServicio);

	string getDatosImportantes();
};

#endif /* CLAVEREGIDSERVFCHCIDCONS_H_ */
