/*
 * NodoInterno.cpp
 *
 *  Created on: 06/10/2013
 *      Author: natalia
 */

#include "NodoInterno.h"

using namespace std;

// constructores y destructores -----------------------------------------------------------------------------
NodoInterno::NodoInterno(){
	this->tipoRegistro = TipoUsuarioDNI; // Asignar default;
	this->idNodo = 0;
	this->nivel = 0;
	this->tamanioBloque = 0;
	this->tamanioTotalDisponibleParaRegistros = 0;
	this->espacioLibre = 0;
	this->idNodoIzq = 0;
	this->cantRegistros = 0;
}

NodoInterno::NodoInterno(TipoRegistro tipoRegistro, Bloque *bloque, unsigned int tamanioTotalDisponibleParaRegistros){
	this->tipoRegistro = tipoRegistro;
	this->tamanioBloque = bloque->getTamanioBloque();
	this->tamanioTotalDisponibleParaRegistros = tamanioTotalDisponibleParaRegistros;
	unsigned int tamanioCompletoRegistroGenericoCabecera = sizeof(unsigned int) +  getTamanioRegistroCabeceraNodo();  // tamanio longitud registroGenerico + longitud datos registroGenerico
	if(tamanioCompletoRegistroGenericoCabecera <= tamanioTotalDisponibleParaRegistros){
		this->espacioLibre = tamanioTotalDisponibleParaRegistros - tamanioCompletoRegistroGenericoCabecera;
	}else{
		cout << "ERROR: El espacio libre insuficiente, no se pueden guardar los datos minimos de la cabecera del nodo." << endl;
		this->espacioLibre = 0;
	}

	list<RegistroGenericoArbol*> registrosGenericos = bloque->getRegistros();

	unsigned int idNodoIzq = 0;
	for (std::list<RegistroGenericoArbol*>::iterator it = registrosGenericos.begin(); it != registrosGenericos.end(); it++){
		if(it == registrosGenericos.begin()){
			// Recupero el primer registro que tiene los datos de la cabecera
			recuperarDatosCabeceraNodo(*it);
			// Chequeo que la cantidad de registros sea igual a la cantidad de registros del bloque -1 (la cabecera)
			if(this->cantRegistros != (registrosGenericos.size() - 1)){
				cout << "ERROR: NodoInterno constructor: La cantidad de nodos obtenida del bloque no es igual a la cantidad guardada en la cabecera." << endl;
				break;
			}
			this->cantRegistros = 0;
			idNodoIzq = this->getIdNodoIzq();
		}else{
			// Recupero todos los registroNodoInterno
			RegistroNodo* registroNodo = recuperarRegistroNodo(*it);
			((RegistroNodoInterno*)registroNodo)->setIdNodoIzq(idNodoIzq);
			idNodoIzq = ((RegistroNodoInterno*)registroNodo)->getIdNodoDer();

			unsigned short resultado =  this->insertarRegistroNodo(registroNodo);

			// Si el registro no se inserta en el arbol entonces liberamos su memoria.
			if((resultado == 0) || (resultado == 3)){
				delete(registroNodo);
			}
		}
	}
}

NodoInterno::NodoInterno(TipoRegistro tipoRegistro, unsigned int idNodo, unsigned int nivel, unsigned int tamanioBloque, unsigned int tamanioTotalDisponibleParaRegistros){
	this->tipoRegistro = tipoRegistro;
	this->idNodo = idNodo;
	this->nivel = nivel;
	this->tamanioBloque = tamanioBloque;
	this->tamanioTotalDisponibleParaRegistros = tamanioTotalDisponibleParaRegistros;
	unsigned int tamanioCompletoRegistroGenericoCabecera = sizeof(unsigned int) +  getTamanioRegistroCabeceraNodo(); // tamanio longitud registroGenerico + longitud datos registroGenerico
	if(tamanioCompletoRegistroGenericoCabecera <= tamanioTotalDisponibleParaRegistros){
		this->espacioLibre = tamanioTotalDisponibleParaRegistros - tamanioCompletoRegistroGenericoCabecera;
	}else{
		cout << "ERROR: El espacio libre insuficiente, no se pueden guardar los datos minimos de la cabecera del nodo." << endl;
		this->espacioLibre = 0;
	}
	this->cantRegistros = 0;
	this->idNodoIzq = 0;
}

NodoInterno::~NodoInterno() {
	this->tipoRegistro = TipoUsuarioDNI; // Asignar default;
	this->idNodo = 0;
	this->nivel = 0;
	this->tamanioBloque = 0;
	this->tamanioTotalDisponibleParaRegistros = 0;
	this->espacioLibre = 0;
	this->idNodoIzq = 0;
	vaciarRegistrosNodo();

}

// Metodos Protected  ---------------------------------------------------------------------------------------
unsigned int NodoInterno::getTamanioRegistroCabeceraNodo(){
	// nivel + idNodo + cantidad registros + idNodoIzquierdo
	unsigned int tamanioRegistroCabeceraNodo = sizeof(unsigned int) + sizeof(unsigned int) + sizeof(unsigned int) + sizeof(unsigned int);
	return tamanioRegistroCabeceraNodo;
}

/**
 *  Devuelve de forma serializada y en un RegistroGenerico los
 *  atributos de la cabecera:
 *   nivel + idNodo + cantidad registros + idNodoIzquierdo
 */
RegistroGenericoArbol* NodoInterno::crearRegistroCabeceraNodo(){
	unsigned int tamanio = getTamanioRegistroCabeceraNodo();
	char* dato = new char[tamanio];
	unsigned int pos = 0;

	// nivel
	this->almacenarEntero(dato, pos, this->getNivel());

	// idNodo
	this->almacenarEntero(dato, pos, this->getIdNodo());

	// cantidad registros
	this->almacenarEntero(dato, pos, this->getCantRegistros());

	// idNodoIzquierdo -> obtengo el idNodoIzquierdo del primer registroNodoInterno si es que tiene alguno.
	if(!registrosNodo.empty()){
		// Obtengo el primer registroNodo de la lista
		RegistroNodo *reg = registrosNodo.front();
		// Por las dudas que se este usando mal el NodoInterno y el casteo a RegistroNodoInterno falle, pongo el try catch
		try{
			RegistroNodoInterno *registroNodoInterno = (RegistroNodoInterno*)reg;
			this->idNodoIzq = registroNodoInterno->getIdNodoIzq();
		}catch(...){
			cout << "ERROR: crearRegistroCabeceraNodo -> El NodoInterno no puede castear sus registrosNodo a la clase RegistroNodoInterno" << endl;
			this->idNodoIzq = 0;
		}
	}else{
		this->idNodoIzq = 0;
	}
	this->almacenarEntero(dato, pos, this->getIdNodoIzq());

	return new RegistroGenericoArbol(dato, tamanio);
}

RegistroGenericoArbol* NodoInterno::crearRegistroGenerico(RegistroNodo* registroNodo){
	RegistroNodoInterno *registroNodoInterno = (RegistroNodoInterno*)registroNodo;

	unsigned int tamanio = registroNodoInterno->getTamanio();
	char *dato = registroNodoInterno->serializar();

	return new RegistroGenericoArbol(dato, tamanio);
}

void NodoInterno::recuperarDatosCabeceraNodo(RegistroGenericoArbol *registroGenerico){
	if(registroGenerico->getTamanio() == this->getTamanioRegistroCabeceraNodo()){
		char* datoLectura = registroGenerico->getDato();
		unsigned int pos = 0;

		// nivel
		this->setNivel(this->recuperarEntero(datoLectura, pos));

		// idNodo
		this->setIdNodo(this->recuperarEntero(datoLectura, pos));

		// cantidad registros
		this->cantRegistros = this->recuperarEntero(datoLectura, pos);

		// idNodoIzquierdo -> obtengo el idNodoIzquierdo del primer registroNodoInterno si es que tiene alguno.
		this->setIdNodoIzq(this->recuperarEntero(datoLectura, pos));

	}else{
		cout << "ERROR: NodoInterno -> recuperarDatosCabeceraNodo: El tamanio del registroGenerico no coincide con el tamanio de la cabecera" << endl;
	}

}

RegistroNodo* NodoInterno::recuperarRegistroNodo(RegistroGenericoArbol *registroGenerico){
	RegistroNodoInterno *registroNodoInterno = new RegistroNodoInterno(this->tipoRegistro, registroGenerico->getDato(), registroGenerico->getTamanio());
	return registroNodoInterno;
}

void NodoInterno::insertarOrdenado(RegistroNodo *registroNodo){
	std::list<RegistroNodo*>::iterator posicionAInsertar = registrosNodo.end();
	if(!registrosNodo.empty()){
		for (std::list<RegistroNodo*>::iterator it = registrosNodo.begin(); it != registrosNodo.end(); it++){
			short int comparacion = (*it)->comparar(registroNodo);
			if(comparacion > 0){
				posicionAInsertar = it;
				actualizarRegistroSiguiente(registroNodo, (*it));
				break; // El registro buscado no se encontro y ya estoy comparando con registros con clave mayor
			}
		}
	}
	if(posicionAInsertar == registrosNodo.end()){
		this->registrosNodo.push_back(registroNodo);
	}else{
		this->registrosNodo.insert(posicionAInsertar,registroNodo);
	}
	this->cantRegistros++;
}

// Modifica idNodoIzq del siguiente registro luego del insertado porque cambia.
void NodoInterno::actualizarRegistroSiguiente(RegistroNodo *registroNodoInsertado, RegistroNodo *registroNodoSiguiente){
	//cout << "****************************** NodoInterno::actualizarRegistroSiguiente" << endl;
	RegistroNodoInterno* registroNodoInternoInsertado = dynamic_cast<RegistroNodoInterno*>(registroNodoInsertado);
	RegistroNodoInterno* registroNodoInternoSiguiente = dynamic_cast<RegistroNodoInterno*>(registroNodoSiguiente);
	if (registroNodoInternoInsertado != NULL && (registroNodoInternoSiguiente != NULL)){
		if(registroNodoInternoInsertado->getIdNodoDer() != registroNodoInternoSiguiente->getIdNodoIzq()){
			cout << "ERROR: NodoInterno -> actualizarRegistroSiguiente(): Reg_New_IdNodoIzq = " << registroNodoInternoInsertado->getIdNodoIzq() << ", Reg_Sig_IdNodoIzq =" << registroNodoInternoSiguiente->getIdNodoIzq() << endl;
		}
		// Actualizamos el id nodo hijo izquierdo del registro siguiente al insertaado con el id nodo hijo derecho del registro insertado
		registroNodoInternoSiguiente->setIdNodoIzq(registroNodoInternoInsertado->getIdNodoDer());
	}else{
		cout << "ERROR: NodoInterno -> actualizarRegistroSiguiente(): No pudo castear los RegistroNodo a RegistroNodoInterno" << endl;
	}
}

// Geters ---------------------------------------------------------------------------------------------------
TipoRegistro NodoInterno::getTipoRegistro(){
	return this->tipoRegistro;
}

unsigned int NodoInterno::getIdNodo(){
	return this->idNodo;
}

unsigned int NodoInterno::getNivel(){
	return this->nivel;
}

unsigned int NodoInterno::getTamanioBloque(){
	return this->tamanioBloque;
}

unsigned int NodoInterno::getTamanioTotalDisponibleParaRegistros(){
	return this->tamanioTotalDisponibleParaRegistros;
}

unsigned int NodoInterno::getEspacioLibre(){
	return this->espacioLibre;
}

unsigned int NodoInterno::getCantRegistros(){
	return this->cantRegistros;
}

list<RegistroNodo*> NodoInterno::getRegistrosNodo(){
	return this->registrosNodo;
}

unsigned int NodoInterno::getIdNodoIzq(){
	return this->idNodoIzq;
}

// Seters ---------------------------------------------------------------------------------------------------
void NodoInterno::setTipoRegistro(TipoRegistro tipo){
	this->tipoRegistro=tipo;
}


void NodoInterno::setIdNodo(unsigned int idNodo){
	this->idNodo = idNodo;
}

void NodoInterno::setNivel(unsigned int nivel){
	this->nivel = nivel;
}

void NodoInterno::setTamanioBloque(unsigned int tamanioBloque){
	this->tamanioBloque = tamanioBloque;
}

void NodoInterno::getTamanioTotalDisponibleParaRegistros(unsigned int tamanioTotalDisponibleParaRegistros){
	this->tamanioTotalDisponibleParaRegistros = tamanioTotalDisponibleParaRegistros;
}

void NodoInterno::setEspacioLibre(unsigned int espacioLibre){
	this->espacioLibre = espacioLibre;
}

void NodoInterno::setIdNodoIzq(unsigned int idNodoIzq){
	this->idNodoIzq = idNodoIzq;
}

void NodoInterno::setCantRegistros(unsigned int cant){
	this->cantRegistros=cant;
}

// Metodos --------------------------------------------------------------------------------------------------

/*
 *  Devuelve:
 * 				0	Si el registroNodo recibido no se actualizo
 * 				1	El nodo se actualizo insertanto el registroNodo
 * 				2	El nodo se desbordo al intentar insertar y no lo inserto
 * 				3	Ya existe un registro con el identificador igual al que se quiso insertar
 */
unsigned short NodoInterno::insertarClaveArbol(ClaveRegistroArbol *clave, TipoRegistro tipoRegistro, unsigned int idNodoIzq, unsigned int idNodoDer){
	char* claveSerializada = clave->serializar();
	ClaveRegistroArbol *claveClonada = FabricaClaveRegistro::crearClaveRegistro(tipoRegistro, claveSerializada);
	delete[] claveSerializada; // Liberamos la memoria ya que no se usara mas.

	RegistroNodoInterno *registro = new RegistroNodoInterno(claveClonada);
	registro->setIdNodoIzq(idNodoIzq);
	registro->setIdNodoDer(idNodoDer);

	unsigned short resultado =  this->insertarRegistroNodo(registro);

	// Si el registro no se inserta en el arbol entonces liberamos su memoria.
	if((resultado == 0) || (resultado == 3)){
		delete(registro);
	}

	return resultado;
}

/*
 *  Devuelve:
 * 				0	Si el registroNodo recibido no se actualizo
 * 				1	El nodo se actualizo insertanto el registroNodo
 * 				2	El nodo se desbordo al intentar insertar y no lo inserto
 * 				3	Ya existe un registro con el identificador igual al que se quiso insertar
 */
unsigned short NodoInterno::insertarRegistroNodo(RegistroNodo *registroNodo){
	//Primero valido que el registroNodo a insertar no sea mas grande del espacio maximo para registros
	unsigned int tamanioCompletoRegistroGenericoCabecera = sizeof(unsigned int) +  getTamanioRegistroCabeceraNodo();  // tamanio longitud registroGenerico + longitud datos registroGenerico
	unsigned int espacioUtilParaRegistros = this->tamanioTotalDisponibleParaRegistros - tamanioCompletoRegistroGenericoCabecera; // tamanio total para registro - tamanio registro cabecera nodo
	unsigned int tamanioMaximoRegistro = (unsigned int) ((espacioUtilParaRegistros * (overflow)) / 100);//(unsigned int) ((espacioUtilParaRegistros * (100 - underflow)) / 100);
	unsigned int espacioTotalNuevoRegistro = sizeof(unsigned int) + registroNodo->getTamanio(); // longitud registro a insertar + tamanio registro

	if(espacioTotalNuevoRegistro <= tamanioMaximoRegistro){
		if(existeRegistro(registroNodo)){
			//cout << "INFO: NodoInterno::insertarRegistroNodo: Ya existe un registro con el identificador igual al que se quiso insertar." << endl;
			//cout << "INFO: " << *registroNodo << endl;
			//cout << "----------------------------------------------------------------------------------------" << endl;
			return 3;
		}else{
			try{
				insertarOrdenado(registroNodo);
				// Calculo el nuevo espacio libre para ver si quede en overflow
				int nuevoEspacioLibre = this->espacioLibre - sizeof(unsigned int) - registroNodo->getTamanio(); // espacio libre - espacio del valor del tamanio registroNodo o registroGenerico - espacio de los datos del registroNodo
				if(nuevoEspacioLibre < (int)((espacioUtilParaRegistros*overflow)/100)){
					this->espacioLibre = 0; // El espacio libre es negativo pero le ponermos 0 porque es un unsigned int y devolvemos 2 para que el arbol sepa que hay que se esta en overflow
					//cout << "INFO: NodoInterno::insertarRegistroNodo: El nodo se desbordo al intentar insertar, se esta en OVERFLOW." << endl;
					return 2;
				}else{
					this->espacioLibre = nuevoEspacioLibre;
					//cout << "INFO: NodoInterno::insertarRegistroNodo: El nodo se actualizo insertanto el registroNodo." << endl;
					return 1;
				}
			}catch(...){
				cout << "ERROR: NodoInterno::insertarRegistroNodo: el registroNodo recibido no se actualizo." << endl;
				return 0;
			}
		}
	}else{
		cout << "**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**" << endl;
		cout << "ERROR: NodoInterno::insertarRegistroNodo: el registroNodo recibido no se actualizo porque su tamanio(" << espacioTotalNuevoRegistro << " bytes) es mayor al 20% del espacio maximo destinado para registros dentro del bloque(" << tamanioMaximoRegistro << " bytes). Necesitara crear un nuevo arbol con mayor tamanio de bloque." << endl;
		cout << "ERROR: NodoInterno::insertarRegistroNodo: La clave que no se puedo insertar fue: " << (*(registroNodo->getClave())) << endl;
		cout << "--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--" << endl;
		return 0;
	}
}

/*
 *  Devuelve:
 * 				0	Si el registroNodo recibido no se actualizo
 * 				1	Elimino el registro con el identificador pasado.
 * 				2   Si hay underflow
 * 				3	No existe un registro con el identificador igual al que se quiso eliminar
 */
unsigned short NodoInterno::eliminarClaveArbol(ClaveRegistroArbol* clave, TipoRegistro tipoRegistro, unsigned int &idNodo){
	char* claveSerializada = clave->serializar();
	ClaveRegistroArbol *claveClonada = FabricaClaveRegistro::crearClaveRegistro(tipoRegistro, claveSerializada);
	delete[] claveSerializada; // Liberamos la memoria ya que no se usara mas.

	RegistroNodoInterno *registro = new RegistroNodoInterno(claveClonada);
	unsigned short respuesta = this->eliminarRegistroNodo(registro,idNodo);
	delete(registro);
	return respuesta;
}


/*
 *  Devuelve:
 * 				0	Si el registroNodo recibido no se actualizo
 * 				1	Elimino el registro con el identificador pasado.
 * 				2   Si hay underflow
 * 				3	No existe un registro con el identificador igual al que se quiso eliminar
 */
unsigned short NodoInterno::eliminarRegistroNodo(RegistroNodo *registroNodo,unsigned int &idNodo){
	if(existeRegistro(registroNodo)){
		std::list<RegistroNodo*>::iterator posicionAEliminar = registrosNodo.end();
		std::list<RegistroNodo*>::iterator it;
		RegistroNodo *registro = NULL;

		unsigned int cant = 0;

		if(!registrosNodo.empty()){
			if(registrosNodo.size() == 1){
				// Obtengo el unico registro del nodo para obtener su tamanio y actualizar el espacio libre
				RegistroNodo *reg = registrosNodo.front();

				// Actualizo el espacio libre en el nododelete(reg);
				this->espacioLibre = this->espacioLibre + sizeof(unsigned int) + reg->getTamanio();

				// Elimino el unico registro del nodo
				registrosNodo.pop_front();
				delete(reg);

				// Actualizo la cantidad de regitros en el nodo
				this->cantRegistros = 0;
				/*if(this->getIdNodo() == 0){
					return 1;
				}else{*/
					return 2;
				//}
			}else{
				for (it = registrosNodo.begin(); it != registrosNodo.end(); it++){
					short int comparacion = (*it)->comparar(registroNodo);
					cant++;
					if(comparacion == 0){
						posicionAEliminar = it;
						registro = (*it);
						break;
					}else if(comparacion > 0){
						// El registro buscado no se encontro y ya estoy comparando con registros con clave mayor
						break;
					}
				}
				if(posicionAEliminar != registrosNodo.end()){
					// Si es un nodo interno y se elimina un nodo que no es el ultimo, actualizamos el idHijoIzquierdo del hermano derecho
					if (this->getNivel() > 0){
						if ((registrosNodo.size() > 1) && (idNodo != 0)){
							if(cant == registrosNodo.size()){//Es el ultimo registro del nodo
								std::list<RegistroNodo*>::iterator itAnt = --it;
								((RegistroNodoInterno*)(*itAnt))->setIdNodoDer(idNodo);
								it++;
							}else{//No es el ultimo registro del nodo
								if (cant!=1){//no es el primer registro entonces tengo que setearle el nodoDerecho al anterior
									std::list<RegistroNodo*>::iterator itAnt = --it;
									((RegistroNodoInterno*)(*itAnt))->setIdNodoDer(idNodo);
									it++;
								}
								std::list<RegistroNodo*>::iterator itSig = ++it;
								((RegistroNodoInterno*)(*itSig))->setIdNodoIzq(idNodo);
							}
						}
					}

					//cout << "INFO: Se elimino el registro: " << *registroNodo << endl;
					this->registrosNodo.erase(posicionAEliminar);
					this->cantRegistros--;

					// Actualizo el espacio libre en el nodo
					this->espacioLibre = this->espacioLibre + sizeof(unsigned int) + registro->getTamanio();

					if (registro != NULL){
						delete(registro);
					}
					unsigned int tamanioCompletoRegistroGenericoCabecera = sizeof(unsigned int) +  this->getTamanioRegistroCabeceraNodo();  // tamanio longitud registroGenerico + longitud datos registroGenerico
					unsigned int espacioUtilParaRegistros = this->tamanioTotalDisponibleParaRegistros - tamanioCompletoRegistroGenericoCabecera;
					unsigned int tamanioMinimoUnderflow = (unsigned int) ((espacioUtilParaRegistros * underflow) / 100);

					if((espacioUtilParaRegistros - this->espacioLibre) < tamanioMinimoUnderflow){
						return 2;
					} else {
						return 1;
					}
					cout<<*this<<endl;
				}else{
					//cout << "ERROR: NodoHoja::eliminarRegistroNodo -> no se elimino el registro: " << *registroNodo << endl;
					return 0;
				}
			}
		}

	}else{
		// Se quiso eliminar un registro que no existe
		//cout << "ADVERTENCIA: NodoHoja::eliminarRegistroNodo -> Se quiso eliminar un registro que no existe: " << *registroNodo << endl;
		return 3;
	}
	return 0;
}

bool NodoInterno::existeRegistro(RegistroNodo *registroNodo){
	if(!registrosNodo.empty()){
		for (std::list<RegistroNodo*>::iterator it = registrosNodo.begin(); it != registrosNodo.end(); it++){
			//RegistroNodo *reg = (*it);
			//cout<< *reg<<endl;
			short int comparacion = (*it)->comparar(registroNodo);
			if(comparacion == 0){
				return true;
			}else if(comparacion > 0){
				break; // El registro buscado no se encontro y ya estoy comparando con registros con clave mayor
			}
		}
	}
	return false;
}

Bloque* NodoInterno::crearBloque(){
	Bloque *bloqueNuevo = new Bloque();
	bloqueNuevo->setNroBloque(this->idNodo);
	bloqueNuevo->setTamanioBloque(this->tamanioBloque);
	bloqueNuevo->setTamanioEspacioLibre(this->espacioLibre);

	// Primero creo e inserto el registroCabeceraNodoInterno con los datos de la cabecera en la lista de registrosGenericos
	bloqueNuevo->push_back(crearRegistroCabeceraNodo());
	// Luego recorro todos los registroNodo y los trasformo e inserto a la lista de registrosGenericos
	for (std::list<RegistroNodo*>::iterator it = registrosNodo.begin(); it != registrosNodo.end(); it++){
		bloqueNuevo->push_back(crearRegistroGenerico(*it));
	}

	return bloqueNuevo;
}

void NodoInterno::vaciarRegistrosNodo(){
	if(this->cantRegistros > 0){
		while (!registrosNodo.empty()){
			RegistroNodo *reg = registrosNodo.front();
			registrosNodo.pop_front();
			delete(reg);
		}
		this->cantRegistros = 0;
	}
}

ClaveRegistroArbol* NodoInterno::getDatosClaveMedio(){

	//nodo en overflow, hay que buscar el elemento del medio
	//que hay que subir al padre

	//obtengo los registros del nodo y busco el del medio
	list<RegistroNodo*> registrosNodo = this->getRegistrosNodo();
	unsigned int cant=0;

	unsigned int tamanioCompletoRegistroGenericoCabecera = sizeof(unsigned int) +  getTamanioRegistroCabeceraNodo();
	unsigned int maximoEspacioLibre = tamanioTotalDisponibleParaRegistros - tamanioCompletoRegistroGenericoCabecera;
	unsigned int mitad = maximoEspacioLibre / 2;

	for (std::list<RegistroNodo*>::iterator it = registrosNodo.begin(); (it != registrosNodo.end()); it++ ){
		cant += sizeof(unsigned int) + (*it)->getTamanio(); // valor del tamanio del registro + tamanio del registro
		if ( cant > mitad ){
			return (*it)->getClave();
		}
	}
	return NULL;
}

unsigned int NodoInterno::buscarNodoHijo(ClaveRegistroArbol* clave){
	//obtengo los registros del nodo
	list<RegistroNodo*> registrosNodo = this->getRegistrosNodo();
	std::list<RegistroNodo*>::iterator it;
	unsigned int cant=0;
	//busco la clave en la lista de registros
	for (it = registrosNodo.begin(); (it != registrosNodo.end()); it++ ){
		if (((*it)->getClave())->comparar(clave)==1) {
			//la clave es menor a la clave del nodo entonces
			//devuelvo el nodo hijo que esta a la izquierda
			return ((RegistroNodoInterno*)(*it))->getIdNodoIzq();
		}
		cant++;
	}
	//la clave es mayor a la ultima clave del nodo
	if (cant == registrosNodo.size()){
		return ((RegistroNodoInterno*)(registrosNodo.back()))->getIdNodoDer();
	}else{ //if (cant==0)
		return 0;
	}
}

NodoInterno* NodoInterno::dividir(unsigned int idNodo){
	//le paso el id del nodo nuevo

	ClaveRegistroArbol* clave = this->getDatosClaveMedio();
	list<RegistroNodo*> registrosNodo = this->getRegistrosNodo();
	list<RegistroNodo*> registrosNodoNuevo;

	NodoInterno *nuevoNodo = new NodoInterno(this->getTipoRegistro(),idNodo,this->nivel,this->getTamanioBloque(), this->tamanioTotalDisponibleParaRegistros);
	RegistroNodo *reg;

	if(!this->registrosNodo.empty()){
		reg = this->registrosNodo.back();
		while (((reg->getClave())->comparar(clave)) > 0) {
			nuevoNodo->insertarRegistroNodo(reg);

			this->registrosNodo.pop_back();
			this->cantRegistros--;

			// Leo el nuevo ultimo
			reg = this->registrosNodo.back();
		}

		// Quito el registro con la clave del medio
		if(((reg->getClave())->comparar(clave)) == 0){
			// Limpio la referencia del registroNodo a la clave dado que se seguira usando para insertar en el nodo padre
			reg->setClave(NULL);
			this->registrosNodo.pop_back();
			delete(reg);
			this->cantRegistros--;
		}else{
			cout << "ERROR: NodoInterno::dividir no pudo quitar el registro con la clave del medio" << endl;
		}

		// Actualizo espacio libre y cantidad de registros
		this->cantRegistros = 0;
		unsigned int tamanioCompletoRegistroGenericoCabecera = sizeof(unsigned int) +  getTamanioRegistroCabeceraNodo();
		this->espacioLibre = this->tamanioTotalDisponibleParaRegistros - tamanioCompletoRegistroGenericoCabecera;

		for (std::list<RegistroNodo*>::iterator it = this->registrosNodo.begin(); it != this->registrosNodo.end(); it++){
			this->cantRegistros++;
			this->espacioLibre = this->espacioLibre - sizeof(unsigned int) - ((*it)->getTamanio()); // espacio libre - espacio del valor del tamanio registroNodo o registroGenerico - espacio de los datos del registroNodo
		}

	}

	return nuevoNodo;
}

string NodoInterno::getDatosImportantes(){
	/*std::stringstream datosImportantes;
	datosImportantes << "( NodoInterno:";
	datosImportantes << "\n idNodo: " << this->idNodo << ", nivel: " << this->nivel << ", tamanioBloque: " << this->tamanioBloque << ", tamanioTotalDisponible: " << this->tamanioTotalDisponibleParaRegistros << ", espacioLibre: " << this->espacioLibre << ", cantRegistros: " << this->cantRegistros << ", idNodoIzq: " << this->idNodoIzq ;

	unsigned int pos = 0;
	for (std::list<RegistroNodo*>::iterator it = registrosNodo.begin(); it != registrosNodo.end(); it++){
		datosImportantes << "\n " << pos << " = " << *(*it);
		pos++;
	}

	datosImportantes << "\n)";

    return  datosImportantes.str();

*/
    return "";
}

unsigned int NodoInterno::recuperarNivel(RegistroGenericoArbol *registro){
	char* datoLectura = registro->getDato();
	unsigned int pos = 0;
	unsigned int nivel = this->recuperarEntero(datoLectura, pos);

	return nivel;
}


unsigned int NodoInterno::leerNodoHno(NodoInterno *nodo,TipoNodoHno &tipoHno){
	unsigned int idNodoHno = 0;
	RegistroNodoInterno* reg;

	for (std::list<RegistroNodo*>::iterator it = nodo->registrosNodo.begin(); it != nodo->registrosNodo.end(); it++){
		reg=(RegistroNodoInterno*)(*it);
		if (reg->getIdNodoIzq() == this->getIdNodo() ) {
			//encontre el nro de nodo y tiene hno derecho entonces lo devuelvo
			idNodoHno = (((RegistroNodoInterno*)(*it))->getIdNodoDer());
			tipoHno = DERECHO;
			break;
		}
	}

	//si no me encontre como nodo izq es porque soy el nodo derecho del ultimo registro
	if (idNodoHno == 0) {
		RegistroNodoInterno *reg =(RegistroNodoInterno*)(nodo->getRegistrosNodo().back());

		//si el ultimo nodo derecho no coincide con el id de nodo que busco devuelve un error = 0
		if (reg->getIdNodoDer() != this->getIdNodo()) {
			idNodoHno = 0;
		}
		else {
			tipoHno = IZQUIERDO;
			idNodoHno = reg->getIdNodoIzq();
		}
	}
	return idNodoHno;
}

bool NodoInterno::tieneCargaMinima(TipoNodoHno tipoHno){

	unsigned int tamanioCompletoRegistroGenericoCabecera = sizeof(unsigned int) +  getTamanioRegistroCabeceraNodo();  // tamanio longitud registroGenerico + longitud datos registroGenerico
	unsigned int espacioUtilParaRegistros = this->tamanioTotalDisponibleParaRegistros - tamanioCompletoRegistroGenericoCabecera;
	unsigned int tamanioMinimoUnderflow = (unsigned int) ((espacioUtilParaRegistros * underflow) / 100);

	RegistroNodo* reg = NULL;
	if (tipoHno==DERECHO){
		//traigo el primer registro del nodo
		reg = this->getRegistrosNodo().front();
	} else if (tipoHno==IZQUIERDO){
		//traigo el ultimo registro del nodo
		reg = this->getRegistrosNodo().back();
	}
	unsigned int nuevoEspacioLibre = this->espacioLibre + sizeof(unsigned int) + reg->getTamanio();
	if((espacioUtilParaRegistros - nuevoEspacioLibre) < tamanioMinimoUnderflow){
		// Estoy en underflow;
		return true;
	}else{
		return false;
	}
}

ClaveRegistroArbol * NodoInterno::obtenerClaveNodo(unsigned int idNodoIzq,unsigned int idNodoDer){

	for (std::list<RegistroNodo*>::iterator it = this->getRegistrosNodo().begin(); it != this->getRegistrosNodo().end(); it++){
		if (((RegistroNodoInterno*)(*it))->getIdNodoIzq()==idNodoIzq && ((RegistroNodoInterno*)(*it))->getIdNodoDer()==idNodoDer) {
			//encontre el registro que esta entre los nodos izq y derecho
			ClaveRegistroArbol *clave  = ((RegistroNodoInterno*)(*it))->getClave();
			return clave;
		}
	}
	return NULL;
}

RegistroNodo* NodoInterno::obtenerRegistroClaveNodo(unsigned int idNodoIzq,unsigned int idNodoDer){

	for (std::list<RegistroNodo*>::iterator it = this->registrosNodo.begin(); it != this->registrosNodo.end(); it++){
		if (((RegistroNodoInterno*)(*it))->getIdNodoIzq()==idNodoIzq && ((RegistroNodoInterno*)(*it))->getIdNodoDer()==idNodoDer) {
			//encontre el registro que esta entre los nodos izq y derecho
			return ((RegistroNodoInterno*)(*it));
		}
	}
	return NULL;
}

void NodoInterno::unir(NodoInterno * nodoHno,ClaveRegistroArbol *claveMedio,unsigned int &idNodo,TipoNodoHno tipoHno){

	if (tipoHno == IZQUIERDO){
		//saco el ultimo registro de la cola del nodoHno
		RegistroNodoInterno *regNodoHno = (RegistroNodoInterno*) nodoHno->getRegistrosNodo().back();
		//saco el primer registro de la cola del hermano derecho
		//nuevo registro
		RegistroNodoInterno *regNuevo = new RegistroNodoInterno(claveMedio);

		//seteo los nuevos id de nodo izq y derecho para el registro nuevo
		regNuevo->setIdNodoIzq(regNodoHno->getIdNodoDer());

		if (this->registrosNodo.empty()){
			regNuevo->setIdNodoDer(idNodo);
		} else {
			RegistroNodoInterno *regNodoHijo =(RegistroNodoInterno*) this->registrosNodo.front();
			regNuevo->setIdNodoDer(regNodoHijo->getIdNodoIzq());
		}
		//inserto la clave del medio
		nodoHno->registrosNodo.push_back(regNuevo);
		if((nodoHno->getEspacioLibre() - sizeof(unsigned int) - regNuevo->getTamanio()) < 0){
			cout << "ERROR: NodoInterno::unir -> El espacio libre dio menor a cero" << endl;
		}
		unsigned int nuevoEspacioLibre = nodoHno->getEspacioLibre() - sizeof(unsigned int) - regNuevo->getTamanio();
		nodoHno->setEspacioLibre(nuevoEspacioLibre);
		nodoHno->cantRegistros++;
		//le asigno al objeto todos los registros del nodoHijo
		RegistroNodo* reg;
		while (this->cantRegistros>0){
			reg = this->registrosNodo.front();
			nodoHno->registrosNodo.push_back(reg);
			this->registrosNodo.pop_front();
			if((nodoHno->getEspacioLibre() - sizeof(unsigned int) - reg->getTamanio()) < 0){
				cout << "ERROR: NodoInterno::unir -> El espacio libre dio menor a cero" << endl;
			}
			unsigned int nuevoEspacioLibreHno = nodoHno->getEspacioLibre() - sizeof(unsigned int) - reg->getTamanio();
			nodoHno->setEspacioLibre(nuevoEspacioLibreHno);
			nodoHno->cantRegistros++;

			unsigned int nuevoEspacioLibre = this->getEspacioLibre() - sizeof(unsigned int) - reg->getTamanio();
			this->setEspacioLibre(nuevoEspacioLibre);
			this->cantRegistros--;

		}
		idNodo=nodoHno->getIdNodo();
	} else if (tipoHno == DERECHO){
		//saco el primer registro de la cola del nodoHno
		RegistroNodoInterno *regNodoHno = (RegistroNodoInterno*) nodoHno->registrosNodo.front();
		//saco el primer registro de la cola del hermano derecho
		//nuevo registro
		//cout<<*this<<endl;
		//cout<<*regNodoHno<<endl;
		RegistroNodoInterno *regNuevo = new RegistroNodoInterno(claveMedio);

		//seteo los nuevos id de nodo izq y derecho para el registro nuevo
		regNuevo->setIdNodoDer(regNodoHno->getIdNodoIzq());

		if (this->registrosNodo.empty()){
			regNuevo->setIdNodoIzq(idNodo);
		} else {
			RegistroNodoInterno *regNodoHijo =(RegistroNodoInterno*) this->registrosNodo.back();
			regNuevo->setIdNodoIzq(regNodoHijo->getIdNodoDer());
		}
		//inserto la clave del medio
		nodoHno->registrosNodo.push_front(regNuevo);
		if((nodoHno->getEspacioLibre() - sizeof(unsigned int) - regNuevo->getTamanio()) < 0){
			cout << "ERROR: NodoInterno::unir -> El espacio libre dio menor a cero" << endl;
		}
		unsigned int nuevoEspacioLibre = nodoHno->getEspacioLibre() - sizeof(unsigned int) - regNuevo->getTamanio();
		nodoHno->setEspacioLibre(nuevoEspacioLibre);
		nodoHno->cantRegistros++;
		//le asigno al objeto todos los registros del nodoHno
		RegistroNodo* reg;
		while (this->cantRegistros>0){
			reg = this->registrosNodo.back();
			nodoHno->registrosNodo.push_front(reg);
			this->registrosNodo.pop_back();
			if((nodoHno->getEspacioLibre() - sizeof(unsigned int) - reg->getTamanio()) < 0){
				cout << "ERROR: NodoInterno::unir -> El espacio libre dio menor a cero" << endl;
			}
			unsigned int nuevoEspacioLibreHno = nodoHno->getEspacioLibre() - sizeof(unsigned int) - reg->getTamanio();
			nodoHno->setEspacioLibre(nuevoEspacioLibreHno);
			nodoHno->cantRegistros++;
			unsigned int nuevoEspacioLibre = this->getEspacioLibre() - sizeof(unsigned int) - reg->getTamanio();
			this->setEspacioLibre(nuevoEspacioLibre);
			this->cantRegistros--;
		}
 		idNodo=nodoHno->getIdNodo();
	}

}

void NodoInterno::balancear(NodoInterno * nodoHno,NodoInterno * nodoPadre,TipoNodoHno tipoHno,TipoRegistro tipoReg){
	//cout << "INFO: NodoInterno::balancear" << endl;
	if (tipoHno == DERECHO) {
		//bajo la clave del padre
		RegistroNodoInterno* regMedio =(RegistroNodoInterno*)nodoPadre->obtenerRegistroClaveNodo(this->getIdNodo(),nodoHno->getIdNodo());

		char* claveSerializada = regMedio->getClave()->serializar();
		ClaveRegistroArbol *claveClonada = FabricaClaveRegistro::crearClaveRegistro(this->tipoRegistro, claveSerializada);
		delete[] claveSerializada; // Liberamos la memoria ya que no se usara mas.

		RegistroNodoInterno *regNuevo = new RegistroNodoInterno(claveClonada);

		//leo el primer registro del nodoHno
		RegistroNodoInterno * regHnoASacar =(RegistroNodoInterno*)(nodoHno->registrosNodo).front();
		// Actualizo el espacio libre
		nodoHno->registrosNodo.pop_front();
		unsigned int nuevoEspacioLibreHno = nodoHno->getEspacioLibre() + sizeof(unsigned int) + regHnoASacar->getTamanio();
		nodoHno->setEspacioLibre(nuevoEspacioLibreHno);
		nodoHno->cantRegistros--;

		//inserto el elemento d en mi lista de registros
		RegistroNodoInterno* regNodoHijo = (RegistroNodoInterno*) (this->registrosNodo).back();

		//seteo los nuevos id de nodo izq y derecho para el registro nuevo
		regNuevo->setIdNodoDer(regHnoASacar->getIdNodoIzq());
		regNuevo->setIdNodoIzq(regNodoHijo->getIdNodoDer());

		this->registrosNodo.push_back(regNuevo);
		unsigned int nuevoEspacioLibreNuevo = this->getEspacioLibre() - sizeof(unsigned int) - regNuevo->getTamanio();
		this->setEspacioLibre(nuevoEspacioLibreNuevo);
		this->cantRegistros++;


     	//Elimino la clave en el padre e inserto la nueva clave que le sigue a la que elimine
		unsigned int idNodo = 0; //No se va a usar si es que todo anda bien.
		cout<<*this<<endl;
		cout<<*nodoHno<<endl;
		cout<<*nodoPadre<<endl;
		if(nodoPadre->eliminarRegistroNodo(regMedio, idNodo) != 1){
			cout << "ERROR: NodoInterno::balancear der -> fallo nodoPadre->eliminarClaveArbol" << endl;
		}
		if(nodoPadre->insertarClaveArbol(regHnoASacar->getClave(), tipoReg, this->getIdNodo(), nodoHno->getIdNodo()) != 1){
			cout << "ERROR: NodoHoja::balancear der -> fallo nodoPadre->insertarClaveArbol" << endl;
		}

		//Libero la memoria del registro sacado
		delete(regHnoASacar);
	}else if (tipoHno == IZQUIERDO) {
		//bajo la clave del padre
		RegistroNodoInterno* regMedio =(RegistroNodoInterno*)nodoPadre->obtenerRegistroClaveNodo(nodoHno->getIdNodo(),this->getIdNodo());

		char* claveSerializada = regMedio->getClave()->serializar();
		ClaveRegistroArbol *claveClonada = FabricaClaveRegistro::crearClaveRegistro(this->tipoRegistro, claveSerializada);
		delete[] claveSerializada; // Liberamos la memoria ya que no se usara mas.

		RegistroNodoInterno *regNuevo = new RegistroNodoInterno(claveClonada);

		//leo el ultimo registro del nodoHno
		RegistroNodoInterno * regHnoASacar =(RegistroNodoInterno*)(nodoHno->registrosNodo).back();
		// Actualizo el espacio libre
		nodoHno->registrosNodo.pop_back();
		unsigned int nuevoEspacioLibreHno = nodoHno->getEspacioLibre() + sizeof(unsigned int) + regHnoASacar->getTamanio();
		nodoHno->setEspacioLibre(nuevoEspacioLibreHno);
		nodoHno->cantRegistros--;
		//inserto el elemento d en mi lista de registros
		RegistroNodoInterno* regNodoHijo = (RegistroNodoInterno*) (this->registrosNodo).front();

		//seteo los nuevos id de nodo izq y derecho para el registro nuevo
		regNuevo->setIdNodoIzq(regHnoASacar->getIdNodoDer());
		regNuevo->setIdNodoDer(regNodoHijo->getIdNodoIzq());

		this->registrosNodo.push_front(regNuevo);
		unsigned int nuevoEspacioLibreNuevo = this->getEspacioLibre() - sizeof(unsigned int) - regNuevo->getTamanio();
		this->setEspacioLibre(nuevoEspacioLibreNuevo);
		this->cantRegistros++;


		//Elimino la clave en el padre e inserto la nueva clave que le sigue a la que elimine
		unsigned int idNodo = 0; //No se va a usar si es que todo anda bien.
		if(nodoPadre->eliminarRegistroNodo(regMedio, idNodo) != 1){
			cout << "ERROR: NodoIntenro::balancear der 2 -> fallo nodoPadre->eliminarClaveArbol" << endl;
		}
		if(nodoPadre->insertarClaveArbol(regHnoASacar->getClave(), tipoReg, nodoHno->getIdNodo(), this->getIdNodo()) != 1){
			cout << "ERROR: NodoHoja::balancear der -> fallo nodoPadre->insertarClaveArbol" << endl;
		}
		//Libero la memoria del registro sacado
		delete(regHnoASacar);
	}

}

bool NodoInterno::permiteCambiarClave(NodoInterno *nodoHno, NodoInterno *nodoHijo, TipoNodoHno tipoHno){
	if(tipoHno == DERECHO){
		int posicion = 1;
		RegistroNodo *registroASubir = NULL;
		for (std::list<RegistroNodo*>::iterator it = nodoHno->registrosNodo.begin(); it != nodoHno->registrosNodo.end(); it++){
			if(posicion == 2){
				registroASubir = (*it);
				break;
			}
			posicion++;
		}
		RegistroNodoInterno *claveABajar = (RegistroNodoInterno*) this->obtenerRegistroClaveNodo(nodoHijo->getIdNodo(), nodoHno->getIdNodo());
		ClaveRegistroArbol *claveASubir = registroASubir->getClave();
		int nuevoEspacioLibre = this->espacioLibre + (claveABajar->getClave())->getTamanio() - claveASubir->getTamanio();
		if(nuevoEspacioLibre < 0){
			return false; // El nodo quedaria en overflow entonces no permite cambiar la clave
		}else{
			return true;
		}
	}else if(tipoHno == IZQUIERDO){
		RegistroNodo *registroASubir = nodoHno->registrosNodo.back();

		RegistroNodoInterno *claveABajar = (RegistroNodoInterno *)this->obtenerRegistroClaveNodo(nodoHno->getIdNodo(), nodoHijo->getIdNodo());
		ClaveRegistroArbol *claveASubir = registroASubir->getClave();
		int nuevoEspacioLibre = this->espacioLibre + (claveABajar->getClave())->getTamanio() - claveASubir->getTamanio();
		if(nuevoEspacioLibre < 0){
			return false; // El nodo quedaria en overflow entonces no permite cambiar la clave
		}else{
			return true;
		}
	}else{
		return false;
	}
}


bool NodoInterno::permiteCambiarClaveInterno(NodoInterno *nodoHno, NodoInterno *nodoHijo, TipoNodoHno tipoHno){
	if(tipoHno == DERECHO){
		RegistroNodo *registroASubir = nodoHno->registrosNodo.front();

		RegistroNodoInterno *regClaveABajar = (RegistroNodoInterno*) this->obtenerRegistroClaveNodo(nodoHijo->getIdNodo(), nodoHno->getIdNodo());
		ClaveRegistroArbol *claveASubir = registroASubir->getClave();
		int nuevoEspacioLibre = this->espacioLibre + (regClaveABajar->getClave())->getTamanio() - claveASubir->getTamanio();
		if(nuevoEspacioLibre < 0){
			return false; // El nodo quedaria en overflow entonces no permite cambiar la clave
		}else{
			return true;
		}
	}else if(tipoHno == IZQUIERDO){
		RegistroNodo *registroASubir = nodoHno->registrosNodo.back();

		RegistroNodoInterno *regClaveABajar = (RegistroNodoInterno *)this->obtenerRegistroClaveNodo(nodoHno->getIdNodo(), nodoHijo->getIdNodo());
		ClaveRegistroArbol *claveASubir = registroASubir->getClave();
		int nuevoEspacioLibre = this->espacioLibre + (regClaveABajar->getClave())->getTamanio() - claveASubir->getTamanio();
		if(nuevoEspacioLibre < 0){
			return false; // El nodo quedaria en overflow entonces no permite cambiar la clave
		}else{
			return true;
		}
	}else{
		return false;
	}
}
