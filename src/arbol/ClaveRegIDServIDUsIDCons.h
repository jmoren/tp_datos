#ifndef CLAVEREGIDSERVIDUSIDCONS_H_
#define CLAVEREGIDSERVIDUSIDCONS_H_

#include "ClaveRegistroArbol.h"
#include "../utils/Funciones.h"

class ClaveRegIDServIDUsIDCons: public ClaveRegistroArbol {
private:
	unsigned long idServicio;
	unsigned long idUsuario;
	unsigned long idConsulta;

public:
	ClaveRegIDServIDUsIDCons();
	virtual ~ClaveRegIDServIDUsIDCons();
	ClaveRegIDServIDUsIDCons(unsigned long idServicio,unsigned long idUsuario,unsigned long idConsulta);

	unsigned long getIdConsulta();
	void setIdConsulta(unsigned long idConsulta);

	unsigned long getIdUsuario();
	void setIdUsuario(unsigned long idUsuario);

	unsigned long getIdServicio();
	void setIdServicio(unsigned long idServicio);

	short int comparar(ClaveRegistroArbol* claveParaComparar);
	unsigned int getTamanio();

	char* serializar();
	void deserializar(char*);

	string getDatosImportantes();

};

#endif /* CLAVEREGIDSERVIDUSIDCONS_H_ */
