/*
 * FabricaClaveRegistro.h
 *
 *  Created on: 06/10/2013
 *      Author: Natalia
 */

#ifndef FABRICACLAVEREGISTRO_H_
#define FABRICACLAVEREGISTRO_H_

#include "../utils/Constantes.h"
#include <cstdio>
#include "ClaveRegistroArbol.h"
#include "ClaveRegTipoUsuarioDNI.h"
#include "ClaveRegProvinciaDNI.h"
#include "ClaveRegIDServIDUsPC.h"
#include "ClaveRegIDServIDUsIDCons.h"
#include "ClaveRegIDServFHPCotizacion.h"
#include "ClaveRegIDServFCHCIDCons.h"
#include "ClaveRegCatIDServ.h"
#include "ClaveRegIDUsIDServ.h"


class FabricaClaveRegistro {
public:
	FabricaClaveRegistro();
	virtual ~FabricaClaveRegistro();

	static ClaveRegistroArbol* crearClaveRegistro(TipoRegistro tipoRegistro, char *dato);
	static bool validarTipoClaveRegistro(TipoRegistro tipoRegistro, ClaveRegistroArbol *clave);
};

#endif /* FABRICACLAVEREGISTRO_H_ */
