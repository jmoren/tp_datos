/*
 * ClaveRegIDServIDUsIDCons.cpp
 *
 *  Created on: 08/10/2013
 *      Author: natalia
 */

#include "ClaveRegIDServIDUsIDCons.h"

ClaveRegIDServIDUsIDCons::ClaveRegIDServIDUsIDCons() {
	this->idServicio = 0;
	this->idUsuario = 0;
	this->idConsulta = 0;
}

ClaveRegIDServIDUsIDCons::ClaveRegIDServIDUsIDCons(unsigned long idServicio,unsigned long idUsuario, unsigned long idConsulta) {
	this->idServicio = idServicio;
	this->idUsuario = idUsuario;
	this->idConsulta = idConsulta;
}

unsigned long ClaveRegIDServIDUsIDCons::getIdConsulta() {
	return idConsulta;
}

void ClaveRegIDServIDUsIDCons::setIdConsulta(unsigned long idConsulta) {
	this->idConsulta = idConsulta;
}

unsigned long ClaveRegIDServIDUsIDCons::getIdUsuario() {
	return idUsuario;
}

void ClaveRegIDServIDUsIDCons::setIdUsuario(unsigned long idUsuario) {
	this->idUsuario = idUsuario;
}

unsigned long ClaveRegIDServIDUsIDCons::getIdServicio() {
	return idServicio;
}

void ClaveRegIDServIDUsIDCons::setIdServicio(unsigned long idServicio) {
	this->idServicio = idServicio;
}

ClaveRegIDServIDUsIDCons::~ClaveRegIDServIDUsIDCons() {}

//devuelve 0 si son iguales, 1 si es mayor y -1 si es menor
short int ClaveRegIDServIDUsIDCons::comparar(ClaveRegistroArbol* claveParaComparar){

	if((claveParaComparar != NULL) ){
		if (this->idServicio == ((ClaveRegIDServIDUsIDCons*)claveParaComparar)->idServicio){
			if (this->idUsuario == ((ClaveRegIDServIDUsIDCons*)claveParaComparar)->idUsuario) {
				if (this->idConsulta == ((ClaveRegIDServIDUsIDCons*)claveParaComparar)->idConsulta) {
					return 0;
				} else if (this->idConsulta > ((ClaveRegIDServIDUsIDCons*)claveParaComparar)->idConsulta) {
					return 1;
				} else {
					return (-1);
				}
			} else if (this->idUsuario > ((ClaveRegIDServIDUsIDCons*)claveParaComparar)->idUsuario) {
				return 1;
			} else {
				return (-1);
			}
		} else if (this->idServicio > ((ClaveRegIDServIDUsIDCons*)claveParaComparar)->idServicio) {
			return 1;
		} else {
			return -1;
		}
	}else{
		return 1; // Si la clave con la que comparo es NULL entonces siempre esta clave sera mayor a NULL
	}
}


unsigned int ClaveRegIDServIDUsIDCons::getTamanio(){

	return (unsigned int)(sizeof(unsigned long)*3);

}

char* ClaveRegIDServIDUsIDCons::serializar(){

	char* cadenaEscritura = new char[getTamanio()];
	unsigned int pos = 0;
	this->almacenarEnteroLargo(cadenaEscritura, pos, getIdServicio());
	this->almacenarEnteroLargo(cadenaEscritura, pos, getIdUsuario());
	this->almacenarEnteroLargo(cadenaEscritura, pos, getIdConsulta());
	return cadenaEscritura;
}

void ClaveRegIDServIDUsIDCons::deserializar(char* cadena){

	unsigned int pos = 0;
	this->idServicio = recuperarEnteroLargo(cadena,pos);
	this->idUsuario  = recuperarEnteroLargo(cadena,pos);
	this->idConsulta = recuperarEnteroLargo(cadena,pos);

}

string ClaveRegIDServIDUsIDCons::getDatosImportantes(){
	std::stringstream datosImportantes;
	datosImportantes << "ClaveRegIDServIDUsIDCons: "<< "Tamanio: " << getTamanio() << " (" << getIdServicio() << ", "<< getIdUsuario() <<"," << getIdConsulta() << ")";

    return  datosImportantes.str();
}
