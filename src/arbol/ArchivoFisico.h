/*
 * ArchivoFisico.h
 *
 *  Created on: 06/10/2013
 *      Author: Natalia
 */

#ifndef ARCHIVOFISICO_H_
#define ARCHIVOFISICO_H_

#include "NodoInterno.h"
#include "NodoHoja.h"
#include "Archivo.h"

class ArchivoFisico: public Archivo {

private:
	// Redefino nuevamente
	FileBlock *fileBlock;

public:
	ArchivoFisico(); // No usar
	ArchivoFisico(TipoRegistro tipoRegistro, unsigned int tamanioBloque);
	virtual ~ArchivoFisico();

	const char* getPathFromTipoRegistro(TipoRegistro tipoRegistro);
	virtual bool existeArchivoFisico();
	virtual void crearArchivo();

	virtual unsigned int getTamanioBloque();
	virtual unsigned int getTamanioDatosDeControlBloque();
	virtual unsigned int getTamanioDatosDeControlRegistro();
	virtual unsigned int getCantidadTotalBloques();
	virtual unsigned int getIdBloqueLibre();
	virtual bool existenBloquesLibres();

	virtual Bloque* getBloque(unsigned int nroBloque);
	virtual Bloque* getRaiz();

	virtual void borrarBloque(unsigned int nroBloque);
	virtual void grabarBloque(Bloque* bloque);

	void imprimir();

};

#endif /* ARCHIVOFISICO_H_ */
