/*
 * Ejemplo2.cpp
 *
 *  Created on: 06/09/2013
 *      Author: natalia
 */

//============================================================================
// Name        : Ejemplo.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <sstream>
#include <typeinfo>
#include "ArchivoMock.h"
#include "ArbolTest.h"
#include "ClaveRegistroArbol.h"
#include "ClaveRegTipoUsuarioDNI.h"
#include "ClaveRegProvinciaDNI.h"
#include "ClaveRegIDServIDUsPC.h"
#include "ClaveRegIDServIDUsIDCons.h"
#include "ClaveRegIDServFHPCotizacion.h"
#include "ClaveRegIDServFCHCIDCons.h"
#include "ClaveRegCatIDServ.h"
#include <getopt.h>

using namespace std;

int main(int argc, char **argv) {
    cout << "!!!Inicio Programa!!!" << endl;

	//-------------------------------------------------------------------------------
	cout << "--------------------------------------------------------------" << endl;
	cout << "Test Arbol" << endl;
	cout << "--------------------------------------------------------------" << endl;

	cout << "Crear Arbol --------------------------------------------------" << endl;
	Arbol *arbol = ArbolTest::creacion(TipoUsuarioDNI);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();

	cout << "Carga inicial ------------------------------------------------" << endl;
	ArbolTest::creacionTipoUsuarioDNI(arbol);


	//ArbolTest::listar(arbol);
	cout << "Imprimir archivo ---------------------------------------------" << endl;
	arbol->getArchivo()->imprimir();
	cout << "--------------------------------------------------------------" << endl;
	cout << "--------------------------------------------------------------" << endl;
	cout << "--------------------------------------------------------------" << endl;
	cout << "--------------------------------------------------------------" << endl;

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();
	//cout << "--------------------------------------------------------------" << endl;
//	ArbolTest::buscarCategoriaServicio(arbol);
	ArbolTest::buscar2(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	cout << "Delete Registros ---------------------------------------------" << endl;
	ArbolTest::deleteRegistrosTest(arbol);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();
	//cout << "--------------------------------------------------------------" << endl;

	ArbolTest::buscar2(arbol);

	if(arbol != NULL){
		delete(arbol);
	}







/*
    cout << "Ingrese: c, i o ? para continuar" << endl;

    string temp;
    int option;
    bool cancel = false;

	while(!cancel){
		cout << "\033[1;34m Ingrese una opcion: \n\033[0m 1) Continuar 2) Sorpresa 0) Salir " << endl;
		cout << " Opcion: " ;
		getline(cin, temp);
		option = atoi(temp.c_str());
		switch(option){
		  case 0:
			cancel = true;	//---------------------------------------------
	cout << "--------------------------------------------------------------" << endl;
	cout << "Test Arbol" << endl;
	cout << "--------------------------------------------------------------" << endl;

	cout << "Crear Arbol --------------------------------------------------" << endl;
	Arbol *arbol = ArbolTest::creacion(TipoUsuarioDNI);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();

	//cout << "Crear Arbol --------------------------------------------------" << endl;
	//Arbol *arbol = ArbolTest::creacion(NombreCategoriaIDServicio);
	//cout << "Imprimir archivo ---------------------------------------------" << endl;

	cout << "Carga inicial ------------------------------------------------" << endl;
	ArbolTest::creacionTipoUsuarioDNI(arbol);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();
	//cout << "--------------------------------------------------------------" << endl;
//	ArbolTest::buscarCategoriaServicio(arbol);
//	ArbolTest::buscar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	ArbolTest::listar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	cout << "Delete Registros ---------------------------------------------" << endl;
//	ArbolTest::deleteRegistrosTest(arbol);

	cout << "Imprimir archivo ---------------------------------------------" << endl;
	arbol->getArchivo()->imprimir();
	cout << "--------------------------------------------------------------" << endl;

	if(arbol != NULL){
		delete(arbol);
	}
			cout << "\n Adios!" << endl << endl;
			return 0;
			break;
		  case 1:
			cancel = true;
			cout << "\n Prosiga!" << endl << endl;
			break;
		  case 2:
			cancel = true;
			cout << "\n Prosiga con misterio!" << endl << endl;
			break;
		  default:
			cout << "\033[0;31m Opcion invalida \033[0m" << endl;
		}
	}
*/

/*
    Archivo *arch1 = new Archivo();
    cout << "arch1->existeArchivoFisico() = " << arch1->existeArchivoFisico() << endl;

    ArchivoMock *arch2 = new ArchivoMock();
    cout << "arch2->existeArchivoFisico() = " << arch2->existeArchivoFisico() << endl;

    Archivo *arch3 = new ArchivoMock();
    cout << "arch3->existeArchivoFisico() = " << arch3->existeArchivoFisico() << endl;


    // Mapa -----------------------------------------------------------------------------------
     std::map<char,int> mymap;
     std::map<char,int>::iterator it;

     mymap['a']=50;
     mymap['b']=100;
     mymap['c']=150;
     mymap['d']=200;

     std::cout << "elements in mymap 1:" << '\n';
	 for( map<char, int>::iterator it=mymap.begin(); it!=mymap.end(); ++it){
		 cout << (*it).first << ": " << (*it).second << endl;
	 }


     it=mymap.find('b');
     mymap.erase (it);	//---------------------------------------------
	cout << "--------------------------------------------------------------" << endl;
	cout << "Test Arbol" << endl;
	cout << "--------------------------------------------------------------" << endl;

	cout << "Crear Arbol --------------------------------------------------" << endl;
	Arbol *arbol = ArbolTest::creacion(TipoUsuarioDNI);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();

	//cout << "Crear Arbol --------------------------------------------------" << endl;
	//Arbol *arbol = ArbolTest::creacion(NombreCategoriaIDServicio);
	//cout << "Imprimir archivo ---------------------------------------------" << endl;

	cout << "Carga inicial ------------------------------------------------" << endl;
	ArbolTest::creacionTipoUsuarioDNI(arbol);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();	//---------------------------------------------
	cout << "--------------------------------------------------------------" << endl;
	cout << "Test Arbol" << endl;
	cout << "--------------------------------------------------------------" << endl;

	cout << "Crear Arbol --------------------------------------------------" << endl;
	Arbol *arbol = ArbolTest::creacion(TipoUsuarioDNI);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();

	//cout << "Crear Arbol --------------------------------------------------" << endl;
	//Arbol *arbol = ArbolTest::creacion(NombreCategoriaIDServicio);
	//cout << "Imprimir archivo ---------------------------------------------" << endl;

	cout << "Carga inicial ------------------------------------------------" << endl;
	ArbolTest::creacionTipoUsuarioDNI(arbol);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();
	//cout << "--------------------------------------------------------------" << endl;
//	ArbolTest::buscarCategoriaServicio(arbol);
//	ArbolTest::buscar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	ArbolTest::listar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	cout << "Delete Registros ---------------------------------------------" << endl;
//	ArbolTest::deleteRegistrosTest(arbol);

	cout << "Imprimir archivo ---------------------------------------------" << endl;
	arbol->getArchivo()->imprimir();
	cout << "--------------------------------------------------------------" << endl;

	if(arbol != NULL){
		delete(arbol);
	}
	//cout << "--------------------------------------------------------------" << endl;
//	ArbolTest::buscarCategoriaServicio(arbol);	//---------------------------------------------
	cout << "--------------------------------------------------------------" << endl;
	cout << "Test Arbol" << endl;
	cout << "--------------------------------------------------------------" << endl;

	cout << "Crear Arbol --------------------------------------------------" << endl;
	Arbol *arbol = ArbolTest::creacion(TipoUsuarioDNI);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();
	//---------------------------------------------
	cout << "--------------------------------------------------------------" << endl;
	cout << "Test Arbol" << endl;
	cout << "--------------------------------------------------------------" << endl;

	cout << "Crear Arbol --------------------------------------------------" << endl;
	Arbol *arbol = ArbolTest::creacion(TipoUsuarioDNI);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();

	//cout << "Crear Arbol --------------------------------------------------" << endl;
	//Arbol *arbol = ArbolTest::creacion(NombreCategoriaIDServicio);
	//cout << "Imprimir archivo ---------------------------------------------" << endl;

	cout << "Carga inicial ------------------------------------------------" << endl;
	ArbolTest::creacionTipoUsuarioDNI(arbol);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();
	//cout << "--------------------------------------------------------------" << endl;
//	ArbolTest::buscarCategoriaServicio(arbol);
//	ArbolTest::buscar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	ArbolTest::listar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	cout << "Delete Registros ---------------------------------------------" << endl;
//	ArbolTest::deleteRegistrosTest(arbol);

	cout << "Imprimir archivo ---------------------------------------------" << endl;
	arbol->getArchivo()->imprimir();
	cout << "--------------------------------------------------------------" << endl;

	if(arbol != NULL){
		delete(arbol);
	}
	//cout << "Crear Arbol --------------------------------------------------" << endl;
	//Arbol *arbol = ArbolTest::creacion(NombreCategoriaIDServicio);
	//cout << "Imprimir archivo ---------------------------------------------" << endl;

	cout << "Carga inicial ------------------------------------------------" << endl;
	ArbolTest::creacionTipoUsuarioDNI(arbol);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();
	//cout << "--------------------------------------------------------------" << endl;
//	ArbolTest::buscarCategoriaServicio(arbol);
//	ArbolTest::buscar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	ArbolTest::listar(arbol);
//	cout << "-------------------	//---------------------------------------------
	cout << "--------------------------------------------------------------" << endl;
	cout << "Test Arbol" << endl;
	cout << "--------------------------------------------------------------" << endl;

	cout << "Crear Arbol --------------------------------------------------" << endl;
	Arbol *arbol = ArbolTest::creacion(TipoUsuarioDNI);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();

	//cout << "Crear Arbol --------------------------------------------------" << endl;
	//Arbol *arbol = ArbolTest::creacion(NombreCategoriaIDServicio);
	//cout << "Imprimir archivo ---------------------------------------------" << endl;

	cout << "Carga inicial ------------------------------------------------" << endl;
	ArbolTest::creacionTipoUsuarioDNI(arbol);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();
	//cout << "--------------------------------------------------------------" << endl;
//	ArbolTest::buscarCategoriaServicio(arbol);
//	ArbolTest::buscar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	ArbolTest::listar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	cout << "Delete Registros ---------------------------------------------" << endl;
//	ArbolTest::deleteRegistrosTest(arbol);

	cout << "Imprimir archivo ---------------------------------------------" << endl;
	arbol->getArchivo()->imprimir();
	cout << "--------------------------------------------------------------" << endl;

	if(arbol != NULL){
		delete(arbol);
	}-------------------------------------------" << endl;

//	cout << "Delete Registros ---------------------------------------------" << endl;
//	ArbolTest::deleteRegistrosTest(arbol);

	cout << "Imprimir archivo ---------------------------------------------" << endl;
	arbol->getArchivo()->imprimir();
	cout << "--------------------------------------------------------------" << endl;
	//---------------------------------------------
	cout << "--------------------------------------------------------------" << endl;
	cout << "Test Arbol" << endl;
	cout << "--------------------------------------------------------------" << endl;

	cout << "Crear Arbol --------------------------------------------------" << endl;
	Arbol *arbol = ArbolTest::creacion(TipoUsuarioDNI);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();

	//cout << "Crear Arbol --------------------------------------------------" << endl;
	//Arbol *arbol = ArbolTest::creacion(NombreCategoriaIDServicio);
	//cout << "Imprimir archivo ---------------------------------------------" << endl;

	cout << "Carga inicial ------------------------------------------------" << endl;
	ArbolTest::creacionTipoUsuarioDNI(arbol);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();
	//cout << "--------------------------------------------------------------" << endl;
//	ArbolTest::buscarCategoriaServicio(arbol);
//	ArbolTest::buscar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	ArbolTest::listar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	cout << "Delete Registros ---------------------------------------------" << endl;
//	ArbolTest::deleteRegistrosTest(arbol);

	cout << "Imprimir archivo ---------------------------------------------" << endl;
	arbol->getArchivo()->imprimir();
	cout << "--------------------------------------------------------------" << endl;

	if(arbol != NULL){
		delete(arbol);
	}
	if(arbol != NULL){
		delete(arbol);
	}
//	ArbolTest::buscar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	ArbolTest::listar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	cout << "Delete Registros ---------------------------------------------" << endl;
//	ArbolTest::deleteRegistrosTest(arbol);

	cout << "Imprimir archivo ---------------------------------------------" << endl;
	arbol->getArchivo()->imprimir();
	cout << "--------------------------------------------------------------" << endl;

	if(arbol != NULL){	//---------------------------------------------
	cout << "--------------------------------------------------------------" << endl;
	cout << "Test Arbol" << endl;
	cout << "--------------------------------------------------------------" << endl;

	cout << "Crear Arbol --------------------------------------------------" << endl;
	Arbol *arbol = ArbolTest::creacion(TipoUsuarioDNI);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();

	//cout << "Crear Arbol --------------------------------------------------" << endl;
	//Arbol *arbol = ArbolTest::creacion(NombreCategoriaIDServicio);
	//cout << "Imprimir archivo ---------------------------------------------" << endl;

	cout << "Carga inicial ------------------------------------------------" << endl;
	ArbolTest::creacionTipoUsuarioDNI(arbol);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();
	//cout << "--------------------------------------------------------------" << endl;
//	ArbolTest::buscarCategoriaServicio(arbol);
//	ArbolTest::buscar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	ArbolTest::listar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	cout << "Delete Registros ---------------------------------------------" << endl;
//	ArbolTest::deleteRegistrosTest(arbol);

	cout << "Imprimir archivo ---------------------------------------------" << endl;
	arbol->getArchivo()->imprimir();
	cout << "--------------------------------------------------------------" << endl;

	if(arbol != NULL){
		delete(arbol);
	}
		delete(arbol);
	}
     mymap.erase (mymap.find('d'));

     it=mymap.find('b');
     cout << "objeto borrado: " << endl;
     if(it==mymap.end()){
    	 cout << "el objeto borrado b no se encuentra en el mapa: " << endl;
     }else{	//---------------------------------------------
	cout << "--------------------------------------------------------------" << endl;
	cout << "Test Arbol" << endl;
	cout << "--------------------------------------------------------------" << endl;

	cout << "Crear Arbol --------------------------------------------------" << endl;
	Arbol *arbol = ArbolTest::creacion(TipoUsuarioDNI);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();

	//cout << "Crear Arbol --------------------------------------------------" << endl;
	//Arbol *arbol = ArbolTest::creacion(NombreCategoriaIDServicio);
	//cout << "Imprimir archivo ---------------------------------------------" << endl;

	cout << "Carga inicial ------------------------------------------------" << endl;
	ArbolTest::creacionTipoUsuarioDNI(arbol);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();
	//cout << "--------------------------------------------------------------" << endl;
//	ArbolTest::buscarCategoriaServicio(arbol);
//	ArbolTest::buscar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	ArbolTest::listar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	cout << "Delete Registros ---------------------------------------------" << endl;
//	ArbolTest::deleteRegistrosTest(arbol);

	cout << "Imprimir archivo ---------------------------------------------" << endl;
	arbol->getArchivo()->imprimir();
	cout << "--------------------------------------------------------------" << endl;

	if(arbol != NULL){
		delete(arbol);
	}
    	 cout << "error, se encontro objeto borrado: " << endl;
     }

     // print content:
     std::cout << "elements in mymap 2:" << '\n';
	 for( map<char, int>::iterator it=mymap.begin(); it!=mymap.end(); ++it){
		 cout << (*it).first << ": " << (*it).second << endl;
	 }
*/


	/*
	//---------------------------------------------
	cout << "--------------------------------------------------------------" << endl;
	cout << "Test Arbol" << endl;
	cout << "--------------------------------------------------------------" << endl;

	cout << "Crear Arbol --------------------------------------------------" << endl;
	Arbol *arbol = ArbolTest::creacion(TipoUsuarioDNI);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();

	//cout << "Crear Arbol --------------------------------------------------" << endl;
	//Arbol *arbol = Arbol	//---------------------------------------------
	cout << "--------------------------------------------------------------" << endl;
	cout << "Test Arbol" << endl;
	cout << "--------------------------------------------------------------" << endl;

	cout << "Crear Arbol --------------------------------------------------" << endl;
	Arbol *arbol = ArbolTest::creacion(TipoUsuarioDNI);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();

	//cout << "Crear Arbol --------------------------------------------------" << endl;
	//Arbol *arbol = ArbolTest::creacion(NombreCategoriaIDServicio);
	//cout << "Imprimir archivo ---------------------------------------------" << endl;

	cout << "Carga inicial ------------------------------------------------" << endl;
	ArbolTest::creacionTipoUsuarioDNI(arbol);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();
	//cout << "--------------------------------------------------------------" << endl;
//	ArbolTest::buscarCategoriaServicio(arbol);
//	ArbolTest::buscar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	ArbolTest::listar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	cout << "Delete Registros ---------------------------------------------" << endl;
//	ArbolTest::deleteRegistrosTest(arbol);

	cout << "Imprimir archivo ---------------------------------------------" << endl;
	arbol->getArchivo()->imprimir();
	cout << "--------------------------------------------------------------" << endl;

	if(arbol != NULL){
		delete(arbol);
	}Test::creacion(NombreCategoriaIDServicio);
	//cout << "Imprimir archivo ---------------------------------------------" << endl;

	cout << "Carga inicial ------------------------------------------------" << endl;
	ArbolTest::creacionTipoUsuarioDNI(arbol);

	//cout << "Imprimir archivo ---------------------------------------------" << endl;
	//arbol->getArchivo()->imprimir();
	//cout << "--------------------------------------------------------------" << endl;
//	ArbolTest::buscarCategoriaServicio(arbol);
//	ArbolTest::buscar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	ArbolTest::listar(arbol);
//	cout << "--------------------------------------------------------------" << endl;

//	cout << "Delete Registros ---------------------------------------------" << endl;
//	ArbolTest::deleteRegistrosTest(arbol);

	cout << "Imprimir archivo ---------------------------------------------" << endl;
	arbol->getArchivo()->imprimir();
	cout << "--------------------------------------------------------------" << endl;

	if(arbol != NULL){
		delete(arbol);
	}

*/


    /*
		cout << "--------------------------------------------------------------" << endl;
		cout << "Test Arbol TipoUsuario DNI" << endl;
		cout << "--------------------------------------------------------------" << endl;

		cout << "Crear Arbol --------------------------------------------------" << endl;
		Arbol *arbol = ArbolTest::creacion(TipoUsuarioDNI);

		cout << "Crear Arbol 2 ------------------------------------------------" << endl;
		ArbolTest::creacionTipoUsuarioDNI(arbol);
		cout << "Imprimir archivo ---------------------------------------------" << endl;
		arbol->getArchivo()->imprimir();

//		cout << "Carga inicial 2 ----------------------------------------------" << endl;
//		ArbolTest::cargaInicialProvinciaDNI(arbol);
//		cout << "Imprimir archivo ---------------------------------------------" << endl;
		//arbol->getArchivo()->imprimir();
		cout << "--------------------------------------------------------------" << endl;

		if(arbol != NULL){
			delete(arbol);
		}
		cout << "--------------------------------------------------------------" << endl;
*/

	/*
	cout << "--------------------------------------------------------------" << endl;
	cout << "Test Arbol Provincia DNI" << endl;
	cout << "--------------------------------------------------------------" << endl;

	cout << "Crear Arbol 2 ------------------------------------------------" << endl;
	Arbol *arbol2 = ArbolTest::creacion(ProvinciaDNI);
	cout << "Imprimir archivo ---------------------------------------------" << endl;
	arbol2->getArchivo()->imprimir();

	cout << "Carga inicial 2 ----------------------------------------------" << endl;
	ArbolTest::cargaInicialProvinciaDNI(arbol2);
	cout << "Imprimir archivo ---------------------------------------------" << endl;
	arbol2->getArchivo()->imprimir();
	cout << "--------------------------------------------------------------" << endl;


	if(arbol2 != NULL){
		delete(arbol2);
	}
	cout << "--------------------------------------------------------------" << endl;
*/

/*
    ClaveRegIDServFCHCIDCons *clave = new ClaveRegIDServFCHCIDCons(1,"20130101","1415", 11);

    // Mayores
    ClaveRegIDServFCHCIDCons *clave2 = new ClaveRegIDServFCHCIDCons(1,"20130101","1415", 22);
    ClaveRegIDServFCHCIDCons *clave3 = new ClaveRegIDServFCHCIDCons(1,"20130101","2215", 11);
    ClaveRegIDServFCHCIDCons *clave4 = new ClaveRegIDServFCHCIDCons(1,"20131201","1415", 11);

    cout << "clave->comparar(clave2) = " << clave->comparar(clave2) << endl;
    cout << "clave->comparar(clave3) = " << clave->comparar(clave3) << endl;
    cout << "clave->comparar(clave4) = " << clave->comparar(clave4) << endl;

    //Igual
    ClaveRegIDServFCHCIDCons *clave5 = new ClaveRegIDServFCHCIDCons(1,"20130101","1415", 11);

    cout << "clave->comparar(clave5) = " << clave->comparar(clave5) << endl;

    // Menores
    ClaveRegIDServFCHCIDCons *clave6 = new ClaveRegIDServFCHCIDCons(1,"20130101","1415", 1);
    ClaveRegIDServFCHCIDCons *clave7 = new ClaveRegIDServFCHCIDCons(1,"20130101","1200", 11);
    ClaveRegIDServFCHCIDCons *clave8 = new ClaveRegIDServFCHCIDCons(1,"20120101","1415", 11);

    cout << "clave->comparar(clave6) = " << clave->comparar(clave6) << endl;
    cout << "clave->comparar(clave7) = " << clave->comparar(clave7) << endl;
    cout << "clave->comparar(clave8) = " << clave->comparar(clave8) << endl;
*/

/*
	cout<<"sizeof(unsigned long)"<<sizeof(unsigned long)<<endl;


	ClaveRegTipoUsuarioDNI* c = new ClaveRegTipoUsuarioDNI(23455678,ADMINISTRADOR);

	char* cadena=c->serializar();

	cout<<"cadena"<<cadena<<endl;
	c->deserializar(cadena);

    cout<<sizeof(TipoUsuario)<<"tipoUsuario"<<endl;

    ClaveRegCatIDServ* c = new ClaveRegCatIDServ(2689,"Servicios");
   	cout<<"datos importantes-------"<<(*c)<<endl;
   	ClaveRegCatIDServ* c1 = new ClaveRegCatIDServ(2689,"Servicios");
   	cout<<"serializar--------------"<<endl;
   	char* cadena=c->serializar();

   	cout<<"cadena------------------"<<endl;
    cout<<"cadena"<<cadena<<endl;

    ClaveRegistroArbol *clave04 = FabricaClaveRegistro::crearClaveRegistro(NombreCategoriaIDServicio, cadena);
   	cout << "clave04: " << *clave04 << endl;
   	cout << "comparar" << endl;
   	cout<<c->comparar(c1)<<endl;

    // cout<<sizeof(TipoUsuario)<<"tipoUsuario"<<endl;
*/

    cout << "!!!Fin Programa!!!" << endl;
    // terminate the program:
    return 0;
}
