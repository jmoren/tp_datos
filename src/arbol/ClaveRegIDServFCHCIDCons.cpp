/*
 * ClaveRegIDServFCHCIDCons.cpp
 *
 *  Created on: 08/10/2013
 *      Author: natalia
 */

#include "ClaveRegIDServFCHCIDCons.h"

ClaveRegIDServFCHCIDCons::ClaveRegIDServFCHCIDCons() {
	this->idServicio=0;
	this->fechaConsulta="20130101";
	this->horaConsulta="0000";
    this->idConsulta=0;
}

ClaveRegIDServFCHCIDCons::ClaveRegIDServFCHCIDCons(unsigned long idServicio,string fechaConsulta, string horaConsulta, unsigned long idConsulta) {
	this->idServicio=idServicio;
	this->fechaConsulta=fechaConsulta;
	this->horaConsulta=horaConsulta;
    this->idConsulta=idConsulta;
}

ClaveRegIDServFCHCIDCons::~ClaveRegIDServFCHCIDCons() {
	// TODO Auto-generated destructor stub
}


string ClaveRegIDServFCHCIDCons::getFechaConsulta() {
	return fechaConsulta;
}

void ClaveRegIDServFCHCIDCons::setFechaConsulta(string fechaConsulta) {
	this->fechaConsulta = fechaConsulta;
}

string ClaveRegIDServFCHCIDCons::getHoraConsulta() {
	return horaConsulta;
}

void ClaveRegIDServFCHCIDCons::setHoraConsulta(string horaConsulta) {
	this->horaConsulta = horaConsulta;
}

unsigned long ClaveRegIDServFCHCIDCons::getIdConsulta() {
	return idConsulta;
}

void ClaveRegIDServFCHCIDCons::setIdConsulta(unsigned long idConsulta) {
	this->idConsulta = idConsulta;
}

unsigned long ClaveRegIDServFCHCIDCons::getIdServicio() {
	return idServicio;
}

void ClaveRegIDServFCHCIDCons::setIdServicio(unsigned long idServicio) {
	this->idServicio = idServicio;
}

//devuelve 0 si son iguales, 1 si es mayor y -1 si es menor
short int ClaveRegIDServFCHCIDCons::comparar(ClaveRegistroArbol* claveParaComparar){
	if((claveParaComparar != NULL) ){
		if (this->idServicio == ((ClaveRegIDServFCHCIDCons*)claveParaComparar)->idServicio){
			if (this->fechaConsulta == ((ClaveRegIDServFCHCIDCons*)claveParaComparar)->fechaConsulta) {
				if (this->horaConsulta == ((ClaveRegIDServFCHCIDCons*)claveParaComparar)->horaConsulta) {
					if (this->idConsulta == ((ClaveRegIDServFCHCIDCons*)claveParaComparar)->idConsulta){
						return 0;
					} else if (this->idConsulta > ((ClaveRegIDServFCHCIDCons*)claveParaComparar)->idConsulta) {
						return 1;
					} else {
						return (-1);
					}
				} else if (this->horaConsulta > ((ClaveRegIDServFCHCIDCons*)claveParaComparar)->horaConsulta) {
					return 1;
				} else {
					return -1;
				}
			} else if (this->fechaConsulta > ((ClaveRegIDServFCHCIDCons*)claveParaComparar)->fechaConsulta) {
				return 1;
			} else {
				return -1;
			}
		} else if (this->idServicio > ((ClaveRegIDServFCHCIDCons*)claveParaComparar)->idServicio) {
			return 1;
		} else {
			return -1;
		}
	}else{
		return 1; // Si la clave con la que comparo es NULL entonces siempre esta clave sera mayor a NULL
	}
}


unsigned int ClaveRegIDServFCHCIDCons::getTamanio(){

	return (unsigned int)(sizeof(unsigned long)*2 + this->fechaConsulta.length() + this->horaConsulta.length());

}

char* ClaveRegIDServFCHCIDCons::serializar(){

	char* cadenaEscritura = new char[getTamanio()];
	unsigned int pos = 0;
	this->almacenarEnteroLargo(cadenaEscritura, pos, getIdServicio());
	this->almacenarString(cadenaEscritura, pos, getFechaConsulta());
	this->almacenarString(cadenaEscritura, pos, getHoraConsulta());
	this->almacenarEnteroLargo(cadenaEscritura, pos, getIdConsulta());
	return cadenaEscritura;
}

void ClaveRegIDServFCHCIDCons::deserializar(char* cadena){

	unsigned int pos=0;
	this->idServicio=recuperarEnteroLargo(cadena,pos);
	this->fechaConsulta=recuperarString(cadena,pos,8);
	this->horaConsulta=recuperarString(cadena,pos,4);
	this->idConsulta=recuperarEnteroLargo(cadena,pos);

}

string ClaveRegIDServFCHCIDCons::getDatosImportantes(){
	std::stringstream datosImportantes;
	datosImportantes << "ClaveRegIDServFCHCIDCons: "<< "Tamanio: " << getTamanio() << " (" << getIdServicio() << ", " << getFechaConsulta() << ", " << getHoraConsulta() << ", " << getIdConsulta()<< ")";

    return  datosImportantes.str();
}
