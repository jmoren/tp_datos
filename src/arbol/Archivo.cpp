/*
 * Archivo.cpp
 *
 *  Created on: 05/10/2013
 *      Author: natalia
 */

#include "Archivo.h"

// Constructores -------------------------------------------------------------------------------
Archivo::Archivo(){
	this->tamanioBloque = 0;
	this->tamanioDatosDeControlBloque = 0;
	this->tamanioDatosDeControlRegistro = 0;
	this->cantidadTotalBloques = 0;
	this->tipoRegistro = TipoUsuarioDNI; // Asignar default;
}

Archivo::Archivo(TipoRegistro tipoRegistro, unsigned int tamanioBloque){
	this->tamanioBloque = tamanioBloque;
	this->tamanioDatosDeControlBloque = 0;
	this->tamanioDatosDeControlRegistro = 0;
	this->cantidadTotalBloques = 0;
	this->tipoRegistro = tipoRegistro;
}

Archivo::~Archivo(){
	this->tamanioBloque = 0;
	this->tamanioDatosDeControlBloque = 0;
	this->tamanioDatosDeControlRegistro = 0;
	this->cantidadTotalBloques = 0;
	this->tipoRegistro = TipoUsuarioDNI; // Asignar default;
}

// Funciones ----------------------------------------------------------------------------------
bool Archivo::existeArchivoFisico(){
	return false;
}

void Archivo::crearArchivo(){
	cout << "Archivo::crearArchivo()" << endl;
	this->cantidadTotalBloques = 0;
}

unsigned int Archivo::getTamanioBloque(){
	return this->tamanioBloque;
}

unsigned int Archivo::getTamanioDatosDeControlBloque(){
	return this->tamanioDatosDeControlBloque;
}

unsigned int Archivo::getTamanioDatosDeControlRegistro(){
	return this->tamanioDatosDeControlRegistro;
}

unsigned int Archivo::getTamanioTotalDisponibleParaRegistros(){
	return (this->tamanioBloque - this->tamanioDatosDeControlBloque);
}

unsigned int Archivo::getCantidadTotalBloques(){
	return this->cantidadTotalBloques;
}

unsigned int Archivo::getIdBloqueLibre(){
	if(existenBloquesLibres()){
		//TODO
		return (this->cantidadTotalBloques + 1);
	}else{
		//Se devuelve el siguiente al ultimo bloque para agregar al final del archivo
		return (this->cantidadTotalBloques + 1);
	}
}

bool Archivo::existenBloquesLibres(){
	//TODO: Se devuelve false hasta que este implementada la administracion de bloques libres.
	return false;
}

Bloque* Archivo::getBloque(unsigned int nroBloque){
	return NULL;
}
Bloque* Archivo::getRaiz(){
	return getBloque(0);
}

void Archivo::borrarBloque(unsigned int nroBloque){
	//TODO
}
void Archivo::grabarBloque(Bloque* bloque){
	//TODO
}

void Archivo::imprimir(){

}
