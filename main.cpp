#include "src/lib/Constantes.h"
#include "src/utils/cli.h"
#include "src/lib/encryption/cipher.h"

int initialize_user_manager(){
  UserManager *manager;
  User *admin;
  Bucket *bucket;

  manager = new UserManager(TAMANIO_BLOQUE_USER);
  manager->hashing->writeTable();
  delete(manager);

  manager= new UserManager();
  bucket = manager->hashing->getNewBucket();
  bucket->dispersion = 1;
  manager->hashing->table->addRow(bucket->id);
  manager->hashing->saveBucket(bucket);
  manager->hashing->writeTable();

  cout << " ** Registrando cuenta Admin ** " << endl;
  admin = manager->addUser(NO_ROLE);
  if(admin){
    manager->printUser(admin);
    delete(admin);
  }
  delete(manager);

  return 0;
}

int initialize_service_manager(){
  ServiceManager *manager;
  Bucket *bucket;

  manager = new ServiceManager(TAMANIO_BLOQUE_SERVICE);
  manager->hashing->writeTable();
  delete(manager);

  manager= new ServiceManager();
  bucket = manager->hashing->getNewBucket();
  bucket->dispersion = 1;
  manager->hashing->table->addRow(bucket->id);
  manager->hashing->saveBucket(bucket);
  manager->hashing->writeTable();
  delete(manager);

  return 0;
}

int initialize_category_manager(){
  CategoryManager *manager;
  Bucket *bucket;

  manager = new CategoryManager(TAMANIO_BLOQUE_CATEGORY);
  manager->hashing->writeTable();
  delete(manager);

  manager= new CategoryManager();
  bucket = manager->hashing->getNewBucket();
  bucket->dispersion = 1;
  manager->hashing->table->addRow(bucket->id);
  manager->hashing->saveBucket(bucket);
  manager->hashing->writeTable();
  delete(manager);

  return 0;
}

int  initialize_message_manager(){
  MessageManager *manager;
  Bucket *bucket;

  manager = new MessageManager(TAMANIO_BLOQUE_MSG);
  manager->hashing->writeTable();
  delete(manager);

  manager= new MessageManager();
  bucket = manager->hashing->getNewBucket();
  bucket->dispersion = 1;
  manager->hashing->table->addRow(bucket->id);
  manager->hashing->saveBucket(bucket);
  manager->hashing->writeTable();
  delete(manager);

  return 0;
}

int  initialize_quote_manager(){
  QuoteManager *manager;
  Bucket *bucket;

  manager = new QuoteManager(TAMANIO_BLOQUE_QUOTE);
  manager->hashing->writeTable();
  delete(manager);

  manager= new QuoteManager();
  bucket = manager->hashing->getNewBucket();
  bucket->dispersion = 1;
  manager->hashing->table->addRow(bucket->id);
  manager->hashing->saveBucket(bucket);
  manager->hashing->writeTable();
  delete(manager);

  return 0;
}

bool validate_master_key(){
  bool is_valid;
  string master_key;

  if(!Funciones::fileExists("config.txt")){
    is_valid = false;
    cout << "No existe el archivo de configuracion. Tiene que crear el archivo 'config.txt' con una clave de 9 caracteres" << endl;
    cout << "Los caracteres posibles son: _ A B C D E F G H I J K L M N O P Q R S T U V W X  Y Z 0 1 2 3 4 5 6 7 8 9 . , - ? " << endl;
 }else{
    master_key = Funciones::read_master_key();
    Cipher *cipher = new Cipher(master_key);
    is_valid = cipher->validateKey();
    delete cipher;
  }

  return is_valid;
}

bool validateApp(){
  bool answer = true;
  string user     = "user_db.bin";
  string category = "category_db.bin";
  string service  = "service_db.bin";
  string message  = "message_db.bin";
  string quote    = "quote_db.bin";

  if(!Funciones::fileExists(user)){
    answer = false;
  }

  if(!Funciones::fileExists(category)){
    answer = false;
  }

  if(!Funciones::fileExists(service)){
    answer = false;
  }

  if(!Funciones::fileExists(message)){
    answer = false;
  }

  if(!Funciones::fileExists(quote)){
    answer = false;
  }

  if(!answer)
    cout << " Debe instalar la aplicacion antes de comenzar.\n Uso: ./pdoradas -i" << endl;

  return answer;
}

int main(int argc, char **argv){
  int option;
  int result_user = 0, result_service = 0;
  int result_message = 0, result_category = 0;
  int result = 0, result_quote = 0;
  bool is_valid_master_key;
  Cli *cli;

  switch(argc){
    case 2:
      while ((option = getopt (argc, argv, "ic")) != -1){
        switch (option){
          case 'c':
            validate_master_key();
            validateApp();
            return 0;
            break;
          case 'i':
            is_valid_master_key = validate_master_key();
            if (!is_valid_master_key){
              return result = -3;
            }else{
              result_user     = initialize_user_manager();
              result_message  = initialize_message_manager();
              result_service  = initialize_service_manager();
              result_category = initialize_category_manager();
              result_quote    = initialize_quote_manager();

              if((result_user == 0) && (result_service == 0) && (result_category == 0) && (result_message == 0) && (result_quote == 0)){
                cout << " Instalacion completa!. Ahora puede iniciar el programa" << endl;
                cout << " Gracias!!" << endl;
                return result = 0;
              }else{
                cout << " Hubo un error en la instalacion. Consulte a los desarrolladores" << endl;
                return result = -3;
              }
            }
            break;
          case '?':
            cout << " Parametro desconocido" << endl;
            result = -1;
        }
      }
      break;
    case 1:
      if(validate_master_key() && validateApp()){
        cout << "\033[1;33m Bienvenido a Paginas Doradas C++ - 2013\033[0m\n" << endl;
        cli = new Cli();
        result = cli->shell();
        delete cli;
      }
      break;
    default:
      cout << " La cantidad de parametros es erronea.\n";
      result = -2;
      break;
  }

  return result;
}
