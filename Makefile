CC = g++
CFLAGS = -g -O0 -pedantic -Wall

SRC   = src
TEST  = test
LIBS  = lib
MODELS= models
UTILS = utils
OBJS  = objs
DBS   = dbs

pdoradas: cli.o
	@echo " Compilando aplicacion"
	@$(CC) $(CFLAGS) -o pdoradas $(OBJS)/*.o main.cpp

# Test targets
test_encryption: funciones.o cipher.o
	$(CC) $(CFLAGS) -o test_cipher $(OBJS)/*.o $(SRC)/$(TEST)/TestEncryption.cpp

test_block:  file_block.o
	$(CC) $(CFLAGS) -o test_block $(OBJS)/*.o $(SRC)/$(TEST)/TestBlock.cpp

test_hashing: file_block.o hashing.o
	$(CC) $(CFLAGS) -o test_hashing $(OBJS)/*.o $(SRC)/$(TEST)/TestHashing.cpp

test_user:  user_manager.o
	$(CC) $(CFLAGS) -o test_user $(OBJS)/*.o $(SRC)/$(TEST)/TestUserHash.cpp

test_file_record: i_serialize.o free_space_table.o registro_variable.o registro_generico.o block.o block_header.o file_block.o file_records.o
	$(CC) $(CFLAGS) -o test_file_record $(ALL_OBJS) $(SRC)/$(TEST)/TestFileRecord.cpp

test_indice: i_serialize.o free_space_table.o registro_variable.o registro_generico.o block.o block_header.o file_block.o file_records.o termino.o reg_vocabulario.o vocabulario.o stop_words.o terminos_x_orden.o ocurrencias.o lista_invertida.o indice_invertido.o index_manager.o
	$(CC) $(CFLAGS) -o test_indice $(OBJS)/*.o $(SRC)/$(TEST)/TestIndice.cpp

# I/O files
i_serialize.o: set_up
	@$(CC) $(CFLAGS) -c -o $(OBJS)/i_serialize.o $(SRC)/$(LIBS)/ISerialize.cpp

free_space_table.o: set_up
	@$(CC) $(CFLAGS) -c -o $(OBJS)/free_space_table.o $(SRC)/$(LIBS)/FreeSpaceTable.cpp

registro_variable.o: i_serialize.o
	@$(CC) $(CFLAGS) -c -o $(OBJS)/registro_variable.o $(SRC)/$(LIBS)/RegistroVariable.cpp

registro_generico.o: set_up
	@$(CC) $(CFLAGS) -c -o $(OBJS)/registro_generico.o $(SRC)/$(LIBS)/RegistroGenerico.cpp

registro_id.o: set_up
	@$(CC) $(CFLAGS) -c -o $(OBJS)/registro_id.o $(SRC)/$(LIBS)/RegistroId.cpp

block.o: set_up
	@$(CC) $(CFLAGS) -c -o $(OBJS)/block.o $(SRC)/$(LIBS)/Block.cpp

block_header.o: set_up
	@$(CC) $(CFLAGS) -c -o $(OBJS)/block_header.o $(SRC)/$(LIBS)/BlockHeader.cpp

file_block.o: block.o block_header.o registro_generico.o
	@echo " Compilando archivo de bloques"
	@$(CC) $(CFLAGS) -c -o $(OBJS)/file_block.o $(SRC)/$(LIBS)/FileBlock.cpp

file_records.o: free_space_table.o registro_variable.o
	@$(CC) $(CFLAGS) -c -o $(OBJS)/file_records.o $(SRC)/$(LIBS)/FileRecords.cpp

# Cipher
cipher.o: set_up
	@$(CC) $(CFLAGS) -c -o $(OBJS)/cipher.o $(SRC)/$(LIBS)/encryption/cipher.cpp

# Indice
termino.o: registro_variable.o
	@$(CC) $(CFLAGS) -c -o $(OBJS)/termino.o $(SRC)/$(LIBS)/indiceInvertido/Termino.cpp

stop_words.o: set_up
	@$(CC) $(CFLAGS) -c -o $(OBJS)/stop_words.o $(SRC)/$(LIBS)/indiceInvertido/StopWords.cpp

terminos_x_orden.o: termino.o
	@$(CC) $(CFLAGS) -c -o $(OBJS)/terminos_x_orden.o $(SRC)/$(LIBS)/indiceInvertido/TerminosXOrden.cpp

ocurrencias.o: termino.o
	@$(CC) $(CFLAGS) -c -o $(OBJS)/ocurrencias.o $(SRC)/$(LIBS)/indiceInvertido/Ocurrencias.cpp

lista_invertida.o:
	@$(CC) $(CFLAGS) -c -o $(OBJS)/lista_invertida.o $(SRC)/$(LIBS)/indiceInvertido/ListaInvertida.cpp

listas_invertidas.o: lista_invertida.o
	@$(CC) $(CFLAGS) -c -o $(OBJS)/listas_invertidas.o $(SRC)/$(LIBS)/indiceInvertido/ListasInvertidas.cpp

vocabulario.o: file_records.o termino.o
	@$(CC) $(CFLAGS) -c -o $(OBJS)/vocabulario.o $(SRC)/$(LIBS)/indiceInvertido/Vocabulario.cpp

indice_invertido.o: terminos_x_orden.o ocurrencias.o listas_invertidas.o vocabulario.o stop_words.o
	@$(CC) $(CFLAGS) -c -o $(OBJS)/indice_invertido.o $(SRC)/$(LIBS)/indiceInvertido/IndiceInvertido.cpp

index_manager.o: indice_invertido.o
	@echo " Compilando Indice invertido"
	@$(CC) $(CFLAGS) -c -o $(OBJS)/index_manager.o $(SRC)/$(LIBS)/indiceInvertido/IndexManager.cpp

# Hashing
table_record.o: set_up
	@$(CC) $(CFLAGS) -c -o $(OBJS)/table_record.o $(SRC)/$(LIBS)/hashing/TableRecord.cpp

table.o: table_record.o
	@$(CC) $(CFLAGS) -c -o $(OBJS)/table.o $(SRC)/$(LIBS)/hashing/Table.cpp

bucket.o: registro_id.o
	@$(CC) $(CFLAGS) -c -o $(OBJS)/bucket.o $(SRC)/$(LIBS)/hashing/Bucket.cpp

hashing.o: table.o bucket.o file_block.o validations.o
	@echo " Compilando hashing"
	@$(CC) $(CFLAGS) -c -o $(OBJS)/hashing.o $(SRC)/$(LIBS)/hashing/Hashing.cpp

# arbol B+
arbol.o:
	@echo " Compilando Arbol B+"

	@$(CC) $(CFLAGS) -c -o $(OBJS)/arbol.o $(SRC)/arbol/Arbol.cpp

	@$(CC) $(CFLAGS) -c -o $(OBJS)/archivo.o $(SRC)/arbol/Archivo.cpp
	@$(CC) $(CFLAGS) -c -o $(OBJS)/archivo_fisico.o $(SRC)/arbol/ArchivoFisico.cpp
	@$(CC) $(CFLAGS) -c -o $(OBJS)/archivo_mock.o $(SRC)/arbol/ArchivoMock.cpp

	@$(CC) $(CFLAGS) -c -o $(OBJS)/bloque_arbol.o $(SRC)/arbol/Bloque.cpp

	@$(CC) $(CFLAGS) -c -o $(OBJS)/registro_generico_arbol.o $(SRC)/arbol/RegistroGenericoArbol.cpp
	@$(CC) $(CFLAGS) -c -o $(OBJS)/registro_nodo.o $(SRC)/arbol/RegistroNodo.cpp
	@$(CC) $(CFLAGS) -c -o $(OBJS)/registro_nodo_interno.o $(SRC)/arbol/RegistroNodoInterno.cpp
	@$(CC) $(CFLAGS) -c -o $(OBJS)/arbol.o $(SRC)/arbol/Arbol.cpp

	@$(CC) $(CFLAGS) -c -o $(OBJS)/nodo_interno.o $(SRC)/arbol/NodoInterno.cpp
	@$(CC) $(CFLAGS) -c -o $(OBJS)/nodo_hoja.o $(SRC)/arbol/NodoHoja.cpp

	@$(CC) $(CFLAGS) -c -o $(OBJS)/clave_registro_arbol.o $(SRC)/arbol/ClaveRegistroArbol.cpp
	@$(CC) $(CFLAGS) -c -o $(OBJS)/clave_tipo_dni.o $(SRC)/arbol/ClaveRegTipoUsuarioDNI.cpp
	@$(CC) $(CFLAGS) -c -o $(OBJS)/clave_provincia_dni.o $(SRC)/arbol/ClaveRegProvinciaDNI.cpp
	@$(CC) $(CFLAGS) -c -o $(OBJS)/clave_servicio_usuario_cotizacion.o $(SRC)/arbol/ClaveRegIDServIDUsPC.cpp
	@$(CC) $(CFLAGS) -c -o $(OBJS)/clave_servicio_usuario_cosulta.o $(SRC)/arbol/ClaveRegIDServIDUsIDCons.cpp
	@$(CC) $(CFLAGS) -c -o $(OBJS)/clave_servicio_fecha_hora_cotizacion.o $(SRC)/arbol/ClaveRegIDServFHPCotizacion.cpp
	@$(CC) $(CFLAGS) -c -o $(OBJS)/clave_servicio_fecha_hora_consulta.o $(SRC)/arbol/ClaveRegIDServFCHCIDCons.cpp
	@$(CC) $(CFLAGS) -c -o $(OBJS)/clave_servicio_categoria.o $(SRC)/arbol/ClaveRegCatIDServ.cpp
	@$(CC) $(CFLAGS) -c -o $(OBJS)/clave_servicio_usuario.o $(SRC)/arbol/ClaveRegIDUsIDServ.cpp
	@$(CC) $(CFLAGS) -c -o $(OBJS)/fabrica_claves.o $(SRC)/arbol/FabricaClaveRegistro.cpp

	@$(CC) $(CFLAGS) -c -o $(OBJS)/persistente.o $(SRC)/arbol/Persistente.cpp

# user & user_manager
user_manager.o: hashing.o arbol.o user.o
	@echo " Compilando usuarios"
	@$(CC) $(CFLAGS) -c -o $(OBJS)/user_manager.o $(SRC)/$(MODELS)/users/user_manager.cpp

user.o: set_up registro_generico.o cipher.o
	@$(CC) $(CFLAGS) -c -o $(OBJS)/user.o $(SRC)/$(MODELS)/users/user.cpp

# quote & quote_manager
quote_manager.o: hashing.o arbol.o quote.o
	@echo " Compilando cotizaciones"
	@$(CC) $(CFLAGS) -c -o $(OBJS)/quote_manager.o $(SRC)/$(MODELS)/quotes/quote_manager.cpp

quote.o: set_up registro_generico.o
	@$(CC) $(CFLAGS) -c -o $(OBJS)/quote.o $(SRC)/$(MODELS)/quotes/quote.cpp

# service & service_manager
service_manager.o: hashing.o service.o message_manager.o quote_manager.o
	@echo " Compilando servicios"
	@$(CC) $(CFLAGS) -c -o $(OBJS)/service_manager.o $(SRC)/$(MODELS)/service/service_manager.cpp

service.o: set_up registro_generico.o
	@$(CC) $(CFLAGS) -c -o $(OBJS)/service.o $(SRC)/$(MODELS)/service/service.cpp

# service & service_manager
category_manager.o: indice_invertido.o index_manager.o hashing.o category.o
	@echo " Compilando categorias"
	@$(CC) $(CFLAGS) -c -o $(OBJS)/category_manager.o $(SRC)/$(MODELS)/category/category_manager.cpp

category.o: set_up registro_generico.o
	@$(CC) $(CFLAGS) -c -o $(OBJS)/category.o $(SRC)/$(MODELS)/category/category.cpp

# message & message_manager
message_manager.o: hashing.o message.o
	@echo " Compilando consultas"
	@$(CC) $(CFLAGS) -c -o $(OBJS)/message_manager.o $(SRC)/$(MODELS)/message/message_manager.cpp

message.o: set_up registro_generico.o
	@$(CC) $(CFLAGS) -c -o $(OBJS)/message.o $(SRC)/$(MODELS)/message/message.cpp

# Helpers target
cli.o: set_up user_manager.o service_manager.o category_manager.o message_manager.o funciones.o
	@echo " Compilando cli"
	@$(CC) $(CFLAGS) -c -o $(OBJS)/cli.o $(SRC)/$(UTILS)/cli.cpp

validations.o: set_up
	@$(CC) $(CFLAGS) -c -o $(OBJS)/validations.o $(SRC)/$(UTILS)/validations.cpp

funciones.o: set_up
	@$(CC) $(CFLAGS) -c -o $(OBJS)/funciones.o $(SRC)/$(UTILS)/Funciones.cpp

set_up:
	@echo " Creando directorios"
	@mkdir -p $(OBJS)
	@mkdir -p $(DBS)

clean:
	@echo " * Eliminamos binarios "
	@rm -rf $(OBJS)
	@rm -rf $(DBS)
	@if [ -f pdoradas ]; then rm pdoradas; fi
	@if [ -f test_block ]; then rm test_block; fi
	@if [ -f test_cipher ]; then rm test_cipher; fi
	@if [ -f test_hashing ]; then rm test_hashing; fi
	@if [ -f test_user ]; then rm test_user; fi
	@if [ -f test_file.bin ]; then rm test_file.bin; fi
	@if [ -f user_db.bin ]; then rm user_db.bin; fi
	@if [ -f service_db.bin ]; then rm service_db.bin; fi
	@if [ -f category_db.bin ]; then rm category_db.bin; fi
	@if [ -f message_db.bin ]; then rm message_db.bin; fi
	@if [ -f quote_db.bin ]; then rm quote_db.bin; fi
